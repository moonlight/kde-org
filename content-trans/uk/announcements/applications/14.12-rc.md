---
aliases:
- ../announce-applications-14.12-rc
custom_spread_install: true
date: '2014-11-27'
description: KDE випущено кандидат у випуски Програм 14.12.
layout: application
title: KDE випущено близьку до готовності версію програм KDE 14.12
---
27 листопада 2014 року. Сьогодні командою KDE випущено близький до готовності варіант нових версій програм. Від цього моменту заморожено залежності та список можливостей, — команда KDE зосереджує зусилля на виправлені вад та удосконаленні нової версії.

Через те, що частину програм побудовано на основі KDE Frameworks 5, випуск 14.12 потребує ретельного тестування з метою підтримання та поліпшення якості та зручності у користуванні. Користувачі є надзвичайно важливою ланкою у підтриманні високої якості випусків KDE, оскільки розробникам просто не вистачить часу перевірити всі можливі комбінацій обладнання та налаштувань системи. Ми розраховуємо на вашу допомогу у якомога швидшому виявленні вад, щоб уможливити виправлення цих вад до остаточного випуску. Будь ласка, долучіться до команди тестувальників , встановивши нову версію <a href='https://bugs.kde.org/'>і повідомивши про всі виявлені вади</a>.

#### Встановлення бінарних пакунків із програмами KDE 14.12 RC

<em>Пакунки</em>. Деякі з виробників дистрибутивів операційних систем Linux/UNIX люб'язно надали можливість користувачам своїх дистрибутивів отримати бінарні пакунки програм KDE 14.12 RC (внутрішнє позначення — 14.11.97) для декількох версій дистрибутивів. У інших випадках такі бінарні версії було створено силами ентузіастів зі спільноти дистрибутива. Доступ до додаткових бінарних пакунків, а також оновлень поточних пакунків можна буде отримати протягом найближчих тижнів.

<em>Розташування пакунків</em>. Поточний список наявних бінарних пакунків, про які відомо проєктові KDE, можна знайти у <a href='http://community.kde.org/KDE_SC/Binary_Packages#KDE_SC_4.4_Beta_1_'>вікі спільноти</a>.

#### Збирання програм KDE 14.12 RC

Повні початкові коди програм KDE 14.12 RC можна <a href='http://download.kde.org/unstable/applications/14.11.97/src/'>отримати безкоштовно</a>. Настанови щодо збирання і встановлення програмного забезпечення KDE можна знайти на <a href='/info/applications/applications-14.11.97'>інформаційній сторінці</a>.
