---
aliases:
- ../announce-4.11.5
date: 2014-01-07
description: KDE випущено робочі простори Плазми, програм і платформи 4.11.5.
title: KDE оголошує про випуск 4.11.5
---
7 січня 2014 року. Сьогодні KDE випущено оновлення робочих просторів, програм та платформи для розробки. Це п'яте оновлення з послідовності щомісячних оновлень випуску 4.11. Як було оголошено під час випуску, підтримка цієї версії оновленням буде здійснюватися до серпня 2015 року. У 4.11.1 ви зможете скористатися багатьма виправленнями вад та оновленими перекладами щодо випуску 4.11. Оновлення є безпечним і корисним для усіх користувачів.

Серед принаймні зареєстрованих виправлень вад поліпшення у роботі комплексу програм для керування особистими даними Kontact, засобу роботи з кодом UML Umbrello, програми для перегляду сторінок інтернету Konqueror, програми для керування файлами Dolphin та інших програм. Нова версія калькулятора Плазми може працювати з грецькими літерами, а Okular може друкувати сторінки з довгими заголовками. Також виправлено ваду, через яку у Konqueror були проблеми з показом вебшрифтів.

З повнішим <a href='https://bugs.kde.org/buglist.cgi?query_format=advanced&amp;short_desc_type=allwordssubstr&amp;short_desc=&amp;long_desc_type=substring&amp;long_desc=&amp;bug_file_loc_type=allwordssubstr&amp;bug_file_loc=&amp;keywords_type=allwords&amp;keywords=&amp;bug_status=RESOLVED&amp;bug_status=VERIFIED&amp;bug_status=CLOSED&amp;emailtype1=substring&amp;email1=&amp;emailassigned_to2=1&amp;emailreporter2=1&amp;emailcc2=1&amp;emailtype2=substring&amp;email2=&amp;bugidtype=include&amp;bug_id=&amp;votes=&amp;chfieldfrom=2013-06-01&amp;chfieldto=Now&amp;chfield=cf_versionfixedin&amp;chfieldvalue=4.11.5&amp;cmdtype=doit&amp;order=Bug+Number&amp;field0-0-0=noop&amp;type0-0-0=noop&amp;value0-0-0='>списком</a> змін можна ознайомитися за допомогою системи стеження за вадами KDE. Крім того, докладний список змін, які було внесено до 4.11.5 можна отримати з журналів відповідної гілки сховища Git.

Посилання на пакунки з початковими кодами та зібраними програмами для встановлення можна знайти на <a href='/info/4/4.11.5'>інформаційній сторінці 4.11.5</a>. Якщо вам хочеться дізнатися більше про версію 4.11 робочих просторів KDE, програм та платформи для розробки, будь ласка, зверніться до <a href='/announcements/4.11/'>нотаток щодо випуску 4.11</a>.

{{< figure class="text-center img-size-medium" src="/announcements/4/4.11.0/screenshots/send-later.png" caption=`Нові можливості з відкладеного надсилання повідомлень у Kontact` width="600px">}}

Програмне забезпечення KDE, зокрема бібліотеки та всі програми, розповсюджується за умов дотримання ліцензійних угод з відкритим доступом до початкового коду. Програмне забезпечення KDE можна отримати у форматі початкових кодів та різноманітних бінарних форматах за посиланнями на сайті <a href='http://download.kde.org/stable/4.11.5/'>download.kde.org</a> або разом з будь-якою з сучасних <a href='/distributions'>поширених систем GNU/Linux та UNIX</a>.
