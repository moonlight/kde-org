---
aliases:
- ../../kde-frameworks-5.28.0
date: 2016-11-15
layout: framework
libCount: 70
src: /announcements/frameworks/5-tp/KDE_QT.jpg
---
### New framework: syntax-highlighting

Syntax highlighting engine for Kate syntax definitions

This is a stand-alone implementation of the Kate syntax highlighting engine. It's meant as a building block for text editors as well as for simple highlighted text rendering (e.g. as HTML), supporting both integration with a custom editor as well as a ready-to-use QSyntaxHighlighter sub-class.

### Breeze Icons

- update kstars action icons (bug 364981)
- Breeze Dark is listed as Breeze in System Settings wrong .themes file (bug 370213)

### Extra CMake-Module

- Make KDECMakeSettings work with KDE_INSTALL_DIRS_NO_DEPRECATED
- Don't require the python bindings dependencies for ECM
- Add the PythonModuleGeneration module

### KActivitiesStats

- Ignoring link status when sorting UsedResources and LinkedResources model

### KDE Doxygen Tools

- [CSS] reverse changes done by doxygen 1.8.12
- Add doxygenlayout file
- Update way of defining group names

### KAuth

- Make sure we can do more than one request
- Make sure we get to know about progress by reading the program output

### KConfig

- Make sure we don't break compilation with past broken units
- Don't be fatal on File field not being properly parsed

### KCoreAddons

- Display bad url
- Load user avatars from AccountsServicePath if it exists (bug 370362)

### KDeclarative

- [QtQuickRendererSettings] Fix default to be empty instead of "false"

### KDELibs 4 Support

- Make the France flag actually use all the pixmap

### KDocTools

- Fix 'checkXML5 generates html files in workdir for valid docbooks' (bug 371987)

### KIconThemes

- Support non integer scale factors in kiconengine (bug 366451)

### KIdleTime

- Disabled spamming the console output with 'waiting for' messages

### KImageFormats

- imageformats/kra.h - overrides for KraPlugin capabilities() and create()

### KIO

- Fix HTTP date format sent by kio_http to always use the C locale (bug 372005)
- KACL: fix memory leaks detected by ASAN
- Fix memory leaks in KIO::Scheduler, detected by ASAN
- Removed duplicate clear button (bug 369377)
- Fix editing autostart entries when /usr/local/share/applications doesn't exist (bug 371194)
- [KOpenWithDialog] Hide TreeView header
- Sanitize the symlink name buffer size (bug 369275)
- Properly finish DropJobs when triggered is not emitted (bug 363936)
- ClipboardUpdater: fix another crash on Wayland (bug 359883)
- ClipboardUpdater: fix crash on Wayland (bug 370520)
- Support non integer scale factors in KFileDelegate (bug 366451)
- kntlm: Distinguish between NULL and empty domain
- Don't show overwrite dialog if file name is empty
- kioexec: use friendly filenames
- Fix focus ownership if url is changed before showing the widget
- Major performance improvement when turning previews off in the file dialog (bug 346403)

### KItemModels

- Add python bindings

### KJS

- Export FunctionObjectImp, used by khtml's debugger

### KNewStuff

- Separate sort roles and filters
- Make it possible to query installed entries

### KNotification

- Don't deref an object we haven't referenced when notification has no action
- KNotification will no longer crash when using it in a QGuiApplication and no notification service is running (bug 370667)
- Fix crashes in NotifyByAudio

### KPackage-Framework

- Make sure we're looking both for json and desktop metadata
- Guard against Q_GLOBAL_STATIC being destroyed at app shutdown
- Fix dangling pointer in KPackageJob (bug 369935)
- Remove discovery associated to a key when removing a definition
- Generate the icon into the appstream file

### KPty

- Use ulog-helper on FreeBSD instead of utempter
- search harder for utempter using basic cmake prefix as well
- workaround find_program ( utempter ...) failure(s)
- use ECM path to find utempter binary, more reliable than simple cmake prefix

### KRunner

- i18n: handle strings in kdevtemplate files

### KTextEditor

- Breeze Dark: Darken current-line background color for better readability (bug 371042)
- Sorted Dockerfile instructions
- Breeze (Dark): Make comments a bit lighter for better readability (bug 371042)
- Fix CStyle and C++/boost indenters when automatic brackets enabled (bug 370715)
- Add modeline 'auto-brackets'
- Fix inserting text after end of file (rare case)
- Fix invalid xml highlighting files
- Maxima: Remove hard-coded colors, fix itemData Label
- Add OBJ, PLY and STL syntax definitions
- Add syntax highlighting support for Praat

### KUnitConversion

- New Thermal and Electrical Units and Unit Convenience Function

### KWallet Framework

- If Gpgmepp is not found, try to use KF5Gpgmepp
- Use Gpgmepp from GpgME-1.7.0

### KWayland

- Improved relocatability of CMake export
- [tools] Fix generation of wayland_pointer_p.h
- [tools] Generate eventQueue methods only for global classes
- [server] Fix crash on updating focused keyboard surface
- [server] Fix possible crash on creation of DataDevice
- [server] Ensure we have a DataSource on the DataDevice in setSelection
- [tools/generator] Improve resource destruction on server side
- Add request to have focus in a PlasmaShellSurface of Role Panel
- Add auto-hiding panel support to PlasmaShellSurface interface
- Support passing generic QIcon through PlasmaWindow interface
- [server] Implement the generic window property in QtSurfaceExtension
- [client] Add methods to get ShellSurface from a QWindow
- [server] Send pointer events to all wl_pointer resources of a client
- [server] Don't call wl_data_source_send_send if DataSource is unbound
- [server] Use deleteLater when a ClientConnection gets destroyed (bug 370232)
- Implement support for the relative pointer protocol
- [server] Cancel previous selection from SeatInterface::setSelection
- [server] Send key events to all wl_keyboard resources of a client

### KWidgetsAddons

- move kcharselect-generate-datafile.py to src subdir
- Import kcharselect-generate-datafile.py script with history
- Remove outdated section
- Add Unicode copyright and permission notice
- Fix warning: Missing override
- Add symbol SMP blocks
- Fix "See also" references
- Add missing Unicode blocks; improve ordering (bug 298010)
- add character categories to the data file
- update the Unicode categories in the data file generation script
- adjust the data file generation file to be able to parse the unicode 5.2.0 data files
- forward port fix for generating translations
- let the script to generate the data file for kcharselect also write a translation dummy
- Add the script to generate the data file for KCharSelect
- new KCharSelect application (using kcharselect widget from kdelibs now)

### KWindowSystem

- Improved relocatability of CMake export
- Add support for desktopFileName to NETWinInfo

### KXMLGUI

- Allow using new style connect in KActionCollection::add<a href="">Action</a>

### ModemManagerQt

- Fix include dir in pri file

### NetworkManagerQt

- Fix include dir in pri file
- Fix moc error due to Q_ENUMS being used in a namespace, with Qt branch 5.8

### Plasma Framework

- make sure OSD doesn't have Dialog flag (bug 370433)
- set context properties before reloading the qml (bug 371763)
- Don't reparse the metadata file if it's already loaded
- Fix crash in qmlplugindump when no QApplication is available
- Don't show "Alternatives" menu by default
- New bool to use activated signal as toggle of expanded (bug 367685)
- Fixes for building plasma-framework with Qt 5.5
- [PluginLoader] Use operator&lt;&lt; for finalArgs instead of initializer list
- use kwayland for shadows and dialog positioning
- Remaining missing icons and network improvements
- Move availableScreenRect/Region up to AppletInterface
- Don't load containment actions for embedded containments (system trays)
- Update applet alternatives menu entry visibility on demand

### Solid

- Fix unstable ordering of query results yet again
- Add a CMake option to switch between HAL and UDisks managers on FreeBSD
- Make UDisks2 backend compile on FreeBSD (and, possibly, other UNIXes)
- Windows: Don't display error dialogs (bug 371012)

### Sicherheitsinformation

Der veröffentlichte Quelltext wurde mit GPG mit folgendem Schlüssel signiert: pub rsa2048/58D0EE648A48B3BB 2016-09-05 David Faure &lt;faure@kde.org&gt; Primärer Fingerprint des Schlüssels: 53E6 B47B 45CE A3E0 D5B7 4577 58D0 EE64 8A48 B3BB

Sie können im Kommentarabschnitt des <a href='https://dot.kde.org/2014/07/07/kde-frameworks-5-makes-kde-software-more-accessible-all-qt-developers'>Dot-Artikels</a> über diese Veröffentlichung diskutieren und Ideen einbringen.
