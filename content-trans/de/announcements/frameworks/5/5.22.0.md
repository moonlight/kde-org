---
aliases:
- ../../kde-frameworks-5.22.0
date: 2016-05-15
layout: framework
libCount: 70
src: /announcements/frameworks/5-tp/KDE_QT.jpg
---
###  Attica

- Properly check if a URL is a local file

### Baloo

- Compilation fixes for Windows

### Breeze Icons

- Many new action and application icons.
- Specify offered extensions as per change in kiconthemes

### Extra CMake-Module

- Android deployment: support projects without things in share or lib/qml (bug 362578)
- Enables KDE_INSTALL_USE_QT_SYS_PATHS if CMAKE_INSTALL_PREFIX Qt5 prefix
- ecm_qt_declare_logging_category: improve error message when using without including

### Framework-Integration

- Remove platformtheme plugin as it's in plasma-integration

### KCoreAddons

- Provide a way to disable inotify use in KDirWatch
- Fix KAboutData::applicationData() to init from current Q*Application metadata
- Make clear that KRandom is not recommended for cryptography purposes

### KDBusAddons

- KDBusService: turn '-' into '_' in object paths

### KDeclarative

- Don't crash if we have no openGL context

### KDELibs 4 Support

- Provide a fallback MAXPATHLEN if not defined
- Fix KDateTime::isValid() for ClockTime values (bug 336738)

### KDocTools

- Added entity applications

### KFileMetaData

- Merge branch 'externalextractors'
- Fixed external plugins and tests
- Added support for external writer plugins
- Added writer plugin support
- Add external extractor plugin support

### KHTML

- Implement toString for Uint8ArrayConstructor and friends
- Merge in several Coverity-related fixes
- Correctly use QCache::insert
- Fix some memory leaks
- Sanity check CSS web font parsing, avoid potential mem leak
- dom: Add tag priorities for 'comment' tag

### KI18n

- libgettext: Fix potential use-after-free using non-g++ compilers

### KIconThemes

- Use appropriate container for internal pointer array
- Add opportunity to reduce unneeded disk accesses, introduces KDE-Extensions
- Save some disk accesses

### KIO

- kurlnavigatortoolbutton.cpp - use buttonWidth in paintEvent()
- New file menu: filter out duplicates (e.g. between .qrc and system files) (bug 355390)
- Fix error message on startup of the cookies KCM
- Remove kmailservice5, it can only do harm at this point (bug 354151)
- Fix KFileItem::refresh() for symlinks. The wrong size, filetype and permissions were being set
- Fix regression in KFileItem: refresh() would lose the file type, so a dir became a file (bug 353195)
- Set text on QCheckbox widget rather than using a separate label (bug 245580)
- Don't enable acl permissions widget if we don't own the file (bug 245580)
- Fix double-slash in KUriFilter results when a name filter is set
- KUrlRequester: add signal textEdited (forwarded from QLineEdit)

### KItemModels

- Fix template syntax for test case generation
- Fix linking with Qt 5.4 (wrongly placed #endif)

### KParts

- Fix the layout of the BrowserOpenOrSaveQuestion dialogue

### KPeople

- Add a check for PersonData being valid

### KRunner

- Fix metainfo.yaml: KRunner is neither a porting aid nor deprecated

### KService

- Remove too-strict maximum string length in KSycoca database

### KTextEditor

- Use proper char syntax '"' instead of '"'
- doxygen.xml: Use default style dsAnnotation for "Custom Tags" as well (less hard-coded colors)
- Add option to show the counter of words
- Improved foreground color contrast for search &amp; replace highlightings
- Fix crash when closing Kate through dbus while the print dialog is open (bug #356813) (bug 356813)
- Cursor::isValid(): add note about isValidTextPosition()
- Add API {Cursor, Range}::{toString, static fromString}

### KUnitConversion

- Inform the client if we don't know the conversion rate
- Add ILS (Israeli New Shekel) currency (bug 336016)

### KWallet Framework

- disable seession restore for kwalletd5

### KWidgetsAddons

- KNewPasswordWidget: Remove size hint on spacer, which was leading to some always empty space in the layout
- KNewPasswordWidget: fix QPalette when widget is disabled

### KWindowSystem

- Fix generation of path to xcb plugin

### Plasma Framework

- [QuickTheme] Fix properties
- highlight/highlightedText from proper color group
- ConfigModel: Don't try to resolve empty source path from package
- [calendar] Only show the events mark on days grid, not month or year
- declarativeimports/core/windowthumbnail.h - fix -Wreorder warning
- reload icon theme properly
- Always write the theme name to plasmarc, also if the default theme is chosen
- [calendar] Add a mark to days containing an event
- add Positive, Neutral, Negative text colors
- ScrollArea: Fix warning when contentItem is not Flickable
- Add a prop and method for aligning the menu against a corner of its visual parent
- Allow setting minimum width on Menu
- Maintain order in stored item list
- Extend API to allow (re)positioning menu items during procedural insert
- bind highlightedText color in Plasma::Theme
- Fix unsetting associated application/urls for Plasma::Applets
- Don't expose symbols of private class DataEngineManager
- add an "event" element in the calendar svg
- SortFilterModel: Invalidate filter when changing filter callback

### Sonnet

- Install parsetrigrams tool for cross compiling
- hunspell: Load/Store a personal dictionary
- Support hunspell 1.4
- configwidget: notify about changed config when ignored words updated
- settings: don't immediately save the config when updating ignore list
- configwidget: fix saving when ignore words updated
- Fix failed to save ignore word issue (bug 355973)

Sie können im Kommentarabschnitt des <a href='https://dot.kde.org/2014/07/07/kde-frameworks-5-makes-kde-software-more-accessible-all-qt-developers'>Dot-Artikels</a> über diese Veröffentlichung diskutieren und Ideen einbringen.
