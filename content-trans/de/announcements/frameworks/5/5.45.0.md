---
aliases:
- ../../kde-frameworks-5.45.0
date: 2018-04-14
layout: framework
libCount: 70
src: /announcements/frameworks/5-tp/KDE_QT.jpg
---
###  Attica

- Explicitly set content type to form data

### Baloo

- Simplify Term operator&amp;&amp; and ||
- Do not retrieve document ID for skipped result entries
- Do not retrieve mtime from database repeatedly when sorting
- Do not export databasesanitizer by default
- baloodb: Add experimental message
- Introduce baloodb CLI tool
- Introduce sanitizer class
- [FileIndexerConfig] Delay populating folders until actually used
- src/kioslaves/search/CMakeLists.txt - link to Qt5Network following changes to kio
- balooctl: checkDb should also verify the last known url for the documentId
- balooctl monitor: Resume to wait for service

### Breeze Icons

- add window-pin icon (bug 385170 add window-pin icon)
- rename 64 px icons added for elisa
- change 32px icons for playlist shuffle and repeat
- Missing icons for inline Messages (bug 392391)
- New icon for Elisa music player
- Add media status icons
- Remove frame around media action icons
- add media-playlist-append and play icons
- add view-media-album-cover for babe

### Extra CMake-Module

- Make use of upstream CMake infrastructure to detect the compiler toolchain
- API dox: fix some "code-block" lines to have empty lines before/after
- Add ECMSetupQtPluginMacroNames
- Provide androiddeployqt with all prefix paths
- Include the "stdcpp-path" in the json file
- Resolve symlinks in QML import paths
- Provide QML import paths to androiddeployqt

### Framework-Integration

- kpackage-install-handlers/kns/CMakeLists.txt - link to Qt::Xml following changes in knewstuff

### KActivitiesStats

- Do not assume SQLite works and do not terminate on errors

### KDE Doxygen Tools

- Look first for qhelpgenerator-qt5 for help generation

### KArchive

- karchive, kzip: try to handle duplicate files in a bit nicer way
- Use nullptr for passing a null pointer to crc32

### KCMUtils

- Make it possible to request a plugin configuration module programatically
- Consistently use X-KDE-ServiceTypes instead of ServiceTypes
- Add X-KDE-OnlyShowOnQtPlatforms to KCModule servicetype definition

### KCoreAddons

- KTextToHTML: return when url is empty
- Cleanup m_inotify_wd_to_entry before invalidating Entry pointers (bug 390214)

### KDeclarative

- Setup QQmlEngine only once in QmlObject

### KDED

- Add X-KDE-OnlyShowOnQtPlatforms to KDEDModule servicetype definition

### KDocTools

- Add entities for Elisa, Markdown, KParts, DOT, SVG to general.entities
- customization/ru: Fix translation of underCCBYSA4.docbook and underFDL.docbook
- Fix duplicate lgpl-notice/gpl-notice/fdl-notice
- customization/ru: Translate fdl-notice.docbook
- change spelling of kwave requested by the maintainer

### KFileMetaData

- taglibextractor: Refactor for better readability

### KGlobalAccel

- Don't assert if used incorrectly from dbus (bug 389375)

### KHolidays

- holidays/plan2/holiday_in_en-gb - update holiday file for India (bug 392503)
- This package was not be updated. Perhaps a problem with script
- Reworked the holiday files for Germany (bug 373686)
- Format README.md as the tools expect (with an Introduction section)

### KHTML

- avoid asking for an empty protocol

### KI18n

- Make sure ki18n can build its own translations
- Don't call PythonInterp.cmake in KF5I18NMacros
- Make it possible to generate po files in parallel
- Create a constructor for KLocalizedStringPrivate

### KIconThemes

- Make KIconEngine export comment accurate
- Avoid an asan runtime error

### KInit

- Delete IdleSlave having temporary authorization

### KIO

- Ensure that the model is set when resetResizing is called
- pwd.h isn't present on windows
- Remove Recently Saved This Month and Recently Saved Last Month entries by default
- Have KIO build for Android
- Temporarily disable installation of file ioslave's kauth helper and policy file
- Handle privilege operation confirmation prompts in SlaveBase rather than in KIO::Job
- Improve consistency of "Open With" UI by always showing top app inline
- Fix crash when device emits ready read after job is finished
- Highlight selected items when showing parent folder from the open/save dialog (bug 392330)
- Support NTFS hidden files
- Consistently use X-KDE-ServiceTypes instead of ServiceTypes
- Fix assert in concatPaths when pasting a full path into KFileWidget's lineedit
- [KPropertiesDialog] Support Checksum tab for any local path (bug 392100)
- [KFilePlacesView] Call KDiskFreeSpaceInfo only if necessary
- FileUndoManager: don't delete non-existing local files
- [KProtocolInfoFactory] Don't clear cache if it had just been built
- Don't try to find an icon for a relative URL either (e.g. '~')
- Use correct item URL for Create New context menu (bug 387387)
- Fix more cases of incorrect parameter to findProtocol
- KUrlCompletion: early return if the URL is invalid like ":/"
- Don't try to find an icon for an empty url

### Kirigami

- Bigger icons in mobile mode
- Force a content size into the background style item
- Add InlineMessage type and Gallery app example page
- better heuristics selective coloring
- make loading from local svgs actually work
- support the android icon loading method as well
- use a coloring strategy similar to the former different styles
- [Card] Use own "findIndex" implementation
- kill network transfers if we change icon while running
- first prototype for a delegate recycler
- Allow OverlaySheet clients to omit the built-in close button
- Components for Cards
- Fix ActionButton size
- Make passiveNotifications last longer, so users can actually read them
- Remove unused QQC1 dependency
- ToolbarApplicationHeader layout
- Make it possible to show the title despite having ctx actions

### KNewStuff

- Actually vote when clicking stars in the list view (bug 391112)

### KPackage-Framework

- Try to fix the FreeBSD build
- Use Qt5::rcc instead of looking for the executable
- Use NO_DEFAULT_PATH to ensure the right command is picked up
- Look also for prefixed rcc executables
- set component for correct qrc generation
- Fix the rcc binary package generation
- Generate the rcc file every time, at install time
- Make org.kde. components include a donate URL
- Mark kpackage_install_package undeprecated for plasma_install_package

### KPeople

- Expose PersonData::phoneNumber to QML

### Kross

- No need to have kdoctools required

### KService

- API dox: consistently use X-KDE-ServiceTypes instead of ServiceTypes

### KTextEditor

- Make it possible for KTextEditor to build on Android NDK's gcc 4.9
- avoid Asan runtime error: shift exponent -1 is negative
- optimization of TextLineData::attribute
- Don't calculate attribute() twice
- Revert Fix: View jumps when Scroll past end of document is enabled (bug 391838)
- don't pollute the clipboard history with dupes

### KWayland

- Add Remote Access interface to KWayland
- [server] Add support for the frame semantics of Pointer version 5 (bug 389189)

### KWidgetsAddons

- KColorButtonTest: remove todo code
- ktooltipwidget: Subtract margins from available size
- [KAcceleratorManager] Only set iconText() if actually changed (bug 391002)
- ktooltipwidget: Prevent offscreen display
- KCapacityBar: set QStyle::State_Horizontal state
- Sync with KColorScheme changes
- ktooltipwidget: Fix tooltip positioning (bug 388583)

### KWindowSystem

- Add "SkipSwitcher" to API
- [xcb] Fix implementation of _NET_WM_FULLSCREEN_MONITORS (bug 391960)
- Reduce plasmashell frozen time

### ModemManagerQt

- cmake: don't flag libnm-util as found when ModemManager is found

### NetworkManagerQt

- Export the NetworkManager include dirs
- Start requiring NM 1.0.0
- device: define StateChangeReason and MeteredStatus as Q_ENUMs
- Fix conversion of AccessPoint flags to capabilities

### Plasma Framework

- Wallpaper templates: set background color to ensure contrast to sample text content
- Add template for Plasma wallpaper with QML extension
- [ToolTipArea] Add "aboutToShow" signal
- windowthumbnail: Use gamma correct scaling
- windowthumbnail: Use mipmap texture filtering (bug 390457)
- Remove unused X-Plasma-RemoteLocation entries
- Templates: drop unused X-Plasma-DefaultSize from applet metadata
- Consistently use X-KDE-ServiceTypes instead of ServiceTypes
- Templates: drop unused X-Plasma-Requires-* entries from applet metadata
- remove anchors of item in a layout
- Reduce plasmashell frozen time
- preload only after the containment emitted uiReadyChanged
- Fix combobox breakage (bug 392026)
- Fix text scaling with non-integer scale factors when PLASMA_USE_QT_SCALING=1 is set (bug 356446)
- new icons for disconnected/disabled devices
- [Dialog] Allow setting outputOnly for NoBackground dialog
- [ToolTip] Check file name in KDirWatch handler
- Disable deprecation warning from kpackage_install_package for now
- [Breeze Plasma Theme] Apply currentColorFix.sh to changed media icons
- [Breeze Plasma Theme] Add media status icons with circles
- Remove frames around media buttons
- [Window Thumbnail] Allow using atlas texture
- [Dialog] Remove now obsolete KWindowSystem::setState calls
- Support Atlas textures in FadingNode
- Fix FadingMaterial fragment with core profile

### QQC2StyleBridge

- fix rendering when disabled
- better layout
- experimental support for auto mnemonics
- Make sure we are taking into account the size of the element when styling
- Fix font rendering for non-HiDPI and integer scale factors (bug 391780)
- fix icons colors with colorsets
- fix icon colors for toolbuttons

### Solid

- Solid can now query for batteries in e.g. wireless gamepads and joysticks
- Use recently introduced UP enums
- add gaming_input devices and others to Battery
- Adding Battery Devices Enum
- [UDevManager] Also explicitly query for cameras
- [UDevManager] Already filter for subsystem before querying (bug 391738)

### Sonnet

- Don't impose using the default client, pick one that supports the requested language.
- Include replacement strings in the suggestion list
- implement NSSpellCheckerDict::addPersonal()
- NSSpellCheckerDict::suggest() returns a list of suggestions
- initialise NSSpellChecker language in NSSpellCheckerDict ctor
- implement NSSpellChecker logging category
- NSSpellChecker requires AppKit
- Move NSSpellCheckerClient::reliability() out of line
- use the preferred Mac platform token
- Use correct directory to lookup trigrams in windows build dir

### Syntax Highlighting

- Make it possible to fully build the project when crosscompiling
- Redesign CMake syntax generator
- Optimize highlighting Bash, Cisco, Clipper, Coffee, Gap, Haml, Haskell
- Add syntax highlighting for MIB files

### Sicherheitsinformation

Der veröffentlichte Quelltext wurde mit GPG mit folgendem Schlüssel signiert: pub rsa2048/58D0EE648A48B3BB 2016-09-05 David Faure &lt;faure@kde.org&gt; Primärer Fingerprint des Schlüssels: 53E6 B47B 45CE A3E0 D5B7 4577 58D0 EE64 8A48 B3BB

Sie können im Kommentarabschnitt des <a href='https://dot.kde.org/2014/07/07/kde-frameworks-5-makes-kde-software-more-accessible-all-qt-developers'>Dot-Artikels</a> über diese Veröffentlichung diskutieren und Ideen einbringen.
