---
aliases:
- ../announce-applications-16.04.3
changelog: true
date: 2016-07-12
description: KDE veröffentlicht die KDE-Anwendungen 16.04.3
layout: application
title: KDE veröffentlicht die KDE-Anwendungen 16.04.3
version: 16.04.3
---
12. Juli 2016. Heute veröffentlicht KDE die dritte Aktualisierung der <a href='../16.04.0'>KDE-Anwendungen 16.04</a>. Diese Veröffentlichung enthält nur Fehlerkorrekturen und aktualisierte Übersetzungen und ist daher für alle Benutzer eine sichere und problemlose Aktualisierung.

Mehr als 20 aufgezeichnete Fehlerkorrekturen enthalten Verbesserungen für Ark, KDEPpim und Umbrello.

Diese Veröffentlichung enthält die Version der KDE Development Platform %1 mit langfristige Unterstützung.
