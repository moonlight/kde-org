---
aliases:
- ../announce-applications-17.08.3
changelog: true
date: 2017-11-09
description: KDE veröffentlicht die KDE-Anwendungen 17.08.3
layout: application
title: KDE veröffentlicht die KDE-Anwendungen 17.08.3
version: 17.08.3
---
09 November 2017. Heute veröffentlicht KDE die dritte Aktualisierung der <a href='../17.08.0'>KDE-Anwendungen 17.08</a>. Diese Veröffentlichung enthält nur Fehlerkorrekturen und aktualisierte Übersetzungen und ist daher für alle Benutzer eine sichere und problemlose Aktualisierung.

Mehr als ein Dutzend aufgezeichnete Fehlerkorrekturen enthalten unter anderem Verbesserungen für  Kontact, Ark, Gwenview, KGpg, KWave, Okular und Spectacle.

Diese Veröffentlichung enthält die letzte Version der KDE Development Platform 4.14.38.

Verbesserungen umfassen:

- Work around a Samba 4.7 regression with password-protected SMB shares
- Okular no longer crashes after certain rotation jobs
- Ark preserves file modification dates when extracting ZIP archives
