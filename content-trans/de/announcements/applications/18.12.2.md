---
aliases:
- ../announce-applications-18.12.2
changelog: true
date: 2019-02-07
description: KDE veröffentlicht die Anwendungen 18.12.2.
layout: application
major_version: '18.12'
release: applications-18.12.2
title: KDE veröffentlicht die KDE-Anwendungen 18.12.2
version: 18.12.2
---
{{% i18n_date %}}

Heute veröffentlicht KDE die erste Aktualisierung der <a href='../18.12.0'>KDE-Anwendungen 18.12</a>. Diese Veröffentlichung enthält nur Fehlerkorrekturen und aktualisierte Übersetzungen und ist daher für alle Benutzer eine sichere und problemlose Aktualisierung.

More than a dozen recorded bugfixes include improvements to Kontact, Ark, Konsole, Lokalize, Umbrello, among others.

Verbesserungen umfassen:

- Ark no longer deletes files saved from inside the embedded viewer</li>
- The address book now remembers birthdays when merging contacts</li>
- Several missing diagram display updates were fixed in Umbrello</li>
