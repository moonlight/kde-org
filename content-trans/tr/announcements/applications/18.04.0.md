---
aliases:
- ../announce-applications-18.04.0
changelog: true
date: 2018-04-19
description: KDE, KDE Uygulamalar 18.04.0'ı Gönderdi
layout: application
title: KDE, KDE Uygulamalar 18.04.0'ı Gönderdi
version: 18.04.0
---
19 Nisan 2018. KDE Uygulamaları 18.04.0 çıktı.

We continuously work on improving the software included in our KDE Application series, and we hope you will find all the new enhancements and bug fixes useful!

## What's new in KDE Applications 18.04

### Sistem

{{<figure src="/announcements/applications/18.04.0/dolphin1804.png" width="600px" >}}

The first major release in 2018 of <a href='https://www.kde.org/applications/system/dolphin/'>Dolphin</a>, KDE's powerful file manager, features many improvements to its panels:

- The 'Places' panel's sections can now be hidden if you prefer not to display them, and a new 'Network' section is now available to hold entries for remote locations.
- 'Terminal' paneli pencerenin herhangi bir tarafına kenetlenebilir ve Konsole kurulmadan onu açmaya çalışırsanız, Dolphin bir uyarı gösterecek ve onu kurmanıza yardımcı olacaktır.
- 'Bilgi' paneli için HiDPI desteği geliştirildi.

Klasör görünümü ve menüler de güncellendi:

- Çöp Kutusu klasöründe artık bir 'Çöp Kutusunu Boşalt' düğmesi görüntüleniyor.
- A 'Show Target' menu item has been added to help finding symlink targets.
- Git integration has been enhanced, as the context menu for git folders now displays two new actions for 'git log' and 'git merge'.

Diğer iyileştirmeler şunları içerir:

- A new shortcut has been introduced giving you the option to open the Filter Bar simply by hitting the slash (/) key.
- Artık fotoğrafları çekildikleri tarihe göre sıralayabilir ve düzenleyebilirsiniz.
- The drag-and-drop of many small files within Dolphin became faster, and users can now undo batch rename jobs.

{{<figure src="/announcements/applications/18.04.0/konsole1804.png" width="600px" >}}

To make working on the command line even more enjoyable, <a href='https://www.kde.org/applications/system/konsole/'>Konsole</a>, KDE's terminal emulator application, can now look prettier:

- Renk şemalarını KNewStuff üzerinden indirebilirsiniz.
- Kaydırma çubuğu, etkin renk şemasıyla daha iyi uyum sağlar.
- Varsayılan olarak sekme çubuğu yalnızca gerektiğinde gösterilir.

Konsole's contributors did not stop there, and introduced many new features:

- Metin kopyalamayı HTML olarak değiştirmek için yeni bir salt okunur mod ve bir profil özelliği eklendi.
- Wayland'ın altında, Konsole artık sürükle ve bırak menüsünü destekliyor.
- Several improvements took place in regards to the ZMODEM protocol: Konsole can now handle the zmodem upload indicator B01, it will show the progress while transferring data, the Cancel button in the dialog now works as it should, and transferring of bigger files is better supported by reading them into memory in 1MB chunks.

Diğer iyileştirmeler şunları içerir:

- Libinput ile fare tekerleği kaydırması düzeltildi ve fare tekerleği ile gezinirken kabuk geçmişinde gezinmek artık önlendi.
- Searches are refreshed after changing the search match regular expression option and when pressing 'Ctrl' + 'Backspace' Konsole will match xterm behaviour.
- The '--background-mode' shortcut has been fixed.

### Çoklu Ortam

<a href='https://juk.kde.org/'>JuK</a>, KDE's music player, now has Wayland support. New UI features include the ability to hide the menu bar and having a visual indication of the currently playing track. While docking to the system tray is disabled, JuK will no longer crash when attempting to quit via the window 'close' icon and the user interface will remain visible. Bugs regarding JuK autoplaying unexpectedly when resuming from sleep in Plasma 5 and handling the playlist column have also been fixed.

For the 18.04 release, contributors of <a href='https://kdenlive.org/'>Kdenlive</a>, KDE's non-linear video editor, focused on maintenance:

- Klibin yeniden boyutlandırılması artık solgunluğu ve ana kareleri bozmaz.
- HiDPI monitörlerde ekran ölçeklendirme kullanıcıları daha net simgelerin keyfini çıkarabilir.
- Bazı yapılandırmalarda başlangıçta olası bir kilitlenme düzeltildi.
- MLT'nin artık 6.6.0 veya daha yeni bir sürüme sahip olması gerekmektedir ve uyumluluk iyileştirilmiştir.

#### Grafikler

{{<figure src="/announcements/applications/18.04.0/gwenview1804.png" width="600px" >}}

Over the last months, contributors of <a href='https://www.kde.org/applications/graphics/gwenview/'>Gwenview</a>, KDE's image viewer and organizer, worked on a plethora of improvements. Highlights include:

- MPRIS denetleyicileri için destek eklendi, böylece artık KDE Bağlan, klavyenizin medya tuşları ve Media Player plasmoid aracılığıyla tam ekran slayt gösterilerini kontrol edebilirsiniz.
- The thumbnail hover buttons can now be turned off.
- The Crop tool received several enhancements, as its settings are now remembered when switching to another image, the shape of the selection box can now be locked by holding down the 'Shift' or 'Ctrl' keys and it can also be locked to the aspect ratio of the currently displayed image.
- Tam ekran modunda artık 'Escape' tuşunu kullanarak çıkabilirsiniz ve renk paleti aktif renk temanızı yansıtacaktır. Bu modda Gwenview'den çıkarsanız, bu ayar hatırlanacak ve yeni oturum da tam ekranda başlayacaktır.

Ayrıntılara dikkat önemlidir, bu nedenle Gwenview aşağıdaki alanlarda parlatılmıştır:

- Gwenview will display more human-readable file paths in the 'Recent Folders' list, show the proper context menu for items in the 'Recent Files' list, and correctly forget everything when using the 'Disable History' feature.
- Clicking on a folder in the sidebar allows toggling between Browse and View modes and remembers the last used mode when switching between folders, thus allowing faster navigation in huge image collections.
- Göz At kipinde klavye odağı doğru şekilde gösterilerek klavye dolaşımı daha da kolaylaştırılmıştır.
- The 'Fit Width' feature has been replaced with a more generalized 'Fill' function.

{{<figure src="/announcements/applications/18.04.0/gwenviewsettings1804.png" width="600px" >}}

Daha küçük geliştirmeler bile çoğu zaman kullanıcının iş akışlarını daha keyifli hale getirebilir:

- To improve consistency, SVG images are now enlarged like all other images when 'Image View → Enlarge Smaller Images' is turned on.
- Bir resmi düzenledikten veya değişiklikleri geri aldıktan sonra, resim görünümü ve küçük resim artık senkronize olmayacaktır.
- When renaming images, the filename extension will be unselected by default and the 'Move/Copy/Link' dialog now defaults to showing the current folder.
- Lots of visual papercuts were fixed, e.g. in the URL bar, for the full screen toolbar, and for the thumbnail tooltip animations. Missing icons were added as well.
- Son olarak, tam ekran fareyle üzerine gelme düğmesi, klasörün içeriğini göstermek yerine doğrudan görüntüyü görüntüleyecektir ve gelişmiş ayarlar artık ICC renk oluşturma amacı üzerinde daha fazla kontrol sağlar.

#### Ofis

In <a href='https://www.kde.org/applications/graphics/okular/'>Okular</a>, KDE's universal document viewer, PDF rendering and text extraction can now be cancelled if you have poppler version 0.63 or higher, which means that if you have a complex PDF file and you change the zoom while it's rendering it will cancel immediately instead of waiting for the render to finish.

AFSimple_Calculate için geliştirilmiş PDF JavaScript desteği bulacaksınız ve poppler 0.64 veya daha yüksek sürümüne sahipseniz Okular, formların salt okunur durumundaki PDF JavaScript değişikliklerini destekleyecektir.

Management of booking confirmation emails in <a href='https://www.kde.org/applications/internet/kmail/'>KMail</a>, KDE's powerful email client, has been significantly enhanced to support train bookings and uses a Wikidata-based airport database for showing flights with correct timezone information. To make things easier for you, a new extractor has been implemented for emails not containing structured booking data.

Diğer iyileştirmeler şunları içerir:

- Mesaj yapısı, yeni 'Uzman' eklentisi ile tekrar gösterilebilir.
- A plugin has been added in the Sieve Editor for selecting emails from the Akonadi database.
- Düzenleyicideki metin arama, normal ifade desteği ile geliştirildi.

#### İzlenceler

{{<figure src="/announcements/applications/18.04.0/spectacle1804.png" width="600px" >}}

Improving the user interface of <a href='https://www.kde.org/applications/graphics/spectacle/'>Spectacle</a>, KDE's versatile screenshot tool, was a major focus area:

- The bottom row of buttons has received an overhaul, and now displays a button to open the Settings window and a new 'Tools' button that reveals methods to open the last-used screenshot folder and launch a screen recording program.
- Son kullanılan kaydetme modu artık varsayılan olarak hatırlanmaktadır.
- Pencere boyutu artık ekran görüntüsünün en boy oranına uyum sağlayarak daha hoş ve alan açısından verimli bir ekran görüntüsü küçük resmi elde edilmesini sağlıyor.
- Ayarlar penceresi büyük ölçüde basitleştirilmiştir.

Ek olarak, kullanıcılar şu yeni özelliklerle iş akışlarını basitleştirebilecekler:

- Belirli bir pencere yakalandığında, başlığı ekran görüntüsü dosyasının adına otomatik olarak eklenebilir.
- Kullanıcı artık herhangi bir kaydetme veya kopyalama işleminden sonra Spectacle'ın otomatik olarak kapanıp kapanmayacağını seçebilir.

Önemli hata düzeltmeleri şunları içerir:

- Drag-and-drop to Chromium windows now works as expected.
- Bir ekran görüntüsünü kaydetmek için klavye kısayollarını tekrar tekrar kullanmak, artık belirsiz bir kısayol uyarı iletişim kutusuyla sonuçlanmaz.
- Dikdörtgen bölge ekran görüntüleri için, seçimin alt kenarı daha doğru bir şekilde ayarlanabilir.
- Birleştirme kapatıldığında pencerelerin ekran kenarlarına dokunarak yakalanmasının güvenilirliği artırıldı.

{{<figure src="/announcements/applications/18.04.0/kleopatra1804.gif" width="600px" >}}

With <a href='https://www.kde.org/applications/utilities/kleopatra/'>Kleopatra</a>, KDE's certificate manager and universal crypto GUI, Curve 25519 EdDSA keys can be generated when used with a recent version of GnuPG. A 'Notepad' view has been added for text based crypto actions and you can now sign/encrypt and decrypt/verify directly in the application. Under the 'Certificate details' view you will now find an export action, which you can use to export as text to copy and paste. What's more, you can import the result via the new 'Notepad' view.

In <a href='https://www.kde.org/applications/utilities/ark/'>Ark</a>, KDE's graphical file compression/decompression tool with support for multiple formats, it is now possible to stop compressions or extractions while using the libzip backend for ZIP archives.

### KDE uygulamaları yayın zamanlamasına katılan uygulamalar

KDE's webcam recorder <a href='https://userbase.kde.org/Kamoso'>Kamoso</a> and backup program <a href='https://www.kde.org/applications/utilities/kbackup/'>KBackup</a> will now follow the Applications releases. The instant messenger <a href='https://www.kde.org/applications/internet/kopete/'>Kopete</a> is also being reintroduced after being ported to KDE Frameworks 5.

### Kendi yayın programına geçen uygulamalar

The hex editor <a href='https://community.kde.org/Applications/18.04_Release_Notes#Tarballs_that_we_do_not_ship_anymore'>Okteta</a> will have its own release schedule after a request from its maintainer.

### Bug Stomping

More than 170 bugs have been resolved in applications including the Kontact Suite, Ark, Dolphin, Gwenview, K3b, Kate, Kdenlive, Konsole, Okular, Umbrello and more!

### Full Changelog
