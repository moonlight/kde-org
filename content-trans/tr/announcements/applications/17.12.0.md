---
aliases:
- ../announce-applications-17.12.0
date: 2017-12-14
description: KDE, KDE Uygulamalar 17.12.0'ı Gönderdi
layout: application
title: KDE, KDE Uygulamalar 17.12.0'ı Gönderdi
version: 17.12.0
---
14 Aralık 2017. KDE Uygulamaları 17.12.0 çıktı.

We continuously work on improving the software included in our KDE Application series, and we hope you will find all the new enhancements and bug fixes useful!

### What's new in KDE Applications 17.12

#### Sistem

{{<figure src="/announcements/applications/17.12.0/dolphin1712.gif" width="600px" >}}

<a href='https://www.kde.org/applications/system/dolphin/'>Dolphin</a>, our file manager, can now save searches and limit the search only to folders. Renaming files is now easier; simply double click on the file name. More file information is now at your hands, as the modification date and origin URL of downloaded files are now displayed in the information panel. Additionally, new Genre, Bitrate, and Release Year columns have been introduced.

#### Grafikler

Our powerful document viewer <a href='https://www.kde.org/applications/graphics/okular/'>Okular</a> gained support for HiDPI displays and Markdown language, and the rendering of documents that are slow to load is now shown progressively. An option is now available to share a document via email.

<a href='https://www.kde.org/applications/graphics/gwenview/'>Gwenview</a> image viewer can now open and highlight images in the file manager, zooming is smoother, keyboard navigation has been improved, and it now supports the FITS and Truevision TGA formats. Images are now protected from being accidentally removed by the Delete key when they are not selected.

#### Çoklu Ortam

<a href='https://www.kde.org/applications/multimedia/kdenlive/'>Kdenlive</a> now uses less memory when handling video projects which include many images, default proxy profiles have been refined, and an annoying bug related to jumping one second forward when playing backward has been fixed.

#### İzlenceler

{{<figure src="/announcements/applications/17.12.0/kate1712.png" width="600px" >}}

<a href='https://www.kde.org/applications/utilities/ark/'>Ark</a>'s zip support in the libzip backend has been improved. <a href='https://www.kde.org/applications/utilities/kate/'>Kate</a> has a new <a href='https://frinring.wordpress.com/2017/09/25/ktexteditorpreviewplugin-0-1-0/'>Preview plugin</a> that allows you to see a live preview of the text document in the final format, applying any available KParts plugins (e.g. for Markdown, SVG, Dot graph, Qt UI, or patches). This plugin also works in <a href='https://www.kde.org/applications/development/kdevelop/'>KDevelop.</a>

#### Geliştirme

{{<figure src="/announcements/applications/17.12.0/kuiviewer1712.png" width="600px" >}}

<a href='https://www.kde.org/applications/development/kompare/'>Kompare</a> now provides a context menu in the diff area, allowing for quicker access to navigation or modification actions. If you are a developer, you might find KUIViewers' new in-pane preview of UI object described by Qt UI files (widgets, dialogs, etc) useful. It now also supports KParts streaming API.

#### Ofis

<a href='https://www.kde.org/applications/office/kontact'>Kontact</a> takımı uygulama kalitesini artırmak için çok çalışıyor! Çoğu mesai, uygulamanın kod kalitesini artırmak için harcandı; ancak kullanıcılar şifreli iletilerin görüntüleme ekranının iyileştirildiğini ve text/pgp ve <a href='https://phabricator.kde.org/D8395'>Apple® Wallet Pass</a> için destek eklendiğini bilmeliler. Tatil yapılandırmasında artık IMAP klasörünü seçmek olanaklı, bir ileti K Posta'da yeniden açılırken ve identity/mailtransport aynı değilse bir uyarı geliyor, <a href='https://micreabog.wordpress.com/2017/10/05/akonadi-ews-resource-now-part-of-kde-pim/'>Microsoft® Exchange™</a> desteği, akonadi-import-wizard'da Nylas Mail ve iyileştirilmiş Geary içe aktarma desteği ve bir çok genel iyileştirme ve hata düzeltmesi var.

#### Oyunlar

{{<figure src="/announcements/applications/17.12.0/ktuberling1712.jpg" width="600px" >}}

<a href='https://www.kde.org/applications/games/ktuberling/'>KTuberling</a> can now reach a wider audience, as it has been <a href='https://tsdgeos.blogspot.nl/2017/10/kde-edu-sprint-in-berlin.html'>ported to Android</a>. <a href='https://www.kde.org/applications/games/kolf/'>Kolf</a>, <a href='https://www.kde.org/applications/games/ksirk/'>KsirK</a>, and <a href='https://www.kde.org/applications/games/palapeli/'>Palapeli</a> complete the porting of KDE games to Frameworks 5.

### More Porting to KDE Frameworks 5

Kdelibs4 tabanlı daha fazla uygulama artık KDE Frameworks 5'e taşındı. Bunlar arasında <a href=' [1]s'>JuK</a> müzik çalar, <a href='%[2 indirme yöneticisi ]s'>K İndir</a>, <a href=' [3]s'>K Miks</a>, <a href='https://www.kde.org/applications/utilities/sweeper/'>Sweeper</a> ve <a gibi yardımcı programlar href=' [5]s'>K Ağız</a> ve <a href='https://www.kde.org/applications/development/kimagemapeditor/'>KImageMapEditor</a> ve Zeroconf-ioslave var. Bunun gerçekleşmesi için zamanlarını ve çalışmalarını gönüllü olarak ayıran çalışkan geliştiricilere çok teşekkürler!

### Kendi yayın programına geçen uygulamalar

<a href='https://www.kde.org/applications/education/kstars/'>KStars</a> now has its own release schedule; check this <a href='https://knro.blogspot.de'>developer's blog</a> for announcements. It is worth noting that several applications such as Kopete and Blogilo are <a href='https://community.kde.org/Applications/17.12_Release_Notes#Tarballs_that_we_do_not_ship_anymore'>no longer shipped</a> with the Application series, as they have not yet been ported to KDE Frameworks 5, or are not actively maintained at the moment.

### Bug Stomping

Kontak Suite, Ark, Dolphin, Gwenview, K3b, Kate, Kdenlive, Konsole, Okular, Umbrello ve daha fazlasını içeren uygulamalarda 110'dan fazla hata çözüldü!

### Full Changelog

If you would like to read more about the changes in this release, <a href='/announcements/changelogs/applications/17.12.0'>head over to the complete changelog</a>. Although a bit intimidating due to its breadth, the changelog can be an excellent way to learn about KDE's internal workings and discover apps and features you never knew you had.
