---
aliases:
- ../announce-applications-16.12.0
changelog: true
date: 2016-12-15
description: KDE, KDE Uygulamaları 16.12.0'ı Gönderdi
layout: application
title: KDE, KDE Uygulamaları 16.12.0'ı Gönderdi
version: 16.12.0
---
December 15, 2016. Today, KDE introduces KDE Applications 16.12, with an impressive array of upgrades when it comes to better ease of access, the introduction of highly useful functionalities and getting rid of some minor issues, bringing KDE Applications one step closer to offering you the perfect setup for your device.

<a href='https://okular.kde.org/'>Okular</a>, <a href='https://konqueror.org/'>Konqueror</a>, <a href='https://www.kde.org/applications/utilities/kgpg/'>KGpg</a>, <a href='https://www.kde.org/applications/education/ktouch/'>KTouch</a>, <a href='https://www.kde.org/applications/education/kalzium/'>Kalzium</a> and more (<a href='https://community.kde.org/Applications/16.12_Release_Notes#Tarballs_that_were_based_on_kdelibs4_and_are_now_KF5_based'>Release Notes</a>) have now been ported to KDE Frameworks 5. We look forward to your feedback and insight into the newest features introduced with this release.

In the continued effort to make applications easier to build standalone, we have split the kde-baseapps, kdepim and kdewebdev tarballs. You can find the newly created tarballs at <a href='https://community.kde.org/Applications/16.12_Release_Notes#Tarballs_that_we_have_split'>the Release Notes document</a>

We have discontinued the following packages: kdgantt2, gpgmepp and kuser. This will help us focus on the rest of the code.

### Kwave ses editörü KDE Uygulamalarına katıldı!

{{<figure src="https://www.kde.org/images/screenshots/kwave.png" width="600px" >}}

<a href='http://kwave.sourceforge.net/'>Kwave</a> is a sound editor, it can record, play back, import and edit many sorts of audio files including multi channel files. Kwave includes some plugins to transform audio files in several ways and presents a graphical view with a complete zoom and scroll capability.

### Duvar kağıdınız olarak dünya

{{<figure src="https://frinring.files.wordpress.com/2016/08/screenshot_20160804_171642.png" width="600px" >}}

Marble artık hem bir duvar Kağıdı hem de gerçek zamanlı gündüz/gece ekranı ile saati dünyanın uydu görünümünün üstünde gösteren bir Plasma aracı içeriyor. Bunlar Plasma 4 için mevcuttu; şimdi Plasma 5 üzerinde çalışacak şekilde güncellendi.

You can find more information on <a href='https://frinring.wordpress.com/2016/08/04/wip-plasma-world-map-wallpaper-world-clock-applet-powered-by-marble/'>Friedrich W. H. Kossebau's blog</a>.

### Emoticons galore!

{{<figure src="/announcements/applications/16.12.0/kcharselect1612.png" width="600px" >}}

K Karakter Seç, Unicode İfadeler bloğunu (ve diğer SMP sembol bloklarını) gösterme yeteneği kazanmıştır.

Ayrıca, tüm sevdiğiniz karakterleri favorilerinize ekleyebilmeniz için bir Yer İmleri menüsü kazandı.

### Math is better with Julia

{{<figure src="https://2.bp.blogspot.com/-BzJNpF5SXZQ/V7skrKcQttI/AAAAAAAAAA8/7KD8g356FfAd9-ipPcWYi6QX5_nCQJFKgCLcB/s640/promo.png" width="600px" >}}

Cantor, Julia için kullanıcılarına bilimsel hesaplamadaki en son gelişmeleri kullanma olanağı sağlayan yeni bir arka uca sahip.

You can find more information on <a href='https://juliacantor.blogspot.com/2016/08/cantor-gets-support-of-julia-language.html'>Ivan Lakhtanov's blog</a>.

### Gelişmiş arşivleme

{{<figure src="https://rthomsen6.files.wordpress.com/2016/11/blog-1612-comp-method.png" width="600px" >}}

Ark'ın birkaç yeni özelliği var:

- Dosyalar ve klasörler artık arşiv içinde yeniden adlandırılabilir, kopyalanabilir veya taşınabilir
- Arşiv oluştururken sıkıştırma ve şifreleme algoritmalarını seçmek artık mümkün
- Ark can now open AR files (e.g. Linux \*.a static libraries)

You can find more information on <a href='https://rthomsen6.wordpress.com/2016/11/26/new-features-in-ark-16-12/'>Ragnar Thomsen's blog</a>.

### Ve dahası!

Kopete, jabber protokolünde X-OAUTH2 SASL kimlik doğrulaması için destek aldı ve OTR şifreleme eklentisi ile ilgili bazı sorunları düzeltti.

Kdenlive has a new Rotoscoping effect, support for downloadable content and an updated Motion Tracker. It also provides <a href='https://kdenlive.org/download/'>Snap and AppImage</a> files for easier installation.

KMail and Akregator can use Google Safe Browsing to check if a link being clicked is malicious. Both have also added back printing support (needs Qt 5.8).

### Aggressive Pest Control

Dolphin, Akonadi, K Adres Defteri, K Notlar, Akregatör, Cantor, Ark, Kdenlive ve daha fazlası dahil olmak üzere uygulamalarda 130'dan fazla hata çözüldü!

### Full Changelog
