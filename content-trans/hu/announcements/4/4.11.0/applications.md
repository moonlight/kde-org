---
date: 2013-08-14
hidden: true
title: 'KDE Applications 4.11: Hatalmas előrelépés a személyes információkezelésben,
  és fejlesztések minden téren'
---
A Dolphin fájlkezelő számos apróbb javítást és optimalizációt hoz ebben a kiadásban. A nagy méretű mappák betöltése gyorsult, és akár 30&#37;-kal kevesebb memóriát is igényelhet. A nagy lemez- és processzorterhelést megelőzendő csak a látható elemek előnézetei töltődnek be. Rengeteg fejlesztés történt, többek között számos, a Részletek nézetmódot érintő hiba került javításra, nem jelennek meg többé &quot;ismeretlen&quot; helykitoltő ikonok egy mappába belépéskor és egy archívumra középső gombbal kattintva mostantól új lapon jelenik meg annak tartalma, sokkal egységesebb működést biztosítva.

{{< figure class="text-center img-size-medium" src="/announcements/4/4.11.0/screenshots/send-later.png" caption=`Az új, küldés később munkafolyamat a Kontactban` width="600px">}}

## A Kontact csomag fejlesztései

A Kontact csomagnál komoly figyelmet fordítottak a stabilitásra, teljesítményre és memóriahasználatra. A mappák importálása, a szolgáltatók közötti váltás, a levelek letöltése, nagy számú üzenet megjelölése vagy mozgatása és az indulási idő is jelentősen javult az elmúlt hat hónapban. További részletekért lásd <a href='http://blogs.kde.org/2013/07/18/memory-usage-improvements-411'>ezt a blogbejegyzést</a>. Az <a href='http://www.aegiap.eu/kdeblog/2013/07/news-in-kdepim-4-11-archive-mail-agent/'>archiváló funkcióban rengeteg hibajavítás történt</a>, valamint az Importvarázsló is fejlődött, lehetővé téve a Trojitá levelezőkliens beállításainak importálását, és jobb importálást más alkalmazásokból. Többet tudhat meg <a href='http://www.progdan.cz/2013/07/whats-new-in-the-akonadi-world/'>itt</a>.

{{< figure class="text-center img-size-medium" src="/announcements/4/4.11.0/screenshots/kmail-archive-agent.png" caption=`The archive agent manages storing email in compressed form` width="600px">}}

This release also comes with some significant new features. There is a <a href='http://www.aegiap.eu/kdeblog/2013/05/news-in-kdepim-4-11-header-theme-33-grantlee-theme-generator-headerthemeeditor/'>new theme editor for email headers</a> and email images can be resized on the fly. The <a href='http://www.aegiap.eu/kdeblog/2013/07/new-in-kdepim-4-11-send-later-agent/'>Send Later feature</a> allows scheduling the sending of emails on a specific date and time, with the added possibility of repeated sending according to a specified interval. KMail Sieve filter support (an IMAP feature allowing filtering on the server) has been improved, users can generate sieve filtering scripts <a href='http://www.aegiap.eu/kdeblog/2013/04/news-in-kdepim-4-11-improve-sieve-support-22/'>with an easy-to-use interface</a>. In the security area, KMail introduces automatic 'scam detection', <a href='http://www.aegiap.eu/kdeblog/2013/04/news-in-kdepim-4-11-scam-detection/'>showing a warning</a> when mails contain typical phishing tricks. You now receive an <a href='http://www.aegiap.eu/kdeblog/2013/06/news-in-kdepim-4-11-new-mail-notifier/'>informative notification</a> when new mail arrives. and last but not least, the Blogilo blog writer comes with a much-improved QtWebKit-based HTML editor.

## Extended Language Support for Kate

Advanced text editor Kate introduces new plugins: Python (2 and 3), JavaScript & JQuery, Django and XML. They introduce features like static and dynamic autocompletion, syntax checkers, inserting of code snippets and the ability to automatically indent XML with a shortcut. But there is more for Python friends: a python console providing in-depth information on an opened source file. Some small UI improvements have also been done, including <a href='http://kate-editor.org/2013/04/02/kate-search-replace-highlighting-in-kde-4-11/'>new passive notifications for the search functionality</a>, <a href='http://kate-editor.org/2013/03/16/kate-vim-mode-papercuts-bonus-emscripten-qt-stuff/'>optimizations to the VIM mode</a> and <a href='http://kate-editor.org/2013/03/27/new-text-folding-in-kate-git-master/'>new text folding functionality</a>.

{{< figure class="text-center img-size-medium" src="/announcements/4/4.11.0/screenshots/kstars.png" caption=`KStars shows interesting upcoming events visible from your location` width="600px">}}

## Other Application Improvements

In the area of games and education several smaller and larger new features and optimizations have arrived. Prospective touch typists might enjoy the right-to-left support in KTouch while the star-gazer's friend, KStars, now has a tool which shows interesting events coming up in your area. Math tools Rocs, Kig, Cantor and KAlgebra all got attention, supporting more backends and calculations. And the KJumpingCube game now has features larger board sizes, new skill levels, faster responses and an improved user interface.

The Kolourpaint simple painting application can deal with the WebP image format and the universal document viewer Okular has configurable review tools and introduces undo/redo support in forms and annotations. The JuK audio tagger/player supports playback and metadata editing of the new Ogg Opus audio format (however, this requires that the audio driver and TagLib also support Ogg Opus).

#### A KDE alkalmazások telepítése

KDE software, including all its libraries and its applications, is available for free under Open Source licenses. KDE software runs on various hardware configurations and CPU architectures such as ARM and x86, operating systems and works with any kind of window manager or desktop environment. Besides Linux and other UNIX based operating systems you can find Microsoft Windows versions of most KDE applications on the <a href='http://windows.kde.org'>KDE software on Windows</a> site and Apple Mac OS X versions on the <a href='http://mac.kde.org/'>KDE software on Mac site</a>. Experimental builds of KDE applications for various mobile platforms like MeeGo, MS Windows Mobile and Symbian can be found on the web but are currently unsupported. <a href='http://plasma-active.org'>Plasma Active</a> is a user experience for a wider spectrum of devices, such as tablet computers and other mobile hardware.

A KDE szoftverek letölthetők forráskódként és különböző bináris formátumokban a <a href='http://download.kde.org/stable/4.11.0'>download.kde.org</a> oldalról valamint beszerezhetők <a href='/download'>CD-ROM</a>-on vagy a <a href='/distributions'>főbb GNU/Linux és UNIX rendszerek</a> is elérhetővé teszik azokat.

##### Csomagok

Néhány Linux/UNIX gyártó biztosít 4.11.0 bináris csomagokat a disztribúciója néhány verziójához, más esetekben a közösség önkéntesei készítették el azokat. <br />

##### Csomagletöltési helyek

A KDE Release Team által pillanatnyilag ismert bináris csomagok listájáért látogassa meg a <a href='http://community.kde.org/KDE_SC/Binary_Packages#KDE_4.11.0'>Community Wiki</a> oldalát.

A 4.11.0 teljes forráskódja <a href='/info/4/4.11.0'>szabadon letölthető</a>. A KDE szoftver 4.11.0 verziójának fordításával és telepítésével kapcsolatos instrukciók elérhetők a <a href='/info/4/4.11.0#binary'>4.11.0 információs oldalán</a>.

#### Rendszerkövetelmények

In order to get the most out of these releases, we recommend to use a recent version of Qt, such as 4.8.4. This is necessary in order to assure a stable and performant experience, as some improvements made to KDE software have actually been done in the underlying Qt framework.<br /> In order to make full use of the capabilities of KDE's software, we also recommend to use the latest graphics drivers for your system, as this can improve the user experience substantially, both in optional functionality, and in overall performance and stability.

## Also Announced Today:

## <a href="../plasma"><img src="/announcements/4/4.11.0/images/plasma.png" class="app-icon" alt="The KDE Plasma Workspaces 4.11" width="64" height="64" /> Plasma Workspaces 4.11 Continues to Refine User Experience</a>

Gearing up for long term maintenance, Plasma Workspaces delivers further improvements to basic functionality with a smoother taskbar, smarter battery widget and improved sound mixer. The introduction of KScreen brings intelligent multi-monitor handling to the Workspaces, and large scale performance improvements combined with small usability tweaks make for an overall nicer experience.

## <a href="../platform"><img src="/announcements/4/4.11.0/images/platform.png" class="app-icon" alt="The KDE Development Platform 4.11"/> KDE Platform 4.11 Delivers Better Performance</a>

This release of KDE Platform 4.11 continues to focus on stability. New features are being implemented for our future KDE Frameworks 5.0 release, but for the stable release we managed to squeeze in optimizations for our Nepomuk framework.
