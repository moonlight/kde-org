---
aliases:
- ../announce-applications-14.12-beta3
custom_spread_install: true
date: '2014-11-20'
description: KDE Ships Applications 14.12 Beta 2.
layout: application
title: KDE에서 KDE 프로그램 14.12의 세 번째 베타 출시
---
2014년 11월 20일. 오늘 KDE에서는 KDE 프로그램의 새로운 베타 버전을 출시했습니다. 의존성 및 추가 기능은 더 추가되지 않을 예정이며, KDE 팀은 이제부터 버그 수정 및 다듬기에 집중할 것입니다.

다양한 프로그램이 KDE 프레임워크 5로 이식되었기 때문에, KDE 프로그램 14.12 릴리스의 품질 및 사용자 경험 유지, 향상을 위하여 지속적인 테스트가 필요합니다. 개발자들이 모든 환경을 테스트하기는 어렵기 때문에 KDE의 높은 품질을 유지하려면 사용자 여러분들의 도움이 필요합니다. 최종 릴리스 이전에 버그를 빠르게 찾으려면 여러분의 도움이 필요합니다. 베타 버전을 설치하고 <a href='https://bugs.kde.org/'>버그 보고(영어)</a>를 통해서 참여할 수 있습니다.

#### KDE 프로그램 14.12 베타 3 바이너리 패키지 설치

<em>Packages</em>. Some Linux/UNIX OS vendors have kindly provided binary packages of KDE Applications 14.12 Beta 3 (internally 14.11.95) for some versions of their distribution, and in other cases community volunteers have done so. Additional binary packages, as well as updates to the packages now available, may become available over the coming weeks.

<em>Package Locations</em>. For a current list of available binary packages of which the KDE Project has been informed, please visit the <a href='http://community.kde.org/KDE_SC/Binary_Packages'>Community Wiki</a>.

#### KDE 프로그램 14.12 베타 3 컴파일

The complete source code for KDE Applications 14.12 Beta 3 may be <a href='http://download.kde.org/unstable/applications/14.11.95/src/'>freely downloaded</a>. Instructions on compiling and installing are available from the <a href='/info/applications/applications-14.11.95'>KDE Applications Beta 3 Info Page</a>.
