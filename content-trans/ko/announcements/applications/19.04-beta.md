---
aliases:
- ../announce-applications-19.04-beta
date: 2019-03-22
description: KDE Ships Applications 19.04 Beta.
layout: application
release: applications-19.03.80
title: KDE에서 KDE 프로그램 19.04 베타 출시
version_number: 19.03.80
version_text: 19.04 Beta
---
2019년 3월 22일. 오늘 KDE에서는 KDE 프로그램의 새로운 베타 버전을 출시했습니다. 의존성 및 추가 기능은 더 추가되지 않을 예정이며, KDE 팀은 이제부터 버그 수정 및 다듬기에 집중할 것입니다.

Check the <a href='https://community.kde.org/Applications/19.04_Release_Notes'>community release notes</a> for information on tarballs and known issues. A more complete announcement will be available for the final release.

KDE 프로그램 19.04 릴리스의 품질 및 사용자 경험 유지, 향상을 위하여 지속적인 테스트가 필요합니다. 개발자들이 모든 환경을 테스트하기는 어렵기 때문에 KDE의 높은 품질을 유지하려면 사용자 여러분들의 도움이 필요합니다. 최종 릴리스 이전에 버그를 빠르게 찾으려면 여러분의 도움이 필요합니다. 베타 버전을 설치하고 <a href='https://bugs.kde.org/'>버그 보고(영어)</a>를 통해서 참여할 수 있습니다.
