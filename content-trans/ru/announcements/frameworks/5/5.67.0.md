---
aliases:
- ../../kde-frameworks-5.67.0
date: 2020-02-02
layout: framework
libCount: 70
---
### Общие

- Многие устаревшие методы перенесены с Qt 5.15 для уменьшения количества предупреждений при сборке.

### Подсистема семантического поиска Baloo

- Замена KConfig на KConfigXt для возможности использования в KCM.

### Значки Breeze

- Новый значок улучшенного текстового редактора Kate, основанный на дизайне предложенном Tyson Tan и соответствущий стилю оформления Breeze;
- Изменён значок VLC для большей схожести с официальными значками;
- Добавлены значки для приложения ktrip из репозитория приложения;
- Добавлен значок для типа «application/sql»;
- Наведён порядок и добавлены недостающие значки повтора воспроизведения размером 22 пикселя;
- Добавлен значок типа «text/vnd.kde.kcrash-report»;
- Для типа «application/x-ms-shortcut» теперь используется значок ярлыка.

### Дополнительные модули CMake

- Добавлена отсутствовавшая переменная окружения «Import»;
- ECMAddAppIcon: Для извлечения расширений из имён добавлен шаблон sc;
- ECMAddQch: поддержка использования макроса &amp; document K_DOXYGEN.

### Интеграция Framework

- Исключена неиспользуемая зависимость QtDBus;

### KActivitiesStats

- Исправлен SQL-запрос в allResourcesQuery.

### KActivities

- Удалены файлы, неподдерживаемые Windows;
- Добавлена проверка сохранения URI без завершающей косой черты.

### Инструменты KDE Doxygen

- Восстановлено импортирование модулей из Python2;
- Для работы с api.kde.org из модулей на Python2, для файловых систем в коде назначено использование кодировки UTF-8.

### Набор библиотек KCMUtils

- Предпочтительное использование новых подключаемых модулей KCM;
- KCModuleQml: Ensure defaulted is emitted with the current configModule-&gt;representsDefaults on load
- Show button respecting what is declared by KCModule
- Update KPluginSelector to allow KCM to show good state for reset, apply and default button

### Модуль KConfig

- Реорганизован код модуля KConfigXT;
- Исправлены привязки Python после изменения ebd14f29f8052ff5119bf97b42e61f404f223615;
- KCONFIG_ADD_KCFG_FILES: regenerate also on new version of kconfig_compiler
- В KCONFIG_ADD_KCFG_FILES возможно передавать цель, а не только список источников;
- Add KSharedConfig::openStateConfig for storing state information
- Исправлена сборка привязок Python после изменения 7ab8275bdb56882692846d046a5bbeca5795b009;

### Модуль KConfigWidgets

- KStandardAction: добавлен метод для создания действия «SwitchApplicationLanguage»;
- [KColorSchemeManager] Дубликаты не будут перечислены;
- [KColorschemeManager] Добавлен параметр отслеживания глобального оформления;

### Модуль KCoreAddons

- demote plugin load errors from warning to debug level + reword
- Document how to filter by servicetype the right way
- Add perlSplit() overload taking a QRegularExpression and deprecate the QRegExp one
- Add mime type for backtraces saved from DrKonqi
- Add utility text function KShell::tildeCollapse
- KPluginMetaData: add initialPreference() getter
- desktoptojson: also convert InitialPreference key

### Модуль KDeclarative

- Correctly compute bottom margin for grid delegates with subtitles
- [ConfigModule] Say which package is invalid

### Модуль KHolidays

- Update holidays and add flagdays and namedays for Sweden

### Модуль KI18n

- ki18n_wrap_ui: error when file doesn't exist
- [Kuit] Revert changes in parseUiMarker()

### Подсистема ввода-вывода KIO

- Add missing renamed event when a destination file already existed
- KFilePlacesModel: On new profile in recent show only recentlyused:/ based entries by default
- Add KFileCustomDialog constructor with a startDir parameter
- Исправлено использование QRegularExpression::wildcardToRegularExpression();
- Возможность обрабатывать приложения, в файле запуска которых содержится «Terminal=True» с правильной поддержкой назначенного MIME-типа (ошибка 410506);
- KOpenWithDialog: Возможность возвращать KService, созданный связанным с MIME-типом;
- Добавлено свойство KIO::DropJobFlag, позволяющее вручную показать меню (ошибка 415917);
- [KOpenWithDialog] Скрываемая группа не будет показана, если скрыты все расположенные в ней параметры (ошибка 415510);
- Revert effective removal of KUrlPixmapProvider from API
- SlaveBase::dispatchLoop: Fix timeout calculation (bug 392768)
- [KDirOperator] Allow renaming files from the context menu (bug 189482)
- Upstream Dolphin's file rename dialog (bug 189482)
- KFilePlaceEditDialog: move logic into isIconEditable()

### Набор компонентов Kirigami

- Clip the flickable parent item (bug 416877)
- Remove header top margin from private ScrollView
- proper size hint for the gridlayout (bug 416860)
- use attached property for isCurrentPage
- Get rid of a couple of warnings
- try to keep the cursor in window when typing in an OverlaySheet
- properly expand fillWidth items in mobile mode
- Add active, link, visited, negative, neutral and positive background colors
- Expose ActionToolBar's overflow button icon name
- Use QQC2 Page as base for Kirigami Page
- Specify where the code is coming from as the URL
- Don't anchor AbstractApplicationHeader blindly
- emit pooled after the properties have been reassigned
- add reused and pooled signals like TableView

### Библиотека поддержки JavaScript KJS

- Abort machine run once a timeout signal has been seen
- Support ** exponentiation operator from ECMAScript 2016
- Added shouldExcept() function that works based on a function

### Модуль получения дополнительных материалов KNewStuff

- Unbreak the KNSQuick::Engine::changedEntries functionality

### Модуль KNotification

- Add new signal for default action activation
- Drop dependency to KF5Codecs by using the new stripRichText function
- Strip richtext on Windows
- Adapt to Qt 5.14 Android changes
- Deprecate raiseWidget
- Port KNotification from KWindowSystem

### Библиотека для работы с контактами KPeople

- Adjust metainfo.yaml to new tier
- Remove legacy plugin loading code

### Модуль создания диаграмм KQuickCharts

- Fix Qt version check
- Register QAbstractItemModel as anonymous type for property assignments
- Hide the line of a line chart if its width is set to 0

### Kross

- addHelpOption already adds by kaboutdata

### Модуль KService

- Support multiple values in XDG_CURRENT_DESKTOP
- Deprecate allowAsDefault
- Make "Default Applications" in mimeapps.list the preferred applications (bug 403499)

### Модуль редактирования текста KTextEditor

- Revert "improve word completion to use highlighting to detect word boundaries" (bug 412502)
- import final breeze icon
- Message-related methods: Use more member-function-pointer-based connect
- DocumentPrivate::postMessage: avoid multiple hash lookups
- fix Drag&amp;copy function (by using Ctrl Key) (bug 413848)
- Проверка использования квадратных значков;
- Исправлен значок Kate в диалоге «О программе» компонента KatePart;
- Встроенные заметки: правильное назначение underMouse();
- Исключено использование старого значка ATM;
- Расширение переменных: добавлена переменная PercentEncoded (ошибка 416509);
- Исправлено аварийное завершение работы при вызове расширения переменной из внешнего приложения;
- KateMessageWidget: убрано неиспользуемое назначение фильтра событий;

### Библиотека KTextWidgets

- Исключена зависимость KWindowSystem;

### Набор библиотек KWallet Framework

- Отменены изменения в readEntryList() для использования QRegExp::Wildcard;
- Исправлено использование QRegularExpression::wildcardToRegularExpression();

### Модуль KWidgetsAddons

- [KMessageWidget] Исправлено вычитание ширины границ;
- [KMessageBox] Выделение текста возможно только мышью (ошибка 416204)
- [KMessageWidget] Использование devicePixelRatioF для пиксельной карты анимации (ошибка 415528)

### Модуль KWindowSystem

- [KWindowShadows] Проверка соединения с X
- Впервые представлено shadows API
- KWindowEffects::markAsDashboard() отмечено в качестве устаревшего;

### Модуль KXMLGUI

- Use KStandardAction convenience method for switchApplicationLanguage
- Allow programLogo property to be a QIcon, too
- Remove ability to report bugs against arbitrary stuff from a static list
- Remove compiler information from bug report dialog
- KMainWindow: fix autoSaveSettings to catch QDockWidgets being shown again
- i18n: Add more semantic context strings
- i18n: Split translations for strings "Translation"

### Модуль Plasma Framework

- Исправлены углы всплывающих подсказок и удалены неиспользуемые аттрибуты цвета
- Удалены задаваемые в коде цвета фонов SVG
- Исправлены размеры и пиксельное выравнивание кнопок-флажков и зависимых переключателей;
- Обновлён внешней вид теней при использовании оформления Breeze;
- [Plasma Quick] Добавлен класс «WaylandIntegration»
- Same behavior for scrollbar as the desktop style
- Make use of KPluginMetaData where we can
- Add edit mode menu item to desktop widget context menu
- Consistency: colored selected buttons
- Port endl to \n Not necessary to flush as QTextStream uses QFile which flush when it's deleted

### Purpose

- Исправлено использование QRegularExpression::wildcardToRegularExpression();

### QQC2StyleBridge

- Remove scrollbar related workarounds from list delegates
- [TabBar] Убрана рамка;
- Add active, link, visited, negative, neutral and positive background colors
- Использование переменной hasTransientTouchInput;
- Всегда выполняется округление x и y;
- Поддержка полос прокрутки для мобильных устройств;
- ScrollView: Полосы прокрутки не перекрывают содержимое;

### Solid

- Add signals for udev events with actions bind and unbind
- Clarify referencing of DeviceInterface (bug 414200)

### Подсветка синтаксиса

- Updates nasm.xml with the latest instructions
- Perl: «say» добавлено в список ключевых слов
- cmake: Fix <code>CMAKE*POLICY**_CMP&amp;lt;N&amp;gt;</code> regex and add special args to <code>get_cmake_property</code>
- Добавлено описание синтаксиса GraphQL

### Информация по безопасности

The released code has been GPG-signed using the following key: pub rsa2048/58D0EE648A48B3BB 2016-09-05 David Faure &lt;faure@kde.org&gt; Primary key fingerprint: 53E6 B47B 45CE A3E0 D5B7 4577 58D0 EE64 8A48 B3BB
