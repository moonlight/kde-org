---
aliases:
- ../announce-applications-19.04.3
changelog: true
date: 2019-07-11
description: KDE Ships Applications 19.04.3.
layout: applications
major_version: '19.04'
release: applications-19.04.3
title: KDE выпускает KDE Applications 19.04.3
version: 19.04.3
---
{{% i18n_date %}}

Today KDE released the third stability update for <a href='../19.04.0'>KDE Applications 19.04</a>. This release contains only bugfixes and translation updates, providing a safe and pleasant update for everyone.

Зафиксировано более 60 исправлений ошибок в электронном секретаре Kontact, архиваторе Ark, приложении для математических вычислений Cantor, музыкальном проигрывателе JuK, приложении для записи дисков K3b, видеоредакторе Kdenlive, клавиатурном тренажёре KTouch, приложении для просмотра изображений Okular, средстве UML-моделирования Umbrello и многих других.

Некоторые из улучшений:

- Исправлено аварийное завершение при выходе из приложений Konqueror и Kontact при использовании библиотеки QtWebEngine версии 5.13
- Исправлено аварийное завершение видеоредактора Kdenlive при вырезании групп в буфер обмена
- The Python importer in Umbrello UML designer now handles parameters with default arguments
