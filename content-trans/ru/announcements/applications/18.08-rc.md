---
aliases:
- ../announce-applications-18.08-rc
date: 2018-08-03
description: KDE Ships Applications 18.08 Release Candidate.
layout: application
release: applications-18.07.90
title: KDE выпускает первую версию-кандидат к выпуску KDE Applications 18.08
---
3 августа 2018 года. Сегодня KDE выпустило версию-кандидат к выпуску KDE Applications. Теперь, когда программные зависимости и функциональность «заморожены», команда KDE сконцентрируется на исправлении ошибок и наведении красоты.

Check the <a href='https://community.kde.org/Applications/18.08_Release_Notes'>community release notes</a> for information on tarballs and known issues. A more complete announcement will be available for the final release

Выпуски KDE Applications 18.08 требуют тщательного тестирования для обеспечения должного уровня качества и удовлетворения пользователей. Очень важную роль в этом играют те, кто используют нашу систему повседневно, ведь сами разработчики просто не могут протестировать все возможные конфигурации. Мы рассчитываем на вашу помощь в поиске ошибок на этом этапе, чтобы мы могли исправить их до выхода финальной версии. По возможности включайтесь в команду — установите эту версию-кандидат в выпуски и <a href='https://bugs.kde.org/'>сообщайте нам</a> о найденных ошибках.
