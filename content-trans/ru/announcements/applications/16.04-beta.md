---
aliases:
- ../announce-applications-16.04-beta
custom_spread_install: true
date: 2016-03-24
description: KDE Ships Applications 16.04 Beta.
layout: application
release: applications-16.03.80
title: Сообщество KDE выпустило бета-версию KDE Applications 16.04
---
24 марта 2016 года. Сегодня KDE выпустило бета-версию KDE Applications. Теперь, когда программные зависимости и функциональность «заморожены», команда KDE сконцентрируется на исправлении ошибок и наведении красоты.

Check the <a href='https://community.kde.org/Applications/16.04_Release_Notes'>community release notes</a> for information on new tarballs, tarballs that are now KF5 based and known issues. A more complete announcement will be available for the final release

With the various applications being based on KDE Frameworks 5, the KDE Applications 16.04 releases need a thorough testing in order to maintain and improve the quality and user experience. Actual users are critical to maintaining high KDE quality, because developers simply cannot test every possible configuration. We're counting on you to help find bugs early so they can be squashed before the final release. Please consider joining the team by installing the beta <a href='https://bugs.kde.org/'>and reporting any bugs</a>.

#### Установка двоичных пакетов бета-версии KDE Applications 16.04

<em>Packages</em>. Some Linux/UNIX OS vendors have kindly provided binary packages of KDE Applications 16.04 Beta (internally 16.03.80) for some versions of their distribution, and in other cases community volunteers have done so. Additional binary packages, as well as updates to the packages now available, may become available over the coming weeks.

<em>Package Locations</em>. For a current list of available binary packages of which the KDE Project has been informed, please visit the <a href='http://community.kde.org/KDE_SC/Binary_Packages'>Community Wiki</a>.

#### Компиляция бета-версии KDE Applications 16.04

The complete source code for KDE Applications 16.04 Beta may be <a href='http://download.kde.org/unstable/applications/16.03.80/src/'>freely downloaded</a>. Instructions on compiling and installing are available from the <a href='/info/applications/applications-16.03.80'>KDE Applications Beta Info Page</a>.
