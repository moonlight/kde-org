---
aliases:
- ../announce-applications-14.12.3
changelog: true
date: '2015-03-03'
description: KDE daje na voljo aplikacije 14.12.3.
layout: application
title: KDE dobavlja aplikacije KDE 14.12.3
version: 14.12.3
---
March 3, 2015. Today KDE released the third stability update for <a href='../14.12.0'>KDE Applications 14.12</a>. This release contains only bugfixes and translation updates, providing a safe and pleasant update for everyone.

With 19 recorded bugfixes it also includes improvements to the anagram game Kanagram, Umbrello UML Modeller, the document viewer Okular and the geometry application Kig.

This release also includes Long Term Support versions of Plasma Workspaces 4.11.17, KDE Development Platform 4.14.6 and the Kontact Suite 4.14.6.
