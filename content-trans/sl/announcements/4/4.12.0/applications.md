---
date: '2013-12-18'
hidden: true
title: Aplikacije KDE 4.12 prinašajo ogromen korak naprej pri upravljanju osebnih
  podatkov in izboljšave vsepovsod
---
Skupnost KDE s ponosom objavlja najnovejše glavne posodobitve KDE aplikacij, ki prinašajo nove zmožnosti in popravke. Ta izdaja označuje velike izboljšave v skladu KDE PIM, ki daje veliko boljše zmogljivosti in številne nove funkcije. Kate poenostavil integracijo Python plugins in dodal začetno Vim-makro podporo in igre in izobraževalne aplikacije prinašajo različne nove funkcije.

{{< figure class="text-center img-size-medium" src="/announcements/4/4.12.0/screenshots/kate.png" width="600px">}}

Najnaprednejši Linuxov urejevalnik grafičnega besedila Kate je ponovno prejel delo na dokončanju kode, tokrat pa uvaja<a href='http://scummos.blogspot.com/2013/10/advanced-code-completion-filtering-in.html'> napredno kodo za ujemanje, rokovanje s kratico in delno ujemanje v razredih</a>. Nova koda bi se na primer ujemala z vtipkanim "QualIdent" s 'QualifiedIdentifier'. Kate dobi tudi <a href='http://dot.kde.org/2013/09/09/kde-commit-digest-18th-august-2013'>začetno podporo makrom Vim</a>. Najboljše od vsega, te izboljšave se pretakajo tudi skozi KDevelop in druge aplikacije z uporabo Kate tehnologije.

{{< figure class="text-center img-size-medium" src="/announcements/4/4.12.0/screenshots/okular.png" width="600px">}}

Pregledovalnik dokumentov Okular <a href='http://tsdgeos.blogspot.com/2013/10/changes-in-okular-printing-for-412.html'>zdaj upošteva robove strojne opreme tiskalnika</a>, ima zvočno in video podporo za epub, boljše iskanje in lahko zdaj obravnava več transformacij, vključno s tistimi iz metapodatkov slike Exif. V orodju UML diagrama Umbrello je zdaj mogoče risati povezave <a href='http://dot.kde.org/2013/09/20/kde-commit-digest-1st-september-2013'></a> z različnimi postavitvami in Umbrello dodaja vizualne povratne informacije, če je <a href='http://dot.kde.org/2013/09/09/kde-commit-digest-25th-august-2013'>gradnik dokumentiran</a>.

Privacy guard KGpg shows more information to users and KWalletManager, the tool to save your passwords, can now <a href='http://www.rusu.info/wp/?p=248'>store them in GPG form</a>. Konsole introduces a new feature: Ctrl-click to directly launch URLs in console output. It can now also <a href='http://martinsandsmark.wordpress.com/2013/11/02/mangonel-1-1-and-more/'>list processes when warning about quit</a>.

{{< figure class="text-center img-size-medium" src="/announcements/4/4.12.0/screenshots/dolphin.png" width="600px">}}

KWebKit adds the ability to <a href='http://dot.kde.org/2013/08/09/kde-commit-digest-7th-july-2013'>automatically scale content to match desktop resolution</a>. File manager Dolphin introduced a number of performance improvements in sorting and showing files, reducing memory usage and speeding things up. KRDC introduced automatic reconnecting in VNC and KDialog now provides access to 'detailedsorry' and 'detailederror' message boxes for more informative console scripts. Kopete updated its OTR plugin and the Jabber protocol has support for XEP-0264: File Transfer Thumbnails. Besides these features the main focus was on cleaning code up and fixing compile warnings.

### Igre in izobraževalna programska oprema

The KDE Games have seen work in various areas. KReversi is <a href='http://tsdgeos.blogspot.ch/2013/10/kreversi-master-is-now-qt-quick-based.html'>now QML and Qt Quick based</a>, making for a prettier and more fluid game experience. KNetWalk has also <a href='http://tsdgeos.blogspot.ch/2013/08/knetwalk-portedx-to-qtquick.html'>been ported</a> with the same benefit as well as the ability to set a grid with custom width and height. Konquest now has a new challenging AI player named 'Becai'.

In the Educational applications there have been some major changes. KTouch <a href='http://blog.sebasgo.net/blog/2013/11/12/what-is-new-for-ktouch-in-kde-sc-4-dot-12/'>introduces custom lesson support and several new courses</a>; KStars has a new, more accurate <a href='http://knro.blogspot.ch/2013/10/demo-of-ekos-alignment-module.html'>alignment module for telescopes</a>, find a <a href='http://www.youtube.com/watch?v=7Dcn5aFI-vA'>youtube video here</a> of the new features. Cantor, which offers an easy and powerful UI for a variety of mathematical backends, now has backends <a href='http://blog.filipesaraiva.info/?p=1171'>for Python2 and Scilab</a>. Read more about the powerful Scilab backend <a href='http://blog.filipesaraiva.info/?p=1159'>here</a>. Marble adds integration with ownCloud (settings are available in Preferences) and adds overlay rendering support. KAlgebra makes it possible to export 3D plots to PDF, giving a great way of sharing your work. Last but not least, many bugs have been fixed in the various KDE Education applications.

### Pošta, koledar in osebni podatki

KDE PIM, KDE's set of applications for handling mail, calendar and other personal information, has seen a lot of work.

Starting with email client KMail, there is now <a href='http://dot.kde.org/2013/10/11/kde-commit-digest-29th-september-2013'>AdBlock support</a> (when HTML is enabled) and improved scam detection support by extending shortened URLs. A new Akonadi Agent named FolderArchiveAgent allows users to archive read emails in specific folders and the GUI of the Send Later functionality has been cleaned up. KMail also benefits from improved Sieve filter support. Sieve allows for server-side filtering of emails and you can now <a href='http://www.aegiap.eu/kdeblog/2013/08/new-in-kdepim-4-12-sieve-script-parsing-22/'>create and modify the filters on the servers</a> and <a href='http://www.aegiap.eu/kdeblog/2013/08/new-in-kdepim-4-12-sieve-12/'>convert existing KMail filters to server filters</a>. KMail's mbox support <a href='http://www.aegiap.eu/kdeblog/2013/08/new-in-kdepim-4-12-mboximporter/'>has also been improved</a>.

In other applications, several changes make work easier and more enjoyable. A new tool is introduced, <a href='http://www.aegiap.eu/kdeblog/2013/11/new-in-kdepim-4-12-kaddressbook/'>the ContactThemeEditor</a>, which allows for creating KAddressBook Grantlee themes for displaying contacts. The addressbook can now also show previews before printing data. KNotes has seen some <a href='http://www.aegiap.eu/kdeblog/2013/11/what-news-in-kdepim-4-12-knotes/'>serious work on solving bugs</a>. Blogging tool Blogilo can now deal with translations and there are a wide variety of fixes and improvements all over the KDE PIM applications.

Benefiting all applications, the underlying KDE PIM data cache has <a href='http://ltinkl.blogspot.ch/2013/11/this-month-october-in-red-hat-kde.html'>seen much work on performance, stability and scalability</a>, fixing <a href='http://www.progdan.cz/2013/10/akonadi-1-10-3-with-postgresql-fix/'>support for PostgreSQL with the latest Qt 4.8.5</a>. And there is a new command line tool, the calendarjanitor which can scan all calendar data for buggy incidences and adds a debug dialog for search. Some very special hugs go to Laurent Montel for the work he is doing on KDE PIM features!

#### Nameščanje aplikacij KDE

Programska oprema KDE, vključno z vsemi knjižnicami in aplikacijami, je prosto na voljo pod odprto-kodnimi licencami. Programska oprema KDE deluje na različnih konfiguracijah strojne opreme in arhitekturah CPE, kot sta ARM in x86, operacijskih sistemih in deluje s kakršnim koli upravljalnikom oken ali namiznim okoljem. Poleg Linuxa in drugih operacijskih sistemov, ki temeljijo na sistemu UNIX, lahko najdete različice večine aplikacij KDE za Microsoft Windows na spletnem mestu <a href='http://windows.kde.org'>Programska oprema KDE za Windows</a> in Apple Mac OS X različice na <a href='http://mac.kde.org/'>Programska oprema KDE na spletnem mestu Mac</a>. Eksperimentalne različice aplikacij KDE za različne mobilne platforme, kot so MeeGo, MS Windows Mobile in Symbian, je mogoče najti na spletu, vendar trenutno niso podprte. <a href='http://plasma-active.org'>Plasma Active</a> je uporabniška izkušnja za širši spekter naprav, kot so tablični računalniki in druga mobilna strojna oprema.

KDE software can be obtained in source and various binary formats from <a href='http://download.kde.org/stable/4.12.0'>download.kde.org</a> and can also be obtained on <a href='/download'>CD-ROM</a> or with any of the <a href='/distributions'>major GNU/Linux and UNIX systems</a> shipping today.

##### Paketi

Some Linux/UNIX OS vendors have kindly provided binary packages of 4.12.0 for some versions of their distribution, and in other cases community volunteers have done so.

##### Mesta paketov

For a current list of available binary packages of which the KDE's Release Team has been informed, please visit the <a href='http://community.kde.org/KDE_SC/Binary_Packages#KDE_SC_4.12.0'>Community Wiki</a>.

The complete source code for 4.12.0 may be <a href='/info/4/4.12.0'>freely downloaded</a>. Instructions on compiling and installing KDE software 4.12.0 are available from the <a href='/info/4/4.12.0#binary'>4.12.0 Info Page</a>.

#### Sistemske zahteve

In order to get the most out of these releases, we recommend to use a recent version of Qt, such as 4.8.4. This is necessary in order to assure a stable and performant experience, as some improvements made to KDE software have actually been done in the underlying Qt framework.

In order to make full use of the capabilities of KDE's software, we also recommend to use the latest graphics drivers for your system, as this can improve the user experience substantially, both in optional functionality, and in overall performance and stability.
