---
aliases:
- ../announce-4.11.5
date: 2014-01-07
description: KDE dobavlja Plasma Workspaces, aplikacije in platformo 4.11.5.
title: KDE objavlja izid 4.11.5
---
7. januar 2014. Danes je KDE izdal posodobitve za svoje delovne prostore, aplikacije in razvojno platformo. Te posodobitve so pete v nizu mesečnih posodobitev stabilizacije serije 4.11. Kot je bilo napovedano ob izdaji, bodo delovni prostori še naprej prejemali posodobitve do avgusta 2015. Ta izdaja vsebuje samo popravke napak in posodobitve prevoda in bo varna in prijetna posodobitev za vse.

Več zabeleženih popravkov napak vključuje izboljšave zbirke za upravljanje osebnih podatkov Kontact, orodja UML Umbrello, pregledovalnika dokumentov Okular, spletnega brskalnika Konqueror, upravitelja datotek Dolphin in drugih. Kalkulator Plasma zdaj lahko obravnava grške črke, Okular pa lahko natisne strani z dolgimi naslovi. Konqueror je dobil boljšo podporo za spletne pisave z odpravo napake.

Bolj popoln <a href='https://bugs.kde.org/buglist.cgi?query_format=advanced&amp;short_desc_type=allwordssubstr&amp;short_desc=&amp;long_desc_type=substring&amp;long_desc=&amp;bug_file_loc_type=allwordssubstr&amp;bug_file_loc=&amp;keywords_type=allwords&amp;keywords=&amp;bug_status=RESOLVED&amp;bug_status=VERIFIED&amp;bug_status=CLOSED&amp;emailtype1=substring&amp;email1=&amp;emailassigned_to2=1&amp;emailreporter2=1&amp;emailcc2=1&amp;emailtype2=substring&amp;email2=&amp;bugidtype=include&amp;bug_id=&amp;votes=&amp;chfieldfrom=2013-06-01&amp;chfieldto=Now&amp;chfield=cf_versionfixedin&amp;chfieldvalue=4.11.5&amp;cmdtype=doit&amp;order=Bug+Number&amp;field0-0-0=noop&amp;type0-0-0=noop&amp;value0-0-0='>seznam</a> sprememb je na voljo v KDE-jevem sledilniku težav. Za podroben seznam sprememb, ki so bile vključene v različico 4.11.5, lahko brskate tudi po dnevnikih Git.

Za prenos izvorne kode ali paketov za namestitev pojdite na stran z <a href='/info/4/4.11.5'>informacijami 4.11.5</a> . Če želite izvedeti več o različicah 4.11 KDE Workspaces, aplikacij in razvojne platforme KDE, si oglejte <a href='/announcements/4.11/'>opombe</a> k izdaji 4.11.

{{< figure class="text-center img-size-medium" src="/announcements/4/4.11.0/screenshots/send-later.png" caption=`Nov potek dela »pošlji pozneje« v Kontactu` width="600px">}}

Programska oprema KDE, vključno z vsemi knjižnicami in aplikacijami, je na voljo brezplačno pod odprto-kodnimi dovoljenji. Programsko opremo KDE lahko dobite kot izvorno kodo in različne binarne formate na <a href='http://download.kde.org/stable/4.11.5/'>download.kde.org</a> ali iz katerega koli od <a href='/distributions'>glavnih sistemov GNU/Linux in UNIX</a> danes.
