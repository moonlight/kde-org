---
aliases:
- ../../kde-frameworks-5.32.0
date: 2017-03-11
layout: framework
libCount: 70
src: /announcements/frameworks/5-tp/KDE_QT.jpg
---
### Baloo

- Permitir etiquetas aniñadas

### Iconas de Breeze

- Engadíronse iconas para Plasma Vault
- Cambiouse o nome das iconas para cartafoles cifrados e descifrados
- Engadir a icona de 22px de torrents
- Engadir iconas da área de notificacións para o xestor de rede (fallo 374672)
- xestión de cores: retirar ligazóns sen definir (fallo 374843)
- Agora system-run é unha acción ata ≤ 32 px e unha icona de aplicación en 48 px (fallo 375970)

### Módulos adicionais de CMake

- Detectar inotify
- Reverter «Marcar automaticamente as clases funcións puramente virtuais como /Abstract/.»

### KActivitiesStats

- Permitir planificar con antelación e definir a orde dun elemento que aínda non estea na lista

### KArchive

- Corrixir unha fuga de memoria potencial á que apuntaba «limitedDev»

### KCMUtils

- Corrixiuse unha quebra potencial nos KCM de QML cando cambia a paleta das aplicacións

### KConfig

- KConfig: deixar de exportar e instalar KConfigBackend

### KConfigWidgets

- KColorScheme: usar o esquema da aplicación de maneira predeterminada se KColorSchemeManager o define (fallo 373764)
- KConfigDialogManager: obter o sinal de cambio de metaObject ou dunha propiedade especial
- Corrixir a comprobación de erros de KCModule::setAuthAction

### KCoreAddons

- Excluír (6) do recoñecemento de emoticonas
- KDirWatch: corrixir unha fuga de memoria na destrución

### Compatibilidade coa versión 4 de KDELibs

- Corrixir un fallo en kfiledialog.cpp que causa unha quebra cando se usan trebellos nativos

### KDocTools

- meinproc5: ligar aos ficheiros, non á biblioteca (fallo 377406)
- Retirar a biblioteca estática KF5::XsltKde
- Exportar unha biblioteca compartida axeitada para KDocTools
- Migrar a rexistros con categoría e limpar as inclusións
- Engadir unha función para extraer un único ficheiro
- Facer que a construción falle cedo se xmllint non está dispoñíbel (fallo 376246)

### KFileMetaData

- Novo mantedor para kfilemetadata
- [ExtractorCollection] Usar a herdanza de tipos MIME para devolver complementos
- engadir unha nova propiedade DiscNumber para ficheiros de son de álbums de varios discos

### KIO

- KCM de cookies: desactivar o botón de «Eliminar» cando non hai un elemento actual
- kio_help: Usar a nova biblioteca compartira exportada por KDocTools
- kpac: desinfectar os URL antes de pasalos a FindProxyForURL (corrección de seguranza)
- Importar o escravo de entrada e saída remotas de plasma-workspace
- kio_trash: permitir cambiar o nome de ficheiros e directorios raíz
- PreviewJob: Retirar de maneira predeterminada o tamaño máximo dos ficheiros locais
- DropJob: permitir engadir accións de aplicación nun menú aberto
- ThumbCreator: marcar DrawFrame como obsoleto, tal e como se discutiu en https://git.reviewboard.kde.org/r/129921/

### KNotification

- Engadir compatibilidade con portais de Flatpak
- Enviar desktopfilename como parte dos consellos de notifyByPopup
- [KStatusNotifierItem] Restaurar as xanelas minimizadas como normais

### Infraestrutura KPackage

- Completar a funcionalidade de apertura de paquetes comprimidos

### KTextEditor

- Lembrar o tipo de ficheiro definido polo usuario entre sesións
- Restabelecer o tipo de ficheiro ao abrir un URL
- Engadiuse un obtedor para o valor de configuración «word-count» (número de palabras)
- Conversión consistente entre cursor e coordenadas
- Actualizar o tipo de ficheiro ao gardar só se a ruta cambia
- Compatibilidade con ficheiros de configuración de EditorConfig (detalles: http://editorconfig.org/)
- Engadir FindEditorConfig a ktexteditor
- Corrección: a acción emmetToggleComment non funciona (fallo 375159)
- Usar maiúscula só ao principio das oracións nos textos das etiquetas dos campos de edición
- Inverter o significado de :split e :vsplit para coincidir coas accións de vi e Kate
- Usar o log2() de C++11 en vez de log() ou log(2)
- KateSaveConfigTab: poñer un espazador detrás do último grupo do separador Avanzadas, non dentro
- KateSaveConfigTab: retirar unha marxe incorrecta arredor do contido do separador «Avanzada»
- Páxina subordinada de configuración dos bordos: corrixir a posición da caixa despregábel da visibilidade da barra de desprazamento

### KWidgetsAddons

- KToolTipWidget: agochar o consello en enterEvent se hideDelay é cero
- Corrixir que KEditListWidget perdese o foco ao premer os botóns
- Engadir a descomposición de sílabas de han-geul en letras de han-geul
- KMessageWidget: corrixir o comportamento de chamadas que se sobrepoñen de animatedShow e animatedHide

### KXMLGUI

- Non usar as teclas de KConfig con barras invertidas

### NetworkManagerQt

- Sincronizar as introspeccións e os ficheiros xerados con NM 1.6.0
- Xestor: Corrixir que se emitise deviceAdded dúas veces cando se reinicie NM

### Infraestrutura de Plasma

- definir os consellos predeterminados cando repr non exporta Layout.* (fallo 377153)
- permitir definir expanded=false para un contedor
- [Menú] Mellorar a corrección de espazo dispoñíbel en openRelative
- mover a lóxica de setImagePath a updateFrameData() (fallo 376754)
- IconItem: Engadir a propiedade roundToIconSize
- [SliderStyle] Permitir fornecer un elemento «hint-handle-size»
- Conectar todas as conexións a unha acción en QMenuItem::setAction
- [ConfigView] Respectar as restricións do módulo de control KIOSK
- Corrixir que se desactivase a animación de voltas cando o indicador de actividade non ten opacidade
- [FrameSvgItemMargins] Non actualizar ao chamarse repaintNeeded
- Iconas para o miniaplicativo de Plasma Vault
- Migrar AppearAnimation e DisappearAnimation a Animators
- Aliñar o extremo inferior co superior de visualParent no caso de TopPosedLeftAlignedPopup
- [ConfigModel] Emitir dataChanged cando unha ConfigCategory cambia
- [ScrollViewStyle] Avaliar a propiedade frameVisible
- [Button Styles] Usar Layout.fillHeight en vez de parent.height nunha Layout (fallo 375911)
- [ContainmentInterface] Aliñar tamén o menú contextual do contedor ao panel

### Prison

- Corrixir a versión mínima de Qt

### Solid

- Agora os disquetes aparecen como «Disquete» en vez de como «Dispositivo extraíbel de 0 B»

### Realce da sintaxe

- Engadir máis palabras clave. Desactivar a corrección ortográfica de palabras clave
- Engadir máis palabras clave
- Engadir a extensión de ficheiro *.RHTML ao realce de Ruby on Rails (fallo 375266)
- Actualizar o salientado de sintaxe de SCSS e CSS (fallo 376005)
- realce de less: corrixir que os comentarios dunha única liña inicien rexións
- Realce de LaTeX: corrixir o ambiente de alignat (fallo 373286)

### Información de seguranza

The released code has been GPG-signed using the following key: pub rsa2048/58D0EE648A48B3BB 2016-09-05 David Faure &lt;faure@kde.org&gt; Primary key fingerprint: 53E6 B47B 45CE A3E0 D5B7 4577 58D0 EE64 8A48 B3BB

Pode comentar e compartir ideas sobre esta versión na sección de comentarios do <a href='https://dot.kde.org/2014/07/07/kde-frameworks-5-makes-kde-software-more-accessible-all-qt-developers'>artigo do Dot</a>.
