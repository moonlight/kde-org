---
aliases:
- ../../kde-frameworks-5.52.0
date: 2018-11-10
layout: framework
libCount: 70
src: /announcements/frameworks/5-tp/KDE_QT.jpg
---
### Baloo

- Corrixir a construción con BUILD_QCH=TRUE
- Usar fileNameTerms e xAttrTerms de verdade
- [Balooshow] Evitar acceso fóra dos límites ao acceder a datos corruptos de base de datos
- [Extractor] Non comprobar QFile::exists para un URL baleiro
- [Planificador] Usar unha marca para facer un seguimento de cando o executor non está activo
- [Extractor] Xestionar correctamente os documentos onde o tipo MIME non debería indexarse
- [Planificador] Corrixir un uso incorrecto da marca de tempo obsoleta QFileInfo::created() (fallo 397549)
- [Extractor] Facer o extractor resistente a quebras (fallo 375131)
- Pasar a FileIndexerConfig como const aos indexadores individuais
- [Configuración] Retirar a compatibilidade con configuración de KDE 4, deixar de escribir ficheiros de configuración arbitrarios
- [Extractor] Mellorar a depuración pola liña de ordes, pasar a saída de erro estándar
- [Planificador] Aproveitar fileinfo de FilteredDirIterator
- [Planificador] Aproveitar o tipo MIME de UnindexedFileIterator no indexador
- [Planificador] Retirar a variábel superflua m_extractorIdle
- Realizar comprobacións de ficheiros non indexados e entradas caducadas do índice no inicio
- [balooctl] Imprimir o estado actual e a indexación de ficheiro ao comezar o monitor (fallo 364858)
- [balooctl] Monitorizar tamén os seus cambios de estado
- [balooctl] Corrixir a orde «index» para ficheiros xa indexados pero movidos (fallo 397242)

### BluezQt

- Engadir a xeración de cabeceira das API de Media e MediaEndpoint

### Iconas de Breeze

- Cambiar as iconas do xestor de paquetes por emblemas
- Engadir de novo a icona de ligazón monocroma como acción
- Mellorar o contraste, a lexibilidade e a consistencia dos emblemas (fallo 399968)
- Permitir o tipo MIME «new» para ficheiros .deb (fallo 399421)

### Módulos adicionais de CMake

- ECMAddQch: help doxygen by predefining more Q*DECL** macros
- API de Python: Permitir usar as rutas do sistema para o directorio de instalación de Python
- API de Python: Retirar INSTALL_DIR_SUFFIX de ecm_generate_python_binding
- Engadir a posibilidade de o desinfectante fuzzer

### KCMUtils

- permitir kcms con varias páxinas

### KConfig

- Engadir un mecanismo para notificar a outros clientes de cambios de configuración por D-Bus
- Exportar o método de obtención KConfig::addConfigSources

### KConfigWidgets

- Permitir que KHelpCenter abra as páxinas correctas da axuda de KDE cando KHelpClient se invoca cunha áncora

### KCrash

- KCrash: corrixir unha quebra cando se usa nunha aplicación sen QCoreApplication

### KDeclarative

- facer push e pop parte da API de ConfigModule

### KDED

- Retirar unha mensaxe inútil de «Non se atopou X-KDE-DBus-ServiceName»

### KDesignerPlugin

- Facer referencia ao produto «KF5» en vez de a «KDE» nos metadatos do trebello

### KDocTools

- Documentación da API: engadir documentación mínima ao espazo de nomes de KDocTools para que Doxygen o inclúa
- Crear un ficheiro QCH coa documentación da API, opcionalmente, usando ECMAddQCH
- Agardar a construír docbookl10nhelper para construír as nosas páxinas de manual
- Usar o intérprete de Perl indicado en vez de confiar en PATH

### KFileMetaData

- [ExtractorCollection] Usar só o complemento de extractor que coincida mellor
- [KFileMetaData] Engadir un extractor para XML e SVG xenéricos
- [KFileMetaData] Engadir un asistente para os metadatos de Dublin Core codificados en XML
- permitir ler etiquetas de ID3 de ficheiros aiff e wav
- engadir máis etiquetas para os metadatos de asf
- extraer as etiquetas de ape de ficheiros ape e wavpack
- fornecer unha lista de tipos MIME compatíbeis para embeddedimagedata
- comparar con QLatin1String e harmonizar a xestión de todos os tipos
- Non quebrar ante datos de exiv2 incorrectos (fallo 375131)
- epubextractor: Engadir a propiedade ReleaseYear
- reorganizar o código de taglibextractor en funcións específicas para o tipo de metadatos
- engadir ficheiros WMA e etiquetas de ASF como tipos MIME compatíbeis
- usar un extractor propio para probar taglibwriter
- engadir un sufixo de cadea ara probar datos e usar para probas de Unicode de taglibwriter
- retirar unha comprobación en tempo de compilación para a versión de taglib
- estender a cobertura de probas a todos os tipos MIME que taglibextractor permite
- Usar a variábel co texto xa obtido en vez de obtelo de novo

### KGlobalAccel

- Corrixir as notificacións de cambio de disposición de teclado (fallo 269403)

### KHolidays

- Engadir un ficheiro de vacacións para Bahrain
- Facer que KHolidays tamén funcione como biblioteca estática

### KIconThemes

- Engadir un QIconEnginePlugin para permitir a deserialización de QIcon (fallo 399989)
- [KIconLoader] Substituír o QImageReader asignado na morea por un asignado na rima
- [KIconLoader] Axustar os bordos dos emblemas segundo o tamaño da icona
- Centrar as iconas de maneira axeitada se non caben (fallo 396990)

### KIO

- Non intentar usar protocolos SSL «menos seguros» como reserva
- [KSambaShare] Retirar o / ao final da ruta para compartir
- [kdirlistertest] Agardar un pouco máis a que remate o listador
- Mostrar unha mensaxe de desculpa se o ficheiro non é local
- kio_help: Corrixir unha quebra en QCoreApplication ao acceder a help:// (fallo 399709)
- Evitar agardar polas accións de usuario cando o impedimento de roubo do foco de KWin está en alto ou extremo
- [KNewFileMenu] Non abrir un QFile baleiro
- Engadíronse iconas que faltaban no código do panel de lugares de KIO
- Retirar os punteiros crus a KFileItem de KCoreDirListerCache
- Engadir a opción «Montar» ao menú contextual do dispositivo desmontado en Lugares
- Engadir unha entrada de «Propiedades» no menú contextual do panel de lugares
- Corrixir o aviso «o comportamento das expansións de macro que producen «defined» non está definido»

### Kirigami

- Corrixir os elementos que faltaban nas construcións estáticas
- funcionalidade básica de páxinas ocultas
- cargar as iconas dos temas de iconas correctos
- (moitas outras correccións)

### KNewStuff

- Mensaxes de erro máis útiles

### KNotification

- Corrixiuse unha quebra causada por unha xestión incorrecta do tempo de vida das notificacións de son baseadas en canberra (fallo 398695)

### KParts

- Corrixir que «Cancelar» non se xestionase no obsoleto BrowserRun::askEmbedOrSave
- Migrar a unha variante non obsoleta de KRun::runUrl
- Migrar KIO::Job::ui() a KJob::uiDelegate()

### KWayland

- Engadir o protocolo de escritorio virtual de KWayland
- Evitar que se elimine a fonte de datos antes de procesar o evento de recepción de oferta de datos (fallo 400311)
- [servidor] Respectar a rexión de entrada de superficies subordinadas no foco de superficie de punteiro
- [xdgshell] Engadir operadores de marca de axuste de restrición de posicionador

### KWidgetsAddons

- Documentación da API: corrixir a nota «Since» de KPageWidgetItem::isHeaderVisible
- Engadir a nova propiedade «headerVisible»

### KWindowSystem

- Non comparar iteradores devoltos de dúas copias distintas devoltas

### KXMLGUI

- Coller 1…n KMainWindows en kRestoreMainWindows

### NetworkManagerQt

- Engadir opcións de IPv4 que faltaban
- Engadir a opción vxlan

### Infraestrutura de Plasma

- desfacer o cambio de tamaño das iconas en móbiles
- Permitir etiquetas fáciles de lembrar
- Retirar a opción PLASMA_NO_KIO
- Buscar correctamente temas de reserva

### Purpose

- Definir a marca Dialog para JobDialog

### Solid

- [solid-hardware5] Listar a icona nos detalles de dispositivo
- [UDisks2] Apagar a unidade ao retirar se se permite (fallo 270808)

### Sonnet

- Corrixir a detección de idioma

### Sindicación

- Engadir un ficheiro README.md que faltaba (necesitábano varios script)

### Realce da sintaxe

- Realce da sintaxe de ficheiros de CLIST de z/OS
- Crear un novo ficheiro de realce de sintaxe para Job Control Language (JCL)
- Retirar o modo de open dunha versión de Qt demasiado nova
- aumentar a versión e corrixir a versión necesaria de Kate á versión actual da infraestrutura
- regra de palabra clave: permitir a inclusión de palabras clave doutra linguaxe ou ficheiro
- Non usar corrección ortográfica para Metamath salvo nos comentarios
- CMake: Engadir variábeis e propiedades relacionadas con XCode introducidas en 3.13
- CMake: introducir novas funcionalidades da futura versión 3.13

### Información de seguranza

The released code has been GPG-signed using the following key: pub rsa2048/58D0EE648A48B3BB 2016-09-05 David Faure &lt;faure@kde.org&gt; Primary key fingerprint: 53E6 B47B 45CE A3E0 D5B7 4577 58D0 EE64 8A48 B3BB

Pode comentar e compartir ideas sobre esta versión na sección de comentarios do <a href='https://dot.kde.org/2014/07/07/kde-frameworks-5-makes-kde-software-more-accessible-all-qt-developers'>artigo do Dot</a>.
