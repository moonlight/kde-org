---
date: 2013-08-14
hidden: true
title: A versión 4.11 da plataforma de KDE fornece un mellor rendemento
---
A versión 4 da plataforma de KDE leva sen admitir novas funcionalidades desde a versión 4.9. Esta versión, polo tanto, só inclúe varias solucións de erros e melloras de rendemento.

The Nepomuk semantic storage and search engine received massive performance improvements, such as a set of read optimizations that make reading data up to six times faster. Indexing has become smarter, being split in two stages. The first stage retrieves general information (such as file type and name) immediately; additional information like media tags, author information, etc. is extracted in a second, somewhat slower stage. Metadata display on newly-created or freshly-downloaded content is now much faster. In addition, the Nepomuk developers improved the backup and restore system. Last but not least, Nepomuk can now also index a variety of document formats including ODF and docx.

{{< figure class="text-center img-size-medium" src="/announcements/4/4.11.0/screenshots/dolphin.png" caption=`Funcionalidades semánticas en acción en Dolphin` width="600px">}}

O formato de almacenamento optimizado e o indexador de correo electrónico rescrito de Nepomuk requiren indexar de novo algúns dos contidos do disco duro. Polo tanto, a nova indexación consumirá unha cantidade significativa de rendemento de computación durante un certo tempo, segundo a cantidade de contido que haxa que indexar. Tras o primeiro inicio de sesión executarase unha conversión automática da base de datos de Nepomuk.

Tamén se solucionaron máis erros secundarios, que <a href='https://projects.kde.org/projects/kde/kdelibs/repository/revisions?rev=KDE%2F4.11'>poden consultarse no historial de Git</a>.

#### Instalar a plataforma de desenvolvemento de KDE

Os programas de KDE, incluídas todas as súas bibliotecas e as súas aplicacións, están dispoñíbeis de maneira gratuíta baixo licenzas de código aberto. Os programas de KDE funcionan en varias configuracións de soporte físico e arquitecturas de CPU como ARM e x86, sistemas operativos e funcionan con calquera xestor de xanelas ou contorno de escritorio. Ademais de Linux e outros sistemas operativos baseados en UNIX pode atopar versións para Microsoft Windows da meirande parte das aplicacións de KDE no sitio web de <a href='http://windows.kde.org'>programas de KDE para Windows</a> e versións para o Mac OS X de Apple no <a href='http://mac.kde.org/'>sitio web de programas de KDE para Mac</a>. En Internet poden atoparse construcións experimentais de aplicacións de KDE para varias plataformas móbiles como MeeGo, MS Windows Mobile e Symbian pero non están mantidas. <a href='http://plasma-active.org'>Plasma Active</a> é unha experiencia de usuario para un maior abanico de dispositivos, como computadores de tableta e outros soportes físicos móbiles.

KDE software can be obtained in source and various binary formats from <a href='http://download.kde.org/stable/4.11.0'>download.kde.org</a> and can also be obtained on <a href='/download'>CD-ROM</a> or with any of the <a href='/distributions'>major GNU/Linux and UNIX systems</a> shipping today.

##### Paquetes

Some Linux/UNIX OS vendors have kindly provided binary packages of 4.11.0 for some versions of their distribution, and in other cases community volunteers have done so. <br />

##### Lugares dos paquetes

For a current list of available binary packages of which the KDE's Release Team has been informed, please visit the <a href='http://community.kde.org/KDE_SC/Binary_Packages#KDE_4.11.0'>Community Wiki</a>.

The complete source code for 4.11.0 may be <a href='/info/4/4.11.0'>freely downloaded</a>. Instructions on compiling and installing KDE software 4.11.0 are available from the <a href='/info/4/4.11.0#binary'>4.11.0 Info Page</a>.

#### Requisitos do sistema

In order to get the most out of these releases, we recommend to use a recent version of Qt, such as 4.8.4. This is necessary in order to assure a stable and performant experience, as some improvements made to KDE software have actually been done in the underlying Qt framework.<br /> In order to make full use of the capabilities of KDE's software, we also recommend to use the latest graphics drivers for your system, as this can improve the user experience substantially, both in optional functionality, and in overall performance and stability.

## Hoxe tamén se anunciaron:

## <a href="../plasma"><img src="/announcements/4/4.11.0/images/plasma.png" class="app-icon" alt="The KDE Plasma Workspaces 4.11" width="64" height="64" /> Plasma Workspaces 4.11 Continues to Refine User Experience</a>

En preparación para mantemento a longo prazo, os espazos de traballo de Plasma inclúen melloras adicionais en funcionalidades básicas cunha barra de tarefas máis suave, un trebello de batería máis intelixente e un mesturador de son mellorado. A introdución de KScreen trae consigo xestión intelixente de varios monitores nos espazos de traballo, e melloras de rendemento a gran escala en combinación con pequenos axustes de facilidade de uso que conseguen unha mellor experiencia xeral.

## <a href="../applications"><img src="/announcements/4/4.11.0/images/applications.png" class="app-icon" alt="The KDE Applications 4.11"/> KDE Applications 4.11 Bring Huge Step Forward in Personal Information Management and Improvements All Over</a>

Esta versión marca melloras masivas no software de xestión de información persoal de KDE, que mellora moito o rendemento e inclúe moitas novas funcionalidades. Kate mellora a produtividade de desenvolvedores de Python e JavaScript con novos complementos, Dolphin volveuse máis rápido e as aplicacións educativas traen consigo novas funcionalidades.
