---
aliases:
- ../announce-applications-15.12.0
changelog: true
date: 2015-12-16
description: KDE Ships Applications 15.12.
layout: application
release: applications-15.12.0
title: KDE publica a versión 15.12.0 das aplicacións de KDE
version: 15.12.0
---
16 de decembro de 2015. Hoxe KDE publicou a versión 15.12 das aplicacións de KDE.

En KDE alégranos anunciar a publicación da versión 15.12 das aplicacións de KDE, a actualización de decembro de 2015 das aplicacións de KDE. Esta versión trae unha nova aplicación e o engadido de funcionalidades e correccións de fallos en varias áreas de aplicacións existentes. O equipo loita por traer sempre a mellor calidade ao seu escritorio e estas aplicacións, así que contamos con vostede para comentarnos a súa experiencia.

In this release, we've updated a whole host of applications to use the new <a href='https://dot.kde.org/2013/09/25/frameworks-5'>KDE Frameworks 5</a>, including <a href='https://edu.kde.org/applications/all/artikulate/'>Artikulate</a>, <a href='https://www.kde.org/applications/system/krfb/'>Krfb</a>, <a href='https://www.kde.org/applications/system/ksystemlog/'>KSystemLog</a>, <a href='https://games.kde.org/game.php?game=klickety'>Klickety</a> and more of the KDE Games, apart from the KDE Image Plugin Interface and its support libraries. This brings the total number of applications that use KDE Frameworks 5 to 126.

### Un engadido espectacular

Tras 14 anos de ser parte de KDE, KSnapshot retirouse e substituíuse por unha nova aplicación de capturas de pantalla: Spectacle.

Con novas funcionalidades e unha interface de usuario completamente nova, Spectable fai sacar capturas de pantalla todo o fácil e simple que pode ser. Ademais do que xa permitía KSnapshot, agora con Spectacle pode sacar capturas de pantalla compostas de menús emerxentes xunto coas xanelas ás que pertencen, ou sacar capturas de toda a pantalla (ou da xanela activa) sen sequera iniciar Spectacle, usando simplemente os atallos de teclado Maiús+ImprPant e Meta+ImprPant respectivamente.

Tamén optimizamos de maneira significativa o tempo de inicio para minimizar en gran medida o tempo que pasa desde que se inicia a aplicación ata que se captura a imaxe.

### Todo ben pulido

Neste ciclo pulíronse de maneira significativa moitas das aplicacións, ademais de correccións de estabilidade e fallos.

<a href='https://userbase.kde.org/Kdenlive'>Kdenlive</a>, the non-linear video editor, has seen major fixes to its User Interface. You can now copy and paste items on your timeline, and easily toggle transparency in a given track. The icon colours automatically adjust to the main UI theme, making them easier to see.

{{<figure src="/announcements/applications/15.12.0/ark1512.png" class="text-center" width="600px" caption=`Agora Ark pode mostrar comentarios de ZIP`>}}

<a href='https://www.kde.org/applications/utilities/ark/'>Ark</a>, the archive manager, can now display comments embedded in ZIP and RAR archives. We've improved support for Unicode characters in file names in ZIP archives, and you can now detect corrupt archives and recover as much data as possible from them. You can also open archived files in their default application, and we've fixed drag-and-drop to the desktop as well previews for XML files.

### Moito xogar e pouco traballar

{{<figure src="/announcements/applications/15.12.0/ksudoku1512.png" class="text-center" width="600px" caption=`Nova pantalla de benvida de KSudoku (nun Mac OS X)`>}}

Os desenvolvedores dos xogos de KDE estiveron traballando duramente durante os últimos meses para optimizar os nosos xogos e fornecer así unha experiencia máis agradábel e rica, e temos moitas cousas nesta área para o goce dos nosos usuarios.

In <a href='https://www.kde.org/applications/games/kgoldrunner/'>KGoldrunner</a>, we've added two new sets of levels, one allowing digging while falling and one not. We've added solutions for several existing sets of levels. For an added challenge, we now disallow digging while falling in some older sets of levels.

In <a href='https://www.kde.org/applications/games/ksudoku/'>KSudoku</a>, you can now print Mathdoku and Killer Sudoku puzzles. The new multi-column layout in KSudoku's Welcome Screen makes it easier to see more of the many puzzle types available, and the columns adjust themselves automatically as the window is resized. We've done the same to Palapeli, making it easier to see more of your jigsaw puzzle collection at once.

We've also included stability fixes for games like KNavalBattle, Klickety, <a href='https://www.kde.org/applications/games/kshisen/'>KShisen</a> and <a href='https://www.kde.org/applications/games/ktuberling/'>KTuberling</a>, and the overall experience is now greatly improved. KTuberling, Klickety and KNavalBattle have also been updated to use the new KDE Frameworks 5.

### Correccións importantes

Don't you simply hate it when your favorite application crashes at the most inconvinient time? We do too, and to remedy that we've worked very hard at fixing lots of bugs for you, but probably we've left some sneak, so don't forget to <a href='https://bugs.kde.org'>report them</a>!

In our file manager <a href='https://www.kde.org/applications/system/dolphin/'>Dolphin</a>, we've included various fixes for stability and some fixes to make scrolling smoother. In <a href='https://www.kde.org/applications/education/kanagram/'>Kanagram</a>, we've fixed a bothersome issue with white text on white backgrounds. In <a href='https://www.kde.org/applications/utilities/kate/'>Kate</a>, we've attempted to fix a crash that was occurring on shutdown, in addition to cleaning up the UI and adding in a new cache cleaner.

The <a href='https://userbase.kde.org/Kontact'>Kontact Suite</a> has seen tons of added features, big fixes and performance optimisations. In fact, there's been so much development this cycle that we've bumped the version number to 5.1. The team is hard at work, and is looking forward to all your feedback.

### Avanzando

Como parte do esforzo para modernizar os nosos produtos retiramos algúns aplicativos dos aplicativos de KDE e xa nos os publicamos como parte da versión 15.12 das aplicacións de KDE

Retiramos 4 aplicacións da publicación: Amor, KTux, KSnapshot e SuperKaramba. Como se indicou antes, a KSnapshot substituíuno Spectacle, e Plasma é en esencia un substituto para SuperKaramba como motor de trebellos. Retiramos os protectores de pantalla independentes porque o bloque de pantalla se xestiona dun xeito moi distinto nos escritorios modernos.

Tamén retiramos 3 paquetes de arte (kde-base-artwork, kde-wallpapers and kdeartwork); o seu contido había tempo que non cambiaba.

### Historial completo de cambios
