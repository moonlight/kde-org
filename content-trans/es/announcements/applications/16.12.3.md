---
aliases:
- ../announce-applications-16.12.3
changelog: true
date: 2017-03-09
description: KDE lanza las Aplicaciones de KDE 16.12.3
layout: application
title: KDE lanza las Aplicaciones de KDE 16.12.3
version: 16.12.3
---
Hoy, 9 de marzo de 2017, KDE ha lanzado la tercera actualización de estabilización para las <a href='../16.12.0'>Aplicaciones KDE 16.12</a>. Esta versión solo contiene soluciones de errores y actualizaciones de traducciones, por lo que será una actualización agradable y segura para todo el mundo.

Entre las más de 20 correcciones de errores registradas, se incluyen mejoras en Kdepim, Ark, Filelight, Gwenview, Kate, Kdenlive y Okular, entre otras aplicaciones.

También se incluye la versión de la Plataforma de desarrollo de KDE 4.14.30 que contará con asistencia a largo plazo.
