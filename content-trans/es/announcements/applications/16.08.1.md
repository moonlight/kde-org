---
aliases:
- ../announce-applications-16.08.1
changelog: true
date: 2016-09-08
description: KDE lanza las Aplicaciones de KDE 16.08.1
layout: application
title: KDE lanza las Aplicaciones de KDE 16.08.1
version: 16.08.1
---
Hoy, 8 de septiembre de 2016, KDE ha lanzado la primera actualización de estabilización para las <a href='../16.08.0'>Aplicaciones de KDE 16.08</a>. Esta versión solo contiene soluciones de errores y actualizaciones de traducciones, por lo que será una actualización agradable y segura para todo el mundo.

Entre las más de 45 correcciones de errores registradas se incluyen mejoras en kdepim, kate, kdenlive, konsole, marble, kajongg, kopete y umbrello, entre otras aplicaciones.

También se incluye la versión de la Plataforma de desarrollo de KDE 4.14.24 que contará con asistencia a largo plazo.
