---
date: 2013-08-14
hidden: true
title: Los espacios de trabajo Plasma de KDE 4.11 siguen perfeccionando la experiencia
  de usuario
---
{{< figure class="text-center img-size-medium" src="/announcements/4/4.11.0/screenshots/empty-desktop.png" caption=`Espacios de trabajo Plasma de KDE 4.11` width="600px" >}}

En la versión 4.11 de los espacios de trabajo de Plasma, la barra de tareas (uno de los elementos gráficos de Plasma más utilizados) <a href='http://blogs.kde.org/2013/07/29/kde-plasma-desktop-411s-new-task-manager'> se ha portado a QtQuick</a>. La nueva barra de tareas, aunque mantiene el aspecto y la funcionalidad de su antecesora, muestra un comportamiento más coherente y fluido. También se han resuelto varios errores que venían arrastrándose desde hacía mucho tiempo. El elemento gráfico de la batería (que antes permitía ajustar el brillo de la pantalla), ahora también permite ajustar el brillo del teclado y puede gestionar las baterías de varios dispositivos periféricos, como el ratón o el teclado inalámbrico. Muestra la carga de la batería para cada dispositivo y avisa cuando alguna alcanza el nivel bajo. El menú de arranque ahora muestra las aplicaciones instaladas en los últimos días. Por último, pero no por ello menos importante, los mensajes emergentes ahora cuentan con un botón de configuración mediante el cual se puede cambiar las preferencias para ese tipo de notificación en particular.

{{< figure class="text-center img-size-medium" src="/announcements/4/4.11.0/screenshots/notifications.png" caption=`Mejor gestión de las notificaciones` width="600px" >}}

KMix, el mezclador de sonido de KDE, ha experimentado una mejora significativa en el rendimiento y en la estabilidad, así como <a href='http://kmix5.wordpress.com/2013/07/26/kmix-mission-statement-2013/'>la implementación del control del reproductor de medios</a> basada en la norma MPRIS2.

{{< figure class="text-center img-size-medium" src="/announcements/4/4.11.0/screenshots/battery-applet.png" caption=`El nuevo diseño de la miniaplicación de batería en acción` width="600px" >}}

## El gestor de ventanas y de composición KWin

Nuestro gestor de ventanas, KWin, vuelve a contar con actualizaciones significativas, lo que ha supuesto una evolución desde una tecnología anterior y la incorporación del protocolo de comunicaciones «XCB». Esto ha dado como resultado una gestión de ventanas más suave y rápida. También se ha incluido la capacidad de admitir OpenGL 3.1 and OpenGL ES 3.0. Esta versión también incorpora, por primera vez y de manera experimental, la capacidad de admitir Wayland, el sucesor de X11. Esto permite utilizar KWin con X11 sobre una pila de Wayland. Para obtener más información sobre cómo utilizar este modo experimental, consulte <a href='http://blog.martin-graesslin.com/blog/2013/06/starting-a-full-kde-plasma-session-in-wayland/'>esta publicación</a>. La interfaz de scripts de KWin incluye grandes mejoras, como la configuración de la interfaz de usuario, nuevas animaciones y efectos gráficos y muchas otras mejoras más pequeñas. Esta versión incluye una mejor gestión de varias pantallas (por ejemplo una opción de bordes brillantes para las «esquinas activas»), una vista de mosaico mejorada y más rápida (con áreas de mosaico configurables) y la considerable cantidad habitual de optimizaciones y soluciones de errores. Consulte <a href='http://blog.martin-graesslin.com/blog/2013/06/what-we-did-in-kwin-4-11/'>este enlace</a> y <a href='http://blog.martin-graesslin.com/blog/2013/06/new-kwin-scripting-feature-in-4-11/'>este otro</a> para obtener más información.

## Gestión de monitores y accesos rápidos de Internet

La configuración del monitor en las preferencias del sistema se ha <a href='http://www.afiestas.org/kscreen-1-0-released/'>sustituido por la nueva herramienta de KScreen</a>. KScreen dota a los espacios de trabajo de Plasma de una gestión más inteligente de varios monitores, configurando automáticamente las nuevas pantallas y recordando las preferencias para los monitores configurados de forma manual. Incorpora una interfaz intuitiva y visual y admite la reorganización de los monitores simplemente arrastrando y soltando.

{{< figure class="text-center img-size-medium" src="/announcements/4/4.11.0/screenshots/kscreen.png" caption=`La nueva gestión de monitores de KScreen` width="600px" >}}

Web Shortcuts, la manera más fácil de encontrar rápidamente lo que está buscando en la web, se ha limpiado y mejorado. Se ha incluido el uso de conexiones con cifrado seguro (TLS/SSL), se han añadido nuevos accesos rápidos de Internet y se han eliminado algunos accesos rápidos obsoletos. El proceso de añadir sus propios accesos rápidos de Internet también se ha mejorado. Puede encontrar más detalles <a href='https://plus.google.com/108470973614497915471/posts/9DUX8C9HXwD'>aquí</a>.

Esta versión supone el final de los espacios de trabajo de Plasma 1, que formaba parte de la serie de funcionalidades KDE SC 4. Para facilitar la transición hacia la nueva generación, esta versión tendrá asistencia durante al menos dos años. Ahora, el desarrollo de mejoras se centra en el paso a los espacios de trabajo de Plasma 2. Las mejoras de rendimiento y las soluciones de errores ahora se centrarán en la serie 4.11.

#### Instalación de Plasma

El software de KDE, incluidas todas sus bibliotecas y aplicaciones, está libremente disponible bajo licencias de Código Abierto. El software de KDE funciona sobre diversas configuraciones de hardware y arquitecturas de CPU, como ARM y x86, sistemas operativos y funciona con cualquier tipo de gestor de ventanas o entorno de escritorio. Además de Linux y otros sistemas operativos basados en UNIX, puede encontrar versiones para Microsoft Windows de la mayoría de las aplicaciones de KDE en el sitio web <a href='http://windows.kde.org'>KDE software on Windows</a> y versiones para Apple Mac OS X en el sitio web <a href='http://mac.kde.org/'>KDE software on Mac</a>. En la web puede encontrar compilaciones experimentales de aplicaciones de KDE para diversas plataformas móviles como MeeGo, MS Windows Mobile y Symbian, aunque en la actualidad no se utilizan. <a href='http://plasma-active.org'>Plasma Active</a> es una experiencia de usuario para una amplia variedad de dispositivos, como tabletas y otro tipo de hardware móvil.

Puede obtener el software de KDE en forma de código fuente y distintos formatos binarios en <a href='http://download.kde.org/stable/4.11.0'>download.kde.org</a>, y también en <a href='/download'>CD-ROM</a> o con cualquiera de los <a href='/distributions'>principales sistemas GNU/Linux y UNIX</a> de la actualidad.

##### Paquetes

Algunos distribuidores del SO Linux/UNIX tienen la gentileza de proporcionar paquetes binarios de 4.11.0 para algunas versiones de sus distribuciones, y en otros casos han sido voluntarios de la comunidad los que lo han hecho posible. <br />

##### Ubicación de los paquetes

Para una lista actual de los paquetes binarios disponibles de los que el Equipo de Lanzamiento de KDE ha sido notificado, visite la <a href='http://community.kde.org/KDE_SC/Binary_Packages#KDE_4.11.0'>Wiki de la Comunidad</a>.

La totalidad del código fuente de %[1] se puede <a href='/info/4/4.11.0'>descargar libremente</a>. Dispone de instrucciones sobre cómo compilar e instalar el software de KDE 4.11.0 en la <a href='/info/4/4.11.0#binary'>Página de información sobre 4.11.0</a>.

#### Requisitos del sistema

Para obtener lo máximo de estos lanzamientos, le recomendamos que use una versión reciente de Qt, como la 4.8.4. Esto es necesario para asegurar una experiencia estable y con rendimiento, ya que algunas de las mejoras realizadas en el software de KDE están hechas realmente en la infraestructura subyacente Qt.<br />Para hacer un uso completo de las funcionalidades del software de KDE, también le recomendamos que use en su sistema los últimos controladores gráficos, ya que pueden mejorar sustancialmente la experiencia del usuario, tanto en las funcionalidades opcionales como en el rendimiento y la estabilidad general.

## También se han anunciado hoy:

## <a href="../applications"><img src="/announcements/4/4.11.0/images/applications.png" class="app-icon" alt="The KDE Applications 4.11"/> Las aplicaciones de KDE 4.11 proporcionan un enorme paso adelante en la gestión de información personal y mejoras en general</a>

Esta versión supone enormes mejoras para KDE PIM, las cuales proporcionan un mejor rendimiento y muchas nuevas funcionalidades. Kate mejora el rendimiento de los desarrolladores de Python y Javascript con nuevos complementos; Dolphin es más rápido y las aplicaciones educativas incorporan varias funcionalidades nuevas.

## <a href="../platform"><img src="/announcements/4/4.11.0/images/platform.png" class="app-icon" alt="The KDE Development Platform 4.11"/> KDE Platform 4.11 proporciona mejor rendimiento</a>

Esta versión de la plataforma de KDE 4.11 continúa centrándose en la estabilidad. Se implementan nuevas funcionalidades de cara a la futura versión de KDE Frameworks 5.0, pero para la versión estable hemos conseguido incluir optimizaciones para nuestro sistema Nepomuk.
