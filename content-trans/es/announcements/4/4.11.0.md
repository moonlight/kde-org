---
aliases:
- ../4.11
custom_about: true
custom_contact: true
date: 2013-08-14
description: KDE lanza los Espacios de trabajo Plasma, las Aplicaciones y la Plataforma
  4.11.
title: La compilación de software KDE 4.11
---
{{<figure src="/announcements/4/4.11.0/screenshots/plasma-4.11.png" class="text-center" caption=`Los espacios de trabajo Plasma de KDE 4.11` >}} <br />

14 de agosto de 2013. La Comunidad de KDE se complace en anunciar la última actualización importante en los espacios de trabajo de Plasma, en las aplicaciones y en la plataforma de desarrollo, la cual supone la implementación de nuevas funcionalidades y soluciones de errores, así como la preparación de la plataforma para una mayor evolución. Los espacios de trabajo de Plasma 4.11 contará con asistencia a largo plazo a medida que el equipo se centra en la transición técnica a Frameworks 5. Por tanto, esta es la última vez que los espacios de trabajo, las aplicaciones y la plataforma de desarrollo se presentan conjuntamente bajo el mismo número de versión.<br />

Esta versión está dedicada a la memoria de <a href='http://en.wikipedia.org/wiki/Atul_Chitnis'>Atul 'toolz' Chitnis</a>, un gran defensor del software abierto y libre en la India. Atul dirigió las conferencias de Linux Bangalore y FOSS.IN desde 2001 y ambas se convirtieron en acontecimientos emblemáticos del panorama FOSS de la India. KDE India nació en la primera FOSS.IN, en diciembre de 2005. Muchos colaboradores indios de KDE empezaron a serlo durante dichos acontecimientos. El KDE Project Day fue siempre un gran éxito en FOSS.IN solo gracias al impulso de Atul. Atul nos dejó el 3 de junio, tras luchar una larga batalla contra el cáncer. Descanse en paz. Le agradecemos su contribución para un mundo mejor.

Estas versiones están traducidas a 54 idiomas; esperamos que se añadan más idiomas en próximas versiones menores de KEA para solución de errores. El equipo de documentación ha actualizad 91 manuales de aplicación para esta versión.

## <a href="./plasma"><img src="/announcements/4/4.11.0/images/plasma.png" class="app-icon" alt="The KDE Plasma Workspaces 4.11" width="64" height="64" /> Los espacios de trabajo de Plasma 4.11 continúan refinando la experiencia de usuario</a>

Preparándose para un mantenimiento a largo plazo, los espacios de trabajo de Plasma proporcionan más mejoras a la funcionalidad básica con una barra de tareas más suave, un elemento gráfico más inteligente para la batería y un mezclador de sonido mejorado. La introducción de KScreen aporta a los espacios de trabajo una gestión inteligente de varios monitores y las mejoras de rendimiento a gran escala combinadas con pequeños ajustes de ergonomía hacen que la experiencia general sea más agradable.

## <a href="./applications"><img src="/announcements/4/4.11.0/images/applications.png" class="app-icon" alt="The KDE Applications 4.11"/> Las aplicaciones de KDE 4.11 proporcionan un enorme paso adelante en la gestión de información personal y mejoras en general</a>

Esta versión supone enormes mejoras para KDE PIM, las cuales proporcionan un mejor rendimiento y muchas nuevas funcionalidades. Kate mejora el rendimiento de los desarrolladores de Python y Javascript con nuevos complementos; Dolphin es más rápido y las aplicaciones educativas incorporan varias funcionalidades nuevas.

## <a href="./platform"><img src="/announcements/4/4.11.0/images/platform.png" class="app-icon" alt="The KDE Development Platform 4.11"/> KDE Platform 4.11 proporciona mejor rendimiento</a>

Esta versión de la plataforma de KDE 4.11 continúa centrándose en la estabilidad. Se implementan nuevas funcionalidades de cara a la futura versión de KDE Frameworks 5.0, pero para la versión estable hemos conseguido incluir optimizaciones para nuestro sistema Nepomuk.

<br />
A la hora de realizar la actualización, consulte las <a href='http://community.kde.org/KDE_SC/4.11_Release_Notes'>notas de la versión</a>.<br />

## Difunda la noticia y vea lo que está ocurriendo: etiquete como «KDE»

KDE anima a todo el mundo a difundir la noticia en la Web social. Envíe artículos a sitios de noticias y use canales como delicious, digg, reddit, twitter o identi.ca. Envíe capturas de pantallas a servicios como Facebook, Flickr, ipernity o Picasa, y publíquelas en los grupos adecuados. Cree screencasts y envíelos a YouTube, Blip.tv o Vimeo. Etiquete los artículos y el material enviado con «KDE». Esto hará que sea más fácil de encontrar y proporcionará al Equipo de Promoción de KDE un modo de analizar la cobertura del lanzamiento del software de KDE 4.11.

## Fiestas del lanzamiento

Como viene siendo habitual, los miembros de la comunidad KDE organizan fiestas del lanzamiento por todo el mundo. Algunas de ellas ya se han programado y otras lo harán en breve. Consulte la <a href='http://community.kde.org/Promo/Events/Release_Parties/4.11'>lista de fiestas aquí</a>. ¡Todo el mundo está invitado a participar! En ellas habrá una combinación de compañía interesante y conversaciones inspiradoras, así como comida y bebida. Son una gran oportunidad para saber más sobre lo que está ocurriendo en KDE, implicarse en el proyecto o solo para conocer a otros usuarios y colaboradores.

Animamos a todo el mundo a organizar sus propias fiestas. ¡Son divertidas de organizar y abiertas a todos! Consulte los <a href='http://community.kde.org/Promo/Events/Release_Parties'>consejos sobre cómo organizar una de estas fiestas</a>.

## Sobre estos anuncios de lanzamiento

Estos anuncios de lanzamiento han sido preparados por Jos Poortvliet, Sebastian Kügler, Markus Slopianka, Burkhard Lück, Valorie Zimmerman, Maarten De Meyer, Frank Reininghaus, Michael Pyne, Martin Gräßli y otros miembros del Equipo de Promoción de KDE y del resto de la comunidad de KDE. Cubren el resumen de los cambios más importantes realizados en el software de KDE durante los últimos seis meses.

#### En apoyo de KDE

<a href="http://jointhegame.kde.org/"> <img src="/announcements/4/4.9.0/images/join-the-game.png" class="img-fluid float-left mr-3" alt="Únase al juego"/> </a>

El nuevo <a href='http://jointhegame.kde.org/'>programa de miembros de apoyo</a> a KDE e.V. está ahora abierto. Por 25&euro; cada tres meses puede asegurar que la comunidad internacional de KDE continúa creciendo para hacer Software Libre de calidad mundial.
