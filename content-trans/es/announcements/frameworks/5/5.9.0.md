---
aliases:
- ../../kde-frameworks-5.9.0
date: '2015-04-10'
layout: framework
libCount: 60
src: /announcements/frameworks/5-tp/KDE_QT.jpg
---
Nuevo módulo: «ModemManagerQt» (envoltorio de Qt para la API de «ModemManager»). De forma alternativa, actualizar a Plasma-NM 5.3 Beta cuando se actualiza a ModemManagerQt 5.9.0.

### KActivities

- Se ha implementado la posibilidad de olvidar un recurso.
- Soluciones de errores de compilación
- Se ha añadido un complemento para registrar eventos para las notificaciones de «KRecentDocument».

### KArchive

- Respetar la preferencia «KZip::extraField» también al escribir entradas de la cabecera central.
- Eliminar dos afirmaciones erróneas que ocurrían cuando se llenaba el disco, error 343214.

### KBookmarks

- Se ha corregido la compilación con Qt 5.5.

### KCMUtils

- Usar el nuevo sistema de complementos basado en JSON. Los módulos KCM se buscan en «kcms/». Por ahora, todavía es necesario instalar un archivo de escritorio en «kservices5/» por motivos de compatibilidad.
- Cargar y empaquetar la versión de solo QML de los módulos «kcm», si es posible.

### KConfig

- Se ha solucionado la afirmación cuando se usa KSharedConfig en un destructor de objetos global.
- kconfig_compiler: Permitir el uso de «CategoryLoggingName» en los archivos «*.kcfgc» para generar llamadas a «qCDebug(category)».

### KI18n

- Precargar el catálogo global de Qt al usar «i18n()».

### KIconThemes

- «KIconDialog» se puede mostrar ahora usando los métodos estándares «show()» y «exec()» de QDialog.
- Se ha corregido KIconEngine::paint para que pueda manejar distintas «devicePixelRatios».

### KIO

- Activar que «KPropertiesDialog» también muestre información del espacio libre en los sistemas de archivos remotos (por ejemplo, «smb»).
- Se ha corregido KUrlNavigator con pixmaps de alta DPI.
- Hacer que «KFileItemDelegate» maneje «devicePixelRatio» no predeterminados en las animaciones.

### KItemModels

- KRecursiveFilterProxyModel: Se ha trabajado para que emita las señales correctas en el momento adecuado.
- KDescendantsProxyModel: Manejar los movimientos de los que informa el modelo fuente.
- KDescendantsProxyModel: Se ha corregido el comportamiento cuando se realiza una selección durante un reinicio.
- KDescendantsProxyModel: Permitir compilación y el uso de «KSelectionProxyModel» de QML.

### KConfigWidgets

- Propagar el código de error a la interfaz «JobView» de DBus.

### KNotifications

- Se ha añadido una versión de «event()» que no necesita icono y usa uno predeterminado.
- Se ha añadido una versión de «event()» que usa «StandardEvent eventId» y «QString iconName».

### KPeople

- Permitir la extensión de metadatos de acciones usando tipos predefinidos.
- Se ha corregido la actualización incorrecta del modelo tras eliminar un contacto en «Person».

### KPty

- Exponer al exterior si «KPty» se ha compilado con la biblioteca «utempter».

### KTextEditor

- Se ha añadido el archivo de resaltado de sintaxis para «kdesrc-buildrc».
- Sintaxis: se ha añadido soporte para literales enteros binarios en el archivo de resaltado de sintaxis de PHP.

### KWidgetsAddons

- Suavizar la animación de «KMessageWidget» con proporciones de píxeles de dispositivo altas.

### KWindowSystem

- Se ha añadido una implementación de Wayland simulada para «KWindowSystemPrivate».
- «KWindowSystem::icon» con «NETWinInfo» no se enlazaba a la plataforma X11.

### KXmlGui

- Preservar el dominio de traducción al fusionar archivos «.rc».
- Se ha corregido la advertencia en tiempo de ejecución «QWidget::setWindowModified»: El título de la ventana no contiene un comodín '[*]'.

### KXmlRpcClient

- Instalación de traducciones

### Plasma framework

- Se han corregido las ayudas emergentes extraviadas cuando su propietario temporal desaparece o se queda vacío.
- Corregir que «TabBar» no se representa correctamente al principio, lo que se puede observar, por ejemplo, en Kickoff.
- Las transiciones de «PageStack» usan ahora «animadores» para unas animaciones más suaves.
- Las transiciones de «TabGroup» usan ahora «animadores» para unas animaciones más suaves.
- Hacer que Svg,FrameSvg funcione con QT_DEVICE_PIXELRATIO

### Solid

- Refrescar las propiedades de la batería tras reanudar la sesión

### Cambios en el sistema de compilación

- Los módulos adicionales de CMake (EMC) ahora se versionan como KDE Frameworks, por tanto, la versión actual es 5.9 aunque la anterior era 1.8.
- Se han corregido muchas infraestructuras para que se puedan utilizar sin tener que buscar sus dependencias privadas. Por ejemplo, las aplicaciones que buscan una infraestructura solo necesitan sus dependencias públicas, no las privadas.
- Permitir que la configuración de SHARE_INSTALL_DIR gestione mejor las distribuciones de varios archivos

### Integración con Frameworks

- Solución de un posible fallo cuando se destruía un QSystemTrayIcon (ocasionado, por ejemplo, por Trojita), error 343976
- Corregir los diálogos de archivo modal nativo en QML, error 334963

Puede comentar esta versión y compartir sus ideas en la sección de comentarios de <a href='https://dot.kde.org/2014/07/07/kde-frameworks-5-makes-kde-software-more-accessible-all-qt-developers'>el artículo de dot</a>.
