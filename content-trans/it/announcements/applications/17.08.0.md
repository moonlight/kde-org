---
aliases:
- ../announce-applications-17.08.0
changelog: true
date: 2017-08-17
description: KDE rilascia KDE Applications 17.08.0
layout: application
title: KDE rilascia KDE Applications 17.08.0
version: 17.08.0
---
17 agosto 2017. KDE Applications 17.08 è qui. Abbiamo lavorato per rendere sia le applicazioni che le librerie sottostanti più stabili e più facili da usare. Stirando le rughe e ascoltando le vostre segnalazioni, abbiamo reso la suite di applicazioni KDE meno soggetta a problemi e molto più amichevoli. Goditi le tue nuove applicazioni!

### Più conversioni a KDE Frameworks 5

Siamo lieti che le seguenti applicazioni basate su kdelibs4 siano ora basate su KDE Frameworks 5: kmag, kmousetool, kgoldrunner, kigo, konquest, kreversi, ksnakeduel, kspaceduel, ksudoku, kubrick, lskat e umbrello. Grazie al duro lavoro degli sviluppatori che hanno offerto volontariamente il loro tempo e lavoro per fare in modo che ciò possa accadere.

### Novità in KDE Applications 17.08

#### Dolphin

Gli sviluppatori di delfini riferiscono che Dolphin ora mostra il l'«ora di eliminazione» nel cestino e mostra l'«ora di creazione» se il sistema operativo lo supporta come su BSD.

#### KIO-Extras

Kio-Extras ora fornisce un supporto migliore per le condivisioni Samba.

#### KAlgebra

Gli sviluppatori di KAlgebra hanno lavorato per migliorare la loro interfaccia Kirigami sul desktop e hanno implementato il completamento del codice.

#### Kontact

- In KMailtransport, gli sviluppatori hanno riattivato il supporto di trasporto di akonadi, hanno creato il supporto delle estensioni e ricreato il supporto di trasporto di posta sendmail.
- In SieveEditor, molti bug negli script di creazione automatica sono stati corretti e chiusi. Insieme alla correzione di bug generici, è stato aggiunto un editor di linea dell'editor espressioni regolari.
- In KMail la possibilità di utilizzare un editor esterno è stata ricreata come estensione.
- Akonadi-import-wizard ora ha il «convertitore universale» come estensione, in modo che gli sviluppatori possano creare facilmente nuovi convertitori.
- Le applicazioni ora dipendono da Qt 5.7. Gli sviluppatori hanno risolto molti errori di compilazione su Windows. Tutto kdepim non compila ancora su Windows, ma gli sviluppatori hanno fatto grandi progressi. Per iniziare, gli sviluppatori hanno creato una ricetta magica per questo. Sono stati corretti molti bug per modernizzare il codice (C++11). Supporto Wayland su Qt 5.9. Kdepim-runtime aggiunge una risorsa facebook.

#### Kdenlive

In Kdenlive, la squadra ha risolto l'effetto «Congelamento» non funzionante. Nelle versioni recenti, era impossibile cambiare il fotogramma congelato per l'effetto Congelamento. Ora è consentito una scorciatoia da tastiera per la caratteristica del fotogramma estratto. Ora l'utente può salvare schermate della sequenza temporale con una scorciatoia da tastiera e viene suggerito un nome in base al numero del fotogramma <a href='https://bugs.kde.org/show_bug.cgi?id=381325'>https://bugs.kde.org/show_bug.cgi?id=381325</a>. Correzione delle luma di transizione scaricate che non venivano visualizzate nell'interfaccia: <a href='https://bugs.kde.org/show_bug.cgi?id=382451'>https://bugs.kde.org/show_bug.cgi?id=382451</a>. Risolve il problema dei clic audio (per ora, richiede la compilazione della dipendenza MLT da Git fino a una versione MLT): <a href='https://bugs.kde.org/show_bug.cgi?id=371849'>https://bugs .kde.org/show_bug.cgi? id=371849</a>.

#### Krfb

Gli sviluppatori hanno terminato la conversione dell'estensione X11 a Qt5 e krfb funziona di nuovo utilizzando un motore X11 molto più veloce dell'estensione Qt. Esiste una nuova pagina delle impostazioni, che consente all'utente di modificare l'estensione framebuffer preferita.

#### Konsole

Konsole ora consente uno scorrimento all'indietro illimitato per estendere il vecchio limite di 2 GB (32bit). Ora Konsole consente agli utenti di inserire qualsiasi posizione per archiviare i file di scorrimento. Inoltre, è stata corretta una regressione, Konsole può ancora una volta consentire a KonsolePart di invocare la finestra Gestisci profilo.

#### KAppTemplate

In KAppTemplate ora esiste un'opzione per installare nuovi modelli dal filesystem. Più modelli sono stati rimossi da KAppTemplate e invece integrati in prodotti correlati; Il modello di estensione di ktexteditor e il modello kpartsapp (ora convertito a Qt5/KF5) sono diventati parte di KDE Frameworks KTextEditor e KParts dal 5.37.0. Queste modifiche dovrebbero semplificare la creazione di modelli nelle applicazioni KDE.

### Ricerca e risoluzione degli errori

Gli oltre 80 errori corretti in applicazioni tra cui la suite Kontact, Ark, Dolphin, K3b, Kdenlive, KGpg, Konsole e altre!

### Elenco completo dei cambiamenti
