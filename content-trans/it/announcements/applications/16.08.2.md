---
aliases:
- ../announce-applications-16.08.2
changelog: true
date: 2016-10-13
description: KDE rilascia KDE Applications 16.08.2
layout: application
title: KDE rilascia KDE Applications 16.08.2
version: 16.08.2
---
13 ottobre 2016. Oggi KDE ha rilasciato il primo aggiornamento di stabilizzazione per <a href='../16.08.0'>KDE Applications 16.08</a>. Questo rilascio contiene solo correzioni di errori e aggiornamenti delle traduzioni, e costituisce un aggiornamento sicuro e gradevole per tutti.

Gli oltre 30 errori corretti includono, tra gli altri, miglioramenti a kdepim, Ark, Dolphin, Kgpg, Kolourpaint e Okular.

Questo rilascio include inoltre le versioni con supporto a lungo termine di KDE Development Platform 4.14.25.
