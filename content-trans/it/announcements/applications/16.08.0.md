---
aliases:
- ../announce-applications-16.08.0
changelog: true
date: 2016-08-18
description: KDE rilascia KDE Applications 16.08.0
layout: application
title: KDE rilascia KDE Applications 16.08.0
version: 16.08.0
---
18 agosto 2016. Oggi KDE introduce le applicazioni KDE 16.08, con una serie impressionante di aggiornamenti in termini di maggiore semplicità di accesso, l'introduzione di funzionalità molto utili ed eliminando  alcuni problemi minori, portando KDE Applications un passo più vicino a offrirti la configurazione perfetta per il tuo dispositivo.

<a href='https://www.kde.org/applications/graphics/kolourpaint/'>Kolourpaint</a>, <a href='https://www.kde.org/applications/development/cervisia/'>Cervisia</a> e KDiskFree ora sono stati portati su KDE Frameworks 5 e non vediamo l'ora di ricevere riscontri da parte vostra e idee sulle nuove funzionalità introdotte con questa versione.

Nel continuo sforzo di separare le librerie della suite Kontact per renderle più facili da usare per terze parti, il tarball kdepimlibs è stato diviso in akonadi-contacts, akonadi-mime e akonadi-notes.

Abbiamo abbandonato i seguenti pacchetti: kdegraphics-strigi-analyzer, kdenetwork-strigi-analyzers, kdesdk-strigi-analyzers, libkdeedu e mplayerthumbs. Questo ci aiuterà a concentrarci sul resto del codice.

### Tenersi in Kontact

<a href='https://userbase.kde.org/Kontact'>La suite Kontact</a> ha ottenuto la solita tornata di pulizia, correzioni di bug e ottimizzazioni in questa versione. Notevole è l'uso di QtWebEngine in vari componenti, che consente di utilizzare un motore di rendering HTML più moderno. Abbiamo anche migliorato il supporto VCard4 e aggiunto nuove estensioni che possono avvertire se alcune condizioni vengono soddisfatte quando si invia un messaggio di posta, ad es. verificando che desideri consentire l'invio di messaggi con una determinata identità o verificare se invii un messaggio come testo in chiaro, ecc.

### Nuova versione di Marble

<a href='https://marble.kde.org/'>Marble</a> 2.0 fa parte delle KDE Applications 16.08 e include oltre 450 modifiche al codice tra cui miglioramenti nella navigazione, rendering e un rendering vettoriale sperimentale dei dati di OpenStreetMap.

### Più archiviazione

<a href='https://www.kde.org/applications/utilities/ark/'>Ark</a> ora può estrarre i file AppImage e .xar e verificare l'integrità degli archivi zip, 7z e rar. Può anche aggiungere/modificare i commenti negli archivi rar

### Miglioramenti al terminale

<a href='https://www.kde.org/applications/system/konsole/'>Konsole</a> è stato oggetto di miglioramenti riguardanti le opzioni di resa dei caratteri e il supporto di accessibilità.

### E altro!

<a href='https://kate-editor.org'>Kate</a> ora possiede schede spostabili. <a href='https://kate-editor.org/2016/06/15/kates-tabbar-gets-movable-tabs/'>Altre informazioni...</a>

<a href='https://www.kde.org/applications/education/kgeography/'>KGeography</a> ha aggiunto province e regioni del Burkina Faso.

### Controllo aggressivo dei parassiti

Oltre 120 errori corretti in applicazioni tra cui la suite Kontact, Ark, Cantor, Dolphin, KCalc, Kdenlive, e altre.

### Elenco completo dei cambiamenti
