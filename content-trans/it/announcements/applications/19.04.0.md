---
aliases:
- ../announce-applications-19.04.0
changelog: true
date: 2019-04-18
description: KDE rilascia Applications 19.04.
layout: application
release: applications-19.04.0
title: KDE rilascia KDE Applications 19.04.0
version: 19.04.0
version_number: 19.04.0
version_text: '19.04'
---
{{%youtube id="1keUASEvIvE"%}}

{{% i18n_date %}}

La comunità KDE è lieta di annunciare il rilascio di KDE Applications 19.04.

La nostra comunità lavora continuamente per migliorare il software incluso nelle nostre serie di KDE Application. Oltre alle nuove funzionalità, abbiamo migliorato la grafica, l'usabilità e la stabilità di tutti le nostre utilità, giochi e strumenti per la creatività. Il nostro obiettivo è semplificare la vita rendendo il software KDE più facile e divertente da usare. Speriamo che le migliorie e le correzioni degli errori contenuti in 19.04 siano di tuo gradimento.

## Novità in KDE Applications 19.04

Sono stati risolti più di 150 errori. Tali correzioni re-implementano funzionalità disabilitate, normalizzano scorciatoie e risolvono blocchi delle applicazioni, per rendere KDE Applications più facile da utilizzare e essere più produttivi.

### Gestione dei file

{{<figure src="/announcements/applications/19.04.0/app1904_dolphin01.png" width="600px" >}}

<a href='https://www.kde.org/applications/system/dolphin/'>Dolphin</a> è il gestore dei file di KDE. Si collega anche ai servizi di rete, come SSH, FTP e i server Samba, e viene distribuito con strumenti avanzati per trovare e organizzare i tuoi dati.

Nuove funzionalità:

+ Abbiamo esteso il supporto per le miniature, e ora Dolphin è in grado di visualizzare le miniature per diversi tipi di nuovi file: <a href='https://phabricator.kde.org/D18768'>Microsoft Office</a>, <a href='https://phabricator.kde.org/D18738'>.epub e .fb2 per eBook</a>, <a href='https://bugs.kde.org/show_bug.cgi?id=246050'>Blender</a> e <a href='https://phabricator.kde.org/D19679'>PCX</a>. In aggiunta, le miniature per i file di testo ora mostrano l'<a href='https://phabricator.kde.org/D19432'>evidenziazione della sintassi</a> per il testo all'interno della miniatura. </li>
+ Ora puoi scegliere <a href='https://bugs.kde.org/show_bug.cgi?id=312834'>quale parte del pannello diviso chiudere</a> quando fai clic sul pulsante «Chiudi la vista». </li>
+ Questa versione di Dolphin introduce il <a href='https://bugs.kde.org/show_bug.cgi?id=403690'>posizionamento intelligente delle schede</a>. Quando apri una cartella in una nuova scheda, questa sarà posizionata immediatamente alla destra di quella attiva anziché alla fine della barra delle schede. </li>
+ L'<a href='https://phabricator.kde.org/D16872'>attribuzione di etichette agli elementi</a> è molto più pratica, dato che ora le etichette possono essere aggiunte o rimosse tramite il menu contestuale. </li>
+ Abbiamo <a href='https://phabricator.kde.org/D18697'>migliorato i parametri per l'ordinamento predefinito</a> di alcune cartelle utilizzate comunemente. Per impostazione predefinita, la cartella Scaricati è ora ordinata per data col raggruppamento abilitato, e la vista Documenti recenti (raggiungibile navigando a recentdocuments:/) è ordinata per data con vista elenco. </li>

Le correzioni di errori includono:

+ Quando utilizzi una versione moderna del protocollo SMB, puoi ora <a href='https://phabricator.kde.org/D16299'>utilizzare le condivisioni Samba</a> per i sistemi Mac e Linux. </li>
+ Quando alcuni elementi vengono nascosti, il <a href='https://bugs.kde.org/show_bug.cgi?id=399430'>riordinamento degli elementi nel pannello Risorse</a> funziona di nuovo correttamente. </li>
+ Dopo aver aperto una nuova scheda in Dolphin, essa ora riceve automaticamente il <a href='https://bugs.kde.org/show_bug.cgi?id=401899'>fuoco della tastiera</a>. </li>
+ Dolphin ora ti avvisa se tenti di uscire quando hai il <a href='https://bugs.kde.org/show_bug.cgi?id=304816'>pannello del terminale aperto</a> e al suo interno un programma in esecuzione. </li>
+ Abbiamo risolto molti problemi inerenti la memoria, migliorando le prestazioni complessive di Dolphin.</li>

Il <a href='https://cgit.kde.org/audiocd-kio.git/'>KIO AudioCD</a> consente alle altre applicazioni KDE di leggere l'audio dai CD e convertirlo automaticamente in altri formati.

+ Il KIO AudioCD ora supporta l'estrazione nel formato <a href='https://bugs.kde.org/show_bug.cgi?id=313768'>Opus</a>. </li>
+ Abbiamo reso il <a href='https://bugs.kde.org/show_bug.cgi?id=400849'>testo informativo del CD</a> davvero trasparente per la visualizzazione. </li>

### Editing video

{{<figure src="/announcements/applications/19.04.0/app1904_kdenlive.png" width="600px" >}}

Questa versione è una pietra miliare per l'editor video di KDE. Il nucleo del codice di <a href='https://kde.org/applications/multimedia/kdenlive/'>Kdenlive</a> è stato ampiamente riscritto e più del 60%% delle sue parti interne sono state modificate, migliorando la sua architettura complessiva.

I miglioramenti includono:

+ La linea temporale è stata riscritta in modo da utilizzare QML.
+ Quando inserisci una clip nella linea temporale, l'audio e il video sono sempre mostrati in tracce separate.
+ La linea temporale ora supporta la navigazione tramite tastiera: clip, composizioni e fotogrammi chiave possono essere spostati tramite tastiera. Inoltre, l'altezza delle tracce è regolabile.
+ In questa versione di Kdenlive, la registrazione audio nella traccia viene fornita con una nuova funzionalità <a href='https://bugs.kde.org/show_bug.cgi?id=367676'>doppiaggio</a>.
+ Abbiamo migliorato i comandi copia/incolla: ora funziona tra diverse finestre del progetto. È stata migliorata anche la gestione delle clip rappresentative, dato che le clip ora possono essere eliminate singolarmente.
+ La versione 19.04 vede il ritorno del supporto per gli schermi dei monitor BlackMagic esterni, e sono presenti anche guide preimpostate per i nuovi monitor.
+ Abbiamo migliorato la gestione dei fotogrammi chiave, dandole un aspetto e un flusso di lavoro più coerente. È stato migliorato anche il gestore dei titoli posizionando meglio i pulsanti per l'allineamento, aggiungendo guide configurabili e colori di sfondo, e rendendo visibili elementi mancanti.
+ Abbiamo corretto un errore della linea temporale che posizionava male o faceva sparire clip coinvolte nello spostamento di un gruppo di clip.
+ Abbiamo corretto un errore per le immagini JPG, che rendeva le immagini come schermata bianca in Windows. Sempre in Windows, abbiamo corretto un errore che interessava la cattura dello schermo.
+ Oltre a tutto quello elencato sopra, sono state fatte tante piccole migliorie che rendono più semplice e coinvolgente l'uso di Kdenlive.

### Ufficio

{{<figure src="/announcements/applications/19.04.0/app1904_okular.png" width="600px" >}}

<a href='https://kde.org/applications/graphics/okular/'>Okular</a> è il visore polivalente di documenti di KDE. Ideale per la leggere e annotare PDF, può aprire anche file ODF (come quelli di LibreOffice e OpenOffice), ebook pubblicati come file ePub, i file per fumetti più comuni, file PostScript e molti altri.

I miglioramenti includono:

+ Per fare in modo che i tuoi documenti si adattino sempre bene, sono state aggiunte <a href='https://bugs.kde.org/show_bug.cgi?id=348172'>opzioni di riscalatura</a> alla finestra di dialogo Stampa di Okular.
+ Okular ora supporta la visualizzazione e la verifica delle <a href='https://cgit.kde.org/okular.git/commit/?id=a234a902dcfff249d8f1d41dfbc257496c81d84e'>firme digitali</a> nei file PDF.
+ Grazie all'integrazione migliorata incrociata con altre applicazioni, Okular ora supporta la modifica di documenti LaTeX in <a href='https://bugs.kde.org/show_bug.cgi?id=404120'>TexStudio</a>.
+ Il supporto migliorato per la <a href='https://phabricator.kde.org/D18118'>navigazione tattile</a> significa che potrai spostarti avanti e indietro utilizzando uno schermo tattile durante la modalità Presentazione.
+ Gli utenti che preferiscono la manipolazione dei documenti tramite riga di comando, potranno eseguire <a href='https://bugs.kde.org/show_bug.cgi?id=362038'>ricerche del testo</a> intelligenti con una nuova opzione a riga di comando che permette l'apertura di un documento e l'evidenziazione di tutte le occorrenze del testo specificato.
+ Okular ora visualizza collegamenti che occupano più di una riga nei <a href='https://bugs.kde.org/show_bug.cgi?id=403247'>documenti Markdown</a>.
+ Gli <a href='https://bugs.kde.org/show_bug.cgi?id=397768'>strumenti di ritaglio</a> ora possiedono nuove icone più elaborate.

{{<figure src="/announcements/applications/19.04.0/app1904_kmail.png" width="600px" >}}

<a href='https://kde.org/applications/internet/kmail/'>KMail</a> è il client per la posta elettronica di KDE che protegge la privacy. Parte della <a href='https://kde.org/applications/office/kontact/'>suite groupware Kontact</a>, KMail supporta tutti i sistemi di posta elettronica e consente di organizzare i messaggi in una casella di posta virtuale condivisa o in account separati (a tua scelta). Supporta tutti i tipi di firma e cifratura dei messaggi e consente di condividere i dati, gli appuntamenti di lavoro e le informazioni di viaggio con le altre applicazioni di Kontact.

I miglioramenti includono:

+ Inoltre, la grammatica! Questa versione di KMail viene fornita col supporto per languagetools (verificatore grammaticale) e grammalecte (verificatore grammaticale per il francese).
+ Ora i numeri di telefono nei messaggi vengono rilevati e possono essere composti direttamente tramite <a href='https://community.kde.org/KDEConnect'>KDE Connect</a>.
+ KMail ora possiede un'opzione per <a href='https://phabricator.kde.org/D19189'>avviarsi direttamente nel vassoio di sistema</a> senza aprire la finestra principale.
+ Abbiamo migliorato il supporto per l'estensione Markdown.
+ Il recupero dei messaggi tramite IMAP non si blocca più quando l'autenticazione non riesce.
+ Abbiamo inoltre apportato numerose correzioni nel motore Akonadi di KMail per migliorare l'affidabilità e le prestazioni.

<a href='https://kde.org/applications/office/korganizer/'>KOrganizer</a> è il componente di calendario di Kontact, che gestisce tutti i tuoi appuntamenti ed eventi.

+ Ora sono di nuovo sincronizzati correttamente gli eventi di <a href='https://bugs.kde.org/show_bug.cgi?id=334569'>Google Calendar</a>.
+ La finestra di avviso dell'evento ora ricorda di <a href='https://phabricator.kde.org/D16247'>apparire in tutti i desktop</a>.
+ L'aspetto delle <a href='https://phabricator.kde.org/T9420'>viste eventi</a> è stato modernizzato.

<a href='https://cgit.kde.org/kitinerary.git/'>Kitinerary</a> è il nuovissimo assistente di viaggio di Kontact, che ti aiuterà a raggiungere la tua destinazione e ti avviserà durante il viaggio.

- È presente un nuovo estrattore generico per i biglietti RCT2 (per es. utilizzati dalle compagnie ferroviarie come DSB, ÖBB, SBB, NS).
- Sono stati migliorati notevolmente il rilevamento e la disambiguazione dei nomi degli aeroporti.
- Abbiamo aggiunto nuovi estrattori personalizzati per i fornitori in precedenza non supportati (per es. BCD Travel, NH Group) e migliorato le variazioni di lingua/formato di quelli già supportati (per es. SNCF, Easyjet, Booking.com, Hertz).

### Sviluppo

{{<figure src="/announcements/applications/19.04.0/app1904_kate.png" width="600px" >}}

<a href='https://kde.org/applications/utilities/kate/'>Kate</a> è l'editor di testo completo di KDE, ideale per la programmazione grazie alle sue funzionalità quali le schede, la modalità a vista divisa, l'evidenziazione della sintassi, un pannello di terminale integrato, il completamento delle parole, la ricerca e sostituzione tramite espressioni regolari e molte altre funzioni ottenibili tramite un'infrastruttura per estensioni flessibile.

I miglioramenti includono:

- Kate ora è in grado di mostrare tutti, e non solo alcuni, i caratteri di spazio non visibili.
- Puoi abilitare e disabilitare facilmente il ritorno a capo statico utilizzando la voce di menu dedicata e per documento, senza dover cambiare le impostazioni globali predefinite.
- I menu di contesto delle schede e del file ora includono una serie di comandi utili quali Rinomina, Elimina, Apri cartella contenitore, Copia percorso del file, Confronta [col documento attivo] e Proprietà.
- Questa versione di Kate contiene più estensioni abilitate in modo predefinito, inclusa la popolare e utile funzione di Terminale integrata.
- Quando esci da Kate, il programma non chiede più di riconoscere i file che sono stati modificati sul disco da qualche altro processo come, per esempio, una modifica da un sistema di controllo di versione.
- La vista ad albero delle estensioni ora visualizza correttamente tutti gli elementi per le voci git che possiedono umlaut nei nomi.
- Quando si aprono più file tramite riga di comando, essi vengono aperti nelle nuove schede nello stesso ordine specificato nella riga di comando.

{{<figure src="/announcements/applications/19.04.0/app1904_konsole.png" width="600px" >}}

<a href='https://kde.org/applications/system/konsole/'>Konsole</a> è l'emulatore di terminale di KDE. Supporta schede, sfondi traslucidi, modalità a vista divisa, schemi di colori personalizzabili e scorciatoie da tastiera, segnalibri per cartelle e SSH, e molte altre funzionalità.

I miglioramenti includono:

+ La gestione delle schede è stata notevolmente migliorata in modo da essere più produttivi. Le nuove schede si possono creare col clic del pulsante centrale sulle parti vuote della barra delle schede, ed esiste anche un'opzione che permette di chiudere le schede sempre con lo stesso pulsante. I pulsanti Chiudi vengono visualizzati per impostazione predefinita e le icone saranno visualizzate solo se utilizzate in un profilo con icone personalizzate. Infine, ma non meno importante, la scorciatoia Ctrl+Tab ti permette di passare dalla scheda attiva a quella precedente.
+ La finestra di dialogo Modifica profilo ha ricevuto un'importante <a href='https://phabricator.kde.org/D17244'>ristrutturazione dell'interfaccia</a>.
+ Lo schema dei colori Brezza è ora utilizzato come schema dei colori predefinito di Konsole, ed è stato migliorato il suo contrasto e la coerenza col tema Brezza del sistema.
+ Abbiamo risolto i problemi durante la visualizzazione del testo in grassetto.
+ Konsole ora visualizza correttamente il cursore in stile sottolineato.
+ Abbiamo migliorato la visualizzazione dei <a href='https://bugs.kde.org/show_bug.cgi?id=402415'>caratteri di riga e incasellati</a>, così come dei <a href='https://bugs.kde.org/show_bug.cgi?id=401298'>caratteri Emoji</a>.
+ Le scorciatoie per il passaggio tra i profili, anziché aprire una nuova scheda con l'altro profilo, ora passano al profilo della scheda attiva.
+ Le schede inattive che ricevono una notifica e hanno il colore del loro titolo cambiato, ora riportano di nuovo il colore al normale colore di titolo di scheda, quando questa è attivata.
+ La funzionalità «Varia lo sfondo per ciascuna scheda» ora funziona quando il colore di sfondo di base è molto scuro o nero.

<a href='https://kde.org/applications/development/lokalize/'>Lokalize</a> è un sistema di traduzione assistita tramite computer che si focalizza sulla produttività e il controllo di qualità. È pensato per la traduzione di software ma integra anche strumenti di conversioni esterni per la traduzione di documenti per ufficio.

I miglioramenti includono:

- Lokalize ora supporta la visualizzazione del file sorgente da tradurre tramite un editor personalizzato.
- Abbiamo migliorato la posizione dei DockWidget e il modo in cui le impostazioni sono salvate e ripristinate.
- Quando si filtrano i messaggi, la posizione dei file .po è ora conservata.
- Abbiamo risolto diversi errori di interfaccia (commenti degli sviluppatori, azioni RegExp nelle sostituzioni in massa, conteggio dei messaggi vuoti non pronti, e altro ancora).

### Accessori

{{<figure src="/announcements/applications/19.04.0/app1904_gwenview.png" width="600px" >}}

<a href='https://kde.org/applications/graphics/gwenview/'>Gwenview</a> è un visore di immagini avanzato e un gestore con strumenti di modifica intuitivi e facili da usare.

I miglioramenti includono:

+ La versione di Gwenview fornita con Applications 19.04 include il <a href='https://phabricator.kde.org/D13901'>supporto tattile</a> completo, con gesti per strisciare, ingrandire o ridurre, spostarsi e altro ancora.
+ Un altro miglioramento apportato a Gwenview è il <a href='https://bugs.kde.org/show_bug.cgi?id=373178'>supporto High DPI</a> completo, che permette un'alta qualità delle immagini visualizzate negli schermi ad alta risoluzione.
+ Il supporto migliorato per i <a href='https://phabricator.kde.org/D14583'>pulsanti del mouse avanti e indietro</a> consente di navigare tra le immagini premendo tali pulsanti.
+ Puoi ora utilizzare Gwenview per aprire file di immagini creati con <a href='https://krita.org/'>Krita</a>, lo strumento di disegno digitale amato da tutti.
+ Gwenview ora supporta le <a href='https://phabricator.kde.org/D6083'>miniature da 512 px</a>, che ti permettono di vedere più agevolmente le immagini in anteprima.
+ Gwenview ora usa la <a href='https://bugs.kde.org/show_bug.cgi?id=395184'>scorciatoia di tastiera Ctrl+L standard</a> per spostare il fuoco al campo dell'URL.
+ È possibile ora utilizzare la <a href='https://bugs.kde.org/show_bug.cgi?id=386531'>funzionalità Filtra per nome</a> con la scorciatoia Ctrl+I, proprio come in Dolphin.

{{<figure src="/announcements/applications/19.04.0/app1904_spectacle.jpg" width="600px" >}}

<a href='https://kde.org/applications/graphics/spectacle/'>Spectacle</a> è l'applicazione per l'acquisizione delle schermate in Plasma. Puoi catturare interi desktop abbracciando diversi schermi, singoli schermi, finestre, sezioni di finestre o aree personalizzate utilizzando la funzione di selezione rettangolare.

I miglioramenti includono:

+ Abbiamo esteso la modalità Regione rettangolare con alcune nuove opzioni. Essa è configurabile per <a href='https://bugs.kde.org/show_bug.cgi?id=404829'>accettare automaticamente</a> il riquadro catturato anziché chiedere prima di regolarlo. Esiste pure una nuova opzione predefinita che memorizza il riquadro di selezione della <a href='https://phabricator.kde.org/D19117'>regione rettangolare</a>, ma solo fino alla chiusura del programma.
+ È possibile configurare cosa succede quando viene premuta la scorciatoia per la schermata <a href='https://phabricator.kde.org/T9855'>quando Spectacle è ancora in esecuzione</a>.
+ Spectacle ti permette di modificare il <a href='https://bugs.kde.org/show_bug.cgi?id=63151'>livello di compressione</a> per i formati immagine con perdita di dati.
+ Le impostazioni per il salvataggio mostrano come apparirà <a href='https://bugs.kde.org/show_bug.cgi?id=381175'>il nome file di una schermata</a>. Puoi regolare facilmente il <a href='https://bugs.kde.org/show_bug.cgi?id=390856'>modello del nome file</a> in base alle tue preferenze facendo semplicemente clic sui segnaposto.
+ Spectacle non visualizza più le opzioni «Schermo intero (tutti i monitor)» e «Schermo attuale» quando al computer è collegato solo uno schermo.
+ Il testo di aiuto nella modalità Regione rettangolare ora appare al centro della visualizzazione primaria, piuttosto che divisa tra gli schermi.
+ Quando avviato in Wayland, Spectacle include solo le funzionalità che funzionano.

### Giochi e istruzione

{{<figure src="/announcements/applications/19.04.0/app1904_kmplot.png" width="600px" >}}

La nostra serie di applicazioni include numerosi <a href='https://games.kde.org/'>giochi</a> e <a href='https://edu.kde.org/'>applicazioni didattiche</a>.

<a href='https://kde.org/applications/education/kmplot'>KmPlot</a> è un disegnatore di funzioni matematiche e contiene un analizzatore avanzato. I grafici possono essere colorati e la vista è scalabile, permettendoti di aumentare o ridurre il livello in base alle tue esigenze. Gli utenti possono disegnare in contemporanea funzioni diverse e combinarle per costruire nuove funzioni.

+ Ora è possibile aumentare o ridurre la visualizzazione tenendo premuto il tasto Ctrl e utilizzando la <a href='https://bugs.kde.org/show_bug.cgi?id=159772'>rotellina del mouse</a>.
+ Questa versione di Kmplot introduce l'opzione <a href='https://phabricator.kde.org/D17626'>anteprima di stampa</a>.
+ Il valore della radice o la coppia (x,y) ora possono essere copiate negli <a href='https://bugs.kde.org/show_bug.cgi?id=308168%'>appunti</a>.

<a href='https://kde.org/applications/games/kolf/'>Kolf</a> è un gioco di golf in miniatura.

+ Abbiamo ripristinato il <a href='https://phabricator.kde.org/D16978'>supporto per il suono</a>.
+ Kolf è stato convertito correttamente da kdelibs4.
