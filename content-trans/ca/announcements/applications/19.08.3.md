---
aliases:
- ../announce-applications-19.08.3
changelog: true
date: 2019-11-07
description: Es distribueixen les aplicacions 19.08 del KDE.
layout: application
major_version: '19.08'
release: applications-19.08.3
title: Es distribueixen les aplicacions 19.08.3 del KDE
version: 19.08.3
---
{{% i18n_date %}}

Avui, KDE distribueix la tercera actualització d'estabilització per a les <a href='../19.08.0'>Aplicacions 19.08 del KDE</a>. Aquesta publicació només conté esmenes d'errors i actualitzacions de traduccions, i proporciona una actualització segura i millor per a tothom.

Hi ha més d'una dotzena d'esmenes registrades d'errors que inclouen millores al Kontact, Ark, Cantor, K3b, Kdenlive, Konsole, Okular, Spectacle i Umbrello, entre d'altres.

Les millores inclouen:

- A l'editor de vídeo Kdenlive, ja no desapareixen les composicions en reobrir un projecte amb les pistes blocades
- La vista d'anotacions de l'Okular ara mostra les hores de creació en la zona horària local en lloc d'UTC
- S'ha millorat el control amb el teclat a la utilitat Spectacle de captura de pantalla
