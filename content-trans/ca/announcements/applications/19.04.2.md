---
aliases:
- ../announce-applications-19.04.2
changelog: true
date: 2019-06-06
description: Es distribueixen les aplicacions 19.04.2 del KDE.
layout: application
major_version: '19.04'
release: applications-19.04.2
title: KDE distribueix les aplicacions 19.04.2 del KDE
version: 19.04.2
---
{{% i18n_date %}}

Avui, KDE distribueix la segona actualització d'estabilització per a les <a href='../19.04.0'>Aplicacions 19.04 del KDE</a>. Aquesta publicació només conté esmenes d'errors i actualitzacions de traduccions, i proporciona una actualització segura i millor per a tothom.

Hi ha prop de 50 esmenes registrades d'errors que inclouen millores al Kontact, Ark, Dolphin, JuK, Kdenlive, KmPlot, Okular i Spectacle entre d'altres.

Les millores inclouen:

- S'ha solucionat una fallada en veure determinats documents EPUB a l'Okular
- Les clau secretes es poden tornar a exportar des del gestor de criptografia Kleopatra
- El recordatori d'esdeveniments del KAlarm ja no torna a fallar en iniciar-se amb les biblioteques PIM més noves
