---
aliases:
- ../announce-applications-16.12.1
changelog: true
date: 2017-01-12
description: KDE distribueix les aplicacions 16.12.1 del KDE
layout: application
title: KDE distribueix les aplicacions 16.12.1 del KDE
version: 16.12.1
---
12 de gener de 2017. Avui KDE distribueix la primera actualització d'estabilització per a les <a href='../16.12.0'>aplicacions 16.12 del KDE</a>. Aquesta publicació només conté esmenes d'errors i actualitzacions de traduccions, proporcionant una actualització segura i millor per a tothom.

Aquest llançament soluciona un error de pèrdua de dades al recurs iCal que fallava en crear el «std.ics» si no existia.

Hi ha més de 40 esmenes registrades d'errors que inclouen millores al Kdepim, Ark, Gwenview, Kajongg, Okular, Kate i Kdenlive entre altres.

Aquest llançament també inclou les versions de suport a llarg termini de la plataforma de desenvolupament KDE 4.14.28.
