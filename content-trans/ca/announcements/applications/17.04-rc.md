---
aliases:
- ../announce-applications-17.04-rc
date: 2017-04-07
description: Es distribueixen les aplicacions 17.04 versió candidata del KDE.
layout: application
release: applications-17.03.90
title: KDE distribueix la versió candidata 17.04 de les aplicacions del KDE
---
7 d'abril de 2017. Avui KDE distribueix la versió candidata de les noves versions de les aplicacions del KDE. S'han congelat les dependències i les funcionalitats, i ara l'equip del KDE se centra a corregir els errors i acabar de polir-la.

Verifiqueu les <a href='https://community.kde.org/Applications/17.04_Release_Notes'>notes de llançament de la comunitat</a> per a informació quant als arxius tar nous, els quals estan basats en els KF5 i quant als problemes coneguts. Hi haurà un anunci més complet disponible per al llançament final.

La distribució 17.04 de les aplicacions del KDE necessita una prova exhaustiva per tal de mantenir i millorar la qualitat i l'experiència d'usuari. Els usuaris reals són imprescindibles per mantenir l'alta qualitat del KDE, perquè els desenvolupadors no poden provar totes les configuracions possibles. Comptem amb vós per ajudar a trobar errors amb anticipació, a fi que es puguin solucionar abans de la publicació final. Considereu unir-vos a l'equip instal·lant la beta <a href='https://bugs.kde.org/'>i informant de qualsevol error</a>.
