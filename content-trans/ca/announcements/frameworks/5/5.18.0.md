---
aliases:
- ../../kde-frameworks-5.18.0
date: 2016-01-09
layout: framework
libCount: 60
src: /announcements/frameworks/5-tp/KDE_QT.jpg
---
### Baloo

- Esmenar diversos problemes amb les cerques relacionades amb el «mtime»
- PostingDB Iter: No declarar en MDB_NOTFOUND
- Estat del Balooctl: Evitar mostrar «Indexació del contingut» quant a les carpetes
- StatusCommand: Mostrar l'estat correcte de les carpetes
- SearchStore: Gestionar adequadament els valors de termes buits (error 356176)

### Icones Brisa

- Actualitzacions d'icones i addicions
- Icones d'estat de mida 22px també per les de 32px, ja que es necessiten a la safata del sistema
- S'ha canviat el valor «Fix» a «Escalable» a les carpetes de 32px en el Brisa fosca

### Mòduls extres del CMake

- Fer global el mòdul CMake del KAppTemplate
- Silenciar els avisos CMP0063 amb els KDECompilerSettings
- ECMQtDeclareLoggingCategory: Incloure &lt;QDebug&gt; amb el fitxer generat
- Esmenar els avisos CMP0054

### KActivities

- Integrar amb eficiència la càrrega del codi en QML per al KCM (error 356832)
- Solució alternativa per l'error SQL de les Qt que no neteja adequadament les connexions (error 348194)
- S'ha integrat un connector que executa aplicacions en canviar l'estat de les activitats
- Adaptació des de la KService a la KPluginLoader
- Adaptar els connectors per usar kcoreaddons_desktop_to_json()

### KBookmarks

- Inicialitzar completament «DynMenuInfo» en el valor de retorn

### KCMUtils

- KPluginSelector::addPlugins: Esmenar la declaració si el paràmetre «config» és per defecte (error 352471)

### KCodecs

- Evitar desbordar deliberadament una memòria intermèdia plena

### KConfig

- Assegura que el grup és sense escapar a kconf_update

### KCoreAddons

- Afegir KAboutData::fromPluginMetaData(const KPluginMetaData &amp;plugin)
- Afegir KPluginMetaData::copyrightText(), extraInformation() i otherContributors()
- Afegir KPluginMetaData::translators() i KAboutPerson::fromJson()
- Esmenar un ús després d'alliberar a l'analitzador de fitxers «desktop»
- Fer que la KPluginMetaData es pugui construir des d'un camí del JSON
- desktoptojson: Fer que en mancar el fitxer de tipus de servei sigui un error per l'executable
- Donar error en cridar kcoreaddons_add_plugin sense SOURCES

### KDBusAddons

- Adaptar al D-Bus en un fil secundari de les Qt 5.6

### KDeclarative

- [DragArea] Afegir la propietat «dragActive»
- [KQuickControlsAddons MimeDatabase] Exposar el comentari de «QMimeType»

### KDED

- kded: Adaptar al D-Bus de fil de les Qt 5.6: «messageFilter» ha d'activar la càrrega dels mòduls en el fil principal

### Compatibilitat amb les KDELibs 4

- kdelibs4support requereix kded (per al kdedmodule.desktop)
- Esmenar l'avís CMP0064 definint la política CMP0054 a «NEW»
- No exportar els símbols que ja existeixen als KWidgetsAddons

### KDESU

- No perdre «fd» en crear un sòcol

### KHTML

- Windows: Eliminar la dependència de «kdewin»

### KI18n

- Documentar la primera regla d'argument per als plurals en el QML
- Reduir els canvis de tipus indesitjats
- Fer possible usar dobles com a índex a les crides i18np*() en el QML

### KIO

- Esmenar el kiod per al D-Bus en els fils de les Qt 5.6: el «messageFilter» ha d'esperar fins que el mòdul estigui carregat abans de retornar
- Canviar el codi d'error en enganxar/moure dins un subdirectori
- Esmenar el problema de la paperera buida bloquejada
- Esmenar un botó incorrecte a la KUrlNavigator per als URL remots
- KUrlComboBox: Esmenar el retorn d'un camí absolut des de «urls()»
- kiod: Desactivar la gestió de sessions
- Afegir la compleció automàtica per l'entrada «.» que presenta tots els fitxers/carpetes* ocults (error 354981)
- ktelnetservice: Esmenar en u la verificació de «argc», pedaç del Steven Bromley

### KNotification

- [Notificació per emergent] Enviar amb l'ID de l'esdeveniment
- Definir un motiu no buit per defecte per a la inhibició de l'estalvi de pantalla (error 334525)
- Afegir un consell per ometre l'agrupament de notificacions (error 356653)

### KNotifyConfig

- [KNotifyConfigWidget] Permetre seleccionar un esdeveniment específic

### Paquets dels Frameworks

- Fer possible subministrar les metadades en JSON

### KPeople

- Fer possible l'esborrat doble en el «DeclarativePersonData»

### KTextEditor

- Ressaltat de la sintaxi per a PLI: s'han afegit funcions incrustades, s'han afegit regions ampliables

### Framework del KWallet

- kwalletd: Esmenar una fuita a FILE*

### KWindowSystem

- Afegir la variant XCB per als mètodes «KStartupInfo::sendFoo» estàtics

### NetworkManagerQt

- Fer que funcioni amb versions NM antigues

### Frameworks del Plasma

- [ToolButtonStyle] Indicar sempre «activeFocus»
- Usar l'indicador «SkipGrouping» per a la notificació «giny suprimit» (error 356653)
- Tractar adequadament els enllaços simbòlics en els camins als paquets
- Afegir «HiddenStatus» per als plasmoides auto ocultables
- Aturar la redirecció de finestres quan un element està desactivat o ocult (error 356938)
- No emetre «statusChanged» si no ha canviat
- Esmenar els ID d'elements per orientació a l'est
- Contenidor: No emetre «appletCreated» amb una miniaplicació nul·la (error 356428)
- [Interfície de contenidor] Esmenar un desplaçament erràtic d'alta precisió
- Llegir la propietat X-Plasma-ComponentTypes de la KPluginMetada com una llista de cadenes
- [Miniatures de finestres] No fallar si el «Composite» està desactivat
- Permetre que els contenidors substitueixin el CompactApplet.qml

Podeu debatre i compartir idees quant a aquest llançament en la secció de comentaris en <a href='https://dot.kde.org/2014/07/07/kde-frameworks-5-makes-kde-software-more-accessible-all-qt-developers'>l'article del Dot</a>.
