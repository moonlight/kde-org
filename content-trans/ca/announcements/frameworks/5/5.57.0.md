---
aliases:
- ../../kde-frameworks-5.57.0
date: 2019-04-13
layout: framework
libCount: 70
---
### Attica

- Accepta qualsevol estat HTTP entre 100 i 199 com a benigne

### Baloo

- [DocumentIdDB] Silencia el missatge de depuració de no error, avisa dels errors
- [baloosearch] Permet especificar una hora en usar p. ex. «mtime»
- [indexcleaner] Evita eliminar carpetes incloses sota les excloses
- [MTimeDB] Soluciona la cerca per a l'interval LessEqual
- [MTimeDB] Soluciona la cerca quan l'interval d'hora retorna un conjunt buit
- Gestió correcta de les assercions/errors a MTimeDB
- Protegeix contra pares no vàlids a la IdTreeDB
- Elimina el document de MTimeDB/DocumentTimeDB encara que el segell de temps sigui 0
- És més precís amb la detecció del tipus MIME (error 403902)
- [timeline] Canonicalitza els URL
- [timeline] Soluciona les crides SlaveBase::finished() que manquen/mal situades
- [balooshow] Sortida d'informació bàsica de fitxer de diverses extensions
- [timeline] Soluciona un avís, afegeix una entrada UDS que manca per «.»
- [balooctl] Redueix el nivell d'imbricació per als arguments d'«addOption», neteja
- Reacciona a les actualitzacions de la configuració dins l'indexador (error 373430)
- Esmena una regressió en obrir la BBDD en mode de lectura-escriptura (error 405317)
- [balooctl] Neteja els espais en blanc finals
- [engine] Soluciona el codi trencat, reverteix el reanomenat de Transaction::abort()
- Harmonitza la gestió del subratllat a l'analitzador de consultes
- Baloo engine: tracta cada codi de no-èxit com una fallada (error 403720)

### BluezQt

- Mou la interfície Media dins Adapter
- Manager: No requereix la interfície Media1 per a la inicialització (error 405478)
- Dispositiu: Verifica el camí de l'objecte al sòcol suprimit de les interfícies (error 403289)

### Icones Brisa

- Afegeix les icones «notifications» i «notifications-disabled» (error 406121)
- Fa «start-here-kde» també disponible com «start-here-kde-plasma»
- Icona de fusió Sublime
- Dona més contrast a «applications-games» i «input-gaming» amb el Brisa fosca
- Fa que «go-up» de 24px sigui realment de 24px
- Afegeix les icones «preferences-desktop-theme-applications» i «preferences-desktop-theme-windowdecorations»
- Afegeix enllaços simbòlics des de «preferences-desktop-theme» a «preferences-desktop-theme-applications»
- Elimina «preferences-desktop-theme» com a preparació per fer-lo un enllaç simbòlic
- Afegeix «collapse/expand-all», «window-shade/unshade» (error 404344)
- Millora la coherència de «window-*» i afegir-ne més
- Fa que «go-bottom/first/last/top» s'assemblin més com «media-skip*»
- Canvia les destinacions dels enllaços simbòlics de «go-up/down-search» a «go-up/down»
- Millora l'alineació de la quadrícula dels píxels de «go-up/down/next/previous/jump»
- Canvia l'estil de «media-skip*» i «media-seek*»
- Força l'estil nou d'icona silenciada a totes les icones d'acció

### Mòduls extres del CMake

- Torna a activar l'opció del QT_PLUGIN_PATH
- ecm_add_wayland_client_protocol: Millora els missatges d'error
- ECMGeneratePkgConfigFile: fa totes les variables dependents de ${prefix}
- Afegeix el mòdul de cerca d'UDev
- ECMGeneratePkgConfigFile: afegeix variables usades per «pkg_check_modules»
- Restaura la compatibilitat cap enrere de FindFontconfig per «plasma-desktop»
- Afegeix el mòdul de cerca de Fontconfig

### Integració del marc de treball

- Usa una icona específica del Plasma més adequada per a la categoria Plasma
- Usa la icona «plasma» com a icona per a la categoria de notificacions del Plasma

### Eines de Doxygen del KDE

- Actualitza els URL per usar «https»

### KArchive

- Soluciona una fallada a KArchive::findOrCreate amb fitxers trencats
- Soluciona una lectura de memòria sense inicialitzar al KZip
- Afegeix Q_OBJECT a KFilterDev

### KCMUtils

- [KCModuleLoader] Passa els arguments al «KQuickAddons::ConfigModule» creat
- Passa el focus a la barra de cerca filla quan el KPluginSelector està amb el focus (error 399516)
- Millora el missatge d'error del KCM
- Afegeix un vigilant en temps d'execució que comprova que les pàgines són KCM al «KCMultiDialog» (error 405440)

### KCompletion

- No estableix cap completador nul en un quadre combinat no editable

### KConfig

- Afegeix la capacitat Notify al «revertToDefault»
- Apunta el «readme» a la pàgina del wiki
- kconfig_compiler: arguments nous HeaderExtension i SourceExtension del «kcfgc»
- [kconf_update] Converteix la tecnologia d'inici personalitzada al qCDebug
- Elimina la referència de «const» KConfigIniBackend::BufferFragment &amp;
- Macro KCONFIG_ADD_KCFG_FILES: assegura que es recullen els canvis de «File=» al «kcfg»

### KCoreAddons

- Esmena «* foo *», ja que no es vol posar en negreta aquesta cadena
- Soluciona l'error 401996 - En clicar l'URL web del contacte =&gt; se selecciona un URL incomplet (error 401996)
- Imprimeix a «strerror» quan falla l'«inotify» (motiu habitual: «massa fitxers oberts»)

### KDBusAddons

- Converteix dues connexions de l'estil antic a l'estil nou

### KDeclarative

- [GridViewKCM] Soluciona un càlcul implícit d'amplada
- Mou la vista de quadrícula a un fitxer separat
- Evita fraccions de les mides i alineacions a GridDelegate

### Compatibilitat amb les KDELibs 4

- Elimina els mòduls de cerca proporcionats pels ECM

### KDocTools

- Actualitza la traducció ucraïnesa
- Actualitzacions del català
- it entities: actualitza els URL per usar «https»
- Actualitza els URL per usar «https»
- Usa la traducció indonèsia
- Actualitza el disseny per tenir una aparença més similar a kde.org
- Afegeix els fitxers necessaris per usar l'idioma natiu indonesi per a tots els documents indonesis

### KFileMetaData

- Implementa el funcionament per escriure informació de valoració a l'escriptor de la «taglib»
- Implementa més etiquetes per a l'escriptor de la «taglib»
- Reescriu l'escriptor de la «taglib» per usar la interfície de propietària
- Prova l'extractor del «ffmpeg» usant l'ajudant del tipus MIME (error 399650)
- Proposa Stefan Bruns com a mantenidor del KFileMetaData
- Declara PropertyInfo com a QMetaType
- Es protegeix contra fitxers no vàlids
- [TagLibExtractor] Usa el tipus MIME correcte en cas d'herència
- Afegeix un ajudant per determinar el tipus MIME real acceptat
- [taglibextractor] Prova l'extracció de propietats amb valors múltiples
- Genera la capçalera per als MimeUtils nous
- Usa la funció de les Qt per donar format a una llista de cadenes
- Soluciona la localització dels nombres per a les propietats
- Verifica els tipus MIME per a tots els fitxers d'exemple existents, n'afegeix més
- Afegeix una funció ajudant per determinar el tipus MIME basada en el contingut i l'extensió (error 403902)
- Afegeix la implementació per extreure dades dels fitxers «ogg» i «ts» (error 399650)
- [ffmpegextractor] Afegeix un cas de prova de vídeo Matroska (error 403902)
- Reescriu l'escriptor de la «taglib» per usar la interfície genèrica PropertyMap (error 403902)
- [ExtractorCollection] Carrega de manera menys estricta els connectors d'extracció
- Esmena l'extracció de la propietat relació d'aspecte
- Augmenta la precisió de la propietat velocitat dels fotogrames

### KHolidays

- Ordena les categories de festius polonesos

### KI18n

- Informa d'un error de manera intel·ligible si es requereix Qt5Widgets, però no s'ha trobat

### KIconThemes

- Esmena el farciment de la icona que no coincideix exactament amb la mida requerida (error 396990)

### KImageFormats

- ora:kra: qstrcmp -&gt; memcmp
- Esmena RGBHandler::canRead
- xcf: no falla amb fitxers que tenen modes de capa no implementats

### KIO

- Substitueix «currentDateTimeUtc().toTime_t()» per «currentSecsSinceEpoch()»
- Substitueix QDateTime::to_Time_t/from_Time_t per «to/fromSecsSinceEpoch»
- Millora les icones dels botons de diàleg de l'executable (error 406090)
- [KDirOperator] Mostra la vista en arbre detallada de manera predeterminada
- KFileItem: invoca «stat()» a demanda, afegeix l'opció SkipMimeTypeDetermination
- KIOExec: soluciona un error quan l'URL remot no té nom de fitxer
- El KFileWidget en mode de desament de fitxer únic, si es prem entrada/retorn al KDirOperator activa «slotOk» (error 385189)
- [KDynamicJobTracker] Usa una interfície D-Bus generada
- [KFileWidget] En desar, ressalta el nom de fitxer després de fer clic en el fitxer existent i també en usar un clic doble
- No crea miniatures per les Caixes fortes encriptades (error 404750)
- Soluciona el reanomenat d'un directori WebDAV si KeepAlive està desactivat
- Mostra la llista d'etiquetes a PlacesView (error 182367)
- Diàleg de confirmació de supressió/paperera: corregeix el títol confús
- Mostra el fitxer/camí correcte al missatge d'error «massa gran per a fat32» (error 405360)
- Expressa el missatge d'error en GiB, no GB (error 405445)
- openwithdialog: usa un indicador recursiu al filtre d'intermediaris
- Elimina els URL que s'estan recuperant quan es completi el llistat de treballs (error 383534)
- [CopyJob] Tracta els URL com a bruts en reanomenar un fitxer com a resolució de conflicte
- Passa el camí del fitxer local a KFileSystemType::fileSystemType()
- Soluciona el reanomenat amb majúscules/minúscules en sistemes de fitxers que no distingeixen majúscules i minúscules
- Soluciona els avisos «URL no vàlid URL: QUrl("quelcom.txt")» al diàleg Desa (error 373119)
- Soluciona una fallada en moure fitxers
- Soluciona la comprovació oculta del NTFS dels enllaços simbòlics a punts de muntatge NTFS (error 402738)
- Fa una mica més segura la sobreescriptura de fitxer (error 125102)

### Kirigami

- Corregeix «implicitWidth» de «listItems»
- Entropia de Shannon per estimar la icona monocroma
- Evita que el calaix de context desaparegui
- Elimina «actionmenuitembase»
- No intenta aconseguir la versió de les construccions estàtiques
- [Mnemonic Handling] Substitueix només la primera ocurrència
- Sincronitza quan s'actualitza qualsevol propietat del model
- Usa «icon.name» a retrocedir/avançar
- Corregeix les barres d'eines per a les capes
- Corregeix els errors als fitxers d'exemple del Kirigami
- Afegeix un component SearchField i PasswordField
- Corregeix la gestió d'icones (error 404714)
- [InlineMessage] No dibuixa ombres al voltant del missatge
- Disposició immediata en canviar l'ordre
- Corregeix la disposició del fil d'Ariadna
- No mostra mai la barra d'eines quan l'element actual no ho demana
- Gestiona enrere/endavant també al filtre
- Implementa els botons de ratolí d'avançar i retrocedir
- Afegeix una instanciació menys estricta per als submenús
- Corregeix les barres d'eines per a les capes
- kirigami_package_breeze_icons: també cerca entre les icones de mida 16
- Corregeix la construcció basada en el QMake
- Obté la propietat adjuntada de l'element adequat
- Corregeix la lògica quant a mostrar la barra d'eines
- Fa possible desactivar la barra d'eines per a les pàgines de disposició
- Mostra sempre la barra d'eines global en els modes globals
- Senyal Page.contextualActionsAboutToShow
- Una mica d'espai a la dreta del títol
- Torna a fer la disposició quan canvia la visibilitat
- ActionTextField: Situa adequadament les accions
- «topPadding» i «BottomPadding»
- El text sobre les imatges cal que sempre sigui blanc (error 394960)
- Retalla l'OverlaySheet (error 402280)
- Evita fer pare a OverlaySheet sobre ColumnView
- Usa un «qpointer» per a la instància del tema (error 404505)
- Oculta el fil d'Ariadna a les pàgines que no volen cap barra d'eines (error 404481)
- No intenta substituir la propietat activada (error 404114)
- Possibilita una capçalera i un peu de pàgina personalitzat al ContextDrawer (error 404978)

### KJobWidgets

- [KUiServerJobTracker] Actualitza «destUrl» abans d'acabar el treball

### KNewStuff

- Canvia els URL a «https»
- Actualitza l'enllaç al projecte «fsearch»
- Gestiona les ordres OCS no implementades, i no sobrevotar (error 391111)
- Ubicació nova per als fitxers KNSRC
- [knewstuff] Elimina el mètode obsolet a les Qt 5.13

### KNotification

- [KStatusNotifierItem] Envia el consell «desktop-entry»
- Permet definir consells personalitzats per a les notificacions

### KNotifyConfig

- Només permet seleccionar els fitxers d'àudio acceptats (error 405470)

### Framework del KPackage

- Corregeix la cerca del fitxer de destinacions de les eines del servidor a l'entorn acoblador de l'Android
- Afegeix la implementació de compilació creuada per al «kpackagetool5»

### KService

- Afegeix X-GNOME-UsesNotifications com a clau reconeguda
- Afegeix la versió mínima 2.4.1 del Bison a causa del %code

### KTextEditor

- Correcció: aplica correctament els colors de text de l'esquema triat (error 398758)
- DocumentPrivate: afegeix l'opció «Recarrega automàticament el document» al menú de Vista (error 384384)
- DocumentPrivate: permet definir un diccionari en seleccionar un bloc
- Corregeix cadenes de paraules i caràcters al «katestatusbar»
- Esmena el Minimap amb l'estil del QtCurve
- KateStatusBar: mostra la icona de cadenat en una etiqueta modificada en mode només lectura
- DocumentPrivate: Omet les cometes automàtiques quan aquestes sembla que ja estan balancejades (error 382960)
- Afegeix una interfície Variable a KTextEditor::Editor
- Fa menys estricte el codi per fer només les assercions en la construcció de depuració, treball en construir el llançament
- Assegura la compatibilitat amb les configuracions antigues
- Ús més gran de la interfície de configuració genèrica
- Simplifica QString KateDocumentConfig::eolString()
- Transfereix una opció del Sonnet a una opció del KTextEditor
- Ara assegura els buits a les claus de configuració
- Converteix més codi a la interfície de configuració genèrica
- Ús més gran de la interfície de configuració genèrica
- Interfície de configuració genèrica
- No falla en els fitxers de ressaltat de sintaxi amb format dolent
- IconBorder: accepta esdeveniments d'arrossegat i deixat anar (error 405280)
- ViewPrivate: fa més fàcil la desselecció amb les tecles de fletxa (error 296500)
- Esmena per mostrar l'arbre de consells d'arguments en una pantalla no primària
- Adapta un mètode obsolet
- Restaura el missatge de continuació de cerca als seus antics tipus i posició (error 398731)
- ViewPrivate: Fa més confortable «Aplica l'ajust de paraules» (error 381985)
- ModeBase::goToPos: Assegura que el destí del salt sigui vàlid (error 377200)
- ViInputMode: Elimina atributs de text no suportats de la barra d'estat
- KateStatusBar: Afegeix el botó de diccionari
- Afegeix un exemple per al problema de l'alçada de la línia

### KWidgetsAddons

- Fa coherent KFontRequester
- Actualitza les «kcharselect-data» a Unicode 12.0

### KWindowSystem

- Envia contrast difuminat/fons als píxels del dispositiu (error 404923)

### NetworkManagerQt

- WireGuard: fa que funcioni la serialització/desserialització dels secrets des del mapa
- Afegeix la implementació per al WireGuard dins la classe base de configuració
- WireGuard: gestiona la clau privada com a secrets
- WireGuard: la propietat «peers» hauria de ser NMVariantMapList
- Afegeix la implementació del tipus de connexió al WireGuard
- ActiveConnection: afegeix el senyal «stateChangedReason» a on es pugui veure el motiu del canvi d'estat

### Frameworks del Plasma

- [AppletInterface] Verifica el Corona abans d'accedir-hi
- [Dialog] No envia l'esdeveniment de passar per sobre quan no hi ha res a qui enviar-ho
- [Menu] Corregeix el senyal activat
- Redueix la importància de diversa informació de depuració de manera que es puguin veure els avisos reals
- [Plasma Components 3 ComboBox] Corregeix el «textColor»
- FrameSvgItem: té en compte els canvis de marge del FrameSvg també fora dels seus propis mètodes
- Afegeix Theme::blurBehindEnabled()
- FrameSvgItem: corregeix «textureRect» dels subelements en mosaic perquè no s'encongeixin a 0
- Soluciona el fons del diàleg del Brisa amb les Qt 5.12.2 (error 405548)
- Elimina una fallada al «plasmashell»
- [Icon Item] També neteja la icona de la imatge en usar un SVG del Plasma (error 405298)
- Alçada del «textfield» basada només en text net (error 399155)
- Vincula «alternateBackgroundColor»

### Purpose

- Afegeix el connector d'SMS al KDE Connect

### QQC2StyleBridge

- L'estil d'escriptori del Plasma implementa l'acoloriment d'icones
- [botó de selecció de valors] Millora el comportament de la roda del ratolí
- Afegeix una mica de farciment a les barres d'eines
- Esmena les icones RoundButton
- Farciment basat en la barra de desplaçament a tots els delegats
- Cerca una vista desplaçable per tenir en compte la seva barra de desplaçament per als marges

### Solid

- Permet construir sense UDev al Linux
- Només obté «clearTextPath» quan s'usa

### Ressaltat de la sintaxi

- Afegeix la definició de sintaxi per al llenguatge Elm a «syntax-highlighting»
- AppArmor &amp; SELinux: elimina un sagnat als fitxers XML
- Doxygen: no usa color negre dins les etiquetes
- Permet commutadors de context de final de línia a les línies buides (error 405903)
- Corregeix el plegat d'«endRegion» a les regles amb «beginRegion+endRegion» (usa length=0) (error 405585)
- Afegeix extensions al ressaltat del Groovy (error 403072)
- Afegeix un fitxer de ressaltat de sintaxi del Smali
- Afegeix «.» com a «weakDeliminator» al fitxer de sintaxi de l'Octave
- Logcat: corregeix el color «dsError» amb «underline="0"»
- Corregeix una fallada del ressaltador per a fitxers HL trencats
- Manté les biblioteques d'enllaç a destí per a una versió més antiga del CMake (error 404835)

### Informació de seguretat

El codi publicat s'ha signat amb GPG usant la clau següent: pub rsa2048/58D0EE648A48B3BB 2016-09-05 David Faure &lt;faure@kde.org&gt; Empremta digital de la clau primària: 53E6 B47B 45CE A3E0 D5B7 4577 58D0 EE64 8A48 B3BB
