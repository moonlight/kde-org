---
aliases:
- ../../kde-frameworks-5.59.0
date: 2019-06-08
layout: framework
libCount: 70
---
### Baloo

- No intenta indexar els bolcats de les bases de dades SQL
- Exclou els fitxers «.gcode» i els de les màquines virtuals de la consideració d'indexació

### BluezQt

- Afegeix l'API del Bluez a l'analitzador/generador XML de D-Bus

### Icones Brisa

- També el gcompris-qt
- Fa que la icona del Falkon sigui un SVG real
- Afegeix les icones que manquen de les aplicacions, per a refer https://bugs.kde.org/show_bug.cgi?id=407527
- Afegeix la icona per al KFourInLine des de l'aplicació, també cal actualització
- Afegeix la icona del Kigo https://bugs.kde.org/show_bug.cgi?id=407527
- Afegeix la icona del Kwave des de l'aplicació, per a refer a l'estil del Brisa
- Enllaç simbòlic de «arrow-_-double» a «go-_-skip», afegeix «go-*-skip» de 24px
- Canvia els estils de les icones de dispositiu «input-*», afegeix icones de 16px
- Afegeix la versió fosca de la icona nova del Knights que s'ha escapat del meu «commit» anterior
- Crea una icona nova per al Knights basat en la icona de l'Anjuta (error 407527)
- Afegeix icones per a les aplicacions que no els tenen al Brisa. S'haurien d'actualitzar per a ser més Brisa, però ara són necessaris per a la kde.org/applications nova
- Icona del KXStitch a partir de kde:kxstitch, per actualitzar
- No englobar-ho tot i buidar quelcom
- Assegura que també es fa «assert» de ScaledDirectories

### Mòduls extres del CMake

- Crea un directori específic per al fitxer de categories de registre de les Qt
- No activa «QT_STRICT_ITERATORS» al Windows

### Integració del marc de treball

- Assegura que se cerca també a la ubicació heretada
- Cerca els fitxers «knsrc» a la ubicació nova

### KArchive

- Prova de lectura i cerca a KCompressionDevice
- KCompressionDevice: Elimina «bIgnoreData»
- KAr: corregeix una lectura fora de límits (per una entrada no vàlida) adaptant-ho a QByteArray
- KAr: corregeix l'anàlisi de noms de fitxer llargs amb les Qt-5.10
- KAr: els permisos són en octal, no en decimal
- KAr::openArchive: També comprova que «ar_longnamesIndex» no és &lt; 0
- KAr::openArchive: corregeix un accés no vàlid a la memòria en fitxers trencats
- KAr::openArchive: Protecció contra un desbordament de memòria intermèdia en monticles en fitxers trencats
- KTar::KTarPrivate::readLonglink: corregeix una fallada en fitxers mal formats

### KAuth

- No posa en el codi font el directori d'instal·lació de la política de D-Bus

### KConfigWidgets

- Usa la moneda de la configuració regional per a la icona de donació

### KCoreAddons

- Corregeix la compilació per a les vinculacions de Python (error 407306)
- Afegeix GetProcessList per recuperar la llista dels processos actualment actius

### KDeclarative

- Corregeix els fitxers «qmldir»

### Compatibilitat amb les KDELibs 4

- Elimina QApplication::setColorSpec (mètode buit)

### KFileMetaData

- Mostra 3 xifres significatives en mostrar els dobles (error 343273)

### KIO

- Manipula bytes en lloc de caràcters
- Corregeix els executables «kioslave» que no surten mai, en definir KDE_FORK_SLAVES
- Corregeix l'enllaç d'escriptori a un fitxer o directori (error 357171)
- Prova el filtre actual abans de definir-ne un de nou (error 407642)
- [kioslave/file] Afegeix un còdec per als noms de fitxer heretats (error 165044)
- Confia en el QSysInfo per recuperar els detalls del sistema
- Afegeix Documents a la llista predeterminada de Llocs
- kioslave: manté «argv[0]», per corregir «applicationDirPath()» en sistemes no Linux
- Permet deixar anar un fitxer o una carpeta al KDirOperator (error 45154)
- Trunca els noms de fitxer llargs abans de crear un enllaç (error 342247)

### Kirigami

- [ActionTextField] Fa coherent el consell d'eines del QML
- Es basa en l'alçada per als elements que han de tenir un farciment superior (error 405614)
- Rendiment: comprimeix els canvis de color sense un QTimer
- [FormLayout] Usa un espaiat superior i inferior senar per al separador (error 405614)
- ScrollablePage: Assegura que la vista desplaçada obté el focus quan s'ha establert (error 389510)
- Millora l'ús amb només el teclat per a la barra d'eines (error 403711)
- Fa que el reciclador sigui un FocusScope

### KNotification

- Gestiona les aplicacions que han definit la propietat «desktopFileName» amb un sufix de nom de fitxer

### KService

- Corregeix l'asserció (hash != 0) a vegades quan un fitxer ha estat suprimit per un altre procés
- Corregeix una altra asserció quan el fitxer desapareix a sota: ASSERT: «ctime != 0»

### KTextEditor

- No suprimeix la línia prèvia sencera en retrocedir a la posició 0 (error 408016)
- Usa la verificació de sobreescriptura de diàleg natiu
- Afegeix l'acció de reiniciar la mida del tipus de lletra
- Mostra sempre el marcador d'ajust estàtic si es requereix
- Assegura el marcador d'inici/final de l'interval ressaltat després de desplegar
- Esmena: no reinicia el ressaltat en desar diversos fitxers (error 407763)
- Sagnat automàtic: usa «std::vector» en lloc de QList
- Esmena: usa el mode de sagnat predeterminat per als fitxers nous (error 375502)
- Elimina les assignacions duplicades
- Respecta l'ajust de parèntesi automàtic en verificar si concorden
- Millora la verificació de caràcters no vàlids en carregar (error 406571)
- Menú nou de ressaltat de sintaxi a la barra d'estat
- Evita un bucle infinit a l'acció «Canvia els nodes continguts»

### KWayland

- Permet que els compositors enviïn valors discrets d'eixos (error 404152)
- Implementa «set_window_geometry»
- Implementa «wl_surface::damage_buffer»

### KWidgetsAddons

- KNewPasswordDialog: afegeix punts als missatges dels ginys

### NetworkManagerQt

- No recupera les estadístiques de dispositiu fins a la construcció

### Frameworks del Plasma

- Fa que els Brisa clara/fosca usin més els colors del sistema
- Exporta la columna d'ordenació del SortFilterModel al QML
- Plasmacore: corregeix «qmldir», ToolTip.qml ja no forma part del mòdul
- Senyal «availableScreenRectChanged» per a totes les miniaplicacions
- Usa un «configure_file» senzill per a generar els fitxers del «plasmacomponents3»
- Actualitza els «*.qmltypes» a l'API actual dels mòduls del QML
- FrameSvg: també neteja la memòria cau de la màscara a «clearCache()»
- FrameSvg: fa que «hasElementPrefix()» també gestioni els prefixos amb un «-» final
- FrameSvgPrivate::generateBackground: també genera el segon pla si «reqp != p»
- FrameSvgItem: també emet «maskChanged» des de «geometryChanged()»
- FrameSvg: evita una fallada en cridar «mask()» quan encara no s'ha creat el marc
- FrameSvgItem: sempre emet «maskChanged» a partir de «doUpdate()»
- API dox: nota del «-» final al FrameSvg::prefix()/actualPrefix()
- API dox: apunta a les versions del Plasma5 de Techbase si són disponibles
- FrameSvg: les vores «l» &amp; «r» o «t» &amp; «b» no necessiten tenir la mateixa alçada respecte a l'amplada

### Purpose

- [JobDialog] També emet el senyal de cancel·lació quan l'usuari tanca la finestra
- Informa la cancel·lació de la configuració com a finalitzada amb un error (error 407356)

### QQC2StyleBridge

- Elimina l'animació de DefaultListItemBackground i MenuItem
- [QQC2 Slider Style] Corregeix el posicionament incorrecte de la maneta quan el valor inicial és 1 (error 405471)
- ScrollBar: fa que també funcioni com a barra de desplaçament horitzontal (error 390351)

### Solid

- Refactoritza la manera que els dorsals de dispositiu es construeixen i registren
- [Fstab] Usa la icona «folder-decrypted» per encriptar muntatges «fuse»

### Ressaltat de la sintaxi

- YAML: només comentaris després d'espais i altres millores/esmenes (error 407060)
- Markdown: usa «includeAttrib» als blocs de codi
- Corregeix el ressaltat de «0» al mode C
- Tcsh: corregeix els operadors i les paraules clau
- Afegeix la definició de sintaxi per al Common Intermediate Language
- SyntaxHighlighter: corregeix el color del fons per al text sense cap ressaltat especial (error 406816)
- Afegeix un exemple d'aplicació per imprimir text ressaltat a un PDF
- Markdown: usa un color amb un contrast més alt per a les llistes (error 405824)
- Elimina l'extensió «.conf» del ressaltat dels «INI Files», per determinar el ressaltador usant el tipus MIME (error 400290)
- Perl: corregeix l'operador // (error 407327)
- Corregeix les minúscules dels tipus UInt* al ressaltat del Julia (error 407611)

### Informació de seguretat

El codi publicat s'ha signat amb GPG usant la clau següent: pub rsa2048/58D0EE648A48B3BB 2016-09-05 David Faure &lt;faure@kde.org&gt; Empremta digital de la clau primària: 53E6 B47B 45CE A3E0 D5B7 4577 58D0 EE64 8A48 B3BB
