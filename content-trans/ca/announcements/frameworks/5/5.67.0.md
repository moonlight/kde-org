---
aliases:
- ../../kde-frameworks-5.67.0
date: 2020-02-02
layout: framework
libCount: 70
---
### General

- Elimina l'adaptació de molts mètodes obsolets a les Qt 5.15, això redueix el nombre d'avisos durant la construcció.

### Baloo

- Migra la configuració des del KConfig al KConfigXt per tal que l'usi el KCM

### Icones Brisa

- Crea la icona del Kate a l'estil Breeze basada en el disseny nou del Tyson Tan
- Canvia la icona VLC perquè sigui com les icones VLC oficials
- Afegeix la icona del KTrip del repositori del KTrip
- Afegeix una icona per «application/sql»
- Neteja i addició de les icones 22px de repetició de suport
- Afegeix una icona per a «text/vnd.kde.kcrash-report»
- Converteix «application/x-ms-shortcut» en una icona de drecera real

### Mòduls extres del CMake

- Afegeix una variable d'entorn d'importació que manca
- ECMAddAppIcon: afegeix «sc» a l'expressió regular per extreure l'extensió dels noms vàlids
- ECMAddQch: implementa i documenta l'ús de la macro «K_DOXYGEN»

### Integració del marc de treball

- Elimina la dependència no usada del QtDBus

### KActivitiesStats

- Corregeix una consulta SQL errònia a «allResourcesQuery»

### KActivities

- Elimina els fitxers que el Windows no pot gestionar
- Assegura emmagatzemar l'URI del recurs sense la barra final

### Eines de Doxygen del KDE

- Soluciona les importacions de mòdul per al Python2
- Escriu UTF-8 al codi font com a codificació del sistema de fitxer per al Python2 per ajudar l'«api.kde.org»

### KCMUtils

- Prefereix els connectors nous dels KCM als antics
- KCModuleQml: assegura que s'emet el predeterminat amb el «configModule-&gt;representsDefaults» actual en carregar
- Mostra el botó respectant allò que s'ha declarat al KCModule
- Actualitza el KPluginSelector per a permetre que el KCM mostri un estat correcte per als botons de reinici, aplicació i predeterminat

### KConfig

- Refactoritza KConfigXT
- Corregeix la compilació de les vinculacions del Python després de «ebd14f29f8052ff5119bf97b42e61f404f223615»
- KCONFIG_ADD_KCFG_FILES: regenera també a la versió nova del «kconfig_compiler»
- Permet passar també un objectiu en lloc d'una llista de codis fonts a KCONFIG_ADD_KCFG_FILES
- Afegeix KSharedConfig::openStateConfig per emmagatzemar la informació d'estat
- Corregeix la compilació de les vinculacions del Python després de «7ab8275bdb56882692846d046a5bbeca5795b009»

### KConfigWidgets

- KStandardAction: afegeix un mètode per a la creació de l'acció SwitchApplicationLanguage
- [KColorSchemeManager] No llista els duplicats
- [KColorschemeManager] Afegeix l'opció per reactivar el seguiment del tema global

### KCoreAddons

- Degrada els errors de càrrega del connector d'avisos al nivell de depuració + reformulació
- Documenta com filtrar per «servicetype» de la manera correcta
- Afegeix la sobrecàrrega de «perlSplit()» utilitzant una QRegularExpression i fent obsoleta la QRegExp
- Afegeix els tipus MIME per a les traces inverses desades del DrKonqi
- Afegeix la utilitat de funció de text KShell::tildeCollapse
- KPluginMetaData: afegeix el «getter» «initialPreference()»
- Desktoptojson: converteix també la clau InitialPreference

### KDeclarative

- Calcula correctament el marge inferior per als delegats de quadrícula amb subtítols
- [ConfigModule] Informa quin paquet no és vàlid

### KHolidays

- Actualitza els festius i afegeix els dies de la bandera i els dies dels Sants per a Suècia

### KI18n

- ki18n_wrap_ui: error quan el fitxer no existeix
- [Kuit] Reverteix els canvis a «parseUiMarker()»

### KIO

- Afegeix un esdeveniment reanomenat que manca quan el fitxer de destinació ja existeix
- KFilePlacesModel: En un perfil nou, a recents només mostra les entrades predeterminades basant-se en «recentlyused:/»
- Afegeix el constructor KFileCustomDialog amb un paràmetre «startDir»
- Corregeix l'ús de QRegularExpression::wildcardToRegularExpression()
- Permet gestionar aplicacions amb Terminal=True al seu fitxer «desktop», gestiona adequadament el seu tipus MIME (error 410506)
- KOpenWithDialog: Permet retornar un KService creat de nou associat a un tipus MIME
- Afegeix KIO::DropJobFlag per permetre mostrar manualment el menú (error 415917)
- [KOpenWithDialog] Oculta el grup de caselles reduïble quan totes les opcions de dins estan ocultes (error 415510)
- Reverteix l'eliminació efectiva de KUrlPixmapProvider de l'API
- SlaveBase::dispatchLoop: corregeix el càlcul del temps d'espera (error 392768)
- [KDirOperator] Permet reanomenar fitxers des del menú contextual (error 189482)
- Diàleg per reanomenar un fitxer des de línia de treball del Dolphin (error 189482)
- KFilePlaceEditDialog: mou la lògica dins «isIconEditable()»

### Kirigami

- Retalla l'element pare «flickable» (error 416877)
- Elimina el marge superior de capçalera del ScrollView privat
- Consell de mida adequada per a la «gridlayout» (error 416860)
- Usa la propietat adjunta per «isCurrentPage»
- Es desfà d'un parell d'avisos
- Intenta mantenir el cursor a la finestra en teclejar en un OverlaySheet
- Amplia adequadament els elements «fillWidth» en el mode mòbil
- Afegeix els colors de fons actiu, enllaç, visitat, negatiu, neutre i positiu
- Exposa el nom de la icona del botó de desbordament d'ActionToolBar
- Usa la pàgina QQC2 com a base per a la pàgina Kirigami
- Especifica d'on ve el codi com un URL
- No ancora AbstractApplicationHeader cegament
- Emet «pooled» després d'haver reassignat les propietats
- Afegeix els senyals «reused» i «pooled» com una TableView

### KJS

- Interromp l'execució de la màquina un cop s'ha vist el senyal de «timeout»
- Permet l'operador exponencial ** de l'ECMAScript 2016
- S'ha afegit la funció «shouldExcept()» que funciona basant-se en una funció

### KNewStuff

- Soluciona la funcionalitat KNSQuick::Engine::changedEntries

### KNotification

- Afegeix un senyal nou per a l'activació de l'acció predeterminada
- Elimina la dependència a KF5Codecs usant la nova funció «stripRichText»
- Elimina el text enriquit al Windows
- Adaptació als canvis de l'Android en les Qt 5.14
- Fa obsolet «raiseWidget»
- Adapta KNotification des de KWindowSystem

### KPeople

- Ajusta «metainfo.yaml» al nivell nou
- Elimina el codi obsolet de càrrega del connector

### KQuickCharts

- Corregeix la comprovació de versió de les Qt
- Registra QAbstractItemModel com a tipus anònim per a les assignacions de propietats
- Oculta la línia d'un diagrama de línies si la seva amplada s'ha definit a 0

### Kross

- «addHelpOption» ja afegeix el «kaboutdata»

### KService

- Admet valors múltiples a XDG_CURRENT_DESKTOP
- Fa obsolet «allowAsDefault»
- Fa les «Default Applications» a «mimeapps.list» les aplicacions preferides (error 403499)

### KTextEditor

- Reverteix «Millora la compleció de paraules en usar el ressaltat per detectar els límits de les paraules» (error 412502)
- Importa la icona final del Brisa
- Mètodes relacionats amb els missatges: usa més connexions «member-function-pointer-based»
- DocumentPrivate::postMessage: evita cerques de resums múltiples
- Corregeix la funció d'arrossegar i copiar (usant la tecla Ctrl) (error 413848)
- Assegura que hi ha una icona quadrada
- Defineix la icona adequada del Kate al diàleg de «quant a» de la KatePart
- Notes incloses: estableix correctament «underMouse()» per a les notes incloses
- Evita l'ús de la mascota antiga ATM
- Expansió de variables: afegeix la variable PercentEncoded (error 416509)
- Corregeix una fallada a l'expansió d'una variable (usada per eines externes)
- KateMessageWidget: elimina la instal·lació d'un filtre d'esdeveniments sense ús

### KTextWidgets

- Elimina la dependència del KWindowSystem

### Framework del KWallet

- Reverteix que «readEntryList()» usi QRegExp::Wildcard
- Corregeix l'ús de QRegularExpression::wildcardToRegularExpression()

### KWidgetsAddons

- [KMessageWidget] Resta el marge correcte
- [KMessageBox] Només permet la selecció de text al quadre de diàleg usant el ratolí (error 416204)
- [KMessageWidget] Usa el «devicePixelRatioF» per a l'animació dels mapes de píxels (error 415528)

### KWindowSystem

- [KWindowShadows] Comprova la connexió amb X
- Presenta les API d'ombres
- Fa obsolet KWindowEffects::markAsDashboard()

### KXMLGUI

- Usa el mètode utilitari KStandardAction per a «switchApplicationLanguage»
- Permet que la propietat «programLogo» també sigui una QIcon
- Elimina la possibilitat d'informar d'errors de programari arbitrari a partir d'una llista estàtica
- Elimina la informació del compilador del diàleg d'informe d'error
- KMainWindow: corregeix «autoSaveSettings» perquè es torni a mostrar QDockWidgets
- i18n: Afegeix més cadenes de context semàntic
- i18n: Divideix les traduccions per a les cadenes «Translation»

### Frameworks del Plasma

- S'han corregit les cantonades del consell d'eina i s'han eliminat els atributs de color sense ús
- S'han eliminat els colors en el codi font als SVG de fons
- Corregeix la mida i l'alineació a píxel de les caselles de selecció i dels botons d'opció
- Actualitza les ombres del tema Brisa
- [Plasma Quick] Afegeix la classe WaylandIntegration
- El mateix comportament de la barra de desplaçament que l'estil de l'escriptori
- Fa ús de KPluginMetaData sempre que pot
- Afegeix un element de menú en el mode d'edició al menú contextual del giny de l'escriptori
- Coherència: botons seleccionats acolorits
- Adapta «endl» a «\n». No és necessari purgar, ja que QTextStream usa QFile que purga quan és suprimit

### Purpose

- Corregeix l'ús de QRegularExpression::wildcardToRegularExpression()

### QQC2StyleBridge

- Elimina les solucions temporals relacionades amb les barres de desplaçament dels delegats de llista
- [TabBar] Elimina el marc
- Afegeix els colors de fons actiu, enllaç, visitat, negatiu, neutre i positiu
- Usa «hasTransientTouchInput»
- Sempre arrodoneix X i Y
- Admet la barra de desplaçament en el mode mòbil
- ScrollView: No superposa les barres de desplaçament sobre el contingut

### Solid

- Afegeix senyals per als esdeveniments «udev» amb accions «bind» i «unbind»
- Clarifica la referència de DeviceInterface (error 414200)

### Ressaltat de la sintaxi

- Actualitza «nasm.xml» amb les darreres instruccions
- Perl: Afegeix «say» a la llista de paraules clau
- CMake: Corregeix l'expressió regular <code>CMAKE*POLICY**_CMP&amp;lt;N&amp;gt;</code> i afegeix arguments especials a <code>get_cmake_property</code>
- Afegeix la definició de ressaltat del GraphQL

### Informació de seguretat

El codi publicat s'ha signat amb GPG usant la clau següent: pub rsa2048/58D0EE648A48B3BB 2016-09-05 David Faure &lt;faure@kde.org&gt; Empremta digital de la clau primària: 53E6 B47B 45CE A3E0 D5B7 4577 58D0 EE64 8A48 B3BB
