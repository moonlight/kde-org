---
aliases:
- ../../kde-frameworks-5.47.0
date: 2018-06-09
layout: framework
libCount: 70
src: /announcements/frameworks/5-tp/KDE_QT.jpg
---
### Baloo

- Finalitza aviat l'execució de la consulta si el subterme retorna un conjunt buit de resultats
- Evita una fallada en llegir dades corrompudes de la BBDD de termes de documents (error 392877)
- Gestiona les llistes de cadenes com una entrada
- Ignora més tipus de fitxers de codi font (error 382117)

### Icones Brisa

- Manetes i desbordament de menú actualitzats

### Mòduls extres del CMake

- Cadena d'eines per a l'Android: permet especificar manualment biblioteques extres
- Android: No defineix «qml-import-paths» si és buit

### KArchive

- Gestiona fitxers ZIP incrustats en fitxers ZIP (error 73821)

### KCMUtils

- [KCModuleQml] Ignora els controls desactivats en recórrer les pestanyes

### KConfig

- kcfg.xsd - No requereix un element «kcfgfile»

### KConfigWidgets

- Esmena l'esquema de color «Predeterminat» per tornar a coincidir amb el Brisa

### KDeclarative

- Defineix el context del KCM adequadament en el context correcte
- [Plotter] No renderitza si el «m_node» és nul (error 394283)

### KDocTools

- Actualitza la llista d'entitats ucraïneses
- Afegeix l'entitat OSD a «general.entities»
- Afegeix les entitats CIFS, NFS, Samba, SMB a «general.entities»
- Afegeix Falkon, Kirigami, macOS, Solid, USB, Wayland, X11, SDDM a les entitats generals

### KFileMetaData

- Verifica que el «ffmpeg» tingui com a mínim la versió 3.1 que aporta l'API requerida
- Cerca les etiquetes artista d'àlbum i «albumartist» al «taglibextractor»
- popplerextractor: no intenta endevinar el títol si no n'hi ha cap

### KGlobalAccel

- Assegura que s'ha processat la petició d'alliberament del teclat abans d'emetre la drecera (error 394689)

### KHolidays

- holiday_es_es - Esmena el dia de la «Comunitat de Madrid»

### KIconThemes

- Verifica si el grup &lt; LastGroup, ja que el KIconEffect no gestiona més l'UserGroup

### KImageFormats

- Elimina el tipus MIME duplicats dels fitxers «json»

### KIO

- Verifica si existeix la destinació en enganxar dades binàries (error 394318)
- Auth support: Retorna la longitud real de la memòria intermèdia del sòcol
- Auth support: Unifica l'API per compartir el descriptor de fitxer
- Auth support: Crea el fitxer de sòcol al directori d'execució de l'usuari
- Auth support: Suprimeix el fitxer de sòcol després de l'ús
- Auth support: Mou la tasca de neteja del fitxer de sòcol al FdReceiver
- Auth support: No usa al Linux el sòcol abstracte per compartir el descriptor de fitxer
- [kcoredirlister] Elimina tantes «url.toString()» com sigui possible
- KFileItemActions: alternativa al tipus MIME predeterminat en seleccionar només fitxers (error 393710)
- Presenta KFileItemListProperties::isFile()
- KPropertiesDialogPlugin ara pot especificar múltiples protocols implementats usant X-KDE-Protocols
- Conserva el fragment en redirigir des d'http a https
- [KUrlNavigator] Emet «tabRequested» quan el camí en el selector de camins es clica amb el mig
- Rendiment: usa la implementació nova de l'UDS
- No redirigeix smb:/ a smb:// i després a smb:///
- Permet acceptar amb doble clic en el diàleg de desar (error 267749)
- Activa la vista prèvia de manera predeterminada en el diàleg de selecció de fitxer
- Oculta la vista prèvia del fitxer quan la icona és massa petita
- i18n: torna a usar la forma plural per als missatges dels connectors
- Usa un diàleg normal en lloc d'un diàleg de llista en enviar a la paperera o suprimir un fitxer individual
- Fa que el text d'avís per a les operacions de supressió emfatitzin la seva permanència i irreversibilitat
- Reverteix «Mostra els botons de mode de vista al diàleg obrir/desar de la barra d'eines»

### Kirigami

- Mostra «action.main» de manera més prominent a la ToolBarApplicationHeader
- Permet construir el Kirigami sense dependència del mode tauleta del KWin
- Filtre de lliscament correcte en RTL
- Redimensió correcta de «contentItem»
- Esmena el comportament de «--reverse»
- Comparteix «contextobject» per accedir sempre a «i18n»
- S'assegura que el consell d'eina estigui ocult
- S'assegura de no assignar variants no vàlides a les propietats seguides
- Gestiona no una MouseArea, senyal «dropped()»
- Sense efectes de passar per sobre en el mòbil
- Icones adequades «overflow-menu-left» i «right»
- Maneta d'arrossegar per reordenar elements a una ListView
- Usa mnemonics als botons de la barra d'eines
- S'han afegit fitxers que mancaven al «.pri» del QMake
- [API dox] Esmena Kirigami.InlineMessageType -&gt; Kirigami.MessageType
- Esmena «applicationheaders» a «applicationitem»
- No permet mostrar/ocultar el calaix quan no hi ha maneta (error 393776)

### KItemModels

- KConcatenateRowsProxyModel: saneja l'entrada adequadament

### KNotification

- Soluciona fallades a NotifyByAudio en tancar aplicacions

### Framework del KPackage

- kpackage*install**package: esmena la dependència que manca entre «.desktop» i «.json»
- Assegura que els camins al «rcc» mai es deriven de camins absoluts

### KRunner

- El procés de D-Bus replica en el fil «::match» (error 394272)

### KTextEditor

- No usa les majúscules de títol per a la casella de selecció «Mostra el comptador de paraules»
- Fa una preferència global del comptador de paraules/caràcters

### KWayland

- Augmenta la versió de la interfície «org_kde_plasma_shell»
- Afegeix «SkipSwitcher» a l'API
- Afegeix el protocol de sortida XDG

### KWidgetsAddons

- [KCharSelect] Esmena la mida de la cel·la de taula amb les Qt 5.11
- [API dox] Elimina l'ús d'«overload», que provocava documents trencats
- [API dox] Diu al «doxygen» que «e.g.» no finalitza la sentència, usa «. »
- [API dox] Elimina HTML escapat innecessari
- No estableix automàticament les icones predeterminades per a cada estil
- Fa que KMessageWidget coincideixi amb l'estil de «inlineMessage» del Kirigami (error 381255)

### NetworkManagerQt

- Dona informació quant a les propietats no gestionades als missatges de depuració
- WirelessSetting: implementa la propietat «assignedMacAddress»

### Frameworks del Plasma

- Plantilles: noms coherents, esmena la traducció dels noms de catàlegs i més
- [Breeze Plasma Theme] Esmena la icona del Kleopatra per usar el full d'estils de colors (error 394400)
- [Dialog] Gestiona adequadament el diàleg en ser minimitzat (error 381242)

### Purpose

- Millora la integració del Telegram
- Tracta les matrius internes com a restriccions OR en lloc d'AND
- Fa possible restringir connectors mitjançant la presència d'un fitxer «desktop»
- Fa possible filtrar connectors per un executable
- Ressalta el dispositiu seleccionat al connector del KDE Connect
- Esmena problemes de «i18n» als «frameworks/purpose/plugins»
- Afegeix un connector del Telegram
- kdeconnect: Notifica quan el procés falla en iniciar (error 389765)

### QQC2StyleBridge

- Usa la propietat «pallet» només en usar «qtquickcontrols» 2.4
- Funciona amb les Qt&lt;5.10
- Esmena l'alçada de la barra de pestanyes
- Usa Control.palette
- [RadioButton] Reanomena «control» a «controlRoot»
- No defineix un interlineat explícit a RadioButton/CheckBox
- [FocusRect] Usa un posicionament manual en lloc d'àncores
- Apaga el «flickable» en una vista desplaçable al «contentItem»
- Mostra el focus recte quan CheckBox o RadioButton estan enfocats
- Esmena ràpida per a la detecció d'una vista desplaçable
- No torna a fixar el pare com a «flickable» a l'àrea del ratolí
- [TabBar] Commuta pestanyes amb la roda del ratolí
- El control no ha de tenir fills (error 394134)
- Restringeix el desplaçament (error 393992)

### Ressaltat de la sintaxi

- Perl6: Afegeix suport per a les extensions .pl6, .p6, o .pm6 (error 392468)
- DoxygenLua: esmena els blocs de tancament de comentari (error 394184)
- Afegeix «pgf» als formats de fitxer de tipus «latex» (mateix format com «tikz»)
- Afegeix més paraules clau del Postgresql
- Ressaltat per a l'OpenSCAD
- debchangelog: afegeix Cosmic Cuttlefish
- CMake: esmena l'avís DetectChar de la barra inversa escapada
- Pony: esmena l'identificador i la paraula clau
- Lua: actualitzat per al Lua5.3

### Informació de seguretat

El codi publicat s'ha signat amb GPG usant la clau següent: pub rsa2048/58D0EE648A48B3BB 2016-09-05 David Faure &lt;faure@kde.org&gt; Empremta digital de la clau primària: 53E6 B47B 45CE A3E0 D5B7 4577 58D0 EE64 8A48 B3BB

Podeu debatre i compartir idees quant a aquest llançament en la secció de comentaris en <a href='https://dot.kde.org/2014/07/07/kde-frameworks-5-makes-kde-software-more-accessible-all-qt-developers'>l'article del Dot</a>.
