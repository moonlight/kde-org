---
aliases:
- ../../kde-frameworks-5.37.0
date: 2017-08-13
layout: framework
libCount: 70
src: /announcements/frameworks/5-tp/KDE_QT.jpg
---
### Nieuw framework: kirigami, een set van QtQuick plug-ins om gebruikersinterfaces gebaseerd op de KDE UX richtlijnen te bouwen

### Breeze pictogrammen

- Kleuren van .h en .h++ bijwerken (bug 376680)
- kleine monochrome pictogram van ktorrent verwijderen (bug 381370)
- bladwijzers is een actiepictogram geen mappictogram (bug 381383)
- systeemmonitor van hulpprogramma's bijwerken (bug 381420)

### Extra CMake-modules

- --gradle toevoegen aan androiddeployqt
- Installatie van apk-target repareren
- Gebruik van query_qmake repareren: maak verschil tussen aanroepen die qmake verwachten of niet
- API-dox voor KDEInstallDirs' KDE_INSTALL_USE_QT_SYS_PATHS toevoegen
- Een metainfo.yaml toevoegen om ECM een juist framework te maken
- Android: scan naar qml-bestanden in de bronmap, niet in de installatiemap

### KActivities

- stuur runningActivityListChanged uit bij aanmaken van activiteit

### KDE Doxygen hulpmiddelen

- Haal HTML uit de zoekopdracht

### KArchive

- Conan-bestanden toevoegen, als een eerste experiment aan ondersteuning van Conan

### KConfig

- Sta toe KConfig te bouwen zonder Qt5Gui
- Standaard sneltoetsen: gebruik Ctrl+PageUp/PageDown voor vorige/volgende tabblad

### KCoreAddons

- Ongebruikte init() declaratie verwijderen uit K_PLUGIN_FACTORY_DECLARATION_WITH_BASEFACTORY_SKEL
- Nieuwe spdx API op KAboutLicense om SPDX licentie-expressies op te halen
- kdirwatch: potentiële crash vermijden als d-ptr is vernietigd vóór KDirWatch (bug 381583)
- Tonen van formatDuration met afronding repareren (bug 382069)

### KDeclarative

- plasmashell unsetting QSG_RENDER_LOOP

### Ondersteuning van KDELibs 4

- 'Verouderde hint voor KUrl::path() is fout op Windows' (bug 382242)
- kdelibs4support bijwerken om het doelgebaseerde ondersteuning te gebruiken geleverd door kdewin
- Constuctors ook markeren als verouderd
- KDE4Defaults.cmake uit kdelibs synchroniseren

### KDesignerPlugin

- Toevoegen van ondersteuning voor nieuw widget kpasswordlineedit

### KHTML

- Ook SVG ondersteunen (bug 355872)

### KI18n

- Laden van i18n catalogi uit willekeurige locaties toestaan
- Ga na dat de target tsfiles wordt gegenereerd

### KIdleTime

- Qt5X11Extras alleen vereisen wanneer we het echt nodig hebben

### KInit

- De juiste functievlag gebruiken om kill(2) mee te nemen

### KIO

- Nieuwe methode urlSelectionRequested toevoegen aan KUrlNavigator
- KUrlNavigator: zichtbaar maken van de KUrlNavigatorButton die een gebeurtenis loslaten ontvangt
- Op de stapel zetten zonder de gebruiker te vragen met een popup Kopieer/Annuleer
- Zorg ervoor dat KDirLister items bijwerkt waarvan de doel-URL is gewijzigd (bug 382341)
- Maak geavanceerde opties van dialoog "openen met" standaard invouwbaar en verborgen (bug 359233)

### KNewStuff

- Een ouder geven aan KMoreToolsMenuFactory menu's
- Bij opvragen uit de cache, rapporteer alle items in het geheel

### KPackage-framework

- kpackagetool kan nu gegevens voor appstream uitvoeren naar een bestand
- nieuwe KAboutLicense::spdx adopteren

### KParts

- Reset url in closeUrl()
- Sjabloon voor een eenvoudige op kpart gebaseerde toepassing toevoegen
- Gebruik van KDE_DEFAULT_WINDOWFLAGS laten vallen

### KTextEditor

- Behandel fijngetuned wheel-gebeurtenis in zooming
- Sjabloon voor een ktexteditor-plug-in toevoegen
- kopieer rechten uit origineel bestand bij opslaan van kopie (bug 377373)
- vermijd hopelijk stringbuild crash (bug 339627)
- probleem met * repareren voor regels buiten commentaar (bug 360456)
- opslaan als kopie repareren, het miste overschrijven toelaten van het bestemmingsbestand (bug 368145)
- Commando 'set-highlight': argumenten met spatie samenvoegen
- crash bij weergave van destructie vanwege niet-deterministisch opschonen van objecten repareren
- Signalen van pictogramrand wanneer geen markering is aangeklikt uitzenden
- Crash in vi inpvoermudus (volgorde: "o" "Esc" "O" "Esc" ".") repareren (bug 377852)
- Onderling exclusieve groep in type standaard markering gebruiken 

### KUnitConversion

- Markeer MPa en PSI als gezamenlijke eenheden

### KWallet Framework

- CMAKE_INSTALL_BINDIR gebruiken voor generatie van dbus-service

### KWayland

- Alle kwayland objecten verwijderen gemaakt door registry wanneer het wordt verwijderd
- connectionDied uitzenden als de QPA wordt verwijderd
- [client] Alle aangemaakte ConnectionThreads volgen en API toevoegen om toegang te hebben
- [server] Tekstinvoer verlaten verzenden als van oppervlak met focus binding wordt verbroken
- [server] Stuur pointer-vertalen indien oppervlak met focus ongebonden wordt
- [client] volg enteredSurface in Keyboard op de juiste manier
- [server] toetsenbord verlaten verzenden wanneer client het oppervlak met focus verwijderd (bug 382280)
- geldigheid van Buffer controleren (bug 381953)

### KWidgetsAddons

- Extraheer lineedit wachtwoordwidget =&gt; nieuwe klasse KPasswordLineEdit
- Een crash gerepareerd bij zoeken met ondersteuning voor toegankelijkheid ingeschakeld (bug 374933)
- [KPageListViewDelegate] geef widget door aan drawPrimitive in drawFocus

### KWindowSystem

- Afhankelijkheid van QWidget-header verwijderen

### KXMLGUI

- Gebruik van KDE_DEFAULT_WINDOWFLAGS laten vallen

### NetworkManagerQt

- Ondersteuning toevoegen aan ipv*.route-metric
- Ongedefinieerde NM_SETTING_WIRELESS_POWERSAVE_FOO enums repareren (bug 382051)

### Plasma Framework

- [Containment Interface] altijd contextualActionsAboutToShow uitsturen voor containment
- Button/ToolButton-labels beschouwen als platte tekst
- Geen wayland specifieke reparaties uitvoeren indien onder X (bug 381130)
- KF5WindowSystem toevoegen aan koppelinginterface
- AppManager.js als pragma-bibliotheek declareren
- [PlasmaComponents] Config.js verwijderen
- standaard naar platte tekst voor labels
- Vertalingen uit KPackage bestanden laden indien gebundeld (bug 374825)
- [PlasmaComponents Menu] Niet crashen op nul-actie
- [Plasma Dialog] vlagcondities repareren
- pictogram van akregator in systeemvak bijwerken (bug 379861)
- [Containment Interface] Containment behouden in RequiresAttentionStatus terwijl contextmenu open is (bug 351823)
- Toetsbehandeling van tabbladbalk in RTL repareren (bug 379894)

### Sonnet

- Sonnet bouwen zonder Qt5Widgets toestaan
- cmake: FindHUNSPELL.cmake herschrijven om pkg-config te gebruiken

### Accentuering van syntaxis

- KSyntaxHighlighter bouwen zonder Qt5Gui toestaan
- Ondersteuning voor kruislings compileren voor accentueringsindexeerder toevoegen
- Thema's: alle ongebruikte metagegevens verwijderen (licentie, auteur, alleen-lezen)
- Thema: velden voor licentie en auteur verwijderen
- Thema: haal vlag voor alleen-lezen uit bestand op schijf
- Syntaxisaccentuering voor YANG-gegevens modelleringstaal toevoegen
- PHP: PHP 7 sleutelwoorden toevoegen (bug 356383)
- PHP: PHP 5 informatie opschonen
- gnuplot repareren, voorloop/achteraan spaties fataal maken
- detectie van 'else if' repareren, we moeten context omschakelen, extra regel toevoegen
- indexeerder controleert op voorloop/achteraan witruimte in XML accentuering
- Doxygen: accentuering van Doxyfile toevoegen
- ontbrekende standaard types aan C accentuering toevoegen en bijwerken voor C11 (bug 367798)
- Q_PI D =&gt; Q_PID
- PHP: accentuering van variabelen in accolades in aanhalingstekens verbeteren (bug 382527)
- Accentuering van PowerShell toevoegen
- Haskell: bestndsextensie .hs-boot toevoegen (bootstrap module) (bug 354629)
- replaceCaptures() laten werken met meer dan 9 vangsten repareren
- Ruby: WordDetect gebruiken in plaats van StringDetect voor volledig woorden overeen laten komen
- Onjuiste accentuering voor BEGIN en END in woorden zoals "EXTENDED" (bug 350709)
- PHP: mime_content_type() verwijderen uit lijst met verouderde functies (bug 371973)
- XML: XBEL extensie/mimetype naar xml accentuering toevoegen (bug 374573)
- Bash: onjuiste accentuering voor commando-opties repareren (bug 375245)
- Perl: heredoc accentuering met voorloopspaties in het scheidingsteken repareren (bug 379298)
- SQL (Oracle) syntaxisbestand bijwerken (bug 368755)
- C++: '-' is geen onderdeel van UDL-tekenreeks repareren (bug 380408)
- C++: printf formaat specificeert: voeg 'n' en 'p' toe, verwijder 'P' (bug 380409)
- C++: char-waarde heeft de kleur van de tekenreeksen repareren (bug 380489)
- VHDL: fout in accentuering repareren bij gebruik van haakjes en attributen (bug 368897)
- zsh accentuering: wiskundige expressie in een subtekenreeksexpressie repareren (bug 380229)
- JavaScript accentuering: ondersteuning voor E4X xml extensie toevoegen (bug 373713)
- "*.conf" extensieregel verwijderen
- Pug/Jade syntaxis

### ThreadWeaver

- Ontbrekende export naar QueueSignals toevoegen

### Beveiligingsinformatie

De vrijgegeven code is ondertekend met GPG met de volgende sleutel: pub rsa2048/58D0EE648A48B3BB 2016-09-05 David Faure &lt;faure@kde.org&gt; Vingerafdruk van primaire sleutel: 53E6 B47B 45CE A3E0 D5B7  4577 58D0 EE64 8A48 B3BB

U kunt discussiëren en ideeën delen over deze uitgave in de section voor commentaar van <a href='https://dot.kde.org/2014/07/07/kde-frameworks-5-makes-kde-software-more-accessible-all-qt-developers'>het artikel in the dot</a>.
