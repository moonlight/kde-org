---
aliases:
- ../../kde-frameworks-5.49.0
date: 2018-08-11
layout: framework
libCount: 70
src: /announcements/frameworks/5-tp/KDE_QT.jpg
---
### Attica

- Maak geen exemplaar van een QStringRef in een QString alleen om in een QStringList te zoeken
- Definieer elementen wanneer ze gedeclareerd worden

### Baloo

- [tags_kio] meerdere kopieën van bestandsnaam repareren
- Draai terug "[tags_kio] UDS_URL gebruiken in plaats van UDS_TARGET_URL."
- [tags_kio] UDS_URL gebruiken in plaats van UDS_TARGET_URL
- [tags_kio] doelbestandspaden van queries in plaats van paden achtervoegen aan het UDS-item van het bestand
- Speciale URL's voor zoeken van bestanden van een bepaald type
- Manipulatie van lijsten met kwadratische complexiteit vermijden
- Niet verouderde fastInsert in baloo gebruiken

### Breeze pictogrammen

- <code>drive-optical</code> pictogram toevoegen (bug 396432)

### Extra CMake-modules

- Android: geen hard gecodeerde versie van random voor de Android SDK gebruiken
- ECMOptionalAddSubdirectory: een beetje meer detail leveren
- Controle op definitie van variabele repareren
- De 'since' versie wijzigen
- ECMAddAppIconMacro verbeteren

### Frameworkintegratie

- Rekening houden met BUILD_TESTING

### KActivities

- Rekening houden met BUILD_TESTING

### KArchive

- Rekening houden met BUILD_TESTING

### KAuth

- Waarschuwingen vermijden voor PolkitQt5-1 headers
- Rekening houden met BUILD_TESTING

### KBookmarks

- Rekening houden met BUILD_TESTING

### KCodecs

- Rekening houden met BUILD_TESTING

### KCompletion

- Rekening houden met BUILD_TESTING

### KConfig

- Rekening houden met BUILD_TESTING

### KConfigWidgets

- Rekening houden met BUILD_TESTING

### KCoreAddons

- Overflow in code voor afronding repareren (bug 397008)
- API dox: not-to-be-there ":"s achter "@note" verwijderen
- API dox: praat over nullptr, niet 0
- KFormat: unicode constante vervangen door unicode codepoint om MSVC bouwen te repareren
- KFormat: tag @since corrigeren voor nieuwe KFormat::formatValue
- KFormat: gebruik van hoeveelheden toestaan naast bytes en seconden
- KFormat::formatBytes voorbeelden corrigeren
- Rekening houden met BUILD_TESTING

### KCrash

- Rekening houden met BUILD_TESTING

### KDBusAddons

- Niet voor altijd blokkeren in ensureKdeinitRunning
- Rekening houden met BUILD_TESTING

### KDeclarative

- ga na dat we altijd schrijven in de root-context van de engine
- betere leesbaarheid
- API docs een beetje verbeteren
- Rekening houden met BUILD_TESTING

### Ondersteuning van KDELibs 4

- qtplugins in KStandardDirs repareren

### KDocTools

- Rekening houden met BUILD_TESTING

### KEmoticons

- Rekening houden met BUILD_TESTING

### KFileMetaData

- API dox: @file toevoegen aan functions-only header om doxygen deze te laten dekken

### KGlobalAccel

- Rekening houden met BUILD_TESTING

### KDE GUI-addons

- Rekening houden met BUILD_TESTING

### KHolidays

- Installeer de berekeningsheader voor zonsopgang/-ondergang
- Schrikkeljaardag als (culturele) vakantiedag voor Noorwegen toegevoegd
- ‘naam’-item toegevoegd voor Noorse vakantiebestanden
- Beschrijving toegevoegd voor bestanden met Noorse vakantiedagen
- meer Japanse vakantiedagen bijgewerkt uit phanect
- holiday_jp_ja, holiday_jp-en_us - bijgewerkt (bug 365241)

### KI18n

- Functie hergebruiken die al hetzelfde doet
- De behandeling van de catalogus en detectie van taaldefinitie op Android repareren
- Leesbaarheid, sla no-op statements over
- KCatalog::translate repareren wanneer vertaling hetzelfde is als de originele tekst
- een bestand is hernoemd
- Laat ki18n macro bestandsnaam de stijl volgen van andere aan find_package gerelateerde bestanden
- De controle op configuratie voor _nl_msg_cat_cntr repareren
- Genereer geen bestanden in de bronmap
- libintl: bepaal of _nl_msg_cat_cntr bestaat voor gebruik (bug 365917)
- De binary-factory builds repareren

### KIconThemes

- Rekening houden met BUILD_TESTING

### KIO

- Aan kio gerelateerd kdebugsettings categoriebestand installeren
- private header hernoemen naar _p.h
- Aangepaste selectie van pictogram voor prullenbak verwijderen (bug 391200)
- Labels bovenaan uitlijnen in eigenschappendialoog
- Foutdialoog presenteren wanneer gebruiker een map genaamd "." of ".." probeert aan te maken (bug 387449)
- API dox: praat over nullptr, niet 0
- kcoredirlister lstItems benchmark
- [KSambaShare] bestand controleren die is gewijzigd voor herladen
- [KDirOperator] alternerende achtergrondkleuren gebruiken voor weergave van meerdere kolommen
- geheugenlekken vermijden in slave-jobs (bug 396651)
- SlaveInterface: maak setConnection/connection verouderd, niemand kan ze in elk geval gebruiken
- Iets snellere UDS-constructor
- [KFilePlacesModel] ondersteun nette baloosearch URL's
- projects.kde.org websneltoets verwijderen
- Schakel KIO::convertSize() om naar KFormat::formatByteSize()
- Niet verouderde fastInsert gebruiken in file.cpp (eerste van vele die nog komen)
- Gitorious websneltoets vervangen door GitLab
- Bevestigingsdialoog voor actie met prullenbak niet standaard tonen (bug 385492)
- Geen wachtwoorden vragen in kfilewidgettest

### Kirigami

- dynamisch toevoegen en verwijderen van titel ondersteunen (bug 396417)
- actionsVisible introduceren (bug 396413)
- marges aanpassen wanneer schuifbalk verschijnt/verdwijnt
- beter beheer van de grootte (bug 396983)
- Opzetten van het palet optimaliseren
- AbstractApplciationItem zou geen eigen grootte moeten hebben, alleen impliciet
- nieuwe signalen pagePushed/pageRemoved
- logica repareren
- element ScenePosition toevoegen (bug 396877)
- Geen noodzaak om het intermediaire palet voor elke status uit te zenden
- verbergen-&gt;tonen
- Invouwbare zijbalkmodus
- kirigami_package_breeze_icons: lijsten niet als elementen behandelen (bug 396626)
- regexp zoeken/vervangen repareren (bug 396294)
- een kleur animeren produceert een tamelijk onplezierig effect (bug 389534)
- op kleur gefocust item voor navigatie met het toetsenbord
- sneltoets afsluiten verwijderen
- Lange tijd verouderde Encoding=UTF-8 uit bureaubladformatbestand verwijderen
- werkbalkgrootte repareren (bug 396521)
- afmetingen van hendel repareren
- Rekening houden met BUILD_TESTING
- Pictogrammen tonen voor acties die een pictogrambron hebben in plaats van een pictogramnaam

### KItemViews

- Rekening houden met BUILD_TESTING

### KJobWidgets

- Rekening houden met BUILD_TESTING

### KJS

- Rekening houden met BUILD_TESTING

### KMediaPlayer

- Rekening houden met BUILD_TESTING

### KNewStuff

- Lange tijd verouderde Encoding=UTF-8 uit bureaubladformatbestanden verwijderen
- Standaard sorteervolgorde wijzigen naar waardering in de downloaddialoog
- Venstermarges in DownloadDialog repareren om te voldoen aan algemene themamarges 
- Per ongeluk verwijderde qCDebug herstellen
- De juiste QSharedPointer API gebruiken
- Behandel lege voorvertoninglijsten

### KNotification

- Rekening houden met BUILD_TESTING

### KPackage-framework

- Rekening houden met BUILD_TESTING

### KParts

- API dox: praat over nullptr, niet 0

### KPeople

- Rekening houden met BUILD_TESTING

### KPlotting

- Rekening houden met BUILD_TESTING

### KPty

- Rekening houden met BUILD_TESTING

### KRunner

- Rekening houden met BUILD_TESTING

### KService

- API dox: praat over nullptr, niet 0
- Vereis uit-bron-bouwen
- subseq operator toevoegen om sub-sequences overeen te laten komen

### KTextEditor

- juiste reparatie voor de rawtekenreeks indentering automatisch aanhalen
- indenteerder repareren om te kunnen werken met nieuw syntaxisbestand in framework voor accentuering van syntaxis
- test naar nieuwe status in opslagruimte voor accentuering van syntaxis
- Bericht "Regelafgebreking in zoektekst" centraal in het zicht voor betere zichtbaarheid
- waarschuwing repareren, gebruik gewoon isNull()
- API voor scripts uitbreiden
- segfault repareren bij zeldzame gevallen waar lege vector voor aantal woorden verschijnt
- wissen van schuifbalkvoorbeeld bij wissen van document (bug 374630)

### KTextWidgets

- API docs: draai gedeeltelijk een eerdere commit terug, werkte niet echt
- KFindDialog: geef de regelbewerking focus bij tonen van een opnieuw gebruikte dialoog
- KFind: aantal resetten bij wijziging van het patroon (bijv. in de dialoog voor zoeken)
- Rekening houden met BUILD_TESTING

### KUnitConversion

- Rekening houden met BUILD_TESTING

### KWallet Framework

- Rekening houden met BUILD_TESTING

### KWayland

- RemoteAccess-buffers opschonen bij aboutToBeUnbound in plaats van vernietiging van object
- Cursor hints ondersteunen bij vergrendelde aanwijzer
- Onnodig lange wachttijden verkleinen bij falende signaalspionnen
- Selectie en seat auto tests repareren
- Overblijvende V5 compat global includes vervangen
- XDG WM basisondersteuning aan uw XDGShell API toevoegen
- XDGShellV5 samen met XDGWMBase te compileren maken

### KWidgetsAddons

- Invoer masker van KTimeComboBox voor AM/PM tijden repareren (bug 361764)
- Rekening houden met BUILD_TESTING

### KWindowSystem

- Rekening houden met BUILD_TESTING

### KXMLGUI

- KMainWindow die onjuiste widgetinstellingen opslaat repareren (bug 395988)
- Rekening houden met BUILD_TESTING

### KXmlRpcClient

- Rekening houden met BUILD_TESTING

### Plasma Framework

- als een applet ongeldig is, heeft het onmiddellijk UiReadyConstraint
- [Plasma PluginLoader] plug-ins in cache zetten tijdens opstarten
- Vervagende node repareren wanneer er een in de atlas wordt gezet
- [Containment] Acties van container niet laden met plasma/containment_actions KIOSK restricties
- Rekening houden met BUILD_TESTING

### Prison

- Gemengd naar hoofdlettermodus vergrendeling in Aztec code generatie

### Omschrijving

- Nagaan dat debugging voor kf5.kio.core.copyjob is uitgeschakeld voor de test
- "test: een meer "atomische" manier van controle of het signaal zich voordeed" terugdraaien
- test: een meer "atomische" manier van controle of het signaal zich voordeed"
- Bluetooth-plug-in toevoegen
- [Telegram] wacht niet totdat Telegram wordt gesloten
- Voorbereiden om statuskleuren van Arc te gebruiken in de afrollijst van revisies
- Rekening houden met BUILD_TESTING

### QQC2StyleBridge

- Grootte van menu's maken verbeteren (bug 396841)
- Dubbele vergelijking verwijderen
- Ga weg van op tekenreeksen gebaseerde verbindingen
- op geldig pictogram controleren

### Solid

- Rekening houden met BUILD_TESTING

### Sonnet

- Sonnet: setLanguage zou een opnieuw accentueren moeten plannen als accentueren is ingeschakeld
- De huidige hunspell API gebruiken

### Accentuering van syntaxis

- CoffeeScript: sjablonen in ingebedde JavaScript code repareren &amp; escapes toevoegen
- Dit in Definition::includedDefinitions() uitsluiten
- In-class member initialisatie waar mogelijk gebruiken
- functies toevoegen voor toegang tot sleutelwoorden
- Definition::::formats() toevoegen
- QVector&lt;Definition&gt; Definition::includedDefinitions() const toevoegen
- Theme::TextStyle Format::textStyle() const; toevoegen
- C++: standaard floating-point constanten repareren (bug 389693)
- CSS: syntaxis bijwerken en enige fouten repareren
- C++: element voor bijwerken voor c++20 en enige fouten in syntaxis repareren
- CoffeeScript &amp; JavaScript: member-objecten repareren. .ts extensie in JS toevoegen (bug 366797)
- Lua: tekenreeks met meerdere regel repareren (bug 395515)
- RPM Spec: MIME-type toevoegen
- Python: escapes in quoted-commentaar repareren (bug 386685)
- haskell.xml: Prelude data-constructors niet anders accentueren ten opzichte van andere
- haskell.xml: types uit sectie "prelude function" verwijderen
- haskell.xml: gepromote data-constructors accentueren
- haskell.xml: trefwoorden family, forall, pattern toevoegen
- Rekening houden met BUILD_TESTING

### ThreadWeaver

- Rekening houden met BUILD_TESTING

### Beveiligingsinformatie

De vrijgegeven code is ondertekend met GPG met de volgende sleutel: pub rsa2048/58D0EE648A48B3BB 2016-09-05 David Faure &lt;faure@kde.org&gt; Vingerafdruk van primaire sleutel: 53E6 B47B 45CE A3E0 D5B7  4577 58D0 EE64 8A48 B3BB

U kunt discussiëren en ideeën delen over deze uitgave in de section voor commentaar van <a href='https://dot.kde.org/2014/07/07/kde-frameworks-5-makes-kde-software-more-accessible-all-qt-developers'>het artikel in the dot</a>.
