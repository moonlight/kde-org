---
aliases:
- ../announce-applications-16.04.0
changelog: true
date: 2016-04-20
description: KDE stelt KDE Applicaties 16.04.0 beschikbaar
layout: application
title: KDE stelt KDE Applicaties 16.04.0 beschikbaar
version: 16.04.0
---
20 april 2016. KDE introduceert vandaag KDE Applications 16.04 met een indrukwekkende hoeveelheid opwaarderingen wanneer het gaat om gemakkelijke toegang, de introductie van zeer nuttige functionaliteiten en reparatie van die kleinere problemen die er toe leiden dat KDE Applications nu een stap dichter is bij het bieden van de perfecte setup voor uw apparaat.

<a href='https://www.kde.org/applications/graphics/kcolorchooser/'>KColorChooser</a>, <a href='https://www.kde.org/applications/utilities/kfloppy/'>KFloppy</a>, <a href='https://www.kde.org/applications/games/kmahjongg/'>KMahjongg</a> en <a href='https://www.kde.org/applications/internet/krdc/'>KRDC</a> zijn nu overgezet naar KDE Frameworks 5 en we zien uit naar uw terugkoppeling en inzicht in de nieuwste mogelijkheden die met deze uitgave zijn geïntroduceerd. We willen ook uw ondersteuning voor <a href='https://www.kde.org/applications/education/minuet/'>Minuet</a> ten zeerste aanmoedigen nu we het verwelkomen in onze KDE Applications en uw input over wat u nog meer wilt zien.

### De nieuwste toevoeging

{{<figure src="/announcements/applications/16.04.0/minuet1604.png" class="text-center" width="600px">}}

Er is een nieuwe toepassing toegevoegd aan de KDE suite voor het onderwijs. <a href='https://minuet.kde.org'>Minuet</a> is software voor Muziekonderwijs met functies voor volledige ondersteuning van MIDI met tempo, toonhoogte en volumebesturing, wat het bruikbaar maakt voor zowel nieuwe als ervaren musici.

Minuet bevat 44 oefeningen voor het trainen van het oor met betrekking tot notenbalken, akkoorden, intervallen en ritmes, die de visualisatie van muzikale inhoud op de toetsen van de piano inschakelt en de naadloze integratie van uw eigen oefeningen toestaat.

### Meer hulp voor u

{{<figure src="/announcements/applications/16.04.0/khelpcenter1604.png" class="text-center" width="600px">}}

KHelpCenter dat eerder werd gedistribueerd onder Plasma wordt nu gedistribueerd als onderdeel van KDE Applications.

Een zeer grote inspanning voor oplossen van bugs en een opschoningscampagne door het team van KHelpCenter heeft geresulteerd in 49 gerepareerde bugs, waarvan er veel gerelateerd waren aan verbeteringen en herstellen van de eerder niet-functionele zoekfunctionaliteit.

Het interne zoeken in documenten dat afhing van de verouderde software ht::/dig is vervangen door een nieuw op Xapian gebaseerde indexerings- en zoeksysteem wat heeft geleid tot het herstel van functionaliteiten zoals zoeken in manpagina's, info-pagina's en door KDE geleverde Documentatie van software, met de toegevoegde voorziening van bladwijzers voor de documentatiepagina's.

Met de verwijdering van KDELibs4 Support en verder opschonen van de code en enige andere kleine bugs, is het onderhoud van de code nu ook volledig gemeubileerd om u KHelpCenter in een nieuwe stralende vorm te geven.

### Agressieve bestrijding van problemen

In de Kontact suite zijn zijn het aanzienlijke aantal van 55 bugs opgelost; sommige waren gerelateerd aan problemen met het instellen van herinneringen en in het importeren van e-mails uit Thunderbird, verkleinen van pictogrammen van Skype &amp; Google talk in de weergave van het Contactpersonenpaneel, aan KMail gerelateerde workarounds zoals importeren van mappen, vCard's, openen van ODF e-mailbijlagen, URL-invoegingen uit Chromium, de verschillen in het menu hulpmiddelen met de app gestart als onderdeel van Kontact in tegenstelling tot alleenstaand gebruik, een ontbrekend menu-item 'Verzenden' en enkele andere. De ondersteuning voor Braziliaans-Portugese RSS-feeds is toegevoegd samen met het repareren van de adressen voor de Hongaarse en Spaanse feeds.

De nieuwe functies omvatten een opnieuw ontworpen bewerker van contactpersonen in KAddressbook, een nieuw standaard headerthema in KMail, verbeteringen in het exporteren van instellingen en reparatie van ondersteuning van favicons in Akregator. Het interface van de opsteller van KMail is opgeschoond samen met de introductie van een nieuw standaard headerthema van KMail met het Grantlee thema gebruikt voor de pagina 'Info over' in KMail evenals in Kontact en Akregator. Akregator gebruikt nu QtWebKit - een van de hoofdengines om webpagina's te renderen en voert javascriptcode uit als de webengine voor renderen en het proces van implementeren van ondersteuning voor QtWebEngine in Akregator en andere toepassingen in de Kontact Suite is al begonnen. Terwijl verschillende functies verplaatst zijn naar plug-ins in kdepim-addons, zijn de pim-bibliotheken gesplitst in vele pakketten.

### Details voor archivering

{{<figure src="/announcements/applications/16.04.0/ark1604.png" class="text-center" width="600px">}}

<a href='https://www.kde.org/applications/utilities/ark/'>Ark</a>, de beheerder van archieven, heeft twee grote reparaties van bugs ondergaan zodat Ark nu de gebruiker waarschuwt als een uitpakken mislukt vanwege onvoldoende ruimte in de bestemmingsmap en het vult niet alle RAM bij het uitpakken van enorme bestanden via slepen en loslaten.

Ark bevat nu ook een eigenschappendialoog die informatie toont zoals het type archief, gecomprimeerde en niet gecomprimeerde grootte, MD5/SHA-1/SHA-256 cryptografische hashes over het nu geopende archief.

Ark kan nu ook RAR-archieven openen en uitpakken zonder gebruik van de niet-vrije rar-hulpmiddelen en kan TAR-archieven openen, uitpakken en maken die gecomprimeerd zijn met de formaten lzop/lzip/lrzip.

Het gebruikersinterface van Ark is gepolijst door het reorganiseren van de menubalk en de werkbalk om de bruikbaarheid te verbeteren en dubbelzinnigheid te verwijderen, evenals besparen op verticale ruimte omdat de statusbalk nu standaard is verborgen.

### Meer precisie bij bewerken van video

{{<figure src="/announcements/applications/16.04.0/kdenlive1604.png" class="text-center" width="600px">}}

<a href='https://www.kde.org/applications/multimedia/kdenlive/'>Kdenlive</a>, de niet-lineaire videobewerker heeft vele nieuwe functies geïmplementeerd gekregen. De maker van titels heeft nu een raster mogelijkheid, gradiënten, de toevoeging van schaduw in tekst en aanpassen van de ruimte tussen letters en lijnen

Geïntegreerd tonen van geluidsniveaus biedt gemakkelijker monitoring van het geluid in uw project samen met nieuwe overlays op de videomonitor, die framerate van afspelen, veilige zones en golfvormen van geluid toont en een werkbalk om markeringen te zoeken en een zoommonitor.

Er is ook een nieuwe bibliotheekfunctie die kopiëren/plakken van sequenties tussen projecten mogelijk maakt en een opgespliste weergave in de tijdlijn om de effecten toegepast op de clip vergelijkt en visualiseert met diegene zonder deze.

De dialoog voor renderen is herschreven met een toegevoegde optie om snellere codering te behalen en dus, grotere bestanden kunnen maken en het versnellingseffect is zo gemaakt dat het ook met geluid werkt.

Krommen in keyframes zijn geïntroduceerd voor een paar effecten en nu kunt u snel toegang krijgen tot uw meest gekozen effecten via het widget met uw favoriete effecten. Bij gebruik van het hulpmiddel scheermes, zullen nu de verticale lijnen in de tijdlijn het accurate frame tonen waar het snijden zal worden uitgevoerd en de nieuw toegevoegde clipgeneratoren zullen u in staat stellen om kleurbalkclips en tellers te maken. Hiernaast is het aantal gebruikte clips opnieuw geïntroduceerd in de Project-bin en de audiominiaturen zijn ook herschreven om ze veel sneller te maken.

De belangrijkste reparatie van bugs bevatten de crash bij het gebruik van titels (die de nieuwste MLT versie vereist), het repareren van fouten wanneer de frames per seconde van het project iets anders is dan 25, de crashes bij verwijderen van tracks, de problematische overschrijfmodus in de tijdlijn en de fouten/verloren effecten bij gebruik van een taalcode met een komma als het scheidingsteken. Los hiervan heeft het team consistent gewerkt aan grote verbeteringen in stabiliteit, werkmethode en kleine gebruiksmogelijkheden.

### En meer!

<a href='https://www.kde.org/applications/graphics/okular/'>Okular</a>, de viewer van documenten brengt nieuwe functies in de vorm van aan te passen inline breedte van randen van annotaties, mogelijkheid om direct ingebedde bestanden te openen in plaats van alleen ze op te slaan en ook het tonen van een markeringen voor een inhoudsopgave wanneer de onderliggende koppelingen zijn ingevouwen.

<a href='https://www.kde.org/applications/utilities/kleopatra'>Kleopatra</a> introduceert verbeterde ondersteuning voor GnuPG 2.1 met een reparatie van zelftestfouten en de problematische verversing veroorzaakt door de nieuwe indeling van de GnuPG (GNU Privacy Guard) map. Generatie van ECC Key is nu toegevoegd met het tonen van Curve-details voor ECC-certificaten. Kleopatra wordt nu vrijgegeven als een apart pakket en ondersteuning voor .pfx en .crt bestanden is meegenomen. Om het verschil in gedrag van geïmporteerde geheime sleutels en gegenereerde sleutels op te lossen stelt Kleopatra nu ter beschikking om het vertrouwen van de eigenaar voor geïmporteerde geheime sleutels op volledig in te stellen. 
