---
aliases:
- ../announce-applications-17.08.3
changelog: true
date: 2017-11-09
description: KDEk, KDE Aplikazioak 17.08.3 kaleratzen du
layout: application
title: KDEk, KDE Aplikazioak 17.08.3 kaleratzen du
version: 17.08.3
---
November 9, 2017. Today KDE released the third stability update for <a href='../17.08.0'>KDE Applications 17.08</a>. This release contains only bugfixes and translation updates, providing a safe and pleasant update for everyone.

About a dozen recorded bugfixes include improvements to Kontact, Ark, Gwenview, KGpg, KWave, Okular, Spectacle, among others.

This release also includes the last version of KDE Development Platform 4.14.38.

Improvements include:

- Work around a Samba 4.7 regression with password-protected SMB shares
- Okular no longer crashes after certain rotation jobs
- Ark preserves file modification dates when extracting ZIP archives
