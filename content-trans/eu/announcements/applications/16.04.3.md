---
aliases:
- ../announce-applications-16.04.3
changelog: true
date: 2016-07-12
description: KDEk, KDE Aplikazioak 16.04.3 kaleratzen du
layout: application
title: KDEk, KDE Aplikazioak 16.04.3 kaleratzen du
version: 16.04.3
---
July 12, 2016. Today KDE released the third stability update for <a href='../16.04.0'>KDE Applications 16.04</a>. This release contains only bugfixes and translation updates, providing a safe and pleasant update for everyone.

More than 20 recorded bugfixes include improvements to ark, cantor, kate. kdepim, umbrello, among others.

This release also includes Long Term Support version of KDE Development Platform 4.14.22.
