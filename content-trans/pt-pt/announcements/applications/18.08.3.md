---
aliases:
- ../announce-applications-18.08.3
changelog: true
date: 2018-11-08
description: O KDE Lança as Aplicações do KDE 18.08.3
layout: application
title: O KDE Lança as Aplicações do KDE 18.08.3
version: 18.08.3
---
8 de Novembro de 2018. Hoje o KDE lançou a terceira actualização de estabilidade para as <a href='../18.08.0'>Aplicações do KDE 18.08</a>. Esta versão contém apenas correcções de erros e actualizações de traduções, pelo que será uma actualização segura e agradável para todos.

As mais de 20 correcções de erros registadas incluem as melhorias no Kontact, no Ark, no Dolphin, nos Jogos do KDE, no Kate, no Okular, no Umbrello, entre outros.

As melhorias incluem:

- A visualização em HTML no KMail fica recordada e volta de novo a carregar as imagens externas, se tiver permissão para tal
- O Kate agora recorda os meta-dados (incluindo os favoritos) entre as sessões de edição
- O deslocamento automático na UI de texto do Telepathy foi corrigida com as versões mais recentes do QtWebEngine
