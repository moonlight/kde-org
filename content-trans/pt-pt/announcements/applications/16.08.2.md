---
aliases:
- ../announce-applications-16.08.2
changelog: true
date: 2016-10-13
description: O KDE Lança as Aplicações do KDE 16.08.2
layout: application
title: O KDE Lança as Aplicações do KDE 16.08.2
version: 16.08.2
---
13 de Outubro de 2016. Hoje o KDE lançou a segunda actualização de estabilidade para as <a href='../16.08.0'>Aplicações do KDE 16.08</a>. Esta versão contém apenas correcções de erros e actualizações de traduções, pelo que será uma actualização segura e agradável para todos.

As mais de 30 correcções de erros registadas incluem as melhorias nos módulos 'kdepim', no 'ark', no 'dolphin', no 'kgpg', no 'kolourpaint', no 'okular', entre outros.

Esta versão também inclui as versões de Suporte de Longo Prazo da Plataforma de Desenvolvimento do KDE 4.14.25.
