---
aliases:
- ../announce-applications-16.12-beta
date: 2016-11-18
description: O KDE Lança as Aplicações do KDE 16.12 Beta.
layout: application
release: applications-16.11.80
title: O KDE Lança a Versão 16.12 Beta das Aplicações
---
18 de Novembro de 2016. Hoje o KDE lançou a segunda das versões beta das Aplicações do KDE. Com as dependências e as funcionalidades estabilizadas, o foco da equipa do KDE é agora a correcção de erros e mais algumas rectificações.

Veja mais informações nas <a href='https://community.kde.org/Applications/16.12_Release_Notes'>notas de lançamento da comunidade</a> sobre novos pacotes, pacotes que sejam agora baseados no KF5 e problemas conhecidos. Será disponibilizado um anúncio mais completo para a versão final

As versões Aplicações do KDE 16.12 precisam de testes aprofundados para manter e melhorar a qualidade e a experiência do utilizador. Os utilizadores actuais são críticos para manter a alta qualidade do KDE, dado que os programadores não podem simplesmente testar todas as configurações possíveis. Contamos consigo para nos ajudar a encontrar erros antecipadamente, para que possam ser rectificados antes da versão final. Por favor, pense em juntar-se à equipa, instalando a versão beta e <a href='https://bugs.kde.org/'>comunicando todos os erros encontrados</a>.
