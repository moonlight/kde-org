---
aliases:
- ../../kde-frameworks-5.14.0
date: 2015-09-12
layout: framework
libCount: 60
src: /announcements/frameworks/5-tp/KDE_QT.jpg
---
### Em muitas plataformas

- Mudança do nome das classes privadas para evitar exportá-las sem querer

### Baloo

- Adição da interface org.kde.baloo ao objecto de topo, por fins de compatibilidade
- Instalação de um org.kde.baloo.file.indexer.xml falso para corrigir a compilação do plasma-desktop 5.4
- Reorganização das interfaces de D-Bus
- Uso dos meta-dados de JSON no 'plugin' do 'kded' e correcção do nome do 'plugin'
- Criação de uma instância de Database por processo (erro 350247)
- Evitar que o baloo_file_extractor seja morto na consolidação dos dados
- Geração de um ficheiro de interface em XML, usando o qt5_generate_dbus_interface
- Correcções do monitor do Baloo
- Passagem da exportação do URL de ficheiros para a tarefa principal
- Certificação de que as configurações encadeadas são tidas em conta
- Não instalar o 'namelink' nas bibliotecas privadas
- Instalação das traduções, detectado por Hrvoje Senjan.

### BluezQt

- Não encaminhar o sinal 'deviceChanged' após a remoção do ficheiro (erro 351051)
- Respeito do -DBUILD_TESTING=OFF

### Módulos Extra do CMake

- Adição de macro para gerar as declarações de categorias no registo do Qt5.
- ecm_generate_headers: Adição da opção COMMON_HEADER e da funcionalidade de inclusões múltiplas
- Adição do -pedantic no código do KF5 (ao usar o 'gcc' ou o 'clang')
- KDEFrameworkCompilerSettings: só activar os iteradores restritos no modo de depuração
- Activação também da visibilidade predefinida para o código em C esconder.

### Integração da Plataforma

- Propagar também os títulos das janelas para as janelas de ficheiros apenas em pastas.

### KActivities

- Só iniciar um carregador de acções (tarefa) quando as acções do FileItemLinkingPlugin não estiverem inicializadas (erro 351585)
- Correcção dos problemas de compilação introduzidos ao mudar o nome das classes Private (11030ffc0)
- Adição do local de inclusão do Boost em falta à compilação no OS X
- A definição dos atalhos passou para a configuração da actividade
- A configuração do modo privado da actividade funciona agora
- Remodelação da UI de configuração
- Os métodos básicos de actividades estão funcionais
- UI da mensagens de configuração e remoção da actividade
- UI básica da secção de criação/remoção/configuração de actividades no KCM
- Aumento do tamanho dos blocos para carregar os resultados
- Adição de uma inclusão em falta do std::set

### Ferramentas de Doxygen do KDE

- Correcção no Windows: remoção dos ficheiros existentes antes de serem substituídos com o os.rename.
- Usar os locais nativos, ao invocar o Python, paa corrigir as compilações no Windows

### KCompletion

- Correcção de um comportamento inválido / erro de memória no Windows (erro 345860)

### KConfig

- Optimização do readEntryGui
- Evitar o QString::fromLatin1() no código gerado
- Minimização das chamadas ao dispendioso QStandardPaths::locateAll()
- Correcção da migração para o QCommandLineParser (tem agora um addPositionalArgument)

### Suporte para a KDELibs 4

- Migração do 'plugin' 'solid-networkstatus' do 'kded' para os meta-dados em JSON
- KPixmapCache: criação da pasta, caso não exista

### KDocTools

- Sincronização do 'user.entities' Catalão com a versão em Inglês.
- Adição das entidades para o 'sebas' e o 'plasma-pa'

### KEmoticons

- Performance: criação de uma instância do KEmoticons e não do KEmoticonsTheme.

### KFileMetaData

- PlainTextExtractor: activação da ramificação O_NOATIME nas plataformas 'libc' da GNU
- PlainTextExtractor: fazer a versão de Linux funcionar também sem o O_NOATIME
- PlainTextExtractor: correcção dos problemas na verificação de erros no open(O_NOATIME)

### KGlobalAccel

- Só iniciar o kglobalaccel5 se necessário.

### KI18n

- Lidar de forma ordeira com a ausência de fim-de-linha no fim do ficheiro 'pmap'

### KIconThemes

- KIconLoader: correcção do reconfigure(), ignorando os temas herdados e as pastas de aplicações
- Melhor adesão à especificação do carregamento de ícones

### KImageFormats

- eps: correcção das inclusões relacionadas com o Registo por Categorias do Qt

### KIO

- Uso do Q_OS_WIN em vez do Q_OS_WINDOWS
- Fazer o KDE_FORK_SLAVES funcionar em Windows
- Desactivação da instalação do ficheiro 'desktop' do módulo ProxyScout do 'kded'
- Oferta de uma ordenação determinista para o KDirSortFilterProxyModelPrivate::compare
- Apresentação dos ícones das pastas personalizadas de novo (erro 350612)
- Passagem do 'kpasswdserver' do 'kded' para o 'kiod'
- Correcção dos erros de migração no kpasswdserver
- Remoção de código antigo para falar com as versões mesmo antigas do 'kpasswdserver'.
- KDirListerTest: uso do QTRY_COMPARE em ambas as instruções, para corrigir um erro de concorrência demonstrado pelo CI
- KFilePlacesModel: implementação do item pendente antigo sobre a utilização do 'trashrc' em vez de um KDirLister completo.

### KItemModels

- Novo modelo 'proxy': KConcatenateRowsProxyModel
- KConcatenateRowsProxyModelPrivate: correcção do tratamento do 'layoutChanged'.
- Mais verificações sobre a selecção após a ordenação.
- KExtraColumnsProxyModel: correcção de um erro no sibling() que danificou p.ex. as selecções

### Plataforma de Pacotes

- O 'kpackagetool' pode desinstalar um pacote a partir do seu ficheiro respectivo
- O 'kpackagetool' é agora mais inteligente a descobrir o tipo de serviço correcto

### KService

- KSycoca: verificação das datas/horas e execução do 'kbuildsycoca' se necessário. Não existem mais dependências do 'kded'.
- Não fechar o 'ksycoca' logo após a sua abertura.
- O KPluginInfo agora lida correctamente com os meta-dados do FormFactor

### KTextEditor

- Junção da alocação do TextLineData com o bloco de contagem de referências.
- Correcção do atalho de teclado predefinido para "ir para a linha de edição anterior"
- Correcção dos comentários no realce de sintaxe de Haskell
- Melhoria de performance na aparição da área de completação de código
- minimap: Tentativa de melhoria na aparência e comportamento (erro 309553)
- comentários encadeados no realce de sintaxe do Haskell
- Correcção de problema com a remoção de indentação problemática no Python (erro 351190)

### KWidgetsAddons

- KPasswordDialog: deixar o utilizador mudar a visibilidade da senha (erro 224686)

### KXMLGUI

- Correcção do KSwitchLanguageDialog a não mostrar mais línguas

### KXmlRpcClient

- Evitar o uso do QLatin1String sempre que alocar memória de dados

### ModemManagerQt

- Correcção do conflito de meta-dados com a última alteração do 'nm-qt'

### NetworkManagerQt

- Adição de novas propriedades das últimas versões do NM

### Plataforma do Plasma

- mudar de novo para intermitente se possível
- correcção da listagem de pacotes
- plasma: Correcção em que as acções da 'applet' podem ser um ponteiro nulo (erro 351777)
- O sinal 'onClicked' do PlasmaComponents.ModelContextMenu agora funciona correctamente
- O PlasmaComponents.ModelContextMenu pode agora criar secções Menu
- Migração do 'plugin' 'platformstatus' do 'kded' para meta-dados em JSON...
- Tratamento de meta-dados inválidos no PluginLoader
- Permissão ao RowLayout para descobrir o tamanho da legenda de texto
- mostrar sempre o menu de edição quando o cursor está visível
- Correcção de ciclo no ButtonStyle
- Não alterar o modo plano de um botão quando for carregado
- as barras de posicionamento em ecrãs por toque ou dispositivos móveis são transitórias
- ajuste na velocidade&amp;aceleração da intermitência à resolução em DPI
- delegação de cursores personalizada apenas em dispositivos móveis
- cursor de texto amigável para ecrãs por toque
- correcção dos itens-pai e política e aparição
- declaração do __editMenu
- adição das delegações de tratamento do cursor em falta
- remodelação da implementação do EditMenu
- uso do menu móvel apenas a título condicional
- mudança do item-pai do menu para o topo

Poderá discutir e partilhar ideias sobre esta versão na secção de comentários do <a href='https://dot.kde.org/2014/07/07/kde-frameworks-5-makes-kde-software-more-accessible-all-qt-'>artigo do Dot</a>.
