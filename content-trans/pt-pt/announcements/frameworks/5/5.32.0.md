---
aliases:
- ../../kde-frameworks-5.32.0
date: 2017-03-11
layout: framework
libCount: 70
src: /announcements/frameworks/5-tp/KDE_QT.jpg
---
### Baloo

- Implementação de marcas encadeadas

### Ícones do Brisa

- Adição de novos ícones do Plasma Vault
- Mudança do nome dos ícones para as pastas codificadas e descodificadas
- Adição do ícone a 22px das torrentes
- Adição de ícones do 'nm-tray' (erro 374672)
- gestão de cores: remoção de ligações indefinidas (erro 374843)
- O 'system-run' é agora uma acção até &lt;= 32px e 48px é um ícone da aplicação (erro 375970)

### Módulos Extra do CMake

- Detecção do 'inotify'
- Reversão da opção "Marcar automaticamente as classes com funções virtuais puras como /Abstract/".

### KActivitiesStats

- Permitir o planeamento antecipado e definição da ordem de um item ainda fora da lista

### KArchive

- Correcção de potencial fuga de memória apontada por 'limitedDev'

### KCMUtils

- Correcção de potencial estoiro nos KCM's em QML quando muda a paleta da aplicação

### KConfig

- KConfig: parar a exportação e instalação do KConfigBackend

### KConfigWidgets

- KColorScheme: uso do esquema da aplicação se for definido pelo KColorSchemeManager (erro 373764)
- KConfigDialogManager: obter a mudança do sinal do 'metaObject' ou de uma propriedade especial
- Correcção da verificação de erros do Fix KCModule::setAuthAction

### KCoreAddons

- Excluir o (6) do reconhecimento de ícones emotivos
- KDirWatch: correcção de fuga de memória ao destruir

### Suporte para a KDELibs 4

- Correcção de erro no kfiledialog.cpp que provoca um estoiro quando são usados elementos gráficos nativos

### KDocTools

- meinproc5: ligação para os ficheiros, não para a biblioteca (erro 377406)
- Remoção da biblioteca estática KF5::XsltKde
- Exportação de uma biblioteca dinâmica adequada para o KDocTools
- Migração para um registo de dados por categorias e limpeza de inclusões
- Adição de função para extrair um único ficheiro
- Interrupção prévia da compilação se o 'xmllint' não estiver disponível (erro 376246)

### KFileMetaData

- Novo responsável de manutenção do 'kfilemetadata'
- [ExtractorCollection] Uso da herança de tipos MIME para devolver os 'plugins'
- adição de uma nova propriedade DiscNumber (nº do disco) para os ficheiros de áudio de álbuns multi-discos

### KIO

- KCM de 'Cookies': desactivação do botão "remover" quando não existe nenhum item actual
- kio_help: Uso da nova biblioteca dinâmica exportada pelo KDocTools
- kpac: Limpeza dos URL's antes de os passar ao FindProxyForURL (erro de segurança)
- Importação do IO-slave 'remote' do 'plasma-workspace'
- kio_trash: implementação da mudança de nome dos ficheiros e pastas de topo
- PreviewJob: Remoção do tamanho máximo dos ficheiros locais por omissão
- DropJob: permitir a adição de acções da aplicação num menu aberto
- ThumbCreator: descontinuação do DrawFrame, como foi discutido em https://git.reviewboard.kde.org/r/129921/

### KNotification

- Adição do suporte para os portais Flatpak
- Envio do 'desktopfilename' como parte das dicas 'notifyByPopup'
- [KStatusNotifierItem] Reposição da janela minimizada como normal

### Plataforma KPackage

- Finalização do suporte para a abertura de pacotes comprimidos

### KTextEditor

- Recordação do tipo de ficheiro definido pelo utilizador entre sessões
- Reposição do tipo de ficheiro ao abrir um URL
- Adição de método de leitura do valor de configuração do 'word-count' (contagem de palavras)
- Conversão consistente de/para cursor de/para coordenadas
- Actualização do tipo de ficheiro, apenas ao gravar, se a localização mudar
- Suporte para os ficheiros de configuração EditorConfig (para mais detalhe: http://editorconfig.org/)
- Adição do FindEditorConfig ao ktexteditor
- Correcção: a acção 'emmetToggleComment' não funciona (erro 375159)
- Uso da capitalização de frases com os textos de legendas dos campos de edição
- Inversão do significado do :split, :vsplit para corresponder as acções do 'vi' com o Kate
- Uso do log2() do C++11  em vez do log() / log(2)
- KateSaveConfigTab: colocação de espaço atrás do último grupo na página Avançado, não dentro
- KateSaveConfigTab: Remoção de margem errada em torno do conteúdo da página Avançado
- Sub-página de configuração dos contornos: correcção da lista de visibilidade da barra deslocamento, que estava fora do lugar

### KWidgetsAddons

- KToolTipWidget: esconder a dica no 'enterEvent' se o 'hideDelay' for zero
- Correcção da perda de foco do KEditListWidget ao carregar nos botões
- Adição da decomposição das Sílabas Hangul em Jamo do Hangul
- KMessageWidget: correcção do comportamento nas chamadas sobrepostas do animatedShow/animatedHide

### KXMLGUI

- Não usar as chaves do KConfig com barras invertidas (\)

### NetworkManagerQt

- Sincronização das introspecções e dos ficheiros gerados com o NM 1.6.0
- Gestor: Correcção do 'deviceAdded' emitido em duplicado quando é reiniciado o NM

### Plataforma do Plasma

- definição das dicas por omissão quando o 'repr' não exporta o Layout.* (erro 377153)
- possibilidade de definir 'expanded=false' num contentor
- [Menu] Melhoria da correcção do espaço disponível para o openRelative
- passagem da lógica do 'setImagePath' para o updateFrameData() (erro 376754)
- IconItem: Adição da propriedade 'roundToIconSize'
- [SliderStyle] Possibilidade de definição de um elemento "hint-handle-size"
- Juntar todas as ligações à acção no QMenuItem::setAction
- [ConfigView] Respeitar as restrições do Módulo de Controlo do KIOSK
- Correcção da desactivação da animação da rotação, quando o indicador de Ocupado não tem opacidade
- [FrameSvgItemMargins] Não actualizar no caso de um 'repaintNeeded'
- Ícones da 'applet' do Plasma Vault
- Migração do AppearAnimation e do DisappearAnimation para Animators
- Alinhamento do extremo inferior com o extremo superior do 'visualParent' no caso do TopPosedLeftAlignedPopup
- [ConfigModel] Emissão de um 'dataChanged' quando uma ConfigCategory muda
- [ScrollViewStyle] Avaliação da propriedade 'frameVisible'
- [Estilos dos Botões] Uso do Layout.fillHeight em vez do 'parent.height' num Layout (erro 375911)
- [ContainmentInterface] Alinhar também o menu de contexto do contentor ao painel

### Prisão

- Correcção da versão mínima do Qt

### Solid

- As disquetes aparecem agora como "Disquete" em vez de "Dispositivo Removível de 0 B"

### Realce de Sintaxe

- Adição de mais palavras-chave. Desactivação da verificação ortográfica das palavras-chave
- Adição de mais palavras-chave
- Adição da extensão de ficheiro *.RHTML ao realce de sintaxe do Ruby on Rails (erro 375266)
- Actualização do realce de sintaxe do SCSS e do CSS (erro 376005)
- Realce do 'less': Correcção dos comentários de linhas únicas que iniciam regiões novas
- Realce de LaTeX: correcção do ambiente 'alignat' (erro 373286)

### Informação de segurança

O código lançado foi assinado com GPG, usando a seguinte chave: pub rsa2048/58D0EE648A48B3BB 2016-09-05 David Faure &lt;faure@kde.org&gt; Impressão digital da chave primária: 53E6 B47B 45CE A3E0 D5B7  4577 58D0 EE64 8A48 B3BB

Poderá discutir e partilhar ideias sobre esta versão na secção de comentários do <a href='https://dot.kde.org/2014/07/07/kde-frameworks-5-makes-kde-software-more-accessible-all-qt-'>artigo do Dot</a>.
