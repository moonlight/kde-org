---
aliases:
- ../../kde-frameworks-5.51.0
date: 2018-10-15
layout: framework
libCount: 70
src: /announcements/frameworks/5-tp/KDE_QT.jpg
---
### Baloo

- Adição das chamadas ao KIO::UDSEntry::reserve nos IO-slaves 'timeline'/'tags'
- [balooctl] Escoamento da linha em 'buffer' "Indexing &lt;ficheiro&gt;" a iniciar a indexação
- [FileContentIndexer] Ligação do sinal 'finished' do processo de extracção
- [PositionCodec] Eliminação de estoiro no caso de dados corrompidos (erro 367480)
- Correcção de constante de caracteres inválida
- [Balooctl] remoção da verificação da pasta-mãe (erro 396535)
- Possibilidade de remoção de pastas não-existentes nas listas de inclusão e exclusão (erro 375370)
- Uso de String para guardar o UDS_USER e o UDS_GROUP do tipo String (erro 398867)
- [tags_kio] Correcção de parêntesis. Por alguma razão passou a verificação do código.
- Exclusão dos ficheiros do 'genome' na indexação

### BluezQt

- Implementação da API do Media e MediaEndpoint

### Ícones do Brisa

- Correcção de "uso-da-pilha-fora-do-âmbito" detectado pelo ASAN no CI
- Correcção de folhas de estilo em falta nos ícones monocromáticos
- Modificação do 'drive-harddisk' para um estilo mais adaptativo
- Adição dos ícones 'firewall-config' e 'firewall-applet'
- Tornar o cadeado visível no 'plasmavault' com o Brisa Escuro
- Adição do símbolo 'mais' no 'document-new.svg' (erro 398850)
- Adição de ícones para a escala 2x

### Módulos Extra do CMake

- Compilação das interfaces de Python com as mesmas opções de SIP usadas pelo PyQt
- Android: Permissão da passagem de locais relativos como pasta do APK
- Android: Definição adequada de uma alternativa para as aplicações que não têm um manifesto
- Android: Verificação de que as traduções do Qm são carregadas
- Correcção das compilações em Android que usam o Cmake 3.12.1
- l10n: Correcção dos algarismos correspondentes no nome do repositório
- Adição do QT_NO_NARROWING_CONVERSIONS_IN_CONNECT como opção predefinida na compilação
- Interfaces: Correcção do tratamento de código que contém UTF-8
- Iteração de facto pelo CF_GENERATED, em vez de verificar o item 0 a toda a hora

### KActivities

- Correcção de referência 'pendurada' com o "auto" a mudar para "QStringBuilder"

### KCMUtils

- gerir os eventos de retorno
- Dimensionamento manual do KCMUtilDialog para o sizeHint() (erro 389585)

### KConfig

- Correcção de problema ao ler listas de localizações
- O kcfg_compiler agora documenta as entradas válidas para o seu tipo 'Color'

### KFileMetaData

- remoção da implementação própria da conversão de QString para TString no 'taglibwriter'
- aumento da cobertura de testes do 'taglibwriter'
- implementação de mais marcas básicas no 'taglibwriter'
- remoção da utilização da função de conversão própria de TString para QString
- aumento da versão obrigatória da 'taglib' para a 1.11.1
- implementação da leitura das marcas 'replaygain' (ganhos de reprodução)

### KHolidays

- adição dos feriados da Costa do Marfim (Francês) (erro 398161)
- holiday*hk** - correcção da data do Festival Tuen Ng em 2019 (erro 398670)

### KI18n

- Limitar adequadamente a mudança de âmbito das CMAKE_REQUIRED_LIBRARIES
- Android: Validação de que são pesquisados os ficheiros .mo no local correcto

### KIconThemes

- Início do desenho dos emblemas no canto inferior direito

### KImageFormats

- kimg_rgb: optimização do QRegExp e do QString::fromLocal8Bit
- [EPS] Correcção de estoiro no encerramento da aplicação (ao tentar persistir uma imagem na área de transferência) (erro 397040)

### KInit

- Menos lixo nos registos, ao não verificar a existência de ficheiros com nome vazio (erro 388611)

### KIO

- possibilidade de direccionamento de um file:// não local para um URL De WebDAV do Windows
- [KFilePlacesView] Mudança de ícone no item do menu de contexto no painel de Locais
- [Painel de Locais] uso de um ícone de rede mais apropriado
- [KPropertiesDialog] Mostrar a informação de montagem das pastas em '/' (raiz)
- Correcção da remoção de ficheiros do DAV (erro 355441)
- Evitar o uso do QByteArray::remove no AccessManagerReply::readData (erro 375765)
- Não tentar repor locais do utilizador inválidos
- Possibilidade de mudar para a pasta acima, mesmo com barras finais no URL
- Os estoiros dos 'KIO slaves' agora são tratados pelo KCrash em vez de código personalizado inadequado
- Correcção da criação de um ficheiro a partir do conteúdo colado da área de transferência só aparecer ao fim de algum tempo
- [PreviewJob] Envio dos 'plugins' de miniaturas activos para o 'slave' 'thumbnail' (erro 388303)
- Melhoria da mensagem de erro "espaço em disco insuficiente"
- IKWS: uso do "X-KDE-ServiceTypes" não-obsoleto na geração do ficheiro .desktop
- Correcção do cabeçalho do destino do WebDAV nas operações COPY e MOVE
- Avisar o utilizador antes da operação de cópia/movimentação se não existir espaço disponível suficiente (erro 243160)
- Passagem do KCM de SMB para a categoria de Configuração da Rede
- trash: Correcção do processamento da 'cache' de tamanhos das pastas
- kioexecd: monitorização das criações ou modificações dos ficheiros temporários (erro 397742)
- Não desenhar contornos e sombras em torno das imagens com transparência (erro 258514)
- Correcção do ícone do tipo de ficheiro na janela de propriedades desenhado com borrões em ecrãs de PPP's elevados

### Kirigami

- abertura adequada da área ao arrastar a pega
- margem extra quando a 'globaltoolbar' da 'pagerow' é uma ToolBar
- suportar também o Layout.preferredWidth para o tamanho da folha
- eliminação dos últimos 'controlos1'
- Possibilidade de criação de Acções de separadores
- possibilidade de um nº arbitrário de colunas no CardsGridview
- Não destruir activamente os itens do menu (erro 397863)
- os ícones no 'actionButton' são monocromáticos
- não tornar os ícones monocromáticos quando não deveriam sê-lo
- repor o dimensionamento arbitrário de 1,5* dos ícones em dispositivos móveis
- reciclagem delegada: Não pedir duas vezes o objecto do contexto
- uso da implementação interna de ondas no formato material
- controlo da largura do cabeçalho pelo 'sourcesize', se for horizontal
- exposição de todas as propriedades da BannerImage nos Cards
- uso do DesktopIcon mesmo no Plasma
- carregamento correcto das localizações 'file://'
- Reversão do "Começar a procurar pelo contexto a partir do próprio delegado"
- Adição de caso de testes que destaca um problema de âmbito no DelegateRecycler
- definição de uma altura explícita nas 'overlayDrawers' (erro 398163)
- Começar a procurar pelo contexto a partir do próprio delegado

### KItemModels

- Uso de referência num ciclo 'for' pelo tipo com um construtor por cópia não trivial

### KNewStuff

- Adição do suporte para as marcas do Attica (erro 398412)
- [KMoreTools] atribuição de um ícone apropriado ao item de menu "Configurar..." (erro 398390)
- [KMoreTools] Redução da hierarquia de menus
- Correcção do evento 'Impossível usar um ficheiro .knsrc nos envios em locais fora do padrão' (erro 397958)
- Capacidade de compilar as ferramentas de testes em Windows
- Correcção da compilação com o Qt 5.9
- Adição do suporte para as marcas do Attica

### KNotification

- Correcção de um estoiro provocado por uma má gestão do tempo de vida da notificação de áudio baseada no Canberra (erro 398695)

### KNotifyConfig

- Correcção de sugestão do ficheiro UI: o KUrlRequester agora tem o QWidget como classe de base

### Plataforma KPackage

- Uso de referência num ciclo 'for' pelo tipo com um construtor por cópia não trivial
- Passagem do Qt5::DBus para os destinos de ligações 'PRIVATE'
- Emissão de sinais quando um pacote é instalado/desinstalado

### KPeople

- Correcção dos sinais que não estavam a ser emitidos quando se reuniam os dados de duas pessoas
- Não estoirar se uma pessoa for removida
- Definição do PersonActionsPrivate como uma classe, como definido acima
- Tornar a API do PersonPluginManager pública

### Kross

- base: tratar de melhores comentários para as acções

### KTextEditor

- Pintar o marcador de dobragem de código apenas nas regiões de dobragem de código com várias linhas
- Inicialização do 'm_lastPosition'
- Programação: o isCode() devolve 'false' para o texto do 'dsAlert' (erro 398393)
- Uso do realce de Programa em R para os testes de indentação do R
- Actualização do motor de indentação de R
- Correcção dos esquemas de cores Solarizado Claro e Escuro (erro 382075)
- Não obrigar ao uso do Qt5::XmlPatterns

### KTextWidgets

- ktextedit: carregamento posterior do objecto QTextToSpeech

### Plataforma da KWallet

- Registar os erros de abertura de carteiras

### KWayland

- Não silenciar o erro se o 'damage' for enviado antes do 'buffer' (erro 397834)
- [servidor] Não retornar previamente em caso do código alternativa do 'touchDown'
- [servidor] Correcção do tratamento de 'buffers' de acesos remoto quando a saída não está associada
- [servidor] Não tentar criar ofertas de dados sem origem
- [servidor] Interrupção do início do arrastamento em condições correctas e sem publicar um erro

### KWidgetsAddons

- [KCollapsibleGroupBox] Respeitar a duração da animação do item gráfico do estilo (erro 397103)
- Remoção de verificação obsoleta da versão do Qt
- Compilar

### KWindowSystem

- Uso do _NET_WM_WINDOW_TYPE_COMBO em vez do _NET_WM_WINDOW_TYPE_COMBOBOX

### KXMLGUI

- Correcção do URL do fornecedor de OCS na janela 'Acerca'

### NetworkManagerQt

- Uso de valor correspondente do enumerado do AuthEapMethodUnknown ao comparar com um AuthEapMethod

### Plataforma do Plasma

- Aumentar os textos de versão dos temas, dado que existem novos ícones no 5.51
- Elevar também a janela de configuração quando a reutilizar
- Adição de componente em falta: RoundButton
- Combinar os ficheiros de ícones na visualização OSD e passar para o tema de ícones do Plasma (erro 395714)
- [Componentes do Plasma 3] Correcção do tamanho implícito da pega
- [Lista dos Componentes do Plasma 3] Mudança de elemento com a roda do rato
- Suporte para os ícones dos botões se estiverem presentes
- Correcção dos nomes das semanas a não aparecerem correctamente no calendário, quando a semana começa num dia que não seja Segunda ou Domingo (erro 390330)
- [DialogShadows] Uso de deslocamento 0 para os contornos desactivados no Wayland

### Prisão

- Correcção do desenho dos códigos do Aztec com uma proporção de tamanho != 1
- Remoção de assunção sobre as proporções do código de barras a partir da integração do QML
- Correcção de problemas no desenho provocados por erros de arredondamento no Code 128
- Adição do suporte para códigos de barras Code128

### Purpose

- Definição do Cmake 3.0 como a versão mínima

### QQC2StyleBridge

- Pequeno preenchimento predefinido quando existir um fundo

### Solid

- Não mostrar um emblema para os discos montados, só para os desmontados
- [Fstab] Remoção do suporte para AIX
- [Fstab] Remoção do suporte para Tru64 (**osf**)
- [Fstab] Mostrar um nome de partilha não-vazio no caso do FS de raiz estar exportado (erro 395562)
- Preferir a legenda indicada pela unidade também nos dispositivos 'loop'

### Sonnet

- Correcção de problemas na adivinha da linguagem
- Impedimento de o realce apagar o texto seleccionado (erro 398661)

### Realce de Sintaxe

- i18n: correcção da extracção dos nomes dos temas
- Fortran: Realce dos alertas nos comentários (erro 349014)
- evitar que o contexto principal possa ser '#pop'ed
- Guarda contra transições de estado sem-fim
- YAML: adição de estilos dos blocos de literais e &amp; dobrados (erro 398314)
- Logcat &amp; SELinux: melhorias nos novos temas Solarizados
- AppArmor: correcção de estoiros nas regras abertas (in KF5.50) e melhorias nos novos esquemas Solarizados
- Junção do git://anongit.kde.org/syntax-highlighting
- Actualização de itens a ignorar no Git
- Uso de referência num ciclo 'for' pelo tipo com um construtor por cópia não trivial
- Correcção: Realce do e-mail por parêntesis não fechados no cabeçalho Subject (assunto) (erro 398717)
- Perl: correcção de parêntesis, variáveis, referências de texto e outros (erro 391577)
- Bash: correcção da expansão de parâmetros &amp; parêntesis (erro 387915)
- Adição dos temas Solarizado Claro e Escuro

### Informação de segurança

O código lançado foi assinado com GPG, usando a seguinte chave: pub rsa2048/58D0EE648A48B3BB 2016-09-05 David Faure &lt;faure@kde.org&gt; Impressão digital da chave primária: 53E6 B47B 45CE A3E0 D5B7  4577 58D0 EE64 8A48 B3BB

Poderá discutir e partilhar ideias sobre esta versão na secção de comentários do <a href='https://dot.kde.org/2014/07/07/kde-frameworks-5-makes-kde-software-more-accessible-all-qt-'>artigo do Dot</a>.
