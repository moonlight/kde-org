---
date: 2013-08-14
hidden: true
title: Aplikacje KDE 4.11 są olbrzymim krokiem naprzód w zarządzaniu informacją osobistą
  i dostarczają wiele ogólnych ulepszeń
---
Zarządca plików Dolphin niesie ze sobą w tym wydaniu wiele małych poprawek i optymalizacji. Wczytywanie dużych katalogów zostało przyspieszone i wymaga do 30&#37; mniej pamięci. Wczytywanie podglądów tylko wokół widocznych elementów zapobiega dużej aktywności dysku i procesora. Wprowadzono wiele innych ulepszeń: na przykład, wiele błędów, które dotyczyły rozwiniętych katalogów w Widoku szczegółów zostało poprawionych, już nie zostaną pokazane ikony &quot;nieznanych&quot; pól wieloznacznych przy wchodzeniu do katalogu, a naciśnięcie środkowym przyciskiem myszy na archiwum otworzy nową kartę z zawartością archiwum, tworzą bardziej spójne odczucia.

{{< figure class="text-center img-size-medium" src="/announcements/4/4.11.0/screenshots/send-later.png" caption=`Nowy przepływ pracy wyślij-później w Kontact` width="600px">}}

## Ulepszenia pakietu Kontact

W Pakiecie Kontakt znów skupiono się na stabilności, wydajności i wykorzystaniu pamięci. Importowanie katalogów, przełączanie pomiędzy mapami, pobieranie poczty, oznaczanie lub przenoszenie dużej liczby wiadomości i czas uruchomienia zostały poprawione w ciągu ostatnich 6 miesięcy. Zajrzyj na <a href='http://blogs.kde.org/2013/07/18/memory-usage-improvements-411'>ten blog</a> po szczegóły.<a href='http://www.aegiap.eu/kdeblog/2013/07/news-in-kdepim-4-11-archive-mail-agent/ '>Pracowano nad funkcjonalnością archiwów i wprowadzono wiele poprawek błędów</a> , a na dodatek wprowadzono wiele ulepszeń w Pomocniku Importowania, pozwalając na importowanie ustawień z klienta pocztowego Trojitá i na lepsze importowanie z innych aplikacji. Więcej informacji znajdziesz <a href='http://www.progdan.cz/2013/07/whats-new-in-the-akonadi-world/'>tutaj</a>.

{{< figure class="text-center img-size-medium" src="/announcements/4/4.11.0/screenshots/kmail-archive-agent.png" caption=`Agent archiwizacji zarządza przechowywaniem wiadomości pocztowych w skompresowanej postaci` width="600px">}}

Wydanie nadchodzi z kilkoma znaczącymi nowymi funkcjami. Powstał <a href='http://www.aegiap.eu/kdeblog/2013/05/news-in-kdepim-4-11-header-theme-33-grantlee-theme-generator-headerthemeeditor/'>nowy edytor motywów dla nagłówków pocztowych</a>, a obrazy pocztowe mogą teraz zmieniać rozmiar w locie. <a href='http://www.aegiap.eu/kdeblog/2013/07/new-in-kdepim-4-11-send-later-agent/'>Funkcja późniejszego wysyłania</a> pozwala na planowanie wysyłania poczty o określonej dacie i czasie, z dodaną możliwością ponownego wysyłania w zależności od podanego przedziału. Obsługa dla filtra Sieve KMail (funkcja IMAP pozwalająca na filtrowanie na serwerze) została ulepszona, użytkownicy mogą generować skrypty filtrujące sieve <a href='http://www.aegiap.eu/kdeblog/2013/04/news-in-kdepim-4-11-improve-sieve-support-22/'>przy pomocy łatwego w użyciu interfejsu</a>. W obszarze bezpieczeństwa, KMail wprowadza samoczynne 'wykrywanie oszustw', <a href='http://www.aegiap.eu/kdeblog/2013/04/news-in-kdepim-4-11-scam-detection/'>pokazując ostrzeżenie</a>, gdy poczta zawierająca typowe sztuczki służące do wyłudzania.  Teraz otrzymasz <a href='http://www.aegiap.eu/kdeblog/2013/06/news-in-kdepim-4-11-new-mail-notifier/'>powiadomienie informujące</a>, gdy nadejdzie nowa poczta. I ostanie, ale nie mniej ważne, Blogilo, program do pisania blogów dostał bardzo ulepszony edytor HTML oparty o QtWebKit.

## Rozszerzona obsługa języka dla Kate

Rozbudowany edytor tekstowy Kate wprowadza nowe wtyczki: Python (2 i 3), JavaScript & JQuery, Django oraz XML. Wprowadzają one takie funkcje jak statyczne i dynamiczne samoczynne uzupełnianie wyrazów, sprawdzanie składni, wstawianie wstawek kodu i zdolność do samoczynnego wcinania XML przy użyciu skrótu. Jest jeszcze trochę dla użytkowników Pythona: konsola python dostarczająca szczegółowe informacje o otworzonym pliku źródłowym. Dokonano także małych ulepszeń interfejsu użytkownika, włączając w to <a href='http://kate-editor.org/2013/04/02/kate-search-replace-highlighting-in-kde-4-11/'>nowe pasywne powiadomienia dla funkcjonalności znajdywania</a>, <a href='http://kate-editor.org/2013/03/16/kate-vim-mode-papercuts-bonus-emscripten-qt-stuff/'>optymalizacje do trybu VIM</a> oraz <a href='http://kate-editor.org/2013/03/27/new-text-folding-in-kate-git-master/'>nową funkcjonalność zwijania tekstu</a>

{{< figure class="text-center img-size-medium" src="/announcements/4/4.11.0/screenshots/kstars.png" caption=`KStars pokazuje interesujące nadchodzące wydarzenia widoczne z twojego położenia` width="600px">}}

## Inny ulepszenia aplikacji

W obszarze gier i edukacji, pojawiło się kilka małych i większych nowych funkcji i optymalizacji. Prospektywni pisarze dotykowi mogą cieszyć się wsparciem od-prawej-do-lewej w KTouch, podczas gdy przyjaciel spoglądających w gwiazdy, KStars, ma teraz narzędzie, które pokazuje interesujące wydarzenia mające nadejść w twoim obszarze. Narzędziom matematycznym Rocs, Kig, Cantor oraz KAlgebra poświęcono uwagę, obsługują więcej silników i obliczeń. Gra KJumpingCube ma teraz nowe większe plansze, nowe poziomy umiejętności, szybsze odpowiedzi i ulepszony interfejs użytkownika.

Prosta aplikacja do malowania Kolourpaint teraz radzi sobie z obrazami w formacie WebP, a uniwersalna przeglądarka dokumentów Okular ma konfigurowalne narzędzie przeglądu i wprowadza obsługę cofnij/przywróć w formularzach i przypisach. Program znaczący/odtwarzacz dźwięku JuK obsługuje odtwarzanie i edytowanie metadanych nowego formatu dźwiękowego Ogg Opus (jednakże, wymaga to, aby nowy sterownik dźwięku i TagLib również wspierały Ogg Opus).

#### Instalowanie aplikacji KDE

Oprogramowanie KDE, włączając w to wszystkie jego biblioteki i aplikacje, jest dostępne na warunkach Wolnego Oprogramowania. Oprogramowanie KDE działa na rozmaitych konfiguracjach sprzętowych i architekturach procesora, takich jak ARM i x86, systemach operacyjnych oraz działa z każdym rodzajem menadżera okien czy otoczeniem pulpitu. Poza Linuksem i innymi, opartymi o UNIKSA, systemami operacyjnymi można też znaleźć wersję na Microsoft Windows na stronie <a href='http://windows.kde.org'>Oprogramowanie KDE na Windows</a> i wersję dla Apple Mac OS X na stronie <a href='http://mac.kde.org/'>Oprogramowanie KDE na Mac</a>. Eksperymentalne wydania aplikacji KDE, dla różnych platform przenośnych, takich jak MeeGo, MS Windows Mobile i Symbian można znaleźć w sieci, aczkolwiek są one teraz niewspierane. <a href='http://plasma-active.org'>Plasma Active</a> jest środowiskiem użytkownika dla szerokiego zakresu urządzeń, takich jak komputery typu tablet i inne przenośne.

Oprogramowanie KDE można pobrać w postaci źródła i różnych formatów binarnych z <a href='http://download.kde.org/stable/Szkieletów KDE %1'> download.kde.org</a>, ale można je także uzyskać na  <a href='/download'>CD-ROM</a>lub w każdym <a href='/distributions'>znaczącym systemie operacyjnym GNU/Linuks i UNIX</a> obecnie dostępnym.

##### Pakiety

Niektórzy wydawcy Linux/UNIX OS uprzejmie dostarczyli pakiety binarne Szkieletów KDE %[1] dla niektórych wersji ich dystrybucji, a w niektórych przypadkach dokonali tego wolontariusze społeczności. <br />

##### Położenie pakietów

Po bieżącą listę dostępnych pakietów binarnych, o których został poinformowany Zespół wydawniczy KDE, proszę zajrzyj na stronę <a href='http://community.kde.org/KDE_SC/Binary_Packages#KDE_4.11.0'> Wiki Społeczności</a>.

Pełny kod źródłowy dla Szkieletów KDE %[1] można <a href='/info/4/4.11.0'> pobrać za darmo</a>. Instrukcje dotyczące kompilowania i wgrywania oprogramowania KDE Szkieletów KDE %[1] są dostępne na <a href='/info/4/4.11.0#binary'>Stronie informacyjnej Szkieletów KDE %[1]</a>.

#### Wymagania systemowe

Aby uzyskać najwięcej z tych wydań, zalecamy użycie najnowszej wersji Qt, takiej jak 4.8.4. Jest to potrzebne, aby zapewnić odczucie stabilności i wydajności, jako iż pewne ulepszenia poczynione w oprogramowaniu KDE, w rzeczywistości zostały dokonane w strukturze programistycznej Qt.<br />Aby w pełni wykorzystać możliwości oprogramowania KDE, zalecamy także użycie najnowszych sterowników graficznych dla twojego systemu, jako iż może to znacznie polepszyć odczucia użytkownika, zarówno w opcjonalnej funkcjonalności, jak i w ogólnej wydajności i stabilności.

## Ogłoszone również dzisiaj:

## <a href="../plasma"><img src="/announcements/4/4.11.0/images/plasma.png" class="app-icon" alt="Przestrzenie Pracy Plazmy od KDE, w wersji 4.11" width="64" height="64" /> Przestrzenie Pracy Plazmy kontynuują udoskonalanie wrażeń użytkowników</a>

Przygotowując się do zapewnienia wsparcia w długim czasie, Przestrzenie Robocze Plazmy dostarczają dalszych ulepszeń do podstawowej funkcjonalności z płynniejszym paskiem zadań, inteligentniejszym elementem interfejsu baterii i ulepszonym mikserem dźwięku. Wprowadzenie KEkran zapewnia inteligentną obsługę wielu monitorów na Przestrzeniach Roboczych, a zakrojone na dużą skalę ulepszenia w wydajności w połączeniu z małymi dostosowaniami używalności dają przyjemniejsze odczucie.

## <a href="../platform"><img src="/announcements/4/4.11.0/images/platform.png" class="app-icon" alt="KDE Development Platform w wersji 4.11"/>  Platforma KDE 4.11 dostarcza wiele ogólnych ulepszeń </a>

Wydanie Platformy KDE 4.11 kontynuuje prace skupione na stabilności. Dla naszego przyszłego wydania Frameworks KDE 5.0, implementowane są nowe funkcje, lecz dla stabilnego wydania daliśmy radę upchnąć optymalizacje dla naszego modułu Nepomuka.
