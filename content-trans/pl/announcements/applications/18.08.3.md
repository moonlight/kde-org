---
aliases:
- ../announce-applications-18.08.3
changelog: true
date: 2018-11-08
description: KDE wydało Aplikacje KDE 18.08.3
layout: application
title: KDE wydało Aplikacje KDE 18.08.3
version: 18.08.3
---
8 listopada 2018. Dzisiaj KDE wydało pierwsze uaktualnienie stabilizujące <a href='../18.08.0'>Aplikacji KDE 18.08</a>. To wydanie zawiera tylko poprawki błędów i uaktualnienia do tłumaczeń; jest to bezpieczne i przyjemne uaktualnienie dla każdego.

About 20 recorded bugfixes include improvements to Kontact, Ark, Dolphin, KDE Games, Kate, Okular, Umbrello, among others.

Wśród ulepszeń znajdują się:

- HTML viewing mode in KMail is remembered, and again loads external images if allowed
- Kate now remembers meta information (including bookmarks) between editing sessions
- Automatic scrolling in the Telepathy text UI was fixed with newer QtWebEngine versions
