---
aliases:
- ../announce-applications-18.04.3
changelog: true
date: 2018-07-12
description: KDE wydało Aplikacje KDE 18.04.3
layout: application
title: KDE wydało Aplikacje KDE 18.04.3
version: 18.04.3
---
12 lipca 2018. Dzisiaj KDE wydało drugie uaktualnienie stabilizujące <a href='../18.04.0'>Aplikacji KDE 18.04</a>. To wydanie zawiera tylko poprawki błędów i uaktualnienia do tłumaczeń; jest to bezpieczne i przyjemne uaktualnienie dla każdego.

About 20 recorded bugfixes include improvements to Kontact, Ark, Cantor, Dolphin, Gwenview, KMag, among others.

Wśród ulepszeń znajdują się:

- Compatibility with IMAP servers that do not announce their capabilities has been restored
- Ark can now extract ZIP archives which lack proper entries for folders
- KNotes on-screen notes again follow the mouse pointer while being moved
