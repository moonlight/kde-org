---
date: 2013-08-14
hidden: true
title: Les environnements de bureaux Plasma 4.11 continue dans l'amélioration de l'ergonomie
  pour l'utilisateur
---
{{< figure class="text-center img-size-medium" src="/announcements/4/4.11.0/screenshots/empty-desktop.png" caption=`Environnements de bureaux Plasma de KDE 4.11` width="600px" >}}

Dans la version 4.11 des environnements de bureaux Plasma, la barre de tâches - l'un des composants graphiques les plus utilisés - <a href='http://blogs.kde.org/2013/07/29/kde-plasma-desktop-411s-new-task-manager'> a été porté sous QtQuick</a>. La nouvelle barre de tâches, bien que conservant l'apparence et la fonctionnalité de son ancienne version, présente un comportement plus cohérent et plus fluide. Le portage a aussi résolu un certain nombre de bogues ouverts depuis longtemps. Le composant graphique de batterie (qui pouvait précédemment modifier la luminosité de l'écran) prend en charge maintenant la luminosité du clavier et peut traiter plusieurs batteries incluses dans des périphériques, tels que une souris ou un clavier sans fil. Il affiche le niveau de batterie pour chaque périphérique et vous avertit lorsque celle-ci est faible. Le menu du lanceur Kickoff affiche maintenant les applications récemment installées dans les derniers jours. Enfin mais non le moindre, les menus contextuels de notifications comportent maintenant un bouton de configuration avec lequel toute personne peut modifier facilement les paramètres de ce type particulier de notifications.

{{< figure class="text-center img-size-medium" src="/announcements/4/4.11.0/screenshots/notifications.png" caption=`Gestion améliorée des notifications` width="600px" >}}

KMix, l'outil de mixage de sons de KDE a gagné de façon significative en performances et en stabilité avec la <a href='http://kmix5.wordpress.com/2013/07/26/kmix-mission-statement-2013/'>prise en charge du contrôle complet d'un lecteur multi-média</a> reposant sur le standard « MPRIS2 ».

{{< figure class="text-center img-size-medium" src="/announcements/4/4.11.0/screenshots/battery-applet.png" caption=`Le composant graphique de batteries totalement revu en action` width="600px" >}}

## Gestionnaire de fenêtres Kwin et de mode composite

Notre gestionnaire de fenêtres, Kwin, a reçu de nouveau des mises à jour significatives, délaissant les technologies anciennes et incorporant le protocole de communication « XCB ». Ceci conduit à une gestion plus fine et plus rapide des fenêtres. La prise en charge de OpenGL 3.1 et de OpenGL ES 3.0 a été aussi ajoutée. Cette version intègre aussi la première prise en charge expérimentale de Wayland, le successeur à X11. Ceci permet l'utilisation de KWin avec X11 au dessus de la pile Wayland. Pour plus d'informations pour utiliser ce mode expérimental, veuillez consulter <a href='http://blog.martin-graesslin.com/blog/2013/06/starting-a-full-kde-plasma-session-in-wayland/'>ce billet</a>. L'interface KWin avec les langages de scripts a bénéficié d'améliorations très conséquentes, la prise en charge facilitée de la configuration de l'interface utilisateur, de nouvelles animations, des effets graphiques et beaucoup d'améliorations plus petites. Cette version apporte une meilleure reconnaissance des configurations multi-écrans (incluant une option pour une surbrillance des bords pour des « coins éclatants »), l'amélioration du passage rapide en mosaïque (avec des zones configurables de mosaïque) et l'habituel volume de corrections de bogues et d'optimisations. Pour plus de détails, veuillez consulter <a href='http://blog.martin-graesslin.com/blog/2013/06/what-we-did-in-kwin-4-11/'>cette page</a> et <a href='http://blog.martin-graesslin.com/blog/2013/06/new-kwin-scripting-feature-in-4-11/'>celle-ci</a> 

## Gestion des écrans et raccourcis Web

La configuration de la surveillance dans le tableau de bord système a été <a href='http://www.afiestas.org/kscreen-1-0-released/'>remplacé par un nouvel outil, KScreen</a>. Celui-ci apporte une prise en charge plus intelligente du mode multi-écran pour les environnements de bureaux Plasma, configurant automatiquement les nouveaux écrans et se souvenant des paramètres lors de configuration manuelle d'écran. Il offre une interface intuitive et très visuelle permettant de ré-arranger les écrans grâce à un simple glisser-déposer.

{{< figure class="text-center img-size-medium" src="/announcements/4/4.11.0/screenshots/kscreen.png" caption=`La nouvelle gestion des écrans avec KScreen` width="600px" >}}

Les raccourcis Web, la façon la plus simple pour trouver facilement ce que vous cherchez sur le Web, ont été nettoyés et améliorés. Beaucoup ont été mis à jour pour utiliser des connexions chiffrées et sûres (TLS/SSL), de nouveaux raccourcis Web ont été ajoutés et quelques uns devenus obsolètes ont été supprimés. La méthode pour l'ajout de vos propres raccourcis Web a été aussi améliorée. Vous pouvez trouver plus de détails <a href='https://plus.google.com/108470973614497915471/posts/9DUX8C9HXwD'>ici</a>.

Cette publication marque la fin des environnements de bureaux Plasma 1, composant les séries de fonctionnalités de KDE SC 4. Pour faciliter la transition vers la prochaine génération, cette publication sera prise en charge au moins pour deux ans. L'attention pour le développement de fonctionnalités se déplacera maintenant vers les environnements de bureaux Plasma 2. Les améliorations de performances et la correction des bogues se concentreront vers les versions 4.11.

#### Installation de Plasma

Les logiciels de KDE, y compris toutes ses bibliothèques et ses applications, sont disponibles gratuitement sous des licences « Open Source ». Ils fonctionnent sur diverses configurations matérielles, sur des architectures processeurs comme « ARM » ou « x86 » et sur des différents systèmes d'exploitation. Ils utilisent tout type de gestionnaire de fenêtres ou environnements de bureaux. A côté des systèmes d'exploitation Linux ou reposant sur UNIX, vous pouvez trouver des versions « Microsoft Windows » de la plupart des applications de KDE sur le site <a href='http://windows.kde.org'>Logiciels KDE sous Windows</a> et des versions « Apple Mac OS X » sur le site <a href='http://mac.kde.org/'>Logiciels KDE sous Mac OS X</a>. Des versions expérimentales des applications de KDE peuvent être trouvées sur Internet pour diverses plate-formes mobiles comme « MeeGo », « MS Windows Mobile » et « Symbian » mais qui sont actuellement non prises en charge. <a href='http://plasma-active.org'>Plasma Active</a> est une interface utilisateur pour une large variété de périphériques comme des tablettes et d'autres matériels mobiles. <br />

Les logiciels KDE peuvent être obtenus sous forme de code source et de nombreux formats binaires à l'adresse <a href='http://download.kde.org/stable/4.11.0'>download.kde.org</a>. Ils peuvent être aussi obtenus sur <a href='/download'>CD-ROM</a> ou avec n'importe quelle distribution <a href='/distributions'>majeure de systèmes GNU / Linux et UNIX</a> publiée à ce jour.

##### Paquets

Quelques fournisseurs de systèmes Linux / Unix mettent à disposition gracieusement des paquets binaires de 4.11.0 pour certaines versions de leurs distributions. Dans les autres cas, des bénévoles de la communauté le font aussi. <br />

##### Emplacements des paquets

Pour obtenir une liste courante des paquets binaires disponibles, connus par l'équipe de publication de KDE, veuillez visiter le <a href='http://community.kde.org/KDE_SC/Binary_Packages#KDE_4.11.0'>Wiki de la communauté</a>.

Le code source complet pour 4.11.0 peut être <a href='/info/4/4.11.0'>librement téléchargé</a>. Les instructions pour la compilation et l'installation des logiciels KDE 4.11.0 sont disponibles à partir de la <a href='/info/4/4.11.0#binary'>Page d'informations 4.11.0</a>.

#### Configuration minimale du système

Pour bénéficier au maximum de ces nouvelles versions, l'utilisation d'une version récente de Qt est recommandée, comme la version 4.8.4. Elle est nécessaire pour garantir un fonctionnement stable et performant, puisque certaines améliorations apportées aux logiciels de KDE ont été réalisées dans les bibliothèques Qt utilisées.<br /> Pour utiliser pleinement toutes les possibilités des logiciels de KDE, l'utilisation des tout derniers pilotes graphiques pour votre système est recommandée, puisque que ceux-ci peuvent grandement améliorer votre expérience d'utilisateur, à la fois dans les fonctionnalités optionnelles que dans les performances et la stabilité générales.

## Également annoncé aujourd'hui : 

## <a href="../applications"><img src="/announcements/4/4.11.0/images/applications.png" class="app-icon" alt="The KDE Applications 4.11"/> Les applications de KDE 4.11 apportent un important pas en avant concernant la gestion des informations personnelles et des améliorations sur toutes les fonctionnalités.</a>

Cette version intègre de très nombreuses améliorations dans la pile de KDE PIM, apportant de bien meilleures performances et plusieurs nouvelles fonctionnalités. Kate améliore la productivité pour les développeurs Python et Javascript avec de nouveaux modules externes. Dolphin est devenu plus rapide et les applications pour l'éducation bénéficient de nombreuses fonctionnalités variées.

## <a href="../platform"><img src="/announcements/4/4.11.0/images/platform.png" class="app-icon" alt="The KDE Development Platform 4.11"/>La plate-forme 4.11 de KDE fournit de meilleures performances</a>.

Cette version de la plate-forme KDE 4.11 continue à se focaliser sur la stabilité. De nouvelles fonctionnalités ont été implémentées pour la version 5.0 du futur environnement de développement de KDE. Mais, il a été prévu de limiter les optimisations pour l'environnement Nepomuk dans les versions stables.
