---
aliases:
- ../announce-applications-15.08.2
changelog: true
date: 2015-10-13
description: KDE publie les applications de KDE 15.08.2
layout: application
title: KDE publie les applications de KDE 15.08.2
version: 15.08.2
---
13 Octobre 2015. Aujourd'hui, KDE a publié la seconde mise à jour de consolidation pour les <a href='../15.08.0'> applications 15.08 de KDE</a>. Cette version ne contient que des corrections de bogues et des mises à jour de traduction, permettant une mise à jour sûre et appréciable pour tout le monde.

Plus de 30 corrections de bogues apportent des améliorations à ark, gwenview, kate, kbruch, kdelibs, kdepim, lokalize and umbrello.

Cette mise à jour inclut aussi une version de prise en charge à long terme pour la plate-forme de développement 4.14.13 de KDE.
