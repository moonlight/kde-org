---
aliases:
- ../announce-applications-15.08.3
changelog: true
date: 2015-11-10
description: KDE publie les applications KDE 15.08.3
layout: application
title: KDE publie les applications KDE 15.08.3
version: 15.08.3
---
10 Novembre 2015. Aujourd'hui, KDE a publié la troisième mise à jour de stabilisation des <a href='../15.08.0'>applications 15.08 de KDE</a>. Cette version ne contient que des corrections de bogues et des mises à jour de traduction. Il s'agit d'une mise à jour sûre et bénéfique pour tout le monde.

Plus de 20 corrections de bogues apportent des améliorations à ark, dolphin, kdenlive, kdepim, kig, lokalize and umbrello.

Cette mise à jour inclut aussi une version de prise en charge à long terme pour la plate-forme de développement 4.14.14 de KDE.
