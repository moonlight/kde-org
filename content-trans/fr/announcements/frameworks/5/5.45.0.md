---
aliases:
- ../../kde-frameworks-5.45.0
date: 2018-04-14
layout: framework
libCount: 70
src: /announcements/frameworks/5-tp/KDE_QT.jpg
---
### Attica

- Explicitly set content type to form data

### Baloo

- Simplify Term operator&amp;&amp; and ||
- Do not retrieve document ID for skipped result entries
- Do not retrieve mtime from database repeatedly when sorting
- Ne pas exporter par défaut « databasesanitizer »
- baloodb : ajout d'un message expérimental
- Introduction de l'outil « baloodb CLI »
- Introduction de la classe « Nettoyeur »
- [FileIndexerConfig] Delay populating folders until actually used
- src/kioslaves/search/CMakeLists.txt - link to Qt5Network following changes to kio
- balooctl: checkDb should also verify the last known url for the documentId
- Surveillance « balooctl » : reprendre pour attendre l'exécution du service

### Icônes « Breeze »

- add window-pin icon (bug 385170 add window-pin icon)
- Renommage des icônes de 64 pixels, ajoutées pour elisa
- change 32px icons for playlist shuffle and repeat
- Missing icons for inline Messages (bug 392391)
- Nouvel icône pour le lecteur de musique Elisa
- Ajout d'icônes d'état pour des média
- Suppression de cadre autour des icônes d'actions sur média.
- add media-playlist-append and play icons
- Ajout de « view-media-album » pour « babe »

### Modules additionnels « CMake »

- Make use of upstream CMake infrastructure to detect the compiler toolchain
- API dox: fix some "code-block" lines to have empty lines before/after
- Ajout de « ECMSetupQtPluginMacroNames »
- Provide androiddeployqt with all prefix paths
- Inclusion de « stdcpp-path »dans le fichier « json »
- Résolution des liens symboliques dans les emplacements d'importation « QML »
- Provide QML import paths to androiddeployqt

### Intégration avec l'environnement de développement

- kpackage-install-handlers/kns/CMakeLists.txt - link to Qt::Xml following changes in knewstuff

### KActivitiesStats

- Do not assume SQLite works and do not terminate on errors

### Outils KDE avec « DOxygen »

- Look first for qhelpgenerator-qt5 for help generation

### KArchive

- karchive, kzip: try to handle duplicate files in a bit nicer way
- Use nullptr for passing a null pointer to crc32

### KCMUtils

- Make it possible to request a plugin configuration module programatically
- Consistently use X-KDE-ServiceTypes instead of ServiceTypes
- Ajout « X-KDE-OnlyShowOnQtPlatforms » à la définition de « KCModule servicetype »

### KCoreAddons

- KTextToHTML : retour si l'« URL » est vide
- Cleanup m_inotify_wd_to_entry before invalidating Entry pointers (bug 390214)

### KDeclarative

- Paramètre une seule fois de « QQmlEngine » dans « QmlObject »

### KDED

- Ajout « X-KDE-OnlyShowOnQtPlatforms » à la définition de « KDEDModule servicetype »

### KDocTools

- Add entities for Elisa, Markdown, KParts, DOT, SVG to general.entities
- customization/ru: Fix translation of underCCBYSA4.docbook and underFDL.docbook
- Fix duplicate lgpl-notice/gpl-notice/fdl-notice
- customization/ru: Translate fdl-notice.docbook
- change spelling of kwave requested by the maintainer

### KFileMetaData

- taglibextractor: Refactor for better readability

### KGlobalAccel

- Don't assert if used incorrectly from dbus (bug 389375)

### KHolidays

- holidays/plan2/holiday_in_en-gb - update holiday file for India (bug 392503)
- This package was not be updated. Perhaps a problem with script
- Reworked the holiday files for Germany (bug 373686)
- Format README.md as the tools expect (with an Introduction section)

### KHTML

- Éviter de demander un protocole vide

### KI18n

- Make sure ki18n can build its own translations
- Don't call PythonInterp.cmake in KF5I18NMacros
- Rendre possible la génération de fichiers « po » en parallèle
- Create a constructor for KLocalizedStringPrivate

### KIconThemes

- Make KIconEngine export comment accurate
- Éviter une erreur à l'exécution « asan »

### KInit

- Delete IdleSlave having temporary authorization

### KIO

- Ensure that the model is set when resetResizing is called
- Le fichier « pwd.h » est absent de Windows
- Remove Recently Saved This Month and Recently Saved Last Month entries by default
- Exécution de la compilation de « Kio » pour Android.
- Temporarily disable installation of file ioslave's kauth helper and policy file
- Handle privilege operation confirmation prompts in SlaveBase rather than in KIO::Job
- Improve consistency of "Open With" UI by always showing top app inline
- Fix crash when device emits ready read after job is finished
- Highlight selected items when showing parent folder from the open/save dialog (bug 392330)
- Prise en charge des fichiers masqués « NTFS »
- Consistently use X-KDE-ServiceTypes instead of ServiceTypes
- Fix assert in concatPaths when pasting a full path into KFileWidget's lineedit
- [KPropertiesDialog] Support Checksum tab for any local path (bug 392100)
- [KFilePlacesView] Call KDiskFreeSpaceInfo only if necessary
- FileUndoManager : ne pas supprimer les fichiers locaux non existants
- [KProtocolInfoFactory] Don't clear cache if it had just been built
- Don't try to find an icon for a relative URL either (e.g. '~')
- Use correct item URL for Create New context menu (bug 387387)
- Fix more cases of incorrect parameter to findProtocol
- KUrlCompletion: early return if the URL is invalid like ":/"
- Don't try to find an icon for an empty url

### Kirigami

- Icônes plus grandes pour les plate-formes mobiles
- Forcer une taille de contenu dans l'élément de style pour l'arrière-plan
- Add InlineMessage type and Gallery app example page
- Meilleure coloration par sélection heuristique
- make loading from local svgs actually work
- support the android icon loading method as well
- Utilisation d'une stratégie de coloration similaires aux précédents styles différents
- [Carte] Utilisation de l'implémentation personnelle de « findIndex »
- kill network transfers if we change icon while running
- Premier prototype pour le recycleur délégué
- Allow OverlaySheet clients to omit the built-in close button
- Composants pour les cartes
- Correction de la taille de « ActionButton »
- Make passiveNotifications last longer, so users can actually read them
- Supprimer les dépendances inutilisées de « QQC1 »
- Format de « ToolbarApplicationHeader »
- Rend possible l'affichage du titre même sans avoir d'actions « ctx »

### KNewStuff

- Actually vote when clicking stars in the list view (bug 391112)

### Environnement de développement « KPackage »

- Tentative de correction de la compilation pour « FreeBSD »
- Utiliser « Qt5::rcc » au lieu de rechercher l'exécutable
- Use NO_DEFAULT_PATH to ensure the right command is picked up
- Regarder aussi les exécutables préfixés « rcc »
- Définir le composant pour la génération correcte de « qrc »
- Correction de la génération de paquets binaires « rcc »
- Generate the rcc file every time, at install time
- Make org.kde. components include a donate URL
- Mark kpackage_install_package undeprecated for plasma_install_package

### KPeople

- Présentation de « PersonData::phoneNumber » à « QML »

### Kross

- Aucun nécessité d'avoir « kdoctools »

### KService

- API dox: consistently use X-KDE-ServiceTypes instead of ServiceTypes

### KTextEditor

- Make it possible for KTextEditor to build on Android NDK's gcc 4.9
- avoid Asan runtime error: shift exponent -1 is negative
- Optimisation de « TextLineData::attribute »
- Ne pas calculer « attribute() » deux fois
- Revert Fix: View jumps when Scroll past end of document is enabled (bug 391838)
- Ne pas polluer l'historique du presse-papier avec des duplicatas

### KWayland

- Ajout d'une interface d'accès à distance à Wayland
- [server] Add support for the frame semantics of Pointer version 5 (bug 389189)

### KWidgetsAddons

- KColorButtonTest : supprimer le code « A faire » 
- ktooltipwidget: Subtract margins from available size
- [KAcceleratorManager] Only set iconText() if actually changed (bug 391002)
- ktooltipwidget: Prevent offscreen display
- KCapacityBar: set QStyle::State_Horizontal state
- Synchroniser les modifications de « KColorScheme »
- ktooltipwidget: Fix tooltip positioning (bug 388583)

### KWindowSystem

- Ajout de « SkipSwitcher » à l'« API »
- [xcb] Fix implementation of _NET_WM_FULLSCREEN_MONITORS (bug 391960)
- Réduction du temps de pause de « plasmashell »

### ModemManagerQt

- cmake: don't flag libnm-util as found when ModemManager is found

### NetworkManagerQt

- Exporter les dossiers d'inclusions du « NetworkManager »
- Démarrage nécessitant « NM 1.0.0 »
- device: define StateChangeReason and MeteredStatus as Q_ENUMs
- Fix conversion of AccessPoint flags to capabilities

### Environnement de développement de Plasma

- Wallpaper templates: set background color to ensure contrast to sample text content
- Ajout d'un modèle pour les fonds d'écran de Plasma avec extension « QML »
- [ToolTipArea] Ajout du signal « aboutToShow »
- windowthumbnail: Use gamma correct scaling
- windowthumbnail: Use mipmap texture filtering (bug 390457)
- Suppression des entrées non utilisées de « X-Plasma-RemoteLocation »
- Templates: drop unused X-Plasma-DefaultSize from applet metadata
- Consistently use X-KDE-ServiceTypes instead of ServiceTypes
- Templates: drop unused X-Plasma-Requires-* entries from applet metadata
- Suppression des ancres d'éléments dans un format
- Réduction du temps de pause de « plasmashell »
- preload only after the containment emitted uiReadyChanged
- Correction d'une rupture pour les listes à menu déroulant (bogue 392026)
- Fix text scaling with non-integer scale factors when PLASMA_USE_QT_SCALING=1 is set (bug 356446)
- Nouvelles icônes pour les périphériques déconnectés ou désactivés
- [Dialog] Allow setting outputOnly for NoBackground dialog
- [ToolTip] Check file name in KDirWatch handler
- Disable deprecation warning from kpackage_install_package for now
- [Breeze Plasma Theme] Apply currentColorFix.sh to changed media icons
- [Breeze Plasma Theme] Add media status icons with circles
- Suppression des trames autour des boutons de média
- [Window Thumbnail] Allow using atlas texture
- [Dialog] Remove now obsolete KWindowSystem::setState calls
- Prise en charge des textures « Atlas » dans « FadingNode »
- Fix FadingMaterial fragment with core profile

### QQC2StyleBridge

- Correction du rendu lorsque désactivée
- Meilleure disposition
- Prise en charge expérimental des moyens mnémotechniques automatiques
- Make sure we are taking into account the size of the element when styling
- Fix font rendering for non-HiDPI and integer scale factors (bug 391780)
- Correction des couleurs d'icônes avec des jeux de couleurs
- Correction des couleurs d'icônes pour les boutons de menus

### Opaque

- Solid can now query for batteries in e.g. wireless gamepads and joysticks
- Utilisation des énumérations récemment définies
- add gaming_input devices and others to Battery
- Ajout d'une énumération des périphériques de batteries
- [UDevManager] Also explicitly query for cameras
- [UDevManager] Already filter for subsystem before querying (bug 391738)

### Sonnet

- Don't impose using the default client, pick one that supports the requested language.
- Include replacement strings in the suggestion list
- implement NSSpellCheckerDict::addPersonal()
- NSSpellCheckerDict::suggest() returns a list of suggestions
- initialise NSSpellChecker language in NSSpellCheckerDict ctor
- implement NSSpellChecker logging category
- « NSSpellChecker » nécessite « AppKit »
- Move NSSpellCheckerClient::reliability() out of line
- Utilisation du jeton préféré pour la plate-forme « Mac »
- Use correct directory to lookup trigrams in windows build dir

### Coloration syntaxique

- Rendre possible la construction complète d'un projet avec compilation croisée
- Nouvelle conception du générateur de syntaxe « CMake »
- Optimize highlighting Bash, Cisco, Clipper, Coffee, Gap, Haml, Haskell
- Ajout d'une coloration syntaxique pour les fichiers « MIB »

### Informations sur la sécurité

Le code publié a été signé en « GPG » avec la clé suivante  pub rsa2048/58D0EE648A48B3BB 2016-09-05 David Faure &lt;faure@kde.org&gt; Empreinte de la clé primaire : 53E6 B47B 45CE A3E0 D5B7 4577 58D0 EE64 8A48 B3BB

Vous pouvez discuter et partager vos idées sur cette version dans la section des commentaires de <a href='https://dot.kde.org/2014/07/07/kde-frameworks-5-makes-kde-software-more-accessible-all-qt-developers'>l'article</a>.
