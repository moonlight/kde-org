---
aliases:
- ../announce-applications-16.08.1
changelog: true
date: 2016-09-08
description: KDE toob välja KDE rakendused 16.08.1
layout: application
title: KDE toob välja KDE rakendused 16.08.1
version: 16.08.1
---
September 8, 2016. Today KDE released the first stability update for <a href='../16.08.0'>KDE Applications 16.08</a>. This release contains only bugfixes and translation updates, providing a safe and pleasant update for everyone.

Rohkem kui 45 teadaoleva veaparanduse hulka kuuluvad Kdepimi, Kate, Kdenlive'i, Konsooli, Marble'i, Kajonggi, Kopete, Umbrello ja teiste rakenduste täiustused.

This release also includes Long Term Support version of KDE Development Platform 4.14.24.
