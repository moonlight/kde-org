---
aliases:
- ../announce-applications-19.04.1
changelog: true
date: 2019-05-09
description: KDE Ships Applications 19.04.1.
layout: application
major_version: '19.04'
release: applications-19.04.1
title: KDE toob välja KDE rakendused 19.04.1
version: 19.04.1
---
{{% i18n_date %}}

Today KDE released the first stability update for <a href='../19.04.0'>KDE Applications 19.04</a>. This release contains only bugfixes and translation updates, providing a safe and pleasant update for everyone.

Umbes 20 teadaoleva veaparanduse hulka kuuluvad muu hulgas Kontacti, Arki, Cantori, Dolphini, Kdenlive'i, Spectacle'i ja Umbrello täiustused.

Täiustused sisaldavad muu hulgas:

- Failide sildistamine töölaual ei kärbi enam sildi nime
- Parandati krahh KMaili teksti jagamise pluginas
- Korrigeeriti mitmeid tagasilangusi videoredaktoris Kdenlive
