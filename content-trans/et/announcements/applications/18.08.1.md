---
aliases:
- ../announce-applications-18.08.1
changelog: true
date: 2018-09-06
description: KDE toob välja KDE rakendused 18.08.1
layout: application
title: KDE toob välja KDE rakendused 18.08.1
version: 18.08.1
---
September 6, 2018. Today KDE released the first stability update for <a href='../18.08.0'>KDE Applications 18.08</a>. This release contains only bugfixes and translation updates, providing a safe and pleasant update for everyone.

Üle tosina teadaoleva veaparanduse hulka kuuluvad muu hulgas Kontacti, Cantori, Gwenview, Okulari ja Umbrello täiustused.

Täiustused sisaldavad muu hulgas:

- KIO-MTP komponenti ei taba enam krahhm kui seadmele on juba juurdepääsu saanud mõni teine rakendus
- Kirjade saatmisel KMailis kasutatakse nüüd parooli, kui see on määratud, paroolidialoogi abil
- Okular jätab nüüd PDF-dokumentide salvestamise järel külgriba režiimi meelde
