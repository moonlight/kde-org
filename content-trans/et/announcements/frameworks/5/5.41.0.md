---
aliases:
- ../../kde-frameworks-5.41.0
date: 2017-12-10
layout: framework
libCount: 70
src: /announcements/frameworks/5-tp/KDE_QT.jpg
---
### Baloo

- Baloo siltide KIO-mooduli täielik ümberkirjutamine (veateade 340099)

### BluezQt

- rfkill-faili deskriptoreid ei lekitata (veateade 386886)

### Breeze'i ikoonid

- Puuduvate ikoonisuuruste lisamine (veateade 384473)
- discover'i paigaldamise ja eemaldamise ikoonide lisamine

### CMake'i lisamoodulid

- Kirjeldussildi lisamine genereeritud pkgconfig-failidele
- ecm_add_test: kohase asukohaeraldaja kasutamine Windows'is
- FindSasl2.cmake'i lisamine ECM-i
- ARGS'i edastamine ainult Makefiles'i tegemisel
- FindGLIB2.cmake'i ja FindPulseAudio.cmake'i lisamine
- ECMAddTests: QT_PLUGIN_PATH'i määramine, et saaks leida kohapeal ehitatud pluginaid
- KDECMakeSettings: põhjalikum dokumentatsioon ehitamiskataloogi ülesehituse kohta

### Raamistike lõimimine

- KNS-i toote 2. ja 3. allalaadimislingi allalaadimise toetus (veateade 385429)

### KActivitiesStats

- libKActivitiesStats.pc: parandamise alustamine (veateade 386933)

### KActivities

- Trügimise parandus, mis käivitas kactivitymanagerd mitu korda

### KAuth

- Ainult kauth-policy-gen'i koodi generaatori ehitamise lubamine
- Märkuse lisamine abilise väljakutsumise kohta mitmelõimelisest rakendusest

### KBookmarks

- Järjehoidjate muutmise toimingut ei näidata, kui keditbookmarks ei ole paigaldatud
- Portimine iganenud KAuthorized::authorizeKAction'i pealt authorizeAction'i peale

### KCMUtils

- klaviatuuriga sisse ja välja liikumine QML-juhtumismoodulites

### KCompletion

- Krahhi vältimine uue redigeerimisrea määramisel redigeeritavasse liitkasti
- KComboBox: varakult tagastamine eelmise väärtuse muudetavaks määramisel
- KComboBox: olemasoleva lõpetusobjekti taaskasutus uuel redigeerimisreal

### KConfig

- Iga kord ei ole tarvis uurida /etc/kderc

### KConfigWidgets

- Vaikevärvide uuendamine vastavaks uute värvidega, mida pakub D7424

### KCoreAddons

- SubJobs'i sisendi valideerimine
- Hoiatus tõrgete eest json-failide parsimisel
- kcfg/kcfgc/ui.rc/knotify &amp; qrc-failide MIME tüübi definitsioonide paigaldamine
- Uue funktsiooni lisamine pikkuse mõõtmiseks teksti järgi
- KAutoSave'i tõrke parandus tühimärki sisaldava faili korral

### KDeclarative

- Muutmine kompileeritavaks windows'is
- muutmine kompileeritavaks QT_NO_CAST_FROM_ASCII/QT_NO_CAST_FROM_BYTEARRAY'ga
- [MouseEventListener] hiiresündmuse aktsepteerimise lubamine
- Üheainsa QML-mootori kasutamine

### KDED

- kded: dbus'i ksplash'i väljakutsete eemaldamine

### KDocTools

- Brasiilia portugali keele tõlke uuendamine
- Vene keele tõlke uuendamine
- Vene keele tõlke uuendamine
- customization/xsl/ru.xml'i uuendamine (nav-home puudus)

### KEmoticons

- KEmoticons: pluginate portimine JSON-i peale ja koos KPluginMetaData'ga laadimise toetus
- pimpl-klasside sümboleid ei lekitata, kaitsmine Q_DECL_HIDDEN'iga

### KFileMetaData

- usermetadatawritertest nõuab Taglib'i
- kui omaduse väärtus on null, eemaldatakse atribuut user.xdg.tag (veateade 376117)
- Failide avamine TagLib'i ekstraktoris kirjutuskaitstuna

### KGlobalAccel

- Mõningate blokkivate dbus'i väljakutsete rühmitamine
- kglobalacceld: välditakse ikoonilaadija põhjendamatut laadimist
- korrektsete kiirklahvistringide genereerimine

### KIO

- KUriFilter: topeltpluginate väljafiltreerimine
- KUriFilter: andmestruktuuride lihtsustamine, mälulekke parandus
- [CopyJob] kui fail on eemaldatud, ei alustata kõike otsast peale
- Kataloogi KNewFileMenu+KIO::mkpath'i kaudu Qt 5.9.3+ peal loomise parandus (veateade 387073)
- Lisafunktsiooni 'KFilePlacesModel::movePlace' loomine
- Expose KFilePlacesModel 'iconName' role
- KFilePlacesModel: tarbetu signaali 'dataChanged' vältimine
- Kehtiva järjehoidjaobjekti tagastamine kõigi KFilePlacesModel'i kirjete korral
- Funktsiooni 'KFilePlacesModel::refresh' loomine
- Staatilise funktsiooni 'KFilePlacesModel::convertedUrl' loomine
- KFilePlaces: sektsiooni 'remote' loomine
- KFilePlaces: eemaldatavate seadmete sektsiooni loomine
- baloo url-ide lisamine asukohtade mudelisse
- KIO::mkpath'i parandus qtbase 5.10 beta 4 peal
- [KDirModel] HasJobRole'i muutuse väljastamine, kui tööd muutuvad
- Pealdis "Muud valikud" asendatakse pealdisega "Terminali valikud"

### Kirigami

- Kerimisriba nihe päise suuruse võrra (veateade 387098)
- alumine veeris toimingunupu olemasolu alusel
- ei eldata applicationWidnow() saadavust
- Väärtuste muutustest ei anta märku, kui me oleme ikka veel konstruktoris
- Teegi nime asendamine lähtekoodis
- värvide toetamine rohkemates kohtades
- tarviduse korral värvilised ikoonid tööriistaribadel
- ikoonivärvide kaalutlemine põhitoimingute nuppudel
- ikooni rühmitamise omadusega alustamine

### KNewStuff

- "Lahtihaakimine enne d viida määramist" tagasivõtmine (veateade 386156)
- töölauafailide koondamiseks ei paigaldata arendustööriista
- [knewstuff] ImageLoader'it ei lekitata tõrke korral

### KPackage raamistik

- Stringide kohane tegemine kpackage'i raamistikus
- metadata.json'i ei genereerita, kui puudub metadata.desktop
- kpluginindex'i puhverdamise parandus
- Tõrkeväljundi täiustus

### KTextEditor

- VI-režimi puhvrikäskude parandus
- juhusliku suurendamise vältimine

### KUnitConversion

- Portimine QDom'i pealt QXmlStreamReader'i peale
- https'i kasutamine rahavahetuskursside allalaadimisel

### KWayland

- wl_display_set_global_filter'i avalikustamine virtuaalse meetodina
- kwayland-testXdgShellV6 parandus
- zwp_idle_inhibit_manager_v1 toetuse lisamine (veateade 385956)
- [server] IdleInterface'i keelamise toetus

### KWidgetsAddons

- Ebaühtlase paroolidialoogi vältimine
- enable_blur_behind vihje määramine nõudmisel
- KPageListView: laiuse uuendamine fondi muutmisel

### KWindowSystem

- [KWindowEffectsPrivateX11] väljakutse reserve() lisamine

### KXMLGUI

- Tööriistariba nime tõlke parandus, kui sel on i18n kontekst

### Plasma raamistik

- #warning direktiiv ei ole universaalne ja kindlasti EI TOETA seda MSVC
- [IconItem] ItemSceneHasChanged'i kasutamine connect'i asemel windowChanged'is
- [Icon Item] overlaysChanged'i otsene väljastamine setter'is sellega ühendumise asemel
- [Dialog] KWindowSystem::isPlatformX11() kasutamine
- Kaheldavate omaduste muutmiste hulga vähendamine ColosScope'is
- [Icon Item] validChanged'i väljastamine ainult siis, kui seda tegelikult muudeti
- Tarbetute kerimistähiste mahasurumine, kui plinkiv on teadaoleva orientatsiooniga loendivaade
- [AppletInterface] configurationRequired'i ja -Reasoni muutmise signaalide väljastamine
- setSize() kasutamine setProperty laiuse ja kõrguse asemel
- Probleemi parandus, kui PlasmaComponents Menu ilmus murtud nurkadega (veateade 381799)
- Probleemi parandus, kui kontekstimenüüd ilmusid murtud nurkadega (veateade 381799)
- API dokumentatsioon: git'i logis leitud iganemismärkuse lisamine
- Komponendi sünkroonimine Kirigami omaga
- Kõigi KF5 komponentide kui selliste otsimine eraldi raamistike asemel
- Kahtlaste signaalide väljastamise vähendamine (veateade 382233)
- Signaalide lisamine, mis annavad märku, kui ekraan lisatakse või eemaldatakse
- Switchi kraami paigaldamine
- Kaasatute kaasatutele ei toetuta
- SortFilterModel'i rollinimede optimeerimine
- DataModel::roleNameToId'i eemaldamine

### Prison

- Azteci koodigeneraatori lisamine

### QQC2StyleBridge

- QQC2 versiooni selgitamine ehitamise ajal (veateade 386289)
- tausta hoidmine vaikimisi nähtamatuna
- tausta lisamine ScrollView's

### Solid

- Kiirem UDevManager::devicesFromQuery

### Sonnet

- Sonneti ristkompileerimise muutmine võimalikuks

### Süntaksi esiletõstmine

- PKGUILD'i lisamine bash'i  süntaksile
- JavaScript: standardsete mime tüüpide kaasamine
- debchangelog: Bionic Beaver'i lisamine
- SQL-i (Oracle'i) süntaksifaili uuendamine (veateade 386221)
- SQL: kommentaaride tuvastamine enne operaatoreid
- crk.xml: päiserea &lt;?xml&gt; lisamine

### Turbeteave

The released code has been GPG-signed using the following key: pub rsa2048/58D0EE648A48B3BB 2016-09-05 David Faure &lt;faure@kde.org&gt; Primary key fingerprint: 53E6 B47B 45CE A3E0 D5B7 4577 58D0 EE64 8A48 B3BB

Väljalaske üle arutada ja mõtteid jagada saab <a href='https://dot.kde.org/2014/07/07/kde-frameworks-5-makes-kde-software-more-accessible-all-qt-developers'>meie uudistelehekülje artikli</a> kommentaarides.
