---
aliases:
- ../announce-4.11.1
date: 2013-09-03
description: KDE Ships Plasma Workspaces, Applications and Platform 4.11.1.
title: KDE toob välja Plasma töötsoonide, rakenduste ja platvormi septembrikuised
  uuendused
---
3. september 2013. KDE laskis täna välja oma töötsoonide, rakenduste ja arendusplatvormi uuendused. Need on esimesed uuendused 4.11 seeria igakuiste stabiliseerimisuuenduste reas. Nagu väljalaske puhul teada anti, saavad töötsoonid uuendusi veel kahe aasta jooksul. See väljalase sisaldab ainult veaparandusi ja tõlgete uuendusi ning peaks olema kõigil turvaline ja mõnus uuendada.

Rohkem kui 70 teadaoleva veaparanduse hulka kuuluvad aknahalduri KWin, failihalduri Dolphin ja muude rakenduste täiustused. Loodetavasti käivitub Plasma töölaud nüüd kiiremini, Dolphin kerib sujuvamalt ning mitmed rakendused ja tööriistad tarvitavad vähem mälu. Muudest täiustustest võib märkida tegumiribalt töölaua vahetajasse lohistamise tagasitulekut, esiletõstmise ja värvide parandusi Kates ja PALJUDE pisivigade parandamist Kmahjonggi mängus. Rohkelt on stabiliseerimisparandusi ja, nagu ikka, on lisandunud tõlkeid.

A more complete <a href='https://bugs.kde.org/buglist.cgi?query_format=advanced&amp;short_desc_type=allwordssubstr&amp;short_desc=&amp;long_desc_type=substring&amp;long_desc=&amp;bug_file_loc_type=allwordssubstr&amp;bug_file_loc=&amp;keywords_type=allwords&amp;keywords=&amp;bug_status=RESOLVED&amp;bug_status=VERIFIED&amp;bug_status=CLOSED&amp;emailtype1=substring&amp;email1=&amp;emailassigned_to2=1&amp;emailreporter2=1&amp;emailcc2=1&amp;emailtype2=substring&amp;email2=&amp;bugidtype=include&amp;bug_id=&amp;votes=&amp;chfieldfrom=2011-06-01&amp;chfieldto=Now&amp;chfield=cf_versionfixedin&amp;chfieldvalue=4.11.1&amp;cmdtype=doit&amp;order=Bug+Number&amp;field0-0-0=noop&amp;type0-0-0=noop&amp;value0-0-0='>list</a> of changes can be found in KDE's issue tracker. For a detailed list of changes that went into 4.11.1, you can also browse the Git logs.

Lähtekoodi või paigaldatavate pakettide allalaadimiseks tasuks külastada <a href='/info/4/4.11.1'>4.11.1 teabelehekülge</a>. Täpsemat teavet KDE töötsoonide, rakenduste ja arendusplatvormi versiooni 4.11 kohta leiab <a href='/announcements/4.11/'>4.11 väljalaskemärkmetest</a>.

{{< figure class="text-center img-size-medium" src="/announcements/4/4.11.0/screenshots/send-later.png" caption=`Uus hiljem saatmise võimalus Kontactis` width="600px">}}

KDE software, including all libraries and applications, is available for free under Open Source licenses. KDE's software can be obtained as source code and various binary formats from <a href='http://download.kde.org/stable/4.11.1/'>download.kde.org</a> or from any of the <a href='/distributions'>major GNU/Linux and UNIX systems</a> shipping today.
