---
aliases:
- ../../kde-frameworks-5.70.0
date: 2020-05-02
layout: framework
libCount: 70
---
### Baloo

- [FileWatch] Ta bort redundant watchIndexedFolders() slot
- [ModifiedFileIndexer] Klargör en  kommentar
- [FileWatch] Rätta uppdatering av övervakare vid inställningsändringar
- [KInotify] Rätta matchning av sökvägar när övervakare tas bort
- [Extractor] Använd kategoriserad loggning
- Använd KFileMetaData för stöd av XAttr istället för privat återimplementering
- Återställ "lägg till Baloo D-Bus-signaler för flyttade eller borttagna filer"
- [QML Monitor] Visa återstående tid så snart som möjligt
- [FileContentIndexer] Rätta tillståndsuppdatering och signalordning
- [Monitor] Rätta övervakningstillstånd och signalordning
- [Extractor] Rätta förloppsrapportering
- [Coding] Undvik upprepade bortkopplings- och storlekskontroller
- [baloo_file] Ta bort KAboutData från baloo_file
- [Searchstore] Reservera utrymme för frågetermer för fraser 
- [SearchStore] Tillåt att fråga efter exakta träffar för icke-egenskaper
- [PhraseAndIterator] Ta bort tillfälliga fält när matchningar kontrolleras
- [Extractor] Bättre balans mellan overksamt och upptaget läge
- [Extractor] Rätta övervakning av overksamhet
- [Extractor] Ta bort omgivande klass IdleStateMonitor
- [OrpostingIterator] Tillåt att hoppa över element, implementera skipTo
- [PhraseAndIterator] Ersätt rekursiv implementering av next()
- [AndPostingIterator] Ersätt rekursiv implementering av next()
- [PostingIterator] Säkerställ att skipTo också fungerar för första elementet
- byt namn till och exportera newBatchTime signalen i filecontentindexer
- [SearchStore] Hantera dubbla värden i egenskapsförfrågningar
- [AdvancedQueryParser] Flytta semantisk hantering av symboler till SearchStore
- [Inotify] Ta bort not-so-OptimizedByteArray
- [Inotify] Ta bort död/duplicerad kod
- [QueryParser] Ersätt engångsanvänd hjälpfunktion med std::none_of

### Breeze-ikoner

- Flytta hörnveck för dokument längst upp till höger i två ikoner
- Lägg till 16 bildpunkters konversation ikon
- korrigera vscode ikonnamn
- Lägg till 16 bildpunkters Vvave ikon
- lägg till alligator ikon
- Lägg till ikonerna preferences-desktop-tablet och preferences-desktop-touchpad
- Uppdatera länkar i README.md
- build: lägg till citationstecken för källkatalogsökväg
- Tillåt att bygga från en skrivskyddad källplats
- Lägg till ikoner för expand/collaps som kompanjoner till befintliga ikonerna expand-all/collapse-all
- Lägg till auth-sim-locked och auth-sim-missing
- Lägg till sim card enhetsikoner
- Lägg till rotationsikoner
- Lägg till 16 bildpunkters systeminställningsikon
- Ändra ButtonFocus till Highlight
- Förbättra utseende hos kcachegrind
- Ta bort kant från format-border-set-* ikoner

### Extra CMake-moduler

- android: inkludera arkitekturen i apk-namnet
- ECMAddQch: rätta användning av citationstecken med PREDEFINED i doxygen inställning
- Anpassa FindKF5 till striktare kontroller i nyare find_package_handle_standard_args
- ECMAddQch: hjälp doxygen att hantera Q_DECLARE_FLAGS, så att sådana typer får dokumentation
- Rätta wayland scanner varningar
- ECM: försök att rätta KDEInstallDirsTest.relative_or_absolute på Windows

### KDE Doxygen-verktyg

- Rätta saknat blanktecken efter "Platform(s):" på framsidan
- Rätta användning av citationstecken för PREDEFINED poster i Doxygile.global
- Lär doxygen om Q_DECL_EQ_DEFAULT och Q_DECL_EQ_DELETE
- Lägg till låda på mobil och städa kod
- Lär doxygen om Q_DECLARE_FLAGS, så att sådana typer kan dokumenteras
- Konvertera till Aether Bootstrap 4
- Gör om api.kde.org att se mer ut som Aether

### KBookmarks

- Skapa alltid actioncollection
- [KBookMarksMenu] Ange objectName för newBookmarkFolderAction

### KCMUtils

- KSettings::Dialog: lägg till stöd för KPluginInfos utan en KService
- Liten optimering: anropa bara kcmServices() en gång
- Nergradera varningar om gammal stil av inställningsmodul till qDebug, tills KF6
- Använd ecm_setup_qtplugin_macro_names

### KConfig

- kconfig_compiler : generera kconfig inställningar med delgrupp
- Rätta några kompilatorvarningar
- Lägg till tvinga att spara beteende i KEntryMap
- Lägg till standardgenväg för "Visa/Dölj dolda filer" (fel 262551)

### KContacts

- Rikta in beskrivningen i metainfo.yaml med den i README.md

### KCoreAddons

- Dokumentation av programmeringsgränssnitt: använd ulong typedef med Q_PROPERTY(percent) för att undvika fel i doxygen
- Dokumentation av programmeringsgränssnitt: dokumentera Q_DECLARE_FLAGS-baserade flaggor
- Markera att uråldrig KLibFactory typedef avråds från
- [KJobUiDelegate] Lägg till flaggan AutoHandlingEnabled

### KCrash

- Ta bort klauncher användning från KCrash

### KDeclarative

- Namngiv innehållet i projektet kcmcontrols riktigt
- Justera dokumentation för kcmcontrols
- Lägg till metoden startCapture
- [KeySequenceHelper] Provisorisk lösning rörande Meta-väljarens beteende
- Släpp också fönstret i destruktorn

### KDED

- Konvertera KToolInvocation::kdeinitExecWait till QProcess
- Använd inte fördröjd andra fas

### KHolidays

- Nicaraguanska helger
- Taiwanesiska helger
- Uppdatera rumänska helger

### KI18n

- KI18N_WRAP_UI makro: ange egenskapen SKIP_AUTOUIC för ui-fil och gen. header

### KIconThemes

- Lägg till anmärkning om konvertering av loadMimeTypeIcon

### KImageFormats

- Lägg till stöd för moderna Gimp-bilder/XCF-filer

### KIO

- [RenameDialog] Lägg till en pil för att indikera riktningen från källa till mål (fel 268600)
- KIO_SILENT Justera dokumentation av programmeringsgränssnitt för att motsvara verkligheten
- Flytta hantering av opålitliga program till ApplicationLauncherJob
- Flytta kontroll av ogiltig tjänst från KDesktopFileActions till ApplicationLauncherJob
- Detektera körbara program utan rättigheten +x i $PATH för att förbättra felmeddelande (fel 415567)
- Gör HTML-filmallen mer användbar (fel 419935)
- Lägg till konstruktorn JobUiDelegate med flaggan AutoErrorHandling och fönster
- Rätta beräkning av cachekatalog vid tillägg i papperskorgen
- Filprotokoll: säkerställ att KIO::StatAcl fungerar utan att implicit bero på KIO::StatBasic
- Lägg till KIO::StatRecursiveSize detaljerat värde så att kio_trash bara gör detta vid behov
- CopyJob: vid stat av mål, använd StatBasic
- [KFileBookMarkHandler] Konvertera till ny KBookmarkMenu-5.69
- Markera att KStatusBarOfflineIndicator avråds från
- Ersätt KLocalSocket med QLocalSocket
- Undvik krasch i utgivningsläge efter varningen om oväntade underliggande objekt (fel 390288)
- Dokumentation: ta bort omnämnande av icke-existerande signal
- [renamedialog] Ersätt KIconLoader användning med QIcon::fromTheme
- kio_trash: Lägg till storlek, ändrings- och åtkomstdatum för trash:/ (fel 413091)
- [KDirOperator] Använd ny standardgenväg "Visa/Dölj dolda filer" (fel 262551)
- Visa förhandsgranskningar av krypterade filsystem (fel 411919)
- [KPropertiesDialog] Inaktivera ändring av ikoner för fjärrkatalog (fel 205954)
- [KPropertiesDialog] Rätta QLayout-varning
- Dokumentation av programmeringsgränssnitt: dokumentera flera av förvalda egenskapsvärden i KUrlRequester
- Rätta DirectorySizeJob så att det inte beror på listningsordning
- KRun: rätta assert vid misslyckande att starta ett program

### Kirigami

- Introducera Theme::smallFont
- Gör BasicListItem mer användbar genom att ge den egenskapen subtitle
- Mindre segmenteringsfelaktig PageRouterAttached
- PageRouter: hitta överliggande objekt bättre
- Ta bort oanvänd QtConcurrent från colorutils
- PlaceholderMessage: Ta bort användning av Plasma enheter
- Tillåt PlaceholderMessage att vara textfri
- centrera blad vertikalt om de inte har en rullningslist (fel 419804)
- Konto för övre och nedre marginal i förvald korthöjd
- Diverse rättningar för nya Cards (fel 420406)
- Icon: förbättra återgivning av ikoner för konfigurationer med flera skärmar och olika punkter/tum
- Rätta fel i PlaceholderMessage: åtgärder är inaktiverade, inte dolda
- Introducera komponenten PlaceholderMessage
- Snabbfix: rätta felaktiga typer i FormLayout fältfunktioner
- Snabbfix för SwipeListItem: använd Array.prototype.*.call
- Snabbfix: använd Array.prototype.some.call i ContextDrawer
- Snabbfix för D28666: använd Array.prototype.*.call istället för att anropa funktioner för 'list'-objekt
- Lägg till saknad variabel m_sourceChanged
- Använd ShadowedRectangle för Card-bakgrunder (fel 415526)
- Uppdatera synlighetskontroll av ActionToolbar genom att kontrollera bredden med mindre-"likhet"
- Några 'triviala' rättningar av felaktig kod
- stäng aldrig när klick är inne i bladets innehåll (fel 419691)
- blad måste vara under andra meddelanderutor (fel 419930)
- Lägg till komponenten PageRouter
- Lägg till ColorUtils
- Tillåt inställning av separat hörnradier för ShadowedRectangle
- Ta bort alternativet STATIC_LIBRARY för att rätta statiska byggen

### KJobWidgets

- Lägg till konstruktorn KDialogJobUiDelegate(KJobUiDelegate::Flags)

### KJS

- Implementera UString operator= för att göra gcc lycklig
- Tysta kompilatorvarning om kopiering av icke-trivial data

### KNewStuff

- KNewStuff: Rätta filsökväg och processanrop (fel 420312)
- KNewStuff: konvertera från KRun::runApplication till KIO::ApplicationLauncherJob
- Ersätt Vokoscreen med VokoscreenNG (fel 416460)
- Introducera mer användarsynlig felrapportering för installationer (fel 418466)

### KNotification

- Implementera uppdatering av underrättelser på Android
- Hantera underrättelser med flera rader och rich-text på Android
- Lägg till konstruktor KNotificationJobUiDelegate(KJobUiDelegate::Flags)
- [KNotificationJobUiDelegate] Lägg till "Failed" sist för felmeddelanden

### KNotifyConfig

- Använd knotify-config.h konsekvent för att skicka in flaggor om Canberra/Phonon

### KParts

- Lägg till StatusBarExtension(KParts::Part *) överlagrad konstruktor

### KPlotting

- Konvertera foreach (som avråds från) till range for

### Kör program

- DBus Runner: Lägg till tjänstegenskap för att begära åtgärder en gång (fel 420311)
- Skriv ut en varning om körprogram inte är kompatibelt med KRunner

### KService

- Avråd från KPluginInfo::service(), eftersom konstruktor med en KService avråds från

### KTextEditor

- rätta dra och släpp på vänster sidas kant i komponent (fel 420048)
- Lagra och hämta fullständig vyinställning i och från sessionsinställning
- Återställ för tidig konvertering till outgiven Qt 5.15 som ändrats i mellantiden

### KTextWidgets

- [NestedListHelper] Rätta indentering av markering, lägg till tester
- [NestedListHelper] Förbättra indenteringskod
- [KRichTextEdit] Säkerställ att rubriker inte stör ångring
- [KRichTextEdit] Rätta att rullningen hoppar omkring när horisontell regel läggs till (fel 195828)
- [KRichTextWidget] Ta bort uråldrig provisorisk lösning och rätta regression (incheckning 1d1eb6f)
- [KRichTextWidget] Lägg till stöd för rubriker
- [KRichTextEdit] Behandla alltid tangentnedtryckning som en enda ändring i ångring (fel 256001)
- [findreplace] Hantera sökning efter bara WholeWordsOnly i Regex-läge

### KUnitConversion

- Lägg till imperial gallon och amerikansk pint (fel 341072)
- Lägg till isländsk Krona i valutor

### Kwayland

- [Wayland] Lägg till PlasmaWindowManagement protokollfönster staplingsordning
- [server] Lägg till några livscykelsignaler för underytor

### KWidgetsAddons

- [KFontChooser] Ta bort NoFixedCheckBox DisplayFlag, redundant
- [KFontChooser] Lägg till ny DisplayFlag; ändra hur flaggor används
- [KFontChooser] Gör styleIdentifier() mer precis genom att lägga till styleName för teckensnitt (fel 420287)
- [KFontRequester] Konvertera från QFontDialog till KFontChooserDialog
- [KMimeTypeChooser] Lägg till möjlighet att filtrera trädvyn med en QSFPM (fel 245637)
- [KFontChooser] Gör koden något mer läsbar
- [KFontChooser] Lägg till en kryssruta för att ändra visning av bara teckensnitt med fast bredd
- Ta bort onödig include

### KWindowSystem

- Skriv ut betydelsefull varning när det inte finns någon QGuiApplication

### KXMLGUI

- [KRichTextEditor] Lägg still support för rubriker
- [KKeySequenceWidget] Provisorisk lösning för beteende hos Meta-väljare

### NetworkManagerQt

- Ersätt foreach med range-for

### Plasma ramverk

- [PlasmaCore.IconItem] Regression: rätta krasch vid källändring (fel 420801)
- [PlasmaCore.IconItem] Omstrukturering av källhantering för olika typer
- Gör textmellanrum för miniprogram verktygstips konsekvent
- [ExpandableListItem] gör den pekvänlig
- [ExpandableListItem] Använd mer semantiskt riktiga ikoner för expandera och dra ihop
- Rätta bindningssnurra för PC3 BusyIndicator
- [ExpandableListItem] Lägg till nytt alternativ showDefaultActionButtonWhenBusy
- Ta bort rundade kanter till plasmoidHeading
- [ExpandableListItem] Lägg till signalen itemCollapsed och skicka inte itemExpanded vid hopdragning
- Lägg till readme filer som klargör plasma komponentversioner
- [configview] Förenkla kod / provisorisk lösning för Qt5.15 krasch
- Skapa ExpandableListItem
- Gör animeringslängder konsekventa med Kirigami-värden

### QQC2StyleBridge

- Detektera QQC2 version vid byggtid med verklig detektering
- [ComboBox] Använd genomskinlig reostat

### Solid

- [Solid] Konvertera foreach till range/index for
- [FakeCdrom] Lägg till ett nytt uppräkningsvärde UnknownMediumType i MediumType
- [FstabWatcher] Rätta förlust av fstab övervakare
- [Fstab] Skicka inte deviceAdded två gånger vid ändring av fstab/mtab

### Syntaxfärgläggning

- debchangelog: lägg till Groovy Gorilla
- Uppdatera syntaxstöd för språket Logtalk
- TypeScript: lägg till typoperatorn "awaited"

### Säkerhetsinformation

Den utgivna koden GPG-signerad genom att använda följande nyckel: pub rsa2048/58D0EE648A48B3BB 2016-09-05 David Faure &lt;faure@kde.org&gt; Primärt nyckelfingeravtryck: 53E6 B47B 45CE A3E0 D5B7 4577 58D0 EE64 8A48 B3BB
