---
aliases:
- ../announce-applications-18.04.2
changelog: true
date: 2018-06-07
description: KDE levererar KDE-program 18.04.2
layout: application
title: KDE levererar KDE-program 18.04.2
version: 18.04.2
---
7:e juni, 2018. Idag ger KDE ut den andra stabilitetsuppdateringen av <a href='../18.04.0'>KDE-program 18.04</a>. Utgåvan innehåller bara felrättningar och översättningsuppdateringar, och är därmed en säker och behaglig uppdatering för alla.

Omkring 25 registrerade felrättningar omfattar förbättringar av bland annat Kontact, Cantor, Dolphin, Gwenview, KGpg, Kig, Konsole, Lokalize och Okular.

Förbättringar omfattar:

- Bildåtgärder i Gwenview kan nu göras om efter de har ångrats
- Kgpg misslyckas inte längre att avkryptera brev utan versionshuvud
- Export av Cantor-arbetsblad till Latex har rättats för Maxima-matriser
