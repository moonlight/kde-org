---
aliases:
- ../announce-applications-18.12.1
changelog: true
date: 2019-01-10
description: KDE levererar Program 18.12.1.
layout: application
major_version: '18.12'
title: KDE levererar KDE-program 18.12.1
version: 18.12.1
---
{{% i18n_date %}}

Idag ger KDE ut den första stabilitetsuppdateringen av <a href='../18.12.0'>KDE-program 18.12</a>. Utgåvan innehåller bara felrättningar och översättningsuppdateringar, och är därmed en säker och behaglig uppdatering för alla.

Omkring 20 registrerade felrättningar omfattar förbättringar av bland annat Kontact, Cantor, Dolphin, JuK, Kdenlive, Konsole och Okular.

Förbättringar omfattar:

- Akregator fungerar nu med WebEngine från Qt 5.11 eller senare
- Sortering av kolumner i musikspelaren JuK har rättats
- Konsole återger tecken för ritning av rutor riktigt igen
