---
aliases:
- ../announce-applications-18.08.2
changelog: true
date: 2018-10-11
description: KDE levererar KDE-program 18.08.2
layout: application
title: KDE levererar KDE-program 18.08.2
version: 18.08.2
---
11:e oktober, 2018. Idag ger KDE ut den andra stabilitetsuppdateringen av <a href='../18.08.0'>KDE-program 18.08</a>. Utgåvan innehåller bara felrättningar och översättningsuppdateringar, och är därmed en säker och behaglig uppdatering för alla.

Fler än ett dussin registrerade felrättningar omfattar förbättringar av bland annat Kontact, Dolphin, Gwenview, KCalc och Umbrello.

Förbättringar omfattar:

- Att dra en fil i Dolphin kan inte längre orsaka att namnbyte på plats påbörjas av misstag
- Kcalc tillåter åter båda tangenterna 'punkt' och 'kommatecken' när decimaler skrivs in
- En visuell glitch i kortleken Paris för KDE:s kortspel har rättats
