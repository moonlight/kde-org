---
aliases:
- ../announce-applications-16.04-beta
custom_spread_install: true
date: 2016-03-24
description: KDE levererar Program 16.04 Beta.
layout: application
release: applications-16.03.80
title: KDE levererar betaversion av KDE-program 16.04
---
24:e mars, 2016. Idag ger KDE ut betautgåvan av de nya versionerna av KDE-program. Med beroenden och funktioner frysta, fokuserar KDE-grupperna nu på att rätta fel och ytterligare finputsning.

Titta i <a href='https://community.kde.org/Applications/16.04_Release_Notes'>gemenskapens versionsfakta</a> för information om nya komprimerade arkiv, komprimerade arkiv som nu är baserade på KF5 och kända problem. Ett fullständigare meddelande kommer att vara tillgängligt för den slutliga utgåvan.

Med de olika programmen baserade på KDE Ramverk 5, behöver KDE-program utgåva 16.04 omfattande utprovning för att behålla och förbättra kvaliteten och användarupplevelsen. Verkliga användare är väsentliga för att upprätthålla hög kvalitet i KDE, eftersom utvecklare helt enkelt inte kan prova varje möjlig konfiguration. Vi räknar med dig för att hjälpa oss hitta fel tidigt, så att de kan krossas innan den slutliga utgåvan. Överväg gärna att gå med i gruppen genom att installera betaversionen och <a href='https://bugs.kde.org/'>rapportera eventuella fel</a>.

#### Installera KDE-program 16.04 Beta binärpaket

Vissa Linux- och UNIX-operativsystemleverantörer har vänligen tillhandahållit binärpaket av KDE-program 16.04 Beta (internt 16.03.80) för vissa versioner av sina distributioner, och i andra fall har volontärer i gemenskapen gjort det. Ytterligare binärpaket, samt uppdateringar av paketen som nu är tillgängliga, kan bli tillgängliga under kommande veckor.

<em>Paketplatser</em>. Besök <a href='http://community.kde.org/KDE_SC/Binary_Packages'>Gemenskapens Wiki</a> för en aktuell lista med tillgängliga binärpaket som har kommit till KDE-projektets kännedom.

#### Kompilera KDE-program 16.04 Beta

Fullständig källkod för KDE-program 16.04 Beta kan <a href='http://download.kde.org/unstable/applications/16.03.80/src/'>laddas ner fritt</a>. Instruktioner om hur man kompilerar och installerar är tillgängliga på <a href='/info/applications/applications-16.03.80.php'>informationssidan om KDE-program Beta</a>.
