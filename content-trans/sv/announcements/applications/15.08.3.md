---
aliases:
- ../announce-applications-15.08.3
changelog: true
date: 2015-11-10
description: KDE levererar KDE-program 15.08.3
layout: application
title: KDE levererar KDE-program 15.08.3
version: 15.08.3
---
10:e november, 2015. Idag ger KDE ut den tredje stabilitetsuppdateringen av <a href='../15.08.0'>KDE-program 15.08</a>. Utgåvan innehåller bara felrättningar och översättningsuppdateringar, och är därmed en säker och behaglig uppdatering för alla.

Mer än 20 registrerade felrättningar omfattar förbättringar av ark, dolphin, kdenlive, kdepim, kig, lokalize och umbrello.

Utgåvan inkluderar också versioner för långtidsunderhåll av KDE:s utvecklingsplattform 4.14.14.
