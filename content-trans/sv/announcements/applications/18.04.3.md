---
aliases:
- ../announce-applications-18.04.3
changelog: true
date: 2018-07-12
description: KDE levererar KDE-program 18.04.3
layout: application
title: KDE levererar KDE-program 18.04.3
version: 18.04.3
---
12:e juli, 2018. Idag ger KDE ut den tredje stabilitetsuppdateringen av <a href='../18.04.0'>KDE-program 18.04</a>. Utgåvan innehåller bara felrättningar och översättningsuppdateringar, och är därmed en säker och behaglig uppdatering för alla.

Omkring 20 registrerade felrättningar omfattar förbättringar av bland annat Kontact, Ark, Dolphin, Gwenview och Kmag.

Förbättringar omfattar:

- Kompatibilitet med IMAP-servrar som inte tillkännager sina funktioner har återställts
- Ark kan nu packa upp zip-arkiv som saknar riktiga poster för kataloger
- Anteckningar på skärmen följer åter muspekaren när de flyttas
