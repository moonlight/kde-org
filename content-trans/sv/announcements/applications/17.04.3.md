---
aliases:
- ../announce-applications-17.04.3
changelog: true
date: 2017-07-13
description: KDE levererar KDE-program 17.04.3
layout: application
title: KDE levererar KDE-program 17.04.3
version: 17.04.3
---
13:e juli, 2017. Idag ger KDE ut den tredje stabilitetsuppdateringen av <a href='../17.04.0'>KDE-program 17.04</a>. Utgåvan innehåller bara felrättningar och översättningsuppdateringar, och är därmed en säker och behaglig uppdatering för alla.

Mer än 25 registrerade felrättningar omfattar förbättringar av bland annat kdepim, dolphin, dragonplayer, kdenlive och umbrello.

Utgåvan inkluderar också versioner för långtidsunderhåll av KDE:s utvecklingsplattform 4.14.34.
