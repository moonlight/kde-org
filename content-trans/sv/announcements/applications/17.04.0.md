---
aliases:
- ../announce-applications-17.04.0
changelog: true
date: 2017-04-20
description: KDE levererar KDE-program 17.04.0
layout: application
title: KDE levererar KDE-program 17.04.0
version: 17.04.0
---
20:e april, 2017. KDE Program 17.04 är här. På det hela taget, har vi arbetat för att göra både programmen och de underliggande biblioteken stabilare och enklare att använda. Genom att undanröja rynkor och lyssna på återkoppling, har vi gjort KDE:s programsvit mindre benägen att råka ut för tekniska fel och mycket vänligare.

Njut av de nya programmen!

#### <a href="https://edu.kde.org/kalgebra/">Kalgebra</a>

{{<figure src="/announcements/applications/17.04.0/kalgebra1704.jpg" width="600px" >}}

Utvecklarna av Kalgebra följer sin egen särskilda väg mot konvergens, genom att ha konverterat mobilversionen av det omfattande utbildningsprogrammet till Kirigami 2.0: det ramverk som är att föredra för att integrera KDE-program på skrivbordet och på mobila plattformar.

Dessutom har skrivbordsversionen konverterat det tredimensionella gränssnittet till GLES, programvaran som låter programmet återge tredimensionella funktioner på både skrivbordet och mobila enheter. Det gör koden enklare och lättare att underhålla.

#### <a href="http://kdenlive.org/">Kdenlive</a>

{{<figure src="/announcements/applications/17.04.0/kdenlive1704.png" width="600px" >}}

KDE:s videoeditor blir stabilare och får fullständigare funktionalitet med varje ny utgåva. Denna gången har utvecklarna konstruerat om dialogrutan för profilval för att göra det enklare att ställa in skärmstorlek, bildhastighet och andra filmparametrar.

Nu kan man också spela upp videon direkt från underrättelsen om att återgivningen är klar. Några krascher som uppstod när klipp flyttades omkring på tidslinjen har rättats, och dvd-guiden har förbättrats.

#### <a href="https://userbase.kde.org/Dolphin">Dolphin</a>

{{<figure src="/announcements/applications/17.04.0/dolphin1704.png" width="600px" >}}

Vår favorit bland filutforskarna och portal till allting (utom möjligen dödsriket) har fått ett flertal förbättringar av stil och användarvänlighet för att göra den ännu kraftfullare.

De sammanhangsberoende menyerna i panelen <i>Platser</i> (normalt till vänster om huvudområdet) har snyggats till, och det är nu möjligt att interagera med de grafiska metadatakomponenterna i verktygstipsen. Förresten fungerar nu verktygstipsen också med Wayland.

#### <a href="https://www.kde.org/applications/utilities/ark/">Ark</a>

{{<figure src="/announcements/applications/17.04.0/ark1704.png" width="600px" >}}

Det populära grafiska programmet för att skapa, avkoda och hantera komprimerade arkiv med filer och kataloger levereras nu med en <i>sökfunktion</i> för att hjälpa till att hitta filer i tätpackade arkiv.

Det gör det också möjligt att aktivera eller inaktivera insticksprogram direkt i dialogrutan <i>Anpassa</i>. När det gäller insticksprogram, så förbättrar det nya insticksprogrammet Libzip stödet för Zip-arkiv.

#### <a href="https://minuet.kde.org/">Minuet</a>

{{<figure src="/announcements/applications/17.04.0/minuet1704.png" width="600px" >}}

Om du undervisar i eller studerar att spela musik, måste du ta en titt på Minuet. Den nya versionen erbjuder mer skalövningar och gehörsövningar för Bebop skalor, harmoniska dur- och mollskalor, pentatoniska och symmetriska skalor.

Det går också att skapa eller utföra prov med det nya <i>provläget</i> för att svara på övningar. Man kan hålla reda på sina framsteg genom att utföra en följd med tio övningar, och få statistik över träffsäkerheten när man är klar.

### Med mera

<a href='https://okular.kde.org/'>Okular</a>, KDE:s dokumentvisare, har fått minst ett halvdussin ändringar för att lägga till funktionalitet och skruva upp användbarheten på pekskärmar. <a href='https://userbase.kde.org/Akonadi'>Akonadi</a> och flera andra program som utgör <a href='https://www.kde.org/applications/office/kontact/'>Kontact</a> (KDE:s e-post-, kalender- och grupprogramsvit) har reviderats, avlusats och optimerats för att hjälpa dig att bli mer produktiv.

<a href='https://www.kde.org/applications/games/kajongg'>Kajongg</a>, <a href='https://www.kde.org/applications/development/kcachegrind/'>KCachegrind</a> med flera (<a href='https://community.kde.org/Applications/17.04_Release_Notes#Tarballs_that_were_based_on_kdelibs4_and_are_now_KF5_based'>Versionsfakta</a>) har nu konverterats till KDE Ramverk 5. Vi ser fram emot dina kommentarer och din insikt i de nyaste funktionerna som introduceras i den här utgåvan.

<a href='https://userbase.kde.org/K3b'>K3b</a> har sällat sig till utgivningen av KDE:s program.

### Felutplåning

Mer än 95 fel har rättats i program, inklusive Kopete, KWalletManager, Marble, Spectacle med flera.

### Fullständig ändringslogg
