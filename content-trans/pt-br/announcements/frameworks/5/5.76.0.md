---
aliases:
- ../../kde-frameworks-5.76.0
date: 2020-11-07
layout: framework
libCount: 70
qtversion: 5.12
---
### Baloo

* Divisão dos termos CJK nos caracteres de pontuação, optimização do código
* Remodelação do código na preparação da integração com o ICU

### Ícones Breeze

* Adição do ícone 'dialog-warning' de 48px
* Mudança do estilo do 'media-repeat-single' para usar o número 1
* Adição de mais ficheiros ignorados pelo Git
* verificação se o ficheiro existe antes de o remover
* Remover sempre o ficheiro de destino primeiro ao criar ligações simbólicas
* adição de alguns ícones de modos de cores do Okular
* Adição do ícone 'task-complete' (erro 397996)
* Adição do ícone 'network-limited'
* Cópia da ligação simbólica do 'kup' a 32px para o apps/48 para corrigir uma falha do teste escalável
* Adição dos ícones 'meeting-organizer' (erro 397996)
* Adição do ícone de impressão digital
* Adição dos ícones "task-recurring" e "appointment-recurring" (erro 392553)
* Desactivação temporária da geração de ícones no Windows
* Ligação simbólica do kup.svg para o preferences-system-backup.svg

### Módulos extra do CMake

* Fazer com que o 'androiddeployqt' descubra as bibliotecas e 'plugins' QML sem instalação
* find-modules/FindReuseTool.cmake - Correcção da pesquisa da ferramenta 'reuse'
* melhoria das opções predefinidas de formatação
* Inclusão de opção para usar o LLVM para os utilizadores com o Qt < 5.14
* Adição da versão mínima em falta do parâmetro RENAME
* Documentar quando foi adicionado o FindGradle
* Adição do FindGradle do KNotification

### KAuth

* Conversão do nome da infra-estrutura para maiúsculas mais cedo
* Adição de utilitário para obter o UID de quem fez a chamada

### KCalendarCore

* Elevar a ambiguidade no ICalFormat::toString() nos testes
* Adição da serialização da propriedade COLOR do RFC7986
* Fazer com que o MemoryCalendar::rawEvents(QDate, QDate) funcione para intervalos abertos

### KCMUtils

* Migração do QStandardPaths::DataLocation para o QStandardPaths::AppDataLocation
* Adição do suporte para espaços de nome na macro KCModuleData do CMake
* Descontinuação do KSettings::PluginPage
* remoção de referência a um cabeçalho indefinido
* Passagem do KCMUtilsGenerateModuleData para o local correcto
* Adição de todas as sub-páginas pré-criadas de um KCM
* Adição de função do CMake para gerar os dados básicos do módulo
* Melhoria da legibilidade do QML incorporado no kcmoduleqml.cpp
* altura adequada do cabeçalho com um item de substituição
* [kcmoduleqml] Correcção da margem superior dos KCM's em QML

### KConfig

* Adição de dependências do Qt5DBus em falta
* kconf_update: Permitir testes repetidos no --testmode, ignorando o 'kconf_updaterc'

### KConfigWidgets

* Mudança do http: para https:

### KContacts

* Correcção do Erro 428276 - o KContacts não pode ser usado num projecto 'qmake' (erro 428276)

### KCoreAddons

* KJob: adição do setProgressUnit(), para definir como é calculada a percentagem
* Correcção de fuga de memória potencial no KAboutData::registerPluginData
* Divisão do suggestName(); o método de divisão não verifica se o ficheiro existe
* KAboutData: descontinuação do pluginData() & registerPluginData()
* Não sair do ciclo de eventos no KJobTest::slotResult()
* Uso da função substituta singleShot() no TestJob::start()

### KDeclarative

* [abstractkcm] Definição de preenchimento explícito
* [simplekcm] Remoção do tratamento de preenchimentos personalizados
* [kcmcontrols] Remoção de código duplicado
* Adição da origem no KDeclarativeMouseEvent
* mudança do item-pai das 'overlaysheets' para o topo
* Mudança do GridViewKCM e ScrollVieKCM para herdarem de AbstractKCM
* Adição de método de leitura do 'subPages'

### KDocTools

* Correcção da formatação de XML do contributor.entities
* Actualização para Coreano: formatação dos ficheiros HTML da GPL, FDL e adição da LGPL

### KFileMetaData

* [ExtractionResult] Reposição da compatibilidade binária
* [TaglibWriter|Extractor] Remoção do tipo MIME do 'speex' em bruto
* [TaglibWriter] Abrir o modo de leitura-escrita também no Windows
* [Extractor|WriterCollection] Remoção dos ficheiros não-bibliotecas
* [EmbeddedImageData] Tentar contrariar os problemas do MSVC
* [ExtractionResult] Descontinuação do ExtractEverything, corrigido desde
* [EmbeddedImageData] Ler a imagem de verdade nos testes apenas uma vez
* [EmbeddedImageData] Remoção da implementação privada da gravação de capas
* [EmbeddedImageData] Passagem da implementação da escrita para o 'plugin' de escrita da 'taglib'
* [EmbeddedImageData] Remoção da implementação privada da extracção de capas
* [EmbeddedImageData] Passagem da implementação para o 'plugin' de extracção da 'taglib'

### KGlobalAccel

* activação do 'dbus' no 'systemd'

### KIconThemes

* Manter as proporções de tamanho ao aumentar a escala

### KIdleTime

* Descontinuação do sinal com um argmento KIdleTime::timeoutReached(int identificador)

### KImageFormats

* Adição do suporte para ficheiros PSD com 16 bits por canal e comprimidos com RLE
* Devolver 'não suportado' ao ler ficheiros PSD comprimidos com RLE de 16 'bits'
* funcionalidade: adição do suporte para o formato PDS com profundidade de cor == 16

### KIO

* comparação condicional com um QUrl em branco em vez do '/' no Windows, no caso do 'mkpathjob'
* KDirModel: duas correcções do QAbstractItemModelTester
* CopyJob: Inclusão dos ficheiros ignorados no cálculo do progresso na mudança de nomes
* CopyJob: não contar os ficheiros ignorados na notificação (erro 417034)
* Nas janelas de ficheiros, seleccione uma pasta existente ao tentar criá-la
* CopyJob: correcção do número total de ficheiros/pastas na janela de progresso (ao mover)
* Mudança para um comportamento mais consistente do FileJob::write()
* Suporte dos 'xattrs' na cópia/movimentação do KIO
* CopyJob: não contar os tamanhos das pastas no tamanho total
* KNewFileMenu: Correcção de estoiro ao tentar usar o 'm_text' em vez do 'm_lineEdit->text()'
* FileWidget: Mostrar a antevisão do ficheiro seleccionado ao afastar o cursor (erro 418655)
* exposição do campo de ajuda de contexto do utilizador no 'kpasswdserver'
* KNewFileMenu: uso do NameFinderJob para obter um nome "Nova Pasta"
* Introdução do NameFinderJob que sugere nomes para a "Nova Pasta"
* Não definir explicitamente as linhas Exec nos KCMs (erro 398803)
* KNewFileMenu: Divisão do código de criação da janela para um método separado
* KNewFileMenu: verificação se o ficheiro não existe já com algum atraso, para uma melhor usabilidade
* [PreviewJob] Reserva de memória suficiente no segmento de SHM (erro 427865)
* Uso do mecanismo de versionamento para adicionar os novos locais nos utilizadores existentes
* Adição de favoritos para as imagens, músicas e vídeos (erro 427876)
* kfilewidget: manter o texto no campo Nome ao navegar (erro 418711)
* Tratamento dos KCMs no OpenUrlJob com a API KService
* Canonização da localização do ficheiro ao obter e criar miniaturas
* KFilePlacesItem: esconder as montagens de 'sshfs' do 'kdeconnect'
* OpenFileManagerWindowJob: selecção da janela correcta na tarefa principal
* Evitar testes inúteis nas imagens de miniaturas inexistentes
* [ERRO] Correcção de regressão ao seleccionar ficheiros que contêm `#`
* KFileWidget: fazer com que os botões de ampliação passem para o tamanho-padrão mais próximo
* colocação do 'minimumkeepsize' de facto no KCM do 'netpref' (erro 419987)
* KDirOperator: simplificação da lógica da barra de ampliação dos ícones
* UDSEntry: documentação do formato de horas esperado para as chaves de horas
* kurlnavigatortest: remoção do 'desktop:', precisa do 'desktop.protocol' para funcionar
* KFilePlacesViewTest: não mostrar nenhuma janela, por não ser necessário
* OpenFileManagerWindowJob: Correcção de estoiro ao recuar para a estratégia do KRun (erro 426282)
* Palavras-chave da Internet: correcção de estoiro e testes falhados se o separador for um espaço
* Preferir os códigos do DuckDuckGo face aos outros separadores
* KFilePlacesModel: ignorar os locais escondidos ao calcular o 'closestItem' (erro 426690)
* SlaveBase: documentação do comportamento do ERR_FILE_ALREADY_EXIST com o copy()
* kio_trash: correcção da lógica onde não é definido nenhum tamanho-limite (erro 426704)
* Nas janelas de ficheiros, a criação de uma pasta que já existe deverá seleccioná-la
* KFileItemActions: Adição de propriedade para o número mín/máx de Url's

### Kirigami

* [avatar]: Transformar os números em nomes inválidos
* [avatar]: Exposição da propriedade 'cache' da imagem
* Definir também um 'maximumWidth' para os ícones na área global (erro 428658)
* Transformar o atalho 'sair' numa acção e expô-lo como uma propriedade apenas para leitura
* Uso dos cursores de mãos no ListItemDragHandle (erro 421544)
* [controls/avatar]: Suporte para nomes CJK nas iniciais
* Melhoria da aparência do FormLayout em dispositivos móveis
* Correcção dos menus no 'contextualActions'
* Não alterar o Item no código invocado pelo destruidor do Item (erro 428481)
* não modificar as outras 'reversetwins' da disposição
* Atribuir/retirar o facto da folha da camada ao abrir/fechar
* Só arrastar a janela pela barra global quando carregar & arrastar
* Fechar o OverlaySheet ao carregar na tecla Esc
* Page: Colocar o 'padding', 'horizontalPadding' e 'verticalPadding' a funcionar
* ApplicationItem: Adição da propriedade 'background'
* AbstractApplicationItem: adição das propriedades & comportamento em falta do ApplicationWindow do QQC2
* limitação da largura dos itens à largura da disposição
* Correcção do botão 'Recuar' que não aparece nos cabeçalhos das páginas em camadas em dispositivos móveis
* Silenciar o aviso do ciclo de associação do "checkable" no ActionToolBar
* troca da ordem das colunas nos formatos RTL
* truque para garantir que o 'ungrabmouse' é sempre chamado
* verificação da existência do 'startSystemMove'
* correcção do separador nas disposições em espelho
* arrastar a janela ao carregar nas áreas em branco
* não deslocar por arrastamento com o rato
* Correcção de casos em que a resposta é nula
* Correcção da remodelação com problemas do Forward/BackButton.qml
* Garantia que um ícone vazio está Pronto e não é pintado como o ícone anterior
* Restrição da altura do botão avançar/recuar no PageRowGlobalToolBarUI
* Silenciar o lixo na consola do ContextDrawer
* Silenciar o lixo na consola do ApplicationHeader
* Silenciar o lixo na consola do 'back/forwardbutton'
* Evitar que o arrastamento do rato arraste um OverlaySheet
* Eliminação do prefixo da biblioteca na compilação no Windows
* correcção da gestão do alinhamento do 'twinformlayouts'
* Melhoria da legibilidade do QML incorporado no código em C++

### KItemModels

* KRearrangeColumnsProxyModel: correcção de um estoiro quando não existe um modelo de origem
* KRearrangeColumnsProxyModel: só a coluna 0 tem filhas

### KNewStuff

* Correcção da lógica errada introduzida no 'e1917b6a'
* Correcção de estoiro por 'delete' duplo no 'kpackagejob' (erro 427910)
* Descontinuação do Button::setButtonText() e correcção da documentação da API, onde nada é antecedido
* Passagem de todas as escritas na 'cache' do disco para mais tarde até que se tenha um segundo de sossego
* Correcção de estoiro quando a lista de ficheiros instalados é vazia

### KNotification

* Marcação do KNotification::activated() como obsoleto
* Aplicar algumas verificações de sanidade das teclas de acções (erro 427717)
* Uso do FindGradle do ECM
* Correcção da condição para usar o 'dbus'
* Correcção: activação da bandeja antiga nas plataformas sem 'dbus'
* remodelação doo 'notifybysnore' para ter um suporte mais fiável para o Windows
* Adição de comentários para descrever o campo DesktopEntry nos ficheiros 'notifyrc'

### Plataforma KPackage

* Tornar o aviso de "sem meta-dados" apenas algo para depuração

### KPty

* Remoção do suporte para AIX, Tru64, Solaris, Irix

### KRunner

* Descontinuação dos métodos obsoletos do RunnerSyntax
* Descontinuação do 'ignoreTypes' e do RunnerContext::Type
* Não definir o tipo como Ficheiro/Pasta se não existir (erro 342876)
* Actualização do responsável de manutenção, como discutido na lista do correio
* Descontinuação do construtor não usado do RunnerManager
* Descontinuação da funcionalidade de categorias
* Remoção da verificação desnecessária se o módulo de execução está suspenso
* Descontinuação dos métodos 'defaultSyntax' e 'setDefaultSyntax'
* Limpeza de utilizações antigas do RunnerSyntax

### KService

* Permitir a utilização de aplicações com NotShowIn=KDE, apresentadas no mimeapps.list (erro 427469)
* Criação de valor de salvaguarda para as linhas Exec do KCM que tenham um executável apropriado (erro 398803)

### KTextEditor

* [EmulatedCommandBar::switchToMode] Não fazer nada quando os modos antigo e novo são os mesmos (erro 368130 como se segue :)
* KateModeMenuList: remoção das margens especiais do Windows
* Correcção de fuga de memória no KateMessageLayout
* tentar evitar apagar estilos personalizados nos realces que não foram mexidos de todo (erro 427654)

### KWayland

* Criação de métodos de conveniência em torno do wl_data_offet_accept()
* Marcação dos enumerados num Q_OBJECT como Q_ENUM

### KWidgetsAddons

* Novo 'setUsernameContextHelp' no KPasswordDialog
* KFontRequester: remoção do utilitário agora redundante 'nearestExistingFont'

### KWindowSystem

* xcb: Correcção da detecção dos tamanhos de ecrãs de PPP's elevados

### NetworkManagerQt

* Adição de enumerado e declarações para permitir a passagem de capacidades ao processo de registo no NetworkManager

### Plasma Framework

* Componente BasicPlasmoidHeading
* Mostrar sempre os botões ExpandableListitem, não apenas à passagem (erro 428624)
* [PlasmoidHeading]: Definição adequada do tamanho implícito
* Bloqueio das cores do cabeçalho dos temas Brisa Escuro e Brisa Claro (erro 427864)
* Unificação das proporções de tamanho dos ícones da bateria a 32px e 22px
* Adição de sugestões de margens no toolbar.svg e remodelação do ToolBar do PC3
* Adição do AbstractButton e do Pane ao PC3
* suporte para grupos de acções exclusivos nas acções de contexto
* Correcção de novo da rotação do BusyIndicator mesmo quando está invisível
* Correcção das cores que não eram aplicadas no ícone de mudança de tarefas em dispositivos móveis
* Adição do selector de tarefas do Plasma em dispositivos móveis e ícones para fechar a aplicação (no painel de tarefas)
* Melhor Menu no PlasmaComponents3
* Remoção de âncoras desnecessárias no ComboBox.contentItem
* Posição da pega da barra arredondada
* [ExpandableListItem] Carregar a vista expandida a pedido
* Adição do `PlasmaCore.ColorScope.inherit: false` em falta
* Definição do colorGroup no PlasmoidHeading no elemento de topo
* [ExpandableListItem] Tornar o texto colorido 100% opaco (erro 427171)
* BusyIndicator: Não rodar quando estiver invisível (erro 426746)
* O ComboBox3.contentItem deverá ser um QQuickTextInput para corrigir a completação automática (erro 424076)
* FrameSvg: Não limpar a 'cache' ao dimensionar
* Comutação dos plasmóides quando o atalho for activado (erro 400278)
* TextField 3: Adição de importação em falta
* Correcção dos IDs no ícone do 'plasmavault_error'
* PC3: correcção da cor da legenda do TabButton
* Uso de uma sugestão em vez de um booleano
* Permitir aos plasmóides ignorar as margens

### Purpose

* Adição de descrição ao fornecedor de YouTube do kaccounts

### QQC2StyleBridge

* Correcção do ciclo de associações do 'contentWidth' do ToolBar
* Referência directa à legenda de atalho por ID em vez de ser de forma implícita
* O ComboBox.contentItem deverá ser um QQuickTextInput para corrigir a completação automática (erro 425865)
* Simplificação das cláusulas condicionais no Connections
* Correcção do aviso do Connections no ComboBox
* Adição do suporte para os ícones .qrc no StyleItem (erro 427449)
* Indicação correcta do estado do foco do ToolButton
* Adição do TextFieldContextMenu para os menus de contexto do botão direito no TextField e no TextArea
* Definição de cor de fundo do ScrollView do ComboBox

### Solid

* Adição do suporte para o 'sshfs' na infra-estrutura do 'fstab'
* CMake: Uso do pkg_search_module ao carregar o 'plist'
* Correcção da infra-estrutura imobiledevice: Verificação da versão da API no DEVICE_PAIRED
* Correcção da compilação da infra-estrutura do 'imobiledevice'
* Adição da infra-estrutura do Solid ao usar a 'libimobiledevice' para procurar dispositivos de iOS
* Uso do QHash para a associação onde não for necessária a ordem

### Sonnet

* Uso da nova sintaxe de ligações 'sinal-slot'

### Realce de sintaxe

* A função de base "compact" está em falta
* comentar a verificação e comentar porque não funciona mais aqui
* Falta o valor do 'position:sticky'
* Correcção da geração do 'php/*' no novo realce de Comentários
* Funcionalidade da sintaxe do Alerts por Special-Comments e remoção das linhas de modo
* Funcionalidade: Adição do `comments.xml` como uma sintaxe de cobertura de vários tipos de comentários
* Correcção: a sintaxe do CMake agora marca o `1` e o `0` como valores booleanos especiais
* Melhoria: Inclusão das regras de linhas de modo onde foi adicionado o Alerts
* Melhoria: Adição de mais alguns valores booleanos no `cmake.xml`
* Temas Solares: melhoria do separador
* Melhoria: Actualizações para o CMake 3.19
* Adição do suporte para os ficheiros de unidades do 'systemd'
* debchangelog: adição do Hirsute Hippo
* Funcionalidade: Permitir várias opções `-s` na ferramenta `kateschema2theme`
* Melhoria: Adição de vários testes ao conversor
* passagem de mais utilitários para uma localização melhor
* passagem do programa 'update-kate-editor-org.pl' para uma localização melhor
* kateschema2theme: Adição de uma ferramenta em Python para converter os ficheiros no esquema antigo
* Diminuição da opacidade do separador dos temas Brisa & Drácula
* Actualização do README com a secção "Ficheiros de temas de cores"
* Correcção: Uso do `KDE_INSTALL_DATADIR` quando instalar os ficheiros de sintaxes
* correcção do desenho do --syntax-trace=region com vários gráficos na mesma posição
* correcção de alguns problemas na consola do 'fish'
* substituição do StringDetect pelo DetectChar / Detect2Chars
* Substituição de alguns RegExpr's pelo StringDetect
* Substituição da RegExpr="." + verificação posterior por um 'fallthroughContext'
* substituição do \s* pelo DetectSpaces

### Informações de segurança

O código lançado foi assinado com GPG, usando a seguinte chave: pub rsa2048/58D0EE648A48B3BB 2016-09-05 David Faure &lt;faure@kde.org&gt; Impressão digital da chave primária: 53E6 B47B 45CE A3E0 D5B7  4577 58D0 EE64 8A48 B3BB
