---
aliases:
- ../../kde-frameworks-5.7.0
date: '2015-02-14'
layout: framework
libCount: 60
src: /announcements/frameworks/5-tp/KDE_QT.jpg
---
### Geral

- Um conjunto de correções para compilação com a futura versão 5.5 do Qt

### KActivities

- Correção do início e interrupção das atividades
- Correção da visualização da atividade, que às vezes mostrava um papel de parede incorreto

### KArchive

- Criação de arquivos temporários na pasta temporária, em vez do atual

### KAuth

- Correção da geração dos arquivos de serviços auxiliares D-Bus do KAuth

### KCMUtils

- Correção de validação quando os caminhos do D-Bus contêm um '.'

### KCodecs

- Adição de suporte para CP949 no KCharsets

### KConfig

- O <i>kconf_update</i> não processa mais os arquivos *.upd do KDE SC 4. Adição de "Version=5" no início do arquivo <i>upd</i> para as atualizações que devem ser aplicadas aos aplicativos Qt5/KF5
- Correção do KCoreConfigSkeleton ao alternar um valor com gravações no intervalo

### KConfigWidgets

- KRecentFilesAction: Correção da ordem de itens do menu (para corresponder à ordem do <i>kdelibs4</i>)

### KCoreAddons

- KAboutData: Chamar o addHelpOption e o addVersionOption automaticamente, por conveniência e consistência
- KAboutData: Retorno do "Use a página http://bugs.kde.org para relatar erros." quando nenhum outro e-mail/URL estiver definido
- KAutoSaveFile: O allStaleFiles() agora funciona como esperado para os arquivos locais e também foi corrigido o staleFiles()
- O KRandomSequence agora usa inteiros internamente e expõe a API de inteiros para remover a ambiguidade dos 64 bits
- Definições de tipos MIME: Os arquivos *.qmltypes e *.qmlproject agora também têm o tipo MIME text/x-qml
- KShell: Fazer o quoteArgs codificar as URLs com o QChar::isSpace(), uma vez que os espaços fora do normal não eram devidamente tratados
- KSharedDataCache: Correção da criação da pasta que contém o <i>cache</i> (erro de migração)

### KDBusAddons

- Adição do método auxiliar KDEDModule::moduleForMessage para criar mais serviços do tipo do <i>kded</i>, como o <i>kiod</i>

### KDeclarative

- Adição de um componente para criar gráficos
- Adição do método de substituição do Formats::formatDuration, que recebe um número inteiro
- Novas propriedades <i>paintedWidth</i> e <i>paintedHeight</i> para o QPixmapItem e o QImageItem
- Correção da pintura do QImageItem e do QPixmapItem

### Kded

- Adição de suporte para o carregamento de módulos do <i>kded</i> com metadados em JSON

### KGlobalAccel

- Agora inclui o componente de execução, tornando-se assim um <i>framework</i> de nível 3 (tier 3)
- A infraestrutura Windows voltou a funcionar
- Reativação da infraestrutura para Mac
- Correção da falha durante a finalização da execução em X11 do KGlobalAccel

### KI18n

- Marcar os resultados como obrigatórios no aviso, quando a API não é usada corretamente
- Adição da opção de compilação BUILD_WITH_QTSCRIPT para permitir um conjunto de funcionalidades reduzido em sistemas embarcados

### KInit

- OSX: Carregamento das bibliotecas compartilhadas corretas na inicialização
- Correções de compilação do Mingw

### KIO

- Correção da falha nas tarefas ao compilar com o KIOWidgets, mas usando apenas uma QCoreApplication
- Correção da edição dos atalhos da Web
- Adição da opção KIOCORE_ONLY, para compilar apenas o KIOCore e seus programas auxiliares, mas não o KIOWidgets ou o KIOFileWidgets, reduzindo consideravelmente as dependências necessárias
- Adição da classe KFileCopyToMenu, que adiciona as opções "Copiar para / Mover para" nos menus de contexto
- Protocolos com SSL ativo: Adição de suporte para os protocolos TLSv1.1 e TLSv1.2, remoção do SSLv3
- Correção do <i>negotiatedSslVersion</i> e <i>negotiatedSslVersionName</i> para devolver o protocolo negociado real
- Aplicação da URL inserida na janela, ao clicar no botão que muda o navegador de volta para o modo de navegação
- Correção das duas janelas/barras de progresso que aparecem nas tarefas de cópia/movimentação de arquivos
- O KIO agora usa seu próprio servidor para reduzir as dependências, chamado <i>kiod</i>, usado para serviços fora do processo que eram anteriormente executados no <i>kded</i>; no momento apenas substitui o <i>kssld</i>
- Correção do erro "Não é possível gravar no &lt;caminho&gt;" quando o <i>kioexec</i> é acionado
- Correção dos avisos "QFileInfo::absolutePath: Construído com um nome de arquivo em branco" ao usar o KFilePlacesModel

### KItemModels

- Correção do KRecursiveFilterProxyModel para o Qt 5.5.0+, devido ao fato de o QSortFilterProxyModel agora usar o parâmetro <i>roles</i> para o sinal <i>dataChanged</i>

### KNewStuff

- Sempre recarrega os dados em XML das URLs remotas

### KNotifications

- Documentação: Menção dos requisitos do nome do arquivo nos arquivos <i>.notifyrc</i>
- Correção de um ponteiro pendente no KNotification
- Correção de vazamento no <i>knotifyconfig</i>
- Instalação do cabeçalho ausente do <i>knotifyconfig</i>

### KPackage

- Renomeação da página de manual (man) do kpackagetool para kpackagetool5
- Correção da instalação nos sistemas de arquivos sem distinção de maiúsculas e minúsculas

### Kross

- Correção do Kross::MetaFunction para que funcione com o sistema de meta-objetos do Qt5

### KService

- Inclusão das propriedades desconhecidas quando converter o KPluginInfo a partir do KService
- KPluginInfo: Correção das propriedades não copiadas a partir do KService::Ptr
- OS X: Correção de desempenho para o kbuildsycoca4 (ignorar os grupos de aplicativos)

### KTextEditor

- Correção da rolagem em <i>touchpads</i> de alta precisão
- Não emitir o <i>documentUrlChanged</i> durante o recarregamento
- Não quebrar a posição do cursor no recarregamento de documentos nas linhas com tabulações
- Não voltar a (des)dobrar a primeira linha, caso tenha sido (des)dobrada manualmente
- Modo VI: Histórico de comandos através das teclas direcionais
- Não tentar criar um código de validação ao receber um sinal KDirWatch::deleted()
- Desempenho: Remoção das inicializações globais

### KUnitConversion

- Correção da recursividade infinita no Unit::setUnitMultiplier

### KWallet

- Detecção e conversão automática das carteiras antigas de ECB para CBC
- Correção do algoritmo de criptografia em CBC
- Garantia que a lista de carteiras é atualizada quando um arquivo de carteira é removido do disco
- Remoção do &lt;/p&gt; excedente no texto visível ao usuário

### KWidgetsAddons

- Uso do <i>kstyleextensions</i> para indicar um elemento de controle personalizado para desenhar a barra do <i>kcapacity</i> quando suportado, permitindo que o widget possa ser devidamente estilizado
- Fornecer um nome acessível para o KLed

### KWindowSystem

- Correção do NETRootInfo::setShowingDesktop(bool) que não funcionava no Openbox
- Adição do método de conveniência KWindowSystem::setShowingDesktop(bool)
- Correções no tratamento do formato dos ícones
- Adição do método NETWinInfo::icccmIconPixmap, que oferece uma imagem de ícone da propriedade WM_HINTS
- Adição de substituto do KWindowSystem::icon que reduz as chamadas ao servidor X
- Adição de suporte para o _NET_WM_OPAQUE_REGION

### NetworkmanagerQt

- Não apresenta uma mensagem sobre a ausência de tratamento da propriedade "AccessPoints"
- Adição de suporte para o NetworkManager 1.0.0 (não necessário)
- Correção do tratamento de senhas do VpnSetting
- Adição da classe GenericSetting para as conexões não gerenciadas pelo NetworkManager
- Adição da propriedade AutoconnectPriority para ConnectionSettings

#### Framework do Plasma

- Correção da abertura inválida de um menu de contexto com problemas, ao abrir a janela do Plasma com o botão do meio
- Mudança do botão de ativação com a roda do mouse
- Nunca redimensionar uma janela maior que a tela
- Recuperação dos painéis quando um miniaplicativo é recuperado
- Correção de teclas de atalho
- Restauração do suporte ao hint-apply-color-scheme
- Recarregamento da configuração com as alterações do <i>plasmarc</i>
- ...

### Solid

- Adição do <i>energyFull</i> e do <i>energyFullDesign</i> à Battery

### Alterações no sistema de compilação (extra-cmake-modules)

- Novo módulo ECMUninstallTarget para criar um alvo de desinstalação
- Fazer o KDECMakeSettings importar o ECMUninstallTarget por padrão
- KDEInstallDirs: Avisar sobre a mistura de caminhos de instalação relativos e completos na linha de comandos
- Fazer com que o módulo ECMAddAppIcon adicione os ícones para os alvos executáveis no Windows e Mac OS X
- Correção do aviso CMP0053 com o CMake 3.1
- Não limpar as variáveis de cache no KDEInstallDirs

### Frameworkintegration

- Correção da atualização da definição de clique simples durante a execução
- Diversas correções na integração com a área de notificação
- Instalação do esquema de cores apenas nos widgets de topo (para corrigir o QQuickWidgets)
- Atualização da configuração do XCursor nas plataformas X11

Você pode discutir e compartilhar ideias sobre esta versão na seção de comentários do <a href='https://dot.kde.org/2014/07/07/kde-frameworks-5-makes-kde-software-more-accessible-all-qt-developers'>artigo do Dot</a>.
