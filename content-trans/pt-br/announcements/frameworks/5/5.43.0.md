---
aliases:
- ../../kde-frameworks-5.43.0
date: 2018-02-12
layout: framework
libCount: 70
src: /announcements/frameworks/5-tp/KDE_QT.jpg
---
### Novos módulos

KHolidays: biblioteca de cálculo de feriados

Esta biblioteca oferece uma API em C++ que determina os feriados e outros eventos especiais para uma região geográfica.

Purpose: Oferece as acções disponíveis para um determinado fim

Esta plataforma oferece a possibilidade de criar serviços e acções integrados em qualquer aplicação sem ter de os implementar de forma específica. O Purpose fornecerá os mecanismos para listar as diferentes alternativas a executar, dado o tipo de acção pedido e simplificará os componentes, de forma que todos os 'plugins' consigam receber toda a informação que necessitam.

### Baloo

- balooctl status: Produção de resultados mais fáceis de processar
- Correcção das cópias completas das pastas marcadas no KIO Slave. Isto quebra a listagem das pastas marcadas na árvore de marcas, mas é melhor que cópias com problemas
- Parar a colocação em espera de ficheiros novos e não-indexáveis, retirando-os imediatamente do índice
- Apagar os novos ficheiros não-indexáveis do índice

### Ícones Breeze

- Adição de ícones em falta do Krusader para a sincronização de pastas (erro 384473)
- Adição do ícone do 'list-remove' com um '-' em vez do ícone para cancelar (erro 382650)
- adição de ícones para o plasmóide do Pulseaudio (erro 385294)
- Usar a mesma opacidade 0,5 em todo o lado
- Novo ícone do VirtualBox (erro 384357)
- tornar o 'weather-fog' neutro ao longo do dia/noite (erro 388865)
- instalação de facto do contexto das novas animações
- A aparência do MIME dos ficheiros QML agora parece igual em todos os tamanhos (erro 376757)
- Actualização dos ícones de animação (erro 368833)
- adição de um ícone 'emblem-shared' colorido
- Correcção de ficheiros 'index.theme' com problemas; o "Context=Status" faltava no 'status/64'
- Remoção da permissão 'executável' dos ficheiros .svg
- O ícone de acção para transferências está associada ao 'edit-download' (erro 382935)
- Actualização do ícone de bandeja do Dropbox (erro 383477)
- Faltava o 'emblem-default-symbolic' (erro 382234)
- Escrita do nome do ficheiro do tipo MIME (erro 386144)
- Uso de um logótipo mais específico do Octave (erro 385048)
- adição dos ícones dos cofres (erro 386587)
- Escala em 'px' dos ícones de estado (erro 386895)

### Módulos extra do CMake

- FindQtWaylandScanner.cmake: Uso do 'qmake-query' para o HINT
- Certificação de que se pesquisa o 'qmlplugindump' baseado no Qt5
- O ECMToolchainAndroidTest já não existe mais (erro 389519)
- Não definir o LD_LIBRARY_PATH no prefix.sh
- Adição do FindSeccomp ao 'find-modules'
- Voltar ao nome da língua nas traduções, caso falhe o nome do 'locale' (região)
- Android: Adição de mais inclusões

### KAuth

- Correcção de regressão da compilação introduzida no 5.42.

### KCMUtils

- Adição de dicas aos dois botões em cada elemento

### KCompletion

- Correcção de emissão incorrecta do textEdited() pelo KLineEdit (erro 373004)

### KConfig

- Uso do Ctrl+Shift+ como atalho-padrão para o "Configurar o &lt;Programa&gt;"

### KCoreAddons

- Corresponder também as chaves do 'spdx' ao LGPL-2.1 &amp; LGPL-2.1+
- Uso do método urls(), muito mais rápido, do QMimeData (erro 342056)
- Optimização da infra-estrutura de 'inotify' do KDirWatch: associar o 'wd' do 'inotify' ao Entry
- Optimização: uso do QMetaObject::invokeMethod com uma função

### KDeclarative

- [ConfigModule] Reutilização do contexto e motor do QML, se existir (erro 388766)
- [ConfigPropertyMap] Adição de inclusão em falta
- [ConfigPropertyMap] Não emitir o 'valueChanged' na criação inicial

### KDED

- Não exportar o 'kded5' como alvo do CMake

### KDELibs 4 Support

- Reorganização do Solid::NetworkingPrivate para ter uma implementação partilhada e outra específica da plataforma
- Correcção do erro de compilação do Mingw "src/kdeui/kapplication_win.cpp:212:22: error: 'kill' was not declared in this scope" (o 'kill' não foi declarado neste ponto)
- Correcção do nome de D-Bus do 'kded' no HOWTO do 'solid-networking'

### KDesignerPlugin

- Tornar a dependência do 'kdoctools' opcional

### KDESU

- Fazer o modo KDESU_USE_SUDO_DEFAULT compilar de novo
- Fazer com que o 'kdesu' funcione quando o PWD for igual a '/usr/bin'

### KGlobalAccel

- Uso da função do CMake 'kdbusaddons_generate_dbus_service_file' do 'kdbusaddons' para gerar o ficheiro do serviço DBus (erro 382460)

### Complementos da interface KDE

- Correcção da compilação do ficheiro QCH criado na documentação do QtGui

### KI18n

- Correcção da pesquisa pelo 'libintl' numa compilação "multi-plataforma" dos pacotes nativos do Yocto

### KInit

- Correcção da compilação multi-plataforma com o MinGW (MXE)

### KIO

- Reparação da cópia de ficheiros para o VFAT sem avisos
- kio_file: ignorar o tratamento de erros das permissões iniciais durante a cópia de ficheiros
- kio_ftp: não emitir um sinal de erro antes de serem testados todos os comandos de listagem (erro 387634)
- Performance: uso do objecto KFileItem de destino para descobrir se pode ser escrito em vez de criar um KFileItemListProperties
- Performance: Uso do construtor por cópia do KFileItemListProperties em vez da conversão de um KFileItemList para um KFileItemListProperties. Isto evita ter de avaliar de novo todos os itens
- Melhoria do tratamento de erros no IO-slave 'file'
- Remoção da opção da tarefa PrivilegeExecution
- KRun: permitir a execução de "add network folder" (adicionar uma pasta de rede) sem uma mensagem de confirmação
- Permitir filtrar os locais com base no nome alternativo da aplicação
- [Fornecedor de Pesquisa do Filtro de URI] Evitar um 'delete' duplo (erro 388983)
- Correcção da sobreposição do primeiro item no KFilePlacesView
- Desactivação temporária do suporte do KAuth no KIO
- previewtest: Permitir a definição dos 'plugins' activos
- [KFileItem] Uso do "emblem-shared" nos ficheiros partilhados
- [DropJob] Activação do arrastamento numa pasta apenas para leitura
- [FileUndoManager] Activação a anulação de alterações em pastas apenas para leitura
- Adição do suporte para a execução de privilégios nas tarefas do KIO (temporarariamente desactivado nesta versão)
- Adição do suporte para a partilha de descritores de ficheiros entre o 'KIO slave' 'file' e o seu utilitário KAuth
- Correcção do KFilePreviewGenerator::LayoutBlocker (erro 352776)
- O KonqPopupMenu/Plugin consegue agora usar a chave X-KDE-RequiredNumberOfUrls para necessitar a selecção de um dado número de ficheiros antes de ser apresentado
- [KPropertiesDialog]: Activação da mudança de linha na descrição do código de validação
- Uso da função do CMake 'kdbusaddons_generate_dbus_service_file' do 'kdbusaddons' para gerar o ficheiro do serviço DBus (erro 388063)

### Kirigami

- suporte para ColorGroups (grupos de cores)
- não efectuar nenhuma reacção ao 'click' se o item não desejar eventos do rato
- solução alternativa para as aplicações que usam os itens da lista incorrectamente
- espaço para a barra de deslocamento (erro 389602)
- Colocação de uma dica para a acção principal
- cmake: Uso da variável oficial do CMake para compilar como um 'plugin' estático
- Actualização legível para o utilizador da designação do nível na documentação da API
- [ScrollView] Deslocar uma página com o Shift+roda
- [PageRow] Navegação entre níveis com os botões para recuar/avançar do rato
- Garantir que o DesktopIcon é desenhado com as proporções de tamanho correctas (erro 388737)

### KItemModels

- KRearrangeColumnsProxyModel: correcção de um estoiro quando não existe um modelo de origem
- KRearrangeColumnsProxyModel: reimplementação do sibling() para que funcione como seria de esperar

### KJobWidgets

- Remoção de duplicação do código no byteSize(double tamanho) (erro 384561)

### KJS

- Tornar a dependência do 'kdoctools' opcional

### KJSEmbed

- Não exportar o 'kjscmd'
- Tornar a dependência do 'kdoctools' opcional

### KNotification

- A acção de notificação do "Executar um Comando" foi corrigida (erro 389284)

### KTextEditor

- Correcção: A janela salta quando está activada a opção para 'Deslocar para além do fim do documento' (erro 306745)
- Uso pelo menos da largura pedida para a árvore de sugestões de argumentos
- ExpandingWidgetModel: descobrir a coluna mais à direita com base na localização

### KWidgetsAddons

- KDateComboBox: correcção do dateChanged() que não é emitido após escrever uma data (erro 364200)
- KMultiTabBar: Correcção de regressão na conversão para o novo estilo do 'connect()'

### Plasma Framework

- Definição de propriedade no Units.qml para os estilos do Plasma
- windowthumbnail: Correcção do código de selecção do GLXFBConfig
- [Dica por Omissão] Correcção do dimensionamento (erro 389371)
- [Janela do Plasma] Invocação dos efeitos da janela, apenas se estiver visível
- Correcção de uma fonte de lixo nos registos referenciada no Erro 388389 (Nome de ficheiro vazio passado à função)
- [Calendário] Ajuste das âncoras da barra de ferramentas do calendário
- [ConfigModel] Definição do contexto de QML no ConfigModule (erro 388766)
- [Item de Ícones] Tratar as fontes que começam com uma barra como um ficheiro local
- correcção da aparência RTL do ComboBox (erro 387558)

### QQC2StyleBridge

- Adição do BusyIndicator à lista de controlos com estilo
- remoção de intermitência ao passar o cursor sobre a barra de deslocamento

### Solid

- [UDisks] Só ignorar o ficheiro de cópia "não-pelo-utilizador" apenas se for conhecido (erro 389358)
- Os dispositivos de armazenamento montados fora da pasta /media, /run/media e $HOME são agora ignorados, assim como os dispositivos de ficheiros cujo (erro 319998)
- [Dispositivo do UDisks] Mostrar o dispositivo 'loop' com o seu nome e ícone do ficheiro referenciado

### Sonnet

- Pesquisa dos dicionários do Aspell no Windows

### Realce de sintaxe

- Correcção da expressão regular do 'var' no C#
- Suporte para sublinhados nos literais numéricos(Python 3.6) (erro 385422)
- Realce dos ficheiros do Khronos Collada e glTF
- Correcção no realce do INI dos valores que contêm caracteres ';' ou '#'
- AppArmor: novas palavras-chave, melhorias &amp; correcções

### Informações de segurança

O código lançado foi assinado com GPG, usando a seguinte chave: pub rsa2048/58D0EE648A48B3BB 2016-09-05 David Faure &lt;faure@kde.org&gt; Impressão digital da chave primária: 53E6 B47B 45CE A3E0 D5B7  4577 58D0 EE64 8A48 B3BB

Você pode discutir e compartilhar ideias sobre esta versão na seção de comentários do <a href='https://dot.kde.org/2014/07/07/kde-frameworks-5-makes-kde-software-more-accessible-all-qt-developers'>artigo do Dot</a>.
