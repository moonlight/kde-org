---
aliases:
- ../announce-applications-17.12.1
changelog: true
date: 2018-01-11
description: O KDE disponibiliza o KDE Applications 17.12.1
layout: application
title: O KDE disponibiliza o KDE Applications 17.12.1
version: 17.12.1
---
11 de Janeiro de 2018. Hoje o KDE lançou a primeira actualização de estabilidade para as <a href='../17.12.0'>Aplicações do KDE 17.12</a>. Esta versão contém apenas correcções de erros e actualizações de traduções, pelo que será uma actualização segura e agradável para todos.

As mais de 20 correcções de erros registadas incluem as melhorias no Kontact, no Dolphin, no Filelight, no Gwenview, no KGet, no Okteta, no Umbrello, entre outros.

As melhorias incluem:

- O envio de e-mails no Kontact foi corrigido para alguns servidores de SMTP
- A linha temporal do Gwenview e as pesquisas por marcas foi melhorada
- A importação de Java foi corrigida na ferramenta de diagramas de UML Umbrello
