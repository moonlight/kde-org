---
aliases:
- ../announce-applications-18.12.0
changelog: true
date: 2018-12-13
description: O KDE Lança as Aplicações do KDE 18.12.
layout: application
release: applications-18.12.0
title: O KDE Lança as Aplicações do KDE 18.12.0
version: 18.12.0
---
{{% i18n_date %}}

As Aplicações do KDE 18.12 foram agora lançadas.

{{%youtube id="ALNRQiQnjpo"%}}

Nós trabalhamos continuamente em melhorar o software incluso na nossa série aplicativos do KDE e esperamos que você ache úteis todas as novas melhorias e consertos de bugs!

## O que há de novo nas Aplicações do KDE 18.12

Mais de 140 erros foram resolvidos em aplicativos incluindo a suíte Kontact, Ark, Cantor, Dolphin, Gwenview, Kate, KmPlot, Konsole, Lokalize, Okular, Spectacle, Umbrello e mais!

### Gerenciador de arquivos

{{<figure src="/announcements/applications/18.12.0/app1812_dolphin01.png" width="600px" >}}

<a href='https://www.kde.org/applications/system/dolphin/'>Dolphin</a>, o poderoso gestor de ficheiros do KDE:

- Nova implementação de MTP que a torna completamente possível de usar em produção
- Grande melhoria de performance para ler ficheiros sobre o protocolo SFTP
- Para as antevisões das miniaturas, as molduras e sombras agora só são desenhadas nos ficheiros de imagens sem transparência, melhorando a aparência dos ícones
- Novas antevisões de Miniaturas para documentos do LibreOffice e aplicações AppImage
- Os ficheiros de vídeo com mais de 5 MB de tamanho agora aparecem nas miniaturas das pastas, quando a funcionalidade para tal estiver activa
- Ao ler CD's de áudio, o Dolphin agora consegue mudar a taxa de dados CBR para o codificador de MP3 e corrige as datas/horas para o FLAC
- O menu 'Controlo' do Dolphin agora mostra os itens 'Criar um Novo…' e tem uma nova opção 'Mostrar os Locais Escondidos'
- O Dolphin agora sai quando existe apenas uma página aberta e se carrega no atalho de teclado para 'Fechar a página' (Ctrl+W)
- Depois de desmontar um volume do painel de Locais, agora é possível montá-lo de novo
- A área de Documentos Recentes (disponível se navegar em recentdocuments:/ no Dolphin) agora mostra os documentos actuais, filtrando automaticamente os URL's da Web
- O Dolphin agora mostra um aviso antes de lhe permitir mudar o nome do ficheiro ou pasta, de forma a que ele ficasse escondido imediatamente
- Já não é possível tentar desmontar os discos para o seu sistema operativo activo ou pasta pessoal no painel de Locais

O <a href='https://www.kde.org/applications/utilities/kfind'>KFind</a>, a pesquisa de ficheiros tradicional do KDE, agora tem um método de pesquisa de meta-dados baseado no KFileMetaData.

### Escritório

<a href='https://www.kde.org/applications/internet/kmail/'>KMail</a>, o cliente de e-mail poderoso do KDE:

- O KMail agora consegue mostrar uma caixa de entrada unificada
- Novo 'plugin': Gerar um e-mail em HTML a partir da linguagem Markdown
- Uso do Purpose para Partilhar Texto (como E-mail)
- Os e-mails por HTML agora são legíveis, independentemente do esquema de cores usado

{{<figure src="/announcements/applications/18.12.0/app1812_okular01.png" width="600px" >}}

<a href='https://www.kde.org/applications/graphics/okular/'>Okular</a>, o visualizador de documentos versátil do KDE:

- Nova ferramenta de anotação 'Máquina de Escrever' que pode ser usada para escrever texto em qualquer lado
- A área do índice hierárquico agora também a possibilidade de expandir e fechar tudo ou apenas uma secção específica
- Comportamento de mudança de linha melhorado nas anotações incorporadas
- Ao passar o cursor do rato sobre uma ligação, o URL agora aparece sempre que se carregar nele, em vez de ser apenas no modo de Navegação
- Os ficheiros ePub que contêm recursos com espaços nos seus URL's agora aparecem correctamente

### Desenvolvimento

{{<figure src="/announcements/applications/18.12.0/app1812_kate01.png" width="600px" >}}

<a href='https://www.kde.org/applications/utilities/kate/'>Kate</a>, o editor de texto avançado do KDE:

- Ao usar o terminal incorporado, agora sincroniza automaticamente a pasta actual com a localização do documento activo no disco
- O terminal incorporado do Kate agora pode entrar e sair de primeiro plano se usar a tecla F4
- O selector de páginas incorporado do Kate agora mostra as localizações completas para os ficheiros com nomes semelhantes
- Os números de linha agora estão activos por omissão
- O 'plugin' extremamente útil e poderoso que é o Filtro de Texto agora está activo por omissão e é mais fácil de descobrir
- Se abrir um documento já aberto com a funcionalidade de Abertura Rápida, agora muda de volta para esse documento
- A funcionalidade de Abertura Rápida agora já não mostra mais elementos duplicados
- Ao usar várias actividades, os ficheiros agora são abertos na actividade correcta
- O Kate agora mostra todos os ícones correctos quando executar no GNOME com o tema de ícones do mesmo

{{<figure src="/announcements/applications/18.12.0/app1812_konsole.png" width="600px" >}}

<a href='https://www.kde.org/applications/system/konsole/'>Konsole</a>, o emulador de terminal do KDE:

- O Konsole agora suporta por completo os caracteres de emojis
- Os ícones das páginas inactivas agora ficam realçados quando recebem um sinal de campainha
- Os dois-pontos finais agora não fazem parte de uma palavra quando o objectivo é seleccionar com um duplo-click, tornando mais fácil seleccionar locais e resultados do 'grep'
- Quando for ligado um rato com botões para avançar e recuar, o Konsole consegue agora usar esses botões para mudar de páginas
- O Konsole agora tem um item de menu para repor o tamanho do texto para a predefinição do perfil, caso o tamanho tenha sido aumentado ou reduzido
- As páginas agora são mais difíceis de separar e são também mais rápidas a reordenar
- Melhoria no comportamento da selecção com Shift+click
- Correcção do duplo-click numa linha de texto que ultrapasse a largura da janela
- A barra de pesquisa mais uma vez volta a fechar quando carregar na tecla Escape

<a href='https://www.kde.org/applications/development/lokalize/'>Lokalize</a>, a ferramenta de traduções do KDE:

- Esconder os ficheiros traduzidos na página do Projecto
- Adição do suporte básico para o 'pology', o sistema de verificação de sintaxe e glossário
- Navegação simplificada com a ordenação das páginas e a abertura de várias páginas
- Correcção de erros de memória devido ao acesso concorrente a objectos da base de dados
- Correcção de arrastar-e-largar inconsistente
- Correcção de um erro nos atalhos que eram diferentes entre editores
- Melhoria no comportamento da pesquisa (a mesma irá procurar e apresentar as formas plurais)
- Reposição da compatibilidade com o Windows, graças ao sistema de compilação Craft

### Utilitários

<a href='https://www.kde.org/applications/graphics/gwenview/'>Gwenview</a>, o visualizador de imagens do KDE:

- A ferramenta de 'Redução dos Olhos Vermelhos' recebeu uma grande variedade de melhorias de usabilidade agradáveis
- O Gwenview agora mostra uma janela de aviso, quando esconder o menu, que lhe indica como repô-lo de volta

{{<figure src="/announcements/applications/18.12.0/app1812_spectacle01.png" width="600px" >}}

<a href='https://www.kde.org/applications/graphics/spectacle/'>Spectacle</a>, o utilitário de capturas do ecrã do KDE:

- O Spectacle agora tem a capacidade de numerar sequencialmente os ficheiros das fotografias, voltando a este esquema de nomes caso limpe o campo de texto do nome do ficheiro
- Correcção da gravação das imagens, num formato que não o .png, quando estiver a usar o Gravar Como…
- Ao usar o Spectacle para abrir uma fotografia numa aplicação externa, agora é possível modificar e gravar a imagem depois de terminar
- O Spectacle agora abre a pasta correcta quando carregar em Ferramentas > Abrir a Pasta das Fotografias
- As datas/horas das fotografias agora reflectem o momento em que foi criada a imagem, e não a hora em que foi gravada
- Todas as opções de gravação agora estão localizadas na página "Gravar"

<a href='https://www.kde.org/applications/utilities/ark/'>Ark</a>, o gestor de pacotes do KDE:

- Adição do suporte para o formato Zstandard (pacotes tar.zst)
- Correcção da antevisão de certos ficheiros no Ark (p.ex., Open Document) como pacotes, em vez de os abrir na aplicação apropriada

### Matemática

O <a href='https://www.kde.org/applications/utilities/kcalc/'>KCalc</a>, a calculadora simples do KDE, agora tem uma opção para repetir o último cálculo várias vezes.

<a href='https://www.kde.org/applications/education/cantor/'>Cantor</a>, a interface matemática do KDE:

- Adição do formato de introdução de dados em Markdown
- Realce animado do elemento de comando em cálculo de momento
- Visualização dos elementos de comandos pendentes (em espera, mas ainda por calcular)
- Possibilidade de formatar os elementos dos comandos (cor de fundo, cor principal, propriedades dos tipos de letra)
- Possibilidade de inserir novos elementos de comandos em lugares arbitrários da folha de trabalho, colocando o cursor na posição desejada e começando a escrever
- Para as expressões que têm vários comandos, mostra os resultados como objectos independentes na folha de trabalho
- Adição do suporte para abrir folhas de trabalho com base em locais relativos da consola
- Adição do suporte para abrir vários ficheiros numa única consola do Cantor
- Muda a cor e o tipo de letra ao perguntar por informações adicionais, para poder discriminar melhor a introdução normal de dados no elemento do comando
- Adição de combinações de teclas para a navegação pelas folhas de trabalho (Ctrl+PageUp, Ctrl+PageDown)
- Adição de acção no submenu 'Ver' para repor o nível de ampliação
- Activar a transferência de projectos do Cantor a partir do store.kde.org (de momento, o envio só funciona a partir da página Web)
- Abertura da folha de trabalho no modo apenas para leitura se a infra-estrutura não estiver disponível no sistema

O <a href='https://www.kde.org/applications/education/kmplot/'>KmPlot</a>, o desenhador de gráficos de funções do KDE, corrigiu muitos problemas:

- Correcção de nomes errados nos gráficos das derivadas e integrais na notação convencional
- A exportação para SVG no Kmplot não funciona correctamente
- A funcionalidade da primeira derivada não tem nenhuma notação prima
- Ao carregar fora de uma função não-focada, agora esconde o seu gráfico:
- Resolução de um estoiro no Kmplot quando é aberta a área para 'Editar Constantes' no editor de funções de forma recursiva
- Resolução de estoiro no KmPlot depois de apagar uma função que o cursor do rato esteja a seguir no gráfico
- Pode agora exportar os dados desenhados em qualquer formato de imagem
