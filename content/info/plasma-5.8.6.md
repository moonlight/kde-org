---
version: "5.8.6"
title: "KDE Plasma 5.8.6, Bugfix Release"
errata:
    link: https://community.kde.org/Plasma/5.8_Errata
    name: 5.8 Errata
type: info/plasma5
signer: Jonathan Riddell
signing_fingerprint: EC94D18F7F05997E
---

This is a Bugfix release of KDE Plasma, featuring Plasma Desktop and
other essential software for your computer. Details in the
[Plasma 5.8.6 announcement](/announcements/plasma-5.8.6).


