<table border="0" cellpadding="4" cellspacing="0">
<tr valign="top"><th align="left">Location</th><th align="left">Size</th><th align="left">SHA1&nbsp;Sum</th></tr>
<tr valign="top"><td><a href="http://download.kde.org/stable/4.4.1/src/kdeaccessibility-4.4.1.tar.bz2">kdeaccessibility-4.4.1</a></td><td align="right">5,4MB</td><td><tt>1c4b1e207e7ff1c6afef4744aceff0aaa2f8e7bb</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/stable/4.4.1/src/kdeadmin-4.4.1.tar.bz2">kdeadmin-4.4.1</a></td><td align="right">1,4MB</td><td><tt>cec6799673fc8f38c75a0e6bb9257e41f61a78c4</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/stable/4.4.1/src/kdeartwork-4.4.1.tar.bz2">kdeartwork-4.4.1</a></td><td align="right">70MB</td><td><tt>f078955fb7987f7b48bce57585c4fa832d433137</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/stable/4.4.1/src/kdebase-4.4.1.tar.bz2">kdebase-4.4.1</a></td><td align="right">3,9MB</td><td><tt>b89a0019859e64d2c45965bacf3d2b09d67a908c</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/stable/4.4.1/src/kdebase-runtime-4.4.1.tar.bz2">kdebase-runtime-4.4.1</a></td><td align="right">7,1MB</td><td><tt>56623a20d92d15921fe9a97247dc1f289c15ab69</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/stable/4.4.1/src/kdebase-workspace-4.4.1.tar.bz2">kdebase-workspace-4.4.1</a></td><td align="right">74MB</td><td><tt>f72e0e31c2ea4dc377e7fc14f2a0f5fb6304e71e</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/stable/4.4.1/src/kdebindings-4.4.1.tar.bz2">kdebindings-4.4.1</a></td><td align="right">4,9MB</td><td><tt>bc5fcf04c4721014fbe11ca5b876173530625bbe</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/stable/4.4.1/src/kdeedu-4.4.1.tar.bz2">kdeedu-4.4.1</a></td><td align="right">58MB</td><td><tt>1c2472795575f095378315f7aafe1884e7a311a0</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/stable/4.4.1/src/kdegames-4.4.1.tar.bz2">kdegames-4.4.1</a></td><td align="right">66MB</td><td><tt>5119d7725c7a8c7684e0f8517391fffc6156e093</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/stable/4.4.1/src/kdegraphics-4.4.1.tar.bz2">kdegraphics-4.4.1</a></td><td align="right">3,7MB</td><td><tt>8c752fd84212f6a646805f041faa0056e5786878</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/stable/4.4.1/src/kdelibs-4.4.1.tar.bz2">kdelibs-4.4.1</a></td><td align="right">14MB</td><td><tt>2430d0c19bc4d62d942de4918e3f73dd11ed6ae5</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/stable/4.4.1/src/kdemultimedia-4.4.1.tar.bz2">kdemultimedia-4.4.1</a></td><td align="right">1,6MB</td><td><tt>00dbc693abe71a656f55fef13bfedbc3da340bfb</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/stable/4.4.1/src/kdenetwork-4.4.1.tar.bz2">kdenetwork-4.4.1</a></td><td align="right">8,1MB</td><td><tt>05499b52a4fa652df0a3bb929da9f545b3069ba7</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/stable/4.4.1/src/kdepim-4.4.1.tar.bz2">kdepim-4.4.1</a></td><td align="right">9,0MB</td><td><tt>cd2e0fb48c1fe4a1b7840d098fd07ab8cc9751a4</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/stable/4.4.1/src/kdepimlibs-4.4.1.tar.bz2">kdepimlibs-4.4.1</a></td><td align="right">2,4MB</td><td><tt>7afe04c73e8106c1909ca3279458ea424318028f</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/stable/4.4.1/src/kdepim-runtime-4.4.1.tar.bz2">kdepim-runtime-4.4.1</a></td><td align="right">616KB</td><td><tt>dccaf4d469419488cdc0b997081f56fd96cad3a2</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/stable/4.4.1/src/kdeplasma-addons-4.4.1.tar.bz2">kdeplasma-addons-4.4.1</a></td><td align="right">1,6MB</td><td><tt>434243c3fa70dc760544edafeb63f01e7d773fc0</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/stable/4.4.1/src/kdesdk-4.4.1.tar.bz2">kdesdk-4.4.1</a></td><td align="right">5,5MB</td><td><tt>fac5201094bef3f440cb450cd1f9f9d7e3d1c518</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/stable/4.4.1/src/kdetoys-4.4.1.tar.bz2">kdetoys-4.4.1</a></td><td align="right">1,3MB</td><td><tt>c7ba0ef349d89262ac055c5047081b541393d5e3</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/stable/4.4.1/src/kdeutils-4.4.1.tar.bz2">kdeutils-4.4.1</a></td><td align="right">2,6MB</td><td><tt>4dc059b4200935e52699e76bc086bc5255383cc8</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/stable/4.4.1/src/kdewebdev-4.4.1.tar.bz2">kdewebdev-4.4.1</a></td><td align="right">2,1MB</td><td><tt>ac38af13f55bcfbb13f88e024cf351ab3a0883e9</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/stable/4.4.1/src/oxygen-icons-4.4.1.tar.bz2">oxygen-icons-4.4.1</a></td><td align="right">128MB</td><td><tt>227be8d4d2c8dfeff1a2e2a21e13db45002f8eed</tt></td></tr>
</table>
