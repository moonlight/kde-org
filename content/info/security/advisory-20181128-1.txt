KDE Project Security Advisory
=============================

Title:          messagelib: HTML email can open browser window automatically
Risk Rating:    Low
CVE:            CVE-2018-19516
Versions:       KDE Applications < 18.12.0
Date:           28 November 2018


Overview
========
messagelib is the library used by KMail to display emails.

messagelib by default displays emails as plain text, but gives the user
an option to "Prefer HTML to plain text" in the settings and if that option
is not enabled there is way to enable HTML display when an email contains HTML.

Some HTML emails can trick messagelib into opening a new browser window when
displaying said email as HTML.

This happens even if the option to allow the HTML emails to access
remote servers is disabled in KMail settings.

This means that the owners of the servers referred in the email can see
in their access logs your IP address.

Workaround
==========
Do not enable "Prefer HTML to plain text" in KMail settings.

Solution
========
Update to KDE Applications >= 18.12.0
Or apply the following patch:
    https://commits.kde.org/messagelib/34765909cdf8e55402a8567b48fb288839c61612

Credits
=======
Thanks to Jany Belluz for the report and to Laurent Montel for the fix.
