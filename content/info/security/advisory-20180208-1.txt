KDE Project Security Advisory
=============================

Title:          Plasma: Notifications can expose user IP address
Risk Rating:    Low
CVE:            CVE-2018-6790
Versions:       Plasma < 5.12.0
Date:           8 February 2018


Overview
========
Plasma has support for the Desktop Nofications specification. That specification allows
embedding images in notifications. Plasma was not sanitizing the HTML that forms the notification.
That allowed for notifications to load a remote image leaking the user IP address. This is in turn
made a bit worse by the fact that some chat software doesn't sanitize the text they send to the
notification system either meaning that a third party could send a carefully crafted message
to a chat room and get the IP addresses of the users in that chat room.

Workaround
==========
Disable notifications

Solution
========
Update to Plasma >= 5.12.0 or Plasma >= 5.8.9

Or apply the following patches:
Plasma 5.8: https://commits.kde.org/plasma-workspace/5bc696b5abcdb460c1017592e80b2d7f6ed3107c

Credits
=======
Thanks to David Edmundson for the fix.
