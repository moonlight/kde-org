---
title: "KDE Applications 15.08.0 Info Page"
announcement: /announcements/announce-applications-15.08.0
layout: applications
build_instructions: https://techbase.kde.org/Getting_Started/Build/Historic/KDE4
---
