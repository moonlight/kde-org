---
version: "6.0.0"
title: "KDE Plasma 6.0.0, Feature Release"
errata:
    link: https://community.kde.org/Plasma/Plasma_6.0_Release_notes
    name: 6.0 Release Notes
type: info/plasma5
signer: Jonathan Riddell
signing_fingerprint: EC94D18F7F05997E
unstable: false
---

This is a Feature release of KDE Plasma, featuring Plasma Desktop and
other essential software for your computer. Details in the
[KDE 6 MegaRelease announcement](/announcements/megarelease/6/).
