---
version: "5.29.0"
title: "KDE Frameworks 5.29.0 Source Info and Download"
type: info/frameworks
date: 2016-12-12
patch_level:
- 5.29.1
---
