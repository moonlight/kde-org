    <table border="0" cellpadding="2" cellspacing="0">
      <tr valign="top">
        <th align="left">Location</th>

        <th align="left">Size</th>

        <th align="left">MD5&nbsp;Sum</th>
      </tr>

      <tr valign="top">
        <td><a href="http://download.kde.org/stable/3.1/src/arts-1.1.tar.bz2">arts-1.1</a></td>

        <td align="right">949KB</td>

        <td><tt>c1c34063de7df4ac6cae0984803d34e1</tt></td>
      </tr>

      <tr valign="top">
        <td><a href="http://download.kde.org/stable/3.1/src/kde-i18n-3.1.tar.bz2">kde-i18n-3.1</a></td>

        <td align="right">132MB</td>

        <td><tt>b2885be0195dcf0b8e81c81e51b16635</tt></td>
      </tr>

      <tr valign="top">
        <td><a href="http://download.kde.org/stable/3.1/src/kdeaddons-3.1.tar.bz2">kdeaddons-3.1</a></td>

        <td align="right">1.1MB</td>

        <td><tt>25d9d456ea43e152ab25e30392b90829</tt></td>
      </tr>

      <tr valign="top">
        <td><a href="http://download.kde.org/stable/3.1/src/kdeadmin-3.1.tar.bz2">kdeadmin-3.1</a></td>

        <td align="right">1.5MB</td>

        <td><tt>4ceed5f59fc5960105278be58ab80f80</tt></td>
      </tr>

      <tr valign="top">
        <td><a href="http://download.kde.org/stable/3.1/src/kdeartwork-3.1.tar.bz2">kdeartwork-3.1</a></td>

        <td align="right">14MB</td>

        <td><tt>f9ca096f60eb3c4673d2a0d9e9d0d563</tt></td>
      </tr>

      <tr valign="top">
        <td><a href="http://download.kde.org/stable/3.1/src/kdebase-3.1.tar.bz2">kdebase-3.1</a></td>

        <td align="right">15MB</td>

        <td><tt>02f0583c7de93cdc458101a7575455b4</tt></td>
      </tr>

      <tr valign="top">
        <td><a href="http://download.kde.org/stable/3.1/src/kdebindings-3.1.tar.bz2">kdebindings-3.1</a></td>

        <td align="right">5.9MB</td>

        <td><tt>05dad7c3e6644c6993601b718834cbf6</tt></td>
      </tr>

      <tr valign="top">
        <td><a href="http://download.kde.org/stable/3.1/src/kdeedu-3.1.tar.bz2">kdeedu-3.1</a></td>

        <td align="right">20MB</td>

        <td><tt>ed587675e6b4a18a416c05e0a106e8a7</tt></td>
      </tr>

      <tr valign="top">
        <td><a href="http://download.kde.org/stable/3.1/src/kdegames-3.1.tar.bz2">kdegames-3.1</a></td>

        <td align="right">8.0MB</td>

        <td><tt>cfc3fef5f162dc7ecd9465a11bdf9b1b</tt></td>
      </tr>

      <tr valign="top">
        <td><a href="http://download.kde.org/stable/3.1/src/kdegraphics-3.1.tar.bz2">kdegraphics-3.1</a></td>

        <td align="right">4.4MB</td>

        <td><tt>1daa1a669131015db73a98f6ca26e232</tt></td>
      </tr>

      <tr valign="top">
        <td><a href="http://download.kde.org/stable/3.1/src/kdelibs-3.1.tar.bz2">kdelibs-3.1</a></td>

        <td align="right">10MB</td>

        <td><tt>4b5cb94d5889134ba4e86749e6b5476c</tt></td>
      </tr>

      <tr valign="top">
        <td><a href="http://download.kde.org/stable/3.1/src/kdemultimedia-3.1.tar.bz2">kdemultimedia-3.1</a></td>

        <td align="right">5.8MB</td>

        <td><tt>c418c435b14ab5bcb51247c2d182b80d</tt></td>
      </tr>

      <tr valign="top">
        <td><a href="http://download.kde.org/stable/3.1/src/kdenetwork-3.1.tar.bz2">kdenetwork-3.1</a></td>

        <td align="right">4.8MB</td>

        <td><tt>f286a6708c80c439401817bf31f63fba</tt></td>
      </tr>

      <tr valign="top">
        <td><a href="http://download.kde.org/stable/3.1/src/kdepim-3.1.tar.bz2">kdepim-3.1</a></td>

        <td align="right">3.1MB</td>

        <td><tt>9a416238193107ed2d937a3f5b02f579</tt></td>
      </tr>

      <tr valign="top">
        <td><a href="http://download.kde.org/stable/3.1/src/kdesdk-3.1.tar.bz2">kdesdk-3.1</a></td>

        <td align="right">2.1MB</td>

        <td><tt>8c4e22ce1dd2be8464d31800cb0b347b</tt></td>
      </tr>

      <tr valign="top">
        <td><a href="http://download.kde.org/stable/3.1/src/kdetoys-3.1.tar.bz2">kdetoys-3.1</a></td>

        <td align="right">1.8MB</td>

        <td><tt>faca3d6fdc5d0ac7671a31b03c9c506a</tt></td>
      </tr>

      <tr valign="top">
        <td><a href="http://download.kde.org/stable/3.1/src/kdeutils-3.1.tar.bz2">kdeutils-3.1</a></td>

        <td align="right">1.4MB</td>

        <td><tt>81e2916ba0fa865053aff858982c040e</tt></td>
      </tr>

      <tr valign="top">
        <td><a href="http://download.kde.org/stable/3.1/src/quanta-3.1.tar.bz2">quanta-3.1</a></td>

        <td align="right">2.6MB</td>

        <td><tt>ce8027c03c82859d28d894cdb10a84f6</tt></td>
      </tr>
    </table>

