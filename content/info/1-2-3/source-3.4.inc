<table border="0" cellpadding="4" cellspacing="0">
<tr valign="top">
  <th align="left">Location</th>
  <th align="left">Size</th>
  <th align="left">MD5&nbsp;Sum</th>
</tr>
<tr valign="top">
   <td><a href="http://download.kde.org/stable/3.4/src/arts-1.4.0.tar.bz2">arts-1.4.0</a></td>
   <td align="right">979kB</td>
   <td><tt>a155bb00f56c71bc475890249e2dcaa9</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/3.4/src/kdeaccessibility-3.4.0.tar.bz2">kdeaccessibility-3.4.0</a></td>
   <td align="right">6.8MB</td>
   <td><tt>274bd9335219f0fefb6fdc4a17891cf7</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/3.4/src/kdeaddons-3.4.0.tar.bz2">kdeaddons-3.4.0</a></td>
   <td align="right">1.5MB</td>
   <td><tt>5a0d82ee1bbaeec8dab74b2e5e604f94</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/3.4/src/kdeadmin-3.4.0.tar.bz2">kdeadmin-3.4.0</a></td>
   <td align="right">1.4MB</td>
   <td><tt>d1db9fac4faca194cbda1e8189453363</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/3.4/src/kdeartwork-3.4.0.tar.bz2">kdeartwork-3.4.0</a></td>
   <td align="right">17MB</td>
   <td><tt>89df94d9e6c63887e5a66312b6514d0d</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/3.4/src/kdebase-3.4.0.tar.bz2">kdebase-3.4.0</a></td>
   <td align="right">21MB</td>
   <td><tt>c88659e558ca98dc45377bf8ddfc26c9</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/3.4/src/kdebindings-3.4.0.tar.bz2">kdebindings-3.4.0</a></td>
   <td align="right">6.8MB</td>
   <td><tt>bac87a665ce5e5704f48336122052fb4</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/3.4/src/kdeedu-3.4.0.tar.bz2">kdeedu-3.4.0</a></td>
   <td align="right">22MB</td>
   <td><tt>c2aab0c4f6439abb6c0eb09413b6532c</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/3.4/src/kdegames-3.4.0.tar.bz2">kdegames-3.4.0</a></td>
   <td align="right">9.0MB</td>
   <td><tt>ab144b71caeda34579817b01855ec287</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/3.4/src/kdegraphics-3.4.0.tar.bz2">kdegraphics-3.4.0</a></td>
   <td align="right">6.3MB</td>
   <td><tt>5a0a32e314422e2ce051290c08390367</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/3.4/src/kde-i18n-3.4.0.tar.bz2">kde-i18n-3.4.0</a></td>
   <td align="right">249MB</td>
   <td><tt>14359b7a14d507b3f9e8302b46031aa2</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/3.4/src/kdelibs-3.4.0.tar.bz2">kdelibs-3.4.0</a></td>
   <td align="right">16MB</td>
   <td><tt>e5961a78b44a3005a7af6ada249e5888</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/3.4/src/kdemultimedia-3.4.0.tar.bz2">kdemultimedia-3.4.0</a></td>
   <td align="right">5.3MB</td>
   <td><tt>4e42790bbea7c4ac0c436da3c7c664ac</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/3.4/src/kdenetwork-3.4.0.tar.bz2">kdenetwork-3.4.0</a></td>
   <td align="right">6.9MB</td>
   <td><tt>47a8d21ce486426caf56bf6129ce993f</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/3.4/src/kdepim-3.4.0.tar.bz2">kdepim-3.4.0</a></td>
   <td align="right">10MB</td>
   <td><tt>7f8cc9a40c0190c5a6723f6325bcba06</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/3.4/src/kdesdk-3.4.0.tar.bz2">kdesdk-3.4.0</a></td>
   <td align="right">4.3MB</td>
   <td><tt>5b88692972e65c5e7d3aafc6400bea2c</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/3.4/src/kdetoys-3.4.0.tar.bz2">kdetoys-3.4.0</a></td>
   <td align="right">3.0MB</td>
   <td><tt>aad06c1e9cc8909bba4db4f3a746f666</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/3.4/src/kdeutils-3.4.0.tar.bz2">kdeutils-3.4.0</a></td>
   <td align="right">2.2MB</td>
   <td><tt>cb7e5402eedaca816e210d460e22e53a</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/3.4/src/kdevelop-3.2.0.tar.bz2">kdevelop-3.2.0</a></td>
   <td align="right">7.9MB</td>
   <td><tt>37352d6f5496849d5704e1503ab0273a</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/3.4/src/kdewebdev-3.4.0.tar.bz2">kdewebdev-3.4.0</a></td>
   <td align="right">5.9MB</td>
   <td><tt>a131b9a14c5da402417b43ed8bc61df1</tt></td>
</tr>

</table>
