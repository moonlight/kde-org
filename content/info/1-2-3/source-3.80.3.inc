<table border="0" cellpadding="4" cellspacing="0">
<tr valign="top"><th align="left">Location</th><th align="left">Size</th><th align="left">MD5&nbsp;Sum</th></tr>
<tr valign="top"><td><a href="http://download.kde.org/unstable/3.80.3/src/kdeaccessibility-3.80.3.tar.bz2">kdeaccessibility-3.80.3</a></td><td align="right">7.4MB</td><td><tt>be866088ec44b66c93f565e252a7f28b</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/unstable/3.80.3/src/kdeaddons-3.80.3.tar.bz2">kdeaddons-3.80.3</a></td><td align="right">889KB</td><td><tt>7ae5fdac5085498e650388add77fad2a</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/unstable/3.80.3/src/kdeadmin-3.80.3.tar.bz2">kdeadmin-3.80.3</a></td><td align="right">1.4MB</td><td><tt>2bf10bdb49e7b52260a8812f477a31be</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/unstable/3.80.3/src/kdeartwork-3.80.3.tar.bz2">kdeartwork-3.80.3</a></td><td align="right">15MB</td><td><tt>393a4e7125951e990b85d9b55053700a</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/unstable/3.80.3/src/kdebase-3.80.3.tar.bz2">kdebase-3.80.3</a></td><td align="right">19MB</td><td><tt>53c9acf88f42f4b30a006511d93b67da</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/unstable/3.80.3/src/kdeedu-3.80.3.tar.bz2">kdeedu-3.80.3</a></td><td align="right">26MB</td><td><tt>7fbd349f574186eb6f959c15f9560d72</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/unstable/3.80.3/src/kdegames-3.80.3.tar.bz2">kdegames-3.80.3</a></td><td align="right">19MB</td><td><tt>5dfdda60cedd4d0fe33247e71c4c207f</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/unstable/3.80.3/src/kdegraphics-3.80.3.tar.bz2">kdegraphics-3.80.3</a></td><td align="right">4.9MB</td><td><tt>3a9a8236e9cf06dfad3e942537c65a52</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/unstable/3.80.3/src/kdelibs-3.80.3.tar.bz2">kdelibs-3.80.3</a></td><td align="right">13MB</td><td><tt>9b091e0d68e7eb8eaef5254e0f127d2d</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/unstable/3.80.3/src/kdemultimedia-3.80.3.tar.bz2">kdemultimedia-3.80.3</a></td><td align="right">3.9MB</td><td><tt>783a043278e2477c44d10f61c21f081c</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/unstable/3.80.3/src/kdenetwork-3.80.3.tar.bz2">kdenetwork-3.80.3</a></td><td align="right">6.7MB</td><td><tt>e5a69359a66a41a9af6127379546e7ba</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/unstable/3.80.3/src/kdepim-3.80.3.tar.bz2">kdepim-3.80.3</a></td><td align="right">9.0MB</td><td><tt>07d4cfdbe51e52b16137609419648670</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/unstable/3.80.3/src/kdepimlibs-3.80.3.tar.bz2">kdepimlibs-3.80.3</a></td><td align="right">1.1MB</td><td><tt>55bbdc96add40bef9e806400d4dd8c3c</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/unstable/3.80.3/src/kdesdk-3.80.3.tar.bz2">kdesdk-3.80.3</a></td><td align="right">4.2MB</td><td><tt>9a393a78a877d92220a98f1a6a9be691</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/unstable/3.80.3/src/kdetoys-3.80.3.tar.bz2">kdetoys-3.80.3</a></td><td align="right">2.2MB</td><td><tt>fcd0a13196a825c128cfe9ce0d00acf1</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/unstable/3.80.3/src/kdeutils-3.80.3.tar.bz2">kdeutils-3.80.3</a></td><td align="right">2.1MB</td><td><tt>72158de6e050abe3c1fb781311563aae</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/unstable/3.80.3/src/kdevelop-3.80.3.tar.bz2">kdevelop-3.80.3</a></td><td align="right">3.2MB</td><td><tt>836ef02dfe54d91ed4315024f2185995</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/unstable/3.80.3/src/kdewebdev-3.80.3.tar.bz2">kdewebdev-3.80.3</a></td><td align="right">4.4MB</td><td><tt>a5d10f3945707f1353a2e9ce70f61625</tt></td></tr>
</table>
