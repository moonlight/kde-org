<ul>

<!-- ASP LINUX -->
<!--
<li><a href="http://www.asp-linux.com/">ASP Linux</a>
  (<a href="http://download.kde.org/binarydownload.html?url=/stable/3.1.4/ASPLinux/README">README</a>)
  : 
  <ul type="disc">
    <li>
        <a href="http://download.kde.org/binarydownload.html?url=/stable/3.1.4/ASPLinux/9/noarch/">Language
        packages</a> (all versions and architectures)</li>
    </li>
    <li>
      9:
      <a href="http://download.kde.org/binarydownload.html?url=/stable/3.1.4/ASPLinux/9/i386/">Intel i386</a>
    </li>
  </ul>
  <p />
</li>
-->

<!-- CONECTIVA LINUX -->
<!--
<li><a href="http://www.conectiva.com/">Conectiva Linux</a>
  (<a href="http://download.kde.org/binarydownload.html?url=/stable/3.1.4/Conectiva/README">README</a>
  /
  <a href="http://download.kde.org/binarydownload.html?url=/stable/3.1.4/Conectiva/LEIAME">LEIAME</a>
  ) :
  <ul type="disc">
    <li>
      9.0:
      <a href="http://download.kde.org/binarydownload.html?url=/stable/3.1.4/Conectiva/cl9/conectiva/RPMS.kdeorg/">Intel
      i386</a> and
      <a href="http://download.kde.org/binarydownload.html?url=/stable/3.1.4/Conectiva/cl9/SRPMS.kdeorg/">SRPMs</a>
    </li>
    <li>
      8.0:
      <a href="http://download.kde.org/binarydownload.html?url=/stable/3.1.4/Conectiva/cl8/conectiva/RPMS.kdeorg/">Intel
      i386</a> and
      <a href="http://download.kde.org/binarydownload.html?url=/stable/3.1.4/Conectiva/cl8/SRPMS.kdeorg/">SRPMs</a>
    </li>
     <li>
      7.0:
      <a href="http://download.kde.org/binarydownload.html?url=/stable/3.1.4/Conectiva/cl7/conectiva/RPMS.kdeorg/">Intel
      i386</a> and
      <a href="http://download.kde.org/binarydownload.html?url=/stable/3.1.4/Conectiva/cl7/SRPMS.kdeorg/">SRPMs</a>
    </li>
  </ul>
  <p />
</li>
-->

<!--  DEBIAN -->
<li><a href="http://www.debian.org/">Debian</a>
  (<a href="http://download.kde.org/stable/3.1.4/Debian/README">README</a>) :
    <ul type="disc">
      <li>
         Debian stable (woody) (Intel i386, IBM PowerPC, DEC Alpha) : <tt>deb http://download.kde.org/stable/3.1.4/Debian stable main</tt>
      </li>
    </ul>
  <p />
</li>

<!--   FREEBSD -->
<!--
<li><a href="http://www.freebsd.org/">FreeBSD</a>
  (<a href="http://download.kde.org/stable/3.1.4/FreeBSD/README">README</a>)
  <p />
</li>
-->

<!--   MAC OS X (FINK PROJECT) -->
<!--
<li>
  <a href="http://www.apple.com/macosx/">Mac OS</a> /
  <a href="http://www.opendarwin.org/">OpenDarwin</a>
  (downloads via the <a href="http://fink.sourceforge.net/">Fink</a> project):
  <ul type="disc">
    <li>
      <a href="http://fink.sourceforge.net/news/kde.php">X</a>
    </li>
  </ul>
  <p />
</li>
-->

<!--   MANDRAKE LINUX -->
<!--
<li>
  <a href="http://www.linux-mandrake.com/">Mandrake Linux</a>
  (<a href="http://download.kde.org/binarydownload.html?url=/stable/3.1.4/Mandrake/README">README</a>): 
  <ul type="disc">
    <li>
      8.2:
      <a href="http://download.kde.org/binarydownload.html?url=/stable/3.1.4/Mandrake/8.2/">Intel
      i586</a>
    </li>
  </ul>
  <p />
</li>
-->

<!--   REDHAT LINUX -->
<li>
  <a href="http://www.redhat.com/">Red Hat</a>
<!--
 (<a href="http://download.kde.org/stable/3.1.4/RedHat/8.0/README">README</a>)
-->
 :
  <ul type="disc">
    <li>
        <a href="http://download.kde.org/binarydownload.html?url=/stable/3.1.4/RedHat/9/noarch/">Language
        packages</a> (all versions and architectures)
    </li>
    <li>
      9: <a href="http://download.kde.org/binarydownload.html?url=/stable/3.1.4/RedHat/9/i386/">Intel i386</a>
    </li>
  </ul>
  <p />
</li>

<!-- SLACKWARE LINUX -->
<li>
  <a href="http://www.slackware.org/">Slackware</a> (Unofficial contribution):
   <ul type="disc">
     <li>
        <a href="http://download.kde.org/binarydownload.html?url=/stable/3.1.4/contrib/Slackware/noarch/">Language
        packages</a> (all versions and architectures)
     </li>
     <li>
       9.0: <a href="http://download.kde.org/binarydownload.html?url=/stable/3.1.4/contrib/Slackware/9.0/">Intel i386</a>
     </li>
   </ul>
  <p />
</li>

<!-- AIX -->
<li>
  <a href="http://www-1.ibm.com/servers/aix/">IBM AIX</a> (Not IBM endorsed):
   <ul type="disc">
     <li>
       5.1+: <a href="http://download.kde.org/binarydownload.html?url=/stable/3.1.4/AIX/5.1/">PowerPC</a>
     </li>
   </ul>
  <p />
</li>

<!-- SOLARIS -->
<!--
<li>
  <a href="http://www.sun.com/solaris/">SUN Solaris</a> (Unofficial contribution):
  <ul type="disc">
    <li>
      8.0: <a href="http://download.kde.org/binarydownload.html?url=/stable/3.1.4/contrib/Solaris/8.0/x86/">Intel x86</a>
    </li>
  </ul>
  <p />
</li>
-->

<!--   SUSE LINUX -->
<!--
<li>
  <a href="http://www.suse.com/">SuSE Linux</a>
  (<a href="http://download.kde.org/binarydownload.html?url=/stable/3.1.4/SuSE/README">README</a>,
   <a href="http://download.kde.org/binarydownload.html?url=/stable/3.1.4/SuSE/SORRY">SORRY</a>):
  <ul type="disc">
    <li>
        <a href="http://download.kde.org/binarydownload.html?url=/stable/3.1.4/SuSE/noarch/">Language
        packages</a> (all versions and architectures)</li>
    </li>
    <li>
      8.2:
      <a href="http://download.kde.org/binarydownload.html?url=/stable/3.1.4/SuSE/i386/8.2/">Intel
      i586</a>
    </li>
    <li>
      8.1:
      <a href="http://download.kde.org/binarydownload.html?url=/stable/3.1.4/SuSE/i386/8.1/">Intel
      i586</a>
    </li>
    <li>
      8.0:
      <a href="http://download.kde.org/binarydownload.html?url=/stable/3.1.4/SuSE/i386/8.0/">Intel
      i386</a>
    </li>
    <li>
      7.3:
      <a href="http://download.kde.org/binarydownload.html?url=/stable/3.1.4/SuSE/i386/7.3/">Intel
      i386</a> and
      <a href="http://download.kde.org/binarydownload.html?url=/stable/3.1.4/SuSE/ppc/7.3/">IBM
      PowerPC</a>
    </li>
    <li>
     7.2:
     <a href="http://download.kde.org/binarydownload.html?url=/stable/3.1.4/SuSE/i386/7.2/">Intel
      i386</a>
    </li>
  </ul>
  <p />
</li>
-->

<!-- TURBO LINUX -->
<!--
<li>
    <a href="http://www.turbolinux.com/">Turbolinux</a>
    (<a href="http://download.kde.org/binarydownload.html?url=/stable/3.1.4/Turbo/Readme.txt">README</a>):
    <ul type="disc">
      <li>
        <a href="http://download.kde.org/binarydownload.html?url=/stable/3.1.4/Turbo/noarch/">Language
        packages</a> (all versions and architectures)</li>
      <li>
       8.0: 
       <a href="http://download.kde.org/binarydownload.html?url=/stable/3.1.4/Turbo/8/i586/">Intel i586</a>
      </li>
      <li>
       7.0:
       <a href="http://download.kde.org/binarydownload.html?url=/stable/3.1.4/Turbo/7/i586/">Intel i586</a>
      </li>
    </ul>
  <p />
</li>
-->

<!--   TRU64 -->
<!--
<li>
  <a href="http://www.tru64unix.compaq.com/">Tru64</a>
  (<a href="http://download.kde.org/binarydownload.html?url=/stable/3.1.4/contrib/Tru64/README.Tru64">README</a>):
  <ul type="disc">
    <li>
      4.0d, e, f and g and 5.x:
      <a href="http://download.kde.org/binarydownload.html?url=/stable/3.1.4/contrib/Tru64/">Compaq Alpha</a>
    </li>
  </ul>
  <p />
</li>
-->

<!--   YELLOWDOG LINUX -->
<!--
  <a href="http://www.yellowdoglinux.com/">YellowDog</a>:
  <a href="http://download.kde.org/binarydownload.html?url=/stable/3.1.4/YellowDog">2.2</a>
-->

<!-- YOPER LINUX -->
<!--
<li>
  <a href="http://www.yoper.com/">Yoper</a>:
  <ul type="disc">
    <li>
      1.0:
      <a href="http://download.kde.org/binarydownload.html?url=/stable/3.1.4/Yoper/">Intel i686 tgz</a>
    </li>
  </ul>
  <p />
</li>
-->

<!-- VECTOR LINUX -->
<!--
<li>
  <a href="http://www.vectorlinux.com/">VECTORLINUX</a>:
  (<a href="http://download.kde.org/stable/3.1.4/contrib/VectorLinux/3.0/README">README</a>):
  <ul type="disc">
    <li>
      3.2:
      <a href="http://download.kde.org/binarydownload.html?url=/stable/3.1.4/contrib/VectorLinux/3.2/">Intel i386</a>
    </li>
    <li>
      3.0:
      <a href="http://download.kde.org/binarydownload.html?url=/stable/3.1.4/contrib/VectorLinux/3.0/">Intel i386</a>
    </li>
  </ul>
  <p />
</li>
-->

</ul>
