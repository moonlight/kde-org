---
title: "KDE's Goals"
summary: KDE's Goals for the Next Couple of Years.
---

<article>
    <p>KDE is a huge and diverse community working on hundreds of projects. To guide our work, we have selected 3 overarching goals to focus on over the next couple of years.</p>
    <div class="text-center mt-3">
        <a class="button ms-1 me-1 mb-3" href="https://dot.kde.org/2022/11/16/kde%E2%80%99s-new-goals-join-kick-meeting">Read the Announcement</a>
        <a class="button ms-1 me-1" href="https://community.kde.org/Goals">Goals wiki page</a>
    </div>
</article>

<article class="mt-4">
    <div>
        <h2>KDE For All</h2>
        <p>This is not the first time a proposal about accessibility was submitted, but this year the community decided it's time to act. Proposal author and new Goal Champion Carl Schwan is well known in the community for his work on KDE websites, NeoChat and more. He notes that making KDE software accessible needs cooperation from many parts of the community: from design to libraries and even the underlying Qt toolkit. Additionally, much more testing will be needed to ensure that whatever accessibility need a KDE user has, they will be able to use the community's software.</p>
    </div>
    <div class="text-center mt-3">
        <a class="button mb-3 ms-1 me-1" href="https://invent.kde.org/groups/teams/accessibility/-/boards">Workboard</a>
        <a class="button mb-3 ms-1 me-1" href="https://community.kde.org/Goals/KDE_For_All">Goal wiki page</a>
        <a class="button ms-1 me-1" href="https://go.kde.org/matrix/#/#kde-accessibility:kde.org">Chat Room</a>
    </div>
</article>

<article class="mt-4">
    <div>
        <h2>Sustainable Software</h2>
        <p>You can call KDE software many things: free, beautiful, performant, customizable... the list goes on. But how about: all that while still being sustainable? This is the question that Cornelius Schumacher, our new Champion, will answer while working with the community towards this Goal. This topic of course is not new to him; he helped set up funding for two contractors hired by the KDE e.V. to work on sustainability topics. Cornelius plans to continue certifying KDE apps, setting up testing for measuring impact of our software, and improving our efficiency where needed.</p>
    </div>
    <div class="text-center mt-3">
        <a class="button mb-3 ms-1 me-1" href="https://invent.kde.org/teams/eco/sustainable-software-goal/-/boards">Workboard</a>
        <a class="button mb-3 ms-1 me-1" href="https://community.kde.org/Goals/Sustainable_Software">Goal wiki page</a>
        <a class="button ms-1 me-1" href="https://go.kde.org/matrix/#/#energy-efficiency:kde.org">Chat Room</a>
    </div>
</article>

<article class="mt-4">
    <div>
        <h2>Automate And Systematize Internal Processes</h2>
        <p>Each year KDE has more apps, more users, and more hardware partners. This is of course fantastic, but at some point relying solely on volunteer efforts for critical parts of delivering quality software to everyone ceases to be scalable. Nate Graham - our first two-time Goal Champion - will not let that happen, and will work to automate processes, change our culture around quality assurance, and involve more people in places where a responsibility lies on a single person.</p>
        <div class="text-center mt-3">
            <a class="button mb-3 me-1 ms-1" href="https://invent.kde.org/groups/teams/automation/-/boards">Workboard</a>
            <a class="button mb-3 me-1 ms-1" href="https://community.kde.org/Goals/Automate_and_systematize_internal_processes">Goal wiki page</a>
            <a class="button me-1 ms-1" href="https://go.kde.org/matrix/#/#kde-institutional-memory:kde.org">Chat Room</a>
        </div>
    </div>
</article>
