---
title: KDE Project Awards
---

## Awards in 2009

### KDE voted Free Software Project of the Year

![Linux Format Readers Choice Award
2009](images/LXF-Logo.png)

[Linux Format](http://www.linuxformat.co.uk/) magazine has unveiled its
annual Reader Awards
([PDF](http://www.linuxformat.co.uk/pdfs/download.php?PDF=LXF115.awards.pdf))
for 2008 and KDE won a 'landslide' victory in the category of Free
Software Project of the year in recognition of the 'incredible' work
done with KDE 4. [Amarok](http://amarok.kde.org),
[Konqueror](http://www.konqueror.org), Qt and the KDE-based Asus Eee PC
were also recognised in the awards.
Read more in the
[dot-article](http://dot.kde.org/2009/01/20/kde-voted-free-software-project-year).

## Awards in 2008

### KDE-Applications win Linux Journal's Reader's Choice Award 2008

![Linux Journal Readers Choice Award
2008](images/ReadersChoice2008_logo-75x125.png)

The readers of [Linux Journal](http://www.linuxjournal.com/) chose the
KDE-Applications [Amarok](http://amarok.kde.org/) as their [Favorite
Audio Tool](http://www.linuxjournal.com/article/10065) and
[digiKam](http://www.digikam.org/) as their [Favorite Digital Photo
Management Tool](http://www.linuxjournal.com/article/10065).

### 2008 LinuxQuestions.org Members' Choice Awards

![LinuxQuestions Award
2008](images/2008_desktop-environment-KDE.png)

[KDE](http://www.kde.org/) won again the
[LinuxQuestions.org](http://www.linuxquestions.org/) Members' Choice
Award [Desktop Environment of the
Year](http://www.linuxquestions.org/questions/2008-linuxquestions.org-members-choice-awards-79/desktop-environment-of-the-year-610190/).

Not only the Desktop Enviroment received excellent remarks. Also some
KDE-Applications were voted number one in their categories. Amarok is by
far the most popular application in the category [Audio Media Player
Application of the
Year](http://www.linuxquestions.org/questions/2007-linuxquestions.org-members-choice-awards-79/audio-media-player-application-of-the-year-610226/)
and K3b is the favoured [Multimedia Utility of the
Year](http://www.linuxquestions.org/questions/2007-linuxquestions.org-members-choice-awards-79/multimedia-utility-of-the-year-610224/).

## Awards in 2007

### 2007 LinuxQuestions.org Members' Choice Awards

![LinuxQuestions Award 2007](images/2007_desktop_btn.png)

[KDE](http://www.kde.org/) is again [Desktop Environment of the
Year](http://www.linuxquestions.org/questions/2007-linuxquestions.org-members-choice-awards-79/desktop-environment-of-the-year-610190/).
The following KDE-Applications were voted number one in their categories
by the members of [LinuxQuestions.org](http://www.linuxquestions.org/).
Amarok ([Audio Media Player Application of the
Year](http://www.linuxquestions.org/questions/2007-linuxquestions.org-members-choice-awards-79/audio-media-player-application-of-the-year-610226/)),
K3b ([Multimedia Utility of the
Year](http://www.linuxquestions.org/questions/2007-linuxquestions.org-members-choice-awards-79/multimedia-utility-of-the-year-610224/))
and Konqueror ([File Manager of the
Year](http://www.linuxquestions.org/questions/2007-linuxquestions.org-members-choice-awards-79/file-manager-of-the-year-610231/)).

## Awards in 2006

### Linux Journal's Editor's Choice Award 2006

![Linux Journal Editors Choice
2006](images/EditorsChoice2006-LinuxJournal-120x120.png) KDE
wins [Linux Journal](http://www.linuxjournal.com/)'s [Editors' Choice
2006](http://www.linuxjournal.com/article/9368) award (Desktop
Environment). The K Desktop Environment is lauded for the richness of
the features, the user friendly and intuitive usability and the depths
of it's power. KDE is the desktop with everything for casual users as
well as for power users.

### Free Software Project of the Year award 2006

![Linux Format Readers Choice Award
2006](images/LXF-Logo.png)

KDE won the "Free Software Project of the Year award" in [Linux
Format's](http://www.linuxformat.co.uk/) Reader Awards.

### 2006 LinuxQuestions.org Members' Choice Awards

![LinuxQuestions Award 2006](images/2006_desktop_btn.png)
The members of [LinuxQuestions.org](http://www.linuxquestions.org/)
voted KDE for [Desktop Environment of the
Year](http://www.linuxquestions.org/questions/2006-linuxquestions.org-members-choice-awards-76/desktop-environment-of-the-year-514945/).
Several other KDE-Applications received very good results. Winners in
their categories are: amaroK ([Audio Media Player Application of the
Year](http://www.linuxquestions.org/questions/2006-linuxquestions.org-members-choice-awards-76/audio-media-player-application-of-the-year-514978/)),
K3b ([Multimedia Utility of the
Year](http://www.linuxquestions.org/questions/2006-linuxquestions.org-members-choice-awards-76/multimedia-utility-of-the-year-514982/))
and Quanta ([Web Development Editor of the
Year](http://www.linuxquestions.org/questions/2006-linuxquestions.org-members-choice-awards-76/web-development-editor-of-the-year-514964/)).

## Awards in 2005

### 2005 LinuxQuestions.org Members' Choice Awards

![2005 LinuxQuestions.org Member's Choice Awards : KDE - Desktop
Environment, amaroK - Audio Multimedia Application, Konqueror - File
Manager and Quanta - Web Development
Editor](images/LQ-2005MCA_Desktop.png)

The members of [LinuxQuestions.org](http://www.linuxquestions.org/)
chose [KDE](http://www.kde.org) as the [Desktop
Environment](http://www.linuxquestions.org/questions/t409028.html),
[amaroK](http://amarok.kde.org) as the [Audio Multimedia
Application](http://www.linuxquestions.org/questions/t409046.html),
[Konqueror](http://www.konqueror.org) as [File
Manager](http://www.linuxquestions.org/questions/t409055.html) and
[Quanta](http://quanta.kdewebdev.org) the [Web Development
Editor](http://www.linuxquestions.org/questions/t409044.html) for the
year of 2005.

### TUX 2005 Readers' Choice Award

![TUX 2005 Readers' Choice
Award](images/tux-readerschoice-winner.png)

The readers of [TUX Magazin](http://www.tuxmagazine.com/) chose
[KDE](http://www.kde.org) as Favorite Desktop Environment and
[Digikam](http://digikam.org) as Favorite Digital Photo Management Tool
in the [TUX 2005 Readers' Choice
Award](http://www.tuxmagazine.com/node/1000151).

### 2005 Usenix Software Tools User Group Award

![Usenix Software Tools User GroupAward](images/usenix.png)

The [Software Tools User Group
Award](http://www.usenix.org/about/stug.html) recognises significant
contributions to the community. The 2005 award was made jointly to
Mattias Ettrich of KDE and Miguel de Icaza of Gnome for their
contribution to desktop Unix.

### Intevation Prize for Achievements in Free Software For Kalzium

Carsten Niehaus won the [Intevation Prize for Achievements in Free
Software](http://dot.kde.org/1139779450/) for his work on
[Kalzium](http://edu.kde.org/kalzium/), KDE's interactive periodic
table.

## Awards in 2004

### 2004 LinuxQuestions.org Members' Choice Awards

![2004 LinuxQuestions.org Member's Choice Awards : KDE - Desktop
Environment, KDevelop - IDE and Konqueror - File Manager
](images/lq_2004mca.png)

The members of [LinuxQuestions.org](http://www.linuxquestions.org/)
chose [KDE](http://www.kde.org/) as the [Desktop
Environment](http://www.linuxquestions.org/questions/t272100.html),
[KDevelop](http://www.kdevelop.org/) as the
[IDE](http://www.linuxquestions.org/questions/t272124.html),
[Konqueror](http://www.konqueror.org/) as [File
Manager](http://www.linuxquestions.org/questions/t272138.html) and
[Quanta](http://quanta.kdewebdev.org) the [Web Development
Editor](http://www.linuxquestions.org/questions/t272123.html) for the
year of 2004.

### Best Mail Client for Kontact/KMail

![Kontact/KMail Best Mail Client in Linux New Media Award
2004](images/lnm_award2004_logo.png)

[Linux New Media](http://www.linuxnewmedia.de/)'s [Editor's Choice
Award](http://www.linuxnewmedia.de/Award_2004/en) for 2004 was granted
to Kontact/KMail in the category "Best Mail Client".

### Favorite Desktop Environment for KDE

![KDE best desktop environment in Linux Journal's 2004 Readers' Choice
Award](images/rc2004.png)

[Linux Journal](http://www.linuxjournal.com/)'s [Readers' Choice
Award](http://www.linuxjournal.com/article.php?sid=7724) for 2004 was
granted to KDE in the category "Favorite Desktop Environment".

### Best Desktop Environment for KDE 3.3

![KDE best in Open Choice Awards
2004](images/openchoice2k4.png)

[Open for Business](http://www.ofb.biz)' [Open Choice Awards
2004](http://www.ofb.biz/modules.php?name=News&file=article&sid=330)
chose KDE to be awarded as "Best Desktop Environment".

### LinuxUser & Developer Awards 2004

![LinuxUser & Developer Expo 2004 Best Desktop
Environment](images/ludex.png)

[LinuxUser & Developer Expo 2004](http://www.linuxuserexpo.com/) is one
of the largest Linux-related shows in the UK. After being nominated for
the award along with Ximian Desktop and Sun Java Desktop, KDE developer
Richard Moore was proud to accept the award for "Best Desktop
Environment" for KDE 3.2 at the awards ceremony.

### 2004 LinuxWorld Conference & Expo Product Excellence Award

![LinuxWorld Conference & Expo - Product Excellence
Awards](images/lwce_award_logo_2004.png)

[IDG World Expo](http://www.idgworldexpo.com), the leading producer of
world-class tradeshows, conferences and events for technology markets
around the globe, [awarded KDevelop 3.0 with the LinuxWorld Product
Excellence Award for Best Development
Tools](http://www.businesswire.com/cgi-bin/f_headline.cgi?bw.012204/240225110).
The LinuxWorld Product Excellence Awards represent major areas of
innovation in the Linux and open source community.

## Awards in 2003

### LinuxWorld Magazine 2003 Readers' Choice Awards

![LinuxWorld Magazin - Readers' Choice
Award](images/LWM_RCA_logo.png)

The [LinuxWorld Magazine](http://www.linuxworld.com) [2003 Readers'
Choice Awards](http://www.linuxworld.com/story/39231.htm), often
referred to as the "Oscars of the Software Industry," recognizes
excellence in the solutions provided by the top Linux vendors in the
market. We are therefore very proud that KDE 3.1 has won first price in
the "Best Linux Desktop Manager" category.

### 2003 LinuxQuestions.org Members' Choice Awards

![LinuxQuestions.org 2003 Members' Choice Award - Winnder - Desktop
Environment](images/LQ-2003MCA-Desktop.png)

The members of [LinuxQuestions.org](http://www.linuxquestions.org/)
chose KDE as the best Desktop Environment and Quanta as the Web
Development Editor of the Year 2003.

### Best Integrated Development Environment for KDevelop

![KDevelop best IDE in Linux New Media's 2003 Editors' Choice
Award](images/LnmAwardLogo.png)

[Linux New Media](http://www.linuxnewmedia.de/en)'s [Editors' Choice
Award](http://www.linux-magazin.de/Artikel/ausgabe/2003/12/award/award.html)
for 2003 was granted to KDevelop in the category "IDE Development
System".

### Favorite Desktop Environment for KDE

![KDE best desktop environment in Linux Journal's 2003 Readers' Choice
Award](images/rc2003.png)

[Linux Journal](http://www.linuxjournal.com/)'s [Readers' Choice
Award](http://pr.linuxjournal.com/article.php?sid=785) for 2003 was
granted to KDE in the category "Favorite Desktop Environment".

### Best Desktop Environment for KDE 3.1, Best Web Browser for Konqueror 3.1

![KDE and Konqueror best in their categories in Open Choice Awards
2003](images/openchoice2k2.png)

[Open for Business](http://www.ofb.biz)' [Open Choice Awards
2003](http://www.ofb.biz/modules.php?name=News&file=article&sid=265)
chose KDE to be awarded as "Best Desktop Environment" and Konqueror as
"Best Web Browser".

## Awards in 2002

### Best Desktop Environment for KDE and Best Email Client for KMail

![KDE and KMail best in their categories in Linux Journal's 2002
Readers' Choice Award](images/rc02_small.png)

[Linux Journal](http://www.linuxjournal.com/)'s [Readers' Choice
Award](http://www.linuxjournal.com/article.php?sid=6380) for 2002 was
granted to KDE in the category "Desktop Environment" and to KMail in
the category "EMail Client".

### Best Consumer Software for KDE 3.0

[Linux Journal](http://www.linuxjournal.com/)'s [Editors' Choice
Award](http://www.linuxjournal.com/article.php?sid=6181) for 2002 was
granted to KDE as "Best Consumer Software". Honorable mentions were
Konqueror in category "Web Client" and KDevelop in category
"Development Tool".

### Best Desktop Environment for KDE 3.0, Best Email Client for KMail and Best Development Tool for KDevelop

![KDE, KMail and KDevelop best in their categories in Open Choice Awards
2002](images/openchoice2k2.png)

[Open for Business](http://www.ofb.biz)' [Open Choice Awards
2002](http://www.ofb.biz/modules.php?name=News&file=article&sid=146)
chose KDE to be awarded as "Best Desktop Environment", KMail as "Best
Email Client" and KDevelop as "Best Development Tool". Additionally
Kopete was mentionend as runner up for "Best Communication Software".

## Awards in 2001

### Best Client-Side Software for Konqueror and Best Programming Tool for KDevelop

These awards, consisting of 3000 DM for each award, were awarded by
Linux New Media at Systeme fair in München (Munich). Read the [dot
story](http://dot.kde.org/1003720345/).

### Linux Journal - 2001 Readers' Choice Awards

![KDE: Favorite Desktop Environment in Linux Journal's 2001 Readers'
Choice Award](images/rc01_small.png)

[Linux Journal](http://www.linuxjournal.com/)'s [Readers' Choice
Award](http://www2.linuxjournal.com/lj-issues/issue91/5441.html) for
2001 was granted to KDE in the category "Desktop Environments". Parts
of the KDE project were also nominated in other categories:

-   KWord - third position in "Favorite Word Processor" category
-   KOffice - second position in "Favorite Office Suite" category
-   KDevelop - third position in "Favorite Development Tool" category
-   Konqueror - third position in "Favorite Web Browser" category
-   KMail - second position in "Favorite Mail Client" category
-   Tea - third position in "Favorite Programming Beverage" category
    :-)

You can also [read the news spot](http://dot.kde.org/1002945009/) over
at [the Dot](http://dot.kde.org/).

### Best Web Browser in Linux Magazine's 2001 Editors' Choice Awards (the Tuxies)

![Konqueror: Best Web Browser in Linux Magazine's 2001 Editors' Choice
Award](images/Tuxie_2001.png)

[Linux Magazine](http://www.linux-mag.com/)'s Editors chose Konqueror
to be awarded with the Tuxie for the Best Browser of 2001. This year's
Linux Magazine Editors' Choice Awards were published in the September
issue.

### LinuxWorld Expo Best Open Source Project

![LinuxWorld Expo Award](images/LWE-award_08_2001.jpg)

Continuing our streak at LinuxWorld Expo! Read the [news
bit](http://dot.kde.org/999120705/)

## Awards in 2000

### Linux Journal Editors' Choice

![Linux Journal Editors' Choice Award
2000](images/ljedchoice-2000.png)

The readers' selected us earlier this year and now it's the editors'
turn! Read the [news bit](http://dot.kde.org/974348698/)

### Linux Journal Readers' Choice

![Linux Journal Readers' Choice Award
2000](images/rc00_small.png)

Second year in a row! Read the [news bit](http://dot.kde.org/971786749/)

### Linux Community Award

[![Linux Community
Award](images/community-mini.jpg)](images/community.jpg)

Read the [news bit](http://dot.kde.org/970766758/)

### LinuxWorld Expo Show Favorite

![LinuxWorld Expo Award](images/LWE-award_08_2000-big.jpg)

Read the [press
release](/announcements/lwe_08_2000.bw.press-release)

## Awards in 1999

### Linux Journal Readers' Choice

![Linux Journal Readers' Choice
Award](images/rc99_small.png)

### CeBIT "Software Innovation of the Year"

![CeBIT Innovation of the Year
Award](images/innov-award.png)

Read the [press-release](/announcements/zd-innov-cebit99)
[[ Pic 1]](images/innov-award-1.jpg) | [[ Pic
2]](images/innov-award-2.jpg) | [[ Pic 3]](images/innov-award-3.jpg)
| [[ Pic 4]](images/innov-award-4.jpg)

### LinuxWorld Editor Choice Award

![LinuxWorld Editor Choice Award](images/lwedchoice-200.png)

Read the [LinuxWorld
article](http://www.linuxworld.com/linuxworld/lw-1999-08/lw-08-penguin_1.html).
