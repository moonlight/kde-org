---
title: Donate and support KDE
description: Help us create software that gives you full freedom and protects your privacy, and be a part of a great community
aliases:
- ../donate
- ../donations
layout: donations
sassFiles:
  - scss/donations.scss
learn: Where does the money go?
activity: Your generous donation helps to pay salaries for employees and contractors who participate amongst the wider KDE Community to create, document, and promote KDE software. It also helps to pay for the cost of hosting conferences and development sprints, server infrastructure, and legal fees incurred defending free software.
reports:
  - link: https://ev.kde.org/reports/ev-2020/
    name: 2020 report
    image: https://ev.kde.org/reports/ev-2020/images/slider/1.jpg
  - link: https://ev.kde.org/reports/ev-2021/
    name: 2021 report
    image: https://ev.kde.org/reports/ev-2021/images/slider/1.jpg
  - link: https://ev.kde.org/reports/ev-2022/
    name: 2022 report
    image: https://ev.kde.org/reports/ev-2022/images/slider/1.jpg
donate: Donate
other_way: Other Way to Donate
other_way_text: We also support donations via direct bank transfer in the EU, personal check in the US, donation matching via Benevity, and [GitHub Sponsors](https://github.com/sponsors/KDE/).
powered_by_paypal: "(Powered by PayPal)"
one_time: One-Time Donations
recurring: Become a Supporting Member

support: Support An Engaged Community
support_text: Our community has developed a wide variety of applications for communication, work, education and entertainment. We have a strong focus on finding innovative solutions to old and new problems, creating a vibrant, open atmosphere for experimentation.
regulary: Donate regularly
previous: PayPal statistics
kdeev: About KDE e.V.
kde: KDE e.V. is the non-profit organization behind the KDE community. It is based in Germany and thus donations are tax deductible in Germany. If you live in another EU country your donation might also be tax deductible. Please consult your tax advisor for details.
hwsv: Hardware and Services
hwsv_donation: You can also donate goods and services to KDE. We maintain a <a href="/thanks">list</a> of the organizations who made a significant contribution.
supporting:
  title: Supporting Member Programme for individuals
  text:  Show your love for KDE by donating 100€/year! Get Invited to attend the annual general assembly of KDE e.V. and get regular first hand reports about KDE's activities
supporting_corp:
  title: Supporting Member Programme for corporations
  text:  Are you part of a business or a corporation interested in sponsoring KDE development? In addition to making great software possible, supporting members also have their logos added to the KDE website and many printed promotional materials. 
menu:
  main:
    weight: 6
    name: Donate
---
