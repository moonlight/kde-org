---
title: Support Plasma 6
layout: plasma6member
jsFiles:
- /js/plasma6member.js
scssFiles:
- /scss/plasma6member.scss
more: "If you'd prefer a one-time donation, [click here](https://kde.org/community/donations/)."
---

## Spark Innovation with Your Donation

Exciting news on the horizon! In February 2024, **Plasma 6** make its grand debut. But we need your help to ensure its successful launch and continuous success.

![](/fundraisers/plasma6member/plasma6.png)

## Why Donate

In short: generosity is at the heart of KDE and keeps  our projects alive.

It's what motivates our volunteers to share their time and knowledge to keep building new features and keeping KDE software up-to-date and secure.

Generosity also drives our supporters. Their contributions and sponsorships keep KDE's gears turning and ensure that our developers can continue their fantastic work.

### How to Become a Supporter

Fill out the form above (or below if you are on a mobile device) and **for less than €10 a month** you too can support the champions who tirelessly improve our software every day. The KDE development ecosystem is a bustling hive of activity, and every supporting member helps keep it buzzing.

If you'd prefer a one-time donation, [please click here](https://kde.org/community/donations/).

### Perks

We know that keeping KDE healthy and running is all the reward you need for your generosity, but, as a token of our appreciation for your support, we are also offering:

- Your name will shine bright on our donation page, acknowledging your contribution.
- Your name will be  displayed in Plasma 6 itself! [**NOTE:** From the 21st of February onwards, new supporters' names will be added to the next patch release, as devs have now sent the software to packagers for the Plasma 6.0 release.]
<!--- More perks coming soon!-->

If you don't want any of the above, that is fine too, of course. Remember to mark the **"[✔️] Make donation anonymous"** checkbox in the donation process above.

## How We Use the Money

This is where your donation will make a difference:

* **Sprints for Developers**: You will help finance the in-person meetups that keep our developers energized and focused on making KDE even better.
* **Travel Costs to Events**: You will support our team's presence at important gatherings and conferences, like FOSDEM, FOSSAsia and LinuxCons.
* **Akademy Event**: You will ensure the success of KDE's yearly community event for all members, and foster collaboration and growth.
* **Running KDE**: You will keep the lights on at KDE HQ and our digital home running smoothly.
* **Paying Support Staff**: You will ensure KDE has on hand the experts we need to assist our contributors and users.

## <span style="color: red; weight: bold;">Mission completed!...</span>

... Or is it? Seeking support for a complex and large organization like KDE is a task that never ends.

As a non-profit, KDE cannot accumulate wealth, so all the money we raise immediately goes into providing for our community.

When a contributor does not have the means to travel to Akademy or sprint to work with their colleagues, your donation allows them to make it to those important meetings.

{{< plasma6member/progress >}}

When a developer needs time off from their day job to push forward with vital features for their app or project, your support gives them the means to buy that time.

When an app becomes popular, the more responsibility its contributors have to more users to keep it updated, safe and properly documented. Your own contribution gives them the means to help everybody.

The more donations we get from you, the easier it becomes to develop software at our own rate, favour individuals (like you) and your community, promote technological sovereignty, and built in tools to protect your personal privacy.

What we are saying is we are not done. We will be leaving this page open, allowing everybody to contribute and guarantee KDE's survival, growth and independence.

All those who contribute through the Membership program will be honoured with a mention in Plasma 6 itself. May your name live on long in Plasma 6.

Thank you

-- The KDE Community
