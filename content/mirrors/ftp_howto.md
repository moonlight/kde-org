---
title: "Setting up a KDE.org Mirror HOWTO"
aliases:
- /mirrors/
---

Setting up a mirror of the KDE server is very simple.  The basic
steps are:

<ol>
<li><a href="#determine">Determing your resources</a></li>
<li><a href="#rsync">Setting up rsync</a></li>
<li><a href="#what">What to mirror</a></li>
<li><a href="#last">Finishing up</a></li>
</ol>

<h2><a id="determine" name="determine"></a>Determing Your Resources</h2>
<p>
Before doing anything else, you should determine whether or not you
have the resources to host a download.kde.org mirror.  Try going through
this rough checklist to gauge your status:
</p>

<ol>
<li><b>Are you in a location that needs a mirror?</b>  If you are in a
country that doesn't have any (or few) mirrors, then disregard this
checklist.  We're happy to have you no matter what the conditions, in
this case.  If you are in a country like the USA or Germany which have
lots of high-bandwidth mirrors, then please continue only if you are
really dedicated to providing a mirror.</li>
<li><b>Do you have enough bandwidth?</b> In general, we like our
mirrors to have at least a 50Mbit/s "pipe."  This should also be a
fairly stable pipe.  In other words, please don't run a mirror out of
your dorm room or over home-use cable modem or DSL.  If you are in a
bandwidth starved country, please disgregard this checkpoint.</li>
<li><b>Do you have enough disk space?</b> A full mirror of download.kde.org
is roughly 520 Gigabytes.  You may elect to mirror just the stable (or
unstable) sections..</li>
<li><b>How dedicated to this mirror are you?</b> If you will only have
access to your server and bandwidth for a few months, then we suggest
you not sign up as a global mirror.  We prefer that our mirrors have
somebody willing to respond to changes to the KDE server in a timely
fashion and will be around for the foreseeable future.</li>
<li><b>Can you offer https *and* rsync access to your mirror?</b> 
Your mirror needs to be accessible via https and rsync. Rsync 
access to your mirror is needed so we can check which files you are 
serving. That way we never redirect users to your mirror when you 
are not serving that file, for example just after a release while you 
have not yet synced. If your mirror is accessible via ftp, that is 
great, but not mandatory. Http and rsync are.
</ol>

<h2><a id="what" name="what"></a>Determing what you want to mirror</h2>
<p>
You need to decide if you want your mirror to be a mirror for 
the stuff we release or if you want to be a mirror for in-app data. 
Both are around 520GB and 320GB respectively currently, but the in-app 
data has more files and changes more frequenty. This is also a fairly new 
area, and we love to have more mirrors. These are the lists of  
current mirrors:
<ul>
<li><a href="https://download.kde.org/?mirrorstats">Official KDE Mirrors</a> 
<li><a href="https://files.kde.org/?mirrorstats">KDE Application Data Mirrors</a>
</ul>

Also:
<ol>
<li>You must have at least a 50Mbit/s bandwidth pipe</li>
<li>You must be subscribed to the <a href="http://mail.kde.org/mailman/listinfo/kde-mirrors">kde-mirrors</a> mailing list and must respond to changes quickly</li>
<li>During a release week, you must update your mirror every two hours
or so if you mirror the stuff we release</li>
<li>You must update your mirror every two hours if you mirror the in-app data</li>
<li>You can mirror both if you like</li>
</ol>

<h2><a id="rsync" name="rsync"></a>Setting Up Rsync</h2>
<p>
The recommended way to update your mirror is to use
<strong>rsync</strong>.
This will allow you to update daily with only the changed files being
transferred.  Please do <em>not</em> update from <tt>ftp.kde.org</tt>.
You may use any update tool you like (like 'emirror' or 'fmirror') but
those won't be discussed here.
</p>
<p>
You may download <tt>rsync</tt> from <a href="http://rsync.samba.org/">http://rsync.samba.org</a>
</p>
<p>
There are two common rsync "modules" that you may want to use for our official releases download area.  The
first, and most popular and recommended, is <strong>[kdeftp]</strong>
(roughly 520 Gigabytes).  The other common one is
<strong>[kdestableftp]</strong> (roughly 50 Gigabytes) which contains <em>only</em> the
<tt>/stable</tt> tree.  Please use the latter one only if you
are severely cramped for space. If you require different access, send
an email to the <a href="&#x6d;&#97;il&#x74;&#x6f;:webmaster&#064;k&#0100;e&#046;org">webmaster</a> as there are
other modules.
</p>
<p>
<table>
<tr><th colspan=3>RSync mirror servers</th></tr>
<tr><th rowspan=3>Official KDE Mirror</th><th>All</th><td>rsync://rsync.kde.org/kdeftp</td></tr>
<tr>                                      <th>Stable only</th><td>rsync://rsync.kde.org/kdestableftp</td></tr>
<tr>                                      <th>Unstable only</th><td>rsync://rsync.kde.org/kdeunstableftp</td></tr>
<tr><th>KDE Application Data Mirror</th><th>&nbsp;</th><td>rsync://rsync.kde.org/applicationdata</td></tr>
<tr><th>Unified data module (official and application data)</th><th>&nbsp;</th><td>rsync://rsync.kde.org/alldata</td></tr>
</table>
</p>
<p>
Here is a quick example of an <tt>rsync</tt> invocation.  This assumes
that your mirror will be in <tt>/home/ftp/pub/kde</tt> and you want a
full KDE mirror.
</p>
<pre>
rsync -rlpt --delete rsync.kde.org::kdeftp /home/ftp/pub/kde
</pre>
<p>
You will likely want to set this up as a <tt>cron</tt> job for
automatic updating.  Here is an example of a job that updates once a
day (assumes that you put the <tt>rsync</tt> command in a script
called <tt>mirror_kde.sh</tt>).
</p>
<pre>
# run at 2am every morning<br />
0 2 * * *       $HOME/bin/mirror_kde.sh
</pre>

## Finishing Up

<p>At this point, you should have a fully working KDE.org FTP mirror.
Now, all you need to do is send an email to the
<a href="mailto:sysadmin@kde.org">KDE Sysadmin</a> to get listed on the 
mirrors page.  Send the following information:
</p>

<ul>
<li>The server address</li>
<li>Your name and email</li>
<li>The http address of the mirror</li>
<li>The ftp address of the mirror (optional)</li>
<li>The rsync address of the mirror</li>
</ul>

<p>
Also, please subscribe to the
<a href="http://mail.kde.org/mailman/listinfo/kde-mirrors">kde-mirrors</a>
mailing list.  It is a low-volume contact list for all KDE mirror admins.
</p>
