---
title: "Screenshots"
sassFiles:
- /scss/index.scss
---
<style>
main::after {
  background-color: inherit;
}
</style>

At KDE we pride ourselves on creating beautiful software.

<section>
  <div class="community-grid container">
    <figure class="text-center card community-1">
      <img src="https://cdn.kde.org/screenshots/okular/okular.png" alt="Okular" class="img-fluid">
      <figcaption class="card-body">
        <a href="https://okular.kde.org">Okular</a> - the universal document viewer
      </figcaption>
    </figure>
    <figure class="text-center card community-2">
      <img src="/content/home/main.jpg" alt="Plasma" class="img-fluid">
      <figcaption class="card-body">
        <a href="/plasma-desktop">Plasma</a> - The next generation Desktop for Linux
      </figcaption>
    </figure>
    <figure class="text-center card community-3">
      <img src="https://cdn.kde.org/screenshots/falkon/falkon.png" alt="Falkon" class="img-fluid">
      <figcaption class="card-body">
        <a href="https://www.falkon.org/">Falkon</a> - KDE's web browser
      </figcaption>
    </figure>
    <figure class="text-center card community-4">
      <img src="https://cdn.kde.org/screenshots/krita/splash.png" alt="Krita" class="img-fluid w-100">
      <figcaption class="card-body">
        <a href="https://krita.org">Krita</a> - Digital painting application
      </figcaption>
    </figure>
    <figure class="text-center card community-5">
      <img src="https://cdn.kde.org/screenshots/k3b/k3b.png" alt="K3b" class="img-fluid">
      <figcaption class="card-body">
        <a href="https://apps.kde.org/k3b">K3b</a> - KDE's CD burning application
      </figcaption>
    </figure>
    <figure class="text-center card community-8">
      <img src="https://cdn.kde.org/screenshots/ikona/colour-screenshot-dark.png" alt="Ikona" class="img-fluid">
      <figcaption class="card-body">
        <a href="https://apps.kde.org/Ikona">Ikona</a> - the icon design companion application
      </figcaption>
    </figure>
    <figure class="text-center card community-7">
      <img src="https://cdn.kde.org/screenshots/kphotoalbum/kphotoalbum.png" alt="KPhotoAlbum" class="img-fluid w-100">
      <figcaption class="card-body">
        <a href="https://kphotoalbum.org/">KPhotoAlbum</a> - Efficient image organization and indexing
      </figcaption>
    </figure>
    <figure class="text-center card community-6">
      <img src="https://cdn.kde.org/screenshots/kpatience/kpatience.png" alt="KPatience" class="img-fluid">
      <figcaption class="card-body">
        <a href="https://apps.kde.org/kpat">KPatience</a> - A relaxing card sorting game
      </figcaption>
    </figure>
  </div>
</section>


