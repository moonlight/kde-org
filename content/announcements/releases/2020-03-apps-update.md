---
layout: page
publishDate: 2020-03-05 15:50:00
summary: What Happened in KDE's Applications This Month
title: KDE's March 2020 Apps Update
type: announcement
---

## New releases

### Choqok 1.7

February started with a long awaited update to our microblogging app [Choqok](https://choqok.kde.org/news/2020/02/04/choqok-1-7-released.html).

Important fixes include the 280 character limit on Twitter, allowing disabling accounts and support extended tweets.

{{< img class="text-center" src="quick-post.png" link="https://choqok.kde.org/" caption="Choqok" >}}

### KPMcore and KDE Partition Manager 4.1.0

Our disk formatting program Partition Manager got a new release with lots of work done to the library KPMCore which is also used by the Calamares distro installer.  The app itself added support for Minix file systems.

{{< img class="text-center" src="kdepartitionmanager.png" link="https://kde.org/applications/system/org.kde.partitionmanager" caption="Partition Manager" >}}

### KPhotoAlbum 5.6

If you have hundreds or even thousands of images on your hard drive, it becomes impossible to remember the story behind every single image or the names of the persons photographed. KPhotoAlbum was created to help you describe your images and then search the big pile of pictures quickly and efficiently.

This release particularly brings huge performance improvements when tagging a large number of images and performance improvements for thumbnail view. Kudos to Robert Krawitz for again diving into the code and finding places for optimization and removal of redundancy!

Additionally, this release adds support for KDE's purpose plugin framework. 

[KPhotoAlbum](https://www.kphotoalbum.org/2020/02/08/0099/)

## Incoming

The MP3 ID tag editor Kid3 was moved into kdereview, the first step to getting new releases.

And the Rocket.chat app Ruqola passed kdereview and was moved to be ready to release.  However the word from the maintainer Laurent Montel is that a rewrite with Qt Widgets is planned so there's still a lot of work to be done.

## App Store

{{< img class="text-center" src="elisa.jpeg" link="https://www.microsoft.com/en-gb/p/elisa/9pb5md7zh8tl?activetab=pivot:overviewtab" caption="Elisa" >}}

New in the Microsoft Store for Windows is [Elisa](https://www.microsoft.com/en-gb/p/elisa/9pb5md7zh8tl?activetab=pivot:overviewtab).

A modern and beautiful music player made with love by KDE. Browse, and play your music very easily! Elisa should be able to read almost any music files.
Elisa is made to be accessible to everybody. 

Meanwhile Digikam is ready to be submitted to the Microsoft Store.

## Website Updates

[KDE Connect](https://kdeconnect.kde.org/), our nifty phone and desktop integration tool, got a new website.

[Kid3](https://kid3.kde.org/) has a shiny new website with news and download links from all the stores it's available from.

## Releases 19.12.3

Some of our projects release on their own timescale and some get released en-masse. The 19.12.3 bundle of projects was released today and should be available through app stores and distros soon. See the [19.12.3 releases page](https://www.kde.org/info/releases-19.12.3.php) for details. This bundle was previously called KDE Applications but has been de-branded to become a release service to avoid confusion with all the other applications by KDE and because there are dozens of different products rather than a single whole.

Some of the fixes included in this release are:

* Support for file attributes for SMB shares has been improved, [various commits](https://cgit.kde.org/kio-extras.git/log/?h=release/19.12)
* K3b now accepts WAV files as audio files [BUG 399056](https://bugs.kde.org/show_bug.cgi?id=399056) [Commit](https://cgit.kde.org/k3b.git/commit/?h=release/19.12&id=2491cc70f9ffb129cf49633cbdc4f0d77789677d)
* Gwenview now can load remote images via https [Commit](https://cgit.kde.org/gwenview.git/commit/?h=release/19.12&id=9973a097d30a91dd50a4d23388b0b2b00eeeb626)

💻 [19.12 release notes](https://community.kde.org/Releases/19.12_Release_Notes) for information on tarballs and known issues. 💻 [Package download wiki page](https://community.kde.org/Get_KDE_Software_on_Your_Linux_Distro) 💻 [19.12.3 source info page](https://kde.org/info/releases-19.12.3) 💻 [19.12 full changelog](https://kde.org/announcements/changelog-releases.php?version=19.12.3) 💻
