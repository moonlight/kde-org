---
aliases:
- ../announce-3.1.5
custom_about: true
custom_contact: true
date: '2004-01-14'
description: KDE Project Ships Fifth and Final Translation and Service Release for
  the KDE 3.1 release
title: KDE 3.1.5 Release Announcement
---

<p>FOR IMMEDIATE RELEASE</p>

<h3 align="center">
   KDE Project Ships Fifth Translation and Service Release for Leading Open Source Desktop
</h3>

<p align="justify">
  
  KDE Project Ships Fifth Translation and Service Release of Third-Generation
  GNU/Linux - UNIX Desktop, Offering Enterprises and Governments a Compelling
  Free and Open Desktop Solution
  
</p>

<p align="justify">
  The <a href="/">KDE
  Project</a> today announced the immediate availability of KDE 3.1.5,
  a maintenance release for the third generation of the most advanced and
  powerful <em>free</em> desktop for GNU/Linux and other UNIXes.  KDE 3.1.5
  ships with a basic desktop and seventeen other packages
  (PIM, administration, network, edutainment, utilities, multimedia, games,
  artwork, web development and more).  KDE's award-winning tools and
  applications are available in 52 languages.
</p>

<p align="justify">
  KDE, including all its libraries and its applications, is
  available <em>for free</em> under Open Source licenses.
  KDE can be obtained in source and numerous binary formats from
  <a href="http://download.kde.org/stable/3.1.5/">download.kde.org</a> and can
  also be obtained on <a href="/download">CD-ROM</a>
  or with any of the major GNU/Linux - UNIX systems shipping today.
</p>

<h4>
  <a id="changes">Enhancements</a>
</h4>
<p align="justify">
  KDE 3.1.5 is a maintenance release which provides corrections of problems
  reported using the KDE <a href="http://bugs.kde.org/">bug tracking system</a>
  and <em><a href="/info/security/advisory-20040114-1.txt">a vulnerability in
  the .VCF file information reader</a></em>.
</p>
<p align="justify">
  For a more detailed list of improvements since the KDE 3.1 release in late
  January, please refer to the
  <a href="/announcements/changelogs/changelog3_1_4to3_1_5">KDE 3.1.5 Changelog</a>.
</p>
<p>
  Additional information about the enhancements of the KDE 3.1.x release
  series is available in the
  <a href="/info/1-2-3/3.1/feature_guide_1.html">KDE 3.1 New Feature Guide</a> and the <a href="../3.1">KDE 3.1 Announcement</a>.
</p>

<h4>
  Installing KDE 3.1.5 Binary Packages
</h4>
<p align="justify">
  <em>Packaging Policies</em>.
  Some Linux/UNIX OS vendors have kindly provided binary packages of
  KDE 3.1.5 for some versions of their distribution, and in other cases
  community volunteers have done so.
  Some of these binary packages are available for free download from KDE's
  <a href="http://download.kde.org/binarydownload.html?url=/stable/3.1.5/">download.kde.org</a>.
  Additional binary packages, as well as updates to the packages now
  available, may become available over the coming weeks.
</p>

<p align="justify">
  <a id="package_locations"><em>Package Locations</em></a>.
  For a current list of available binary packages of which the KDE
  Project has been informed, please visit the
  <a href="/info/1-2-3/3.1.5">KDE 3.1.5 Info Page</a>.
</p>

<h4>
  Compiling KDE 3.1.5
</h4>
<p align="justify">
  <a id="source_code"></a><em>Source Code</em>.
  The complete source code for KDE 3.1.5 may be
  <a href="http://download.kde.org/stable/3.1.5/src/">freely
  downloaded</a>.  Instructions on compiling and installing KDE 3.1.5
  are available from the <a href="/info/1-2-3/3.1.5#binary">KDE
  3.1.5 Info Page</a>.
</p>

<h4>
  KDE Sponsorship
</h4>
<p align="justify">
  Besides the superb and invaluable efforts by the
  <a href="http://www.kde.org/people/">KDE developers</a>
  themselves, significant support for KDE development has been provided by
  <a href="http://www.mandrakesoft.com/">MandrakeSoft</a>,
  <a href="http://www.trolltech.com/">Trolltech</a> and
  <a href="http://www.suse.com/">SuSE</a>.  In addition,
  the members of the <a href="http://www.kdeleague.org/">KDE
  League</a> provide significant support for KDE promotion,
  <a href="http://www.ibm.com/">IBM</a> has donated significant hardware
  to the KDE Project, and the
  <a href="http://www.uni-tuebingen.de/">University of T&uuml;bingen</a>
  and the <a href="http://www.uni-kl.de/">University of Kaiserslautern</a>
  provide most of the Internet bandwidth for the KDE project.  Thanks!
</p>

<h4>
  About KDE
</h4>
<p align="justify">
  KDE is an independent project of hundreds of developers, translators,
  artists and other professionals worldwide collaborating over the Internet
  to create and freely distribute a sophisticated, customizable and stable
  desktop and office environment employing a flexible, component-based,
  network-transparent architecture and offering an outstanding development
  platform.  KDE provides a stable, mature desktop, a full, component-based
  office suite (<a href="http://www.koffice.org/">KOffice</a>), a large
  set of networking and administration tools and utilities, and an
  efficient, intuitive development environment featuring the excellent IDE
  <a href="http://www.kdevelop.org/">KDevelop</a>.  KDE is working proof
  that the Open Source "Bazaar-style" software development model can yield
  first-rate technologies on par with and superior to even the most complex
  commercial software.
</p>

<hr />

<p align="justify">
  <font size="2">
  <em>Trademark Notices.</em>
  KDE and K Desktop Environment are trademarks of KDE e.V.

Linux is a registered trademark of Linus Torvalds.

UNIX is a registered trademark of The Open Group in the United States and
other countries.

All other trademarks and copyrights referred to in this announcement are
the property of their respective owners.
</font>

</p>

<hr />

<table id ="press" border=0 cellpadding=8 cellspacing=0 align="center">
<tr>
  <th colspan=2 align="left">
    Press Contacts:
  </th>
</tr>
<tr valign="top">
    <td>
      North/South America
    </td>
    <td nowrap="nowrap">
      George Staikos<br />
      889 Bay St. #205<br />
      Toronto, ON, M5S 3K5<br />
      Canada<br />

[staikos@kde.org](mailto:staikos@kde.org)
<br />
(1) 416 925 4030

  </td>
</tr>

 <tr valign="top">
    <td nowrap="nowrap">
      South America
    </td>
    <td nowrap="nowrap">
    Helio Chissini de Castro<br />
    R. Jos&eacute; de Alencar 120, apto 1906<br />
    Curitiba, PR 80050-240<br />
    Brazil<br />
    
  [helio@kde.org](mailto:helio@kde.org)<br />
    +55(41)262-0782 / +55(41)360-2670<br />
    </td>
  </tr>
<tr valign="top"><td>
Europe (French and English):
</td><td >
David Faure<br>

[faure@kde.org](mailto:faure@kde.org)<br>
(33) 4 3250 1445

</td></tr>
<tr Valign="top">
  <td >
    Europe (English and German):
  </td>
  <td>
    Ralf Nolden<br>
    
  [nolden@kde.org](mailto:nolden@kde.org) <br>
      (49) 2421 502758
  </td>
</tr>
<tr>
  <td colspan="2" nowrap="nowrap">
  <div align="center">
    <a href="http://www.kde.org/contact/">Press Contacts in your country</a>
  </div>
  </td>
  </tr>
</table>
