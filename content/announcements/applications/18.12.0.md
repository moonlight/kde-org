---
aliases:
- ../announce-applications-18.12.0
changelog: true
date: 2018-12-13
description: KDE Ships Applications 18.12.
layout: application
release: applications-18.12.0
title: KDE Ships KDE Applications 18.12.0
version: '18.12.0'
---

{{% i18n_date %}}

KDE Applications 18.12 are now released.

{{%youtube id="ALNRQiQnjpo"%}}

We continuously work on improving the software included in our KDE Application series, and we hope you will find all the new enhancements and bug fixes useful!

## What's new in KDE Applications 18.12

More than 140 bugs have been resolved in applications including the Kontact Suite, Ark, Cantor, Dolphin, Gwenview, Kate, KmPlot, Konsole, Lokalize, Okular, Spectacle, Umbrello and more!

### File Management

{{<figure src="/announcements/applications/18.12.0/app1812_dolphin01.png" width="600px" >}}

<a href='https://www.kde.org/applications/system/dolphin/'>Dolphin</a>, KDE's powerful file manager:

- New MTP implementation that makes it fully usable for production
- Huge performance improvement for reading files over the SFTP protocol
- For thumbnail previews, frames and shadows are now only drawn for image files with no transparency, improving the display for icons
- New thumbnail previews for LibreOffice documents and AppImage apps
- Video files larger than 5 MB in size are now displayed on directory thumbnails when directory thumbnailing is enabled
- When reading audio CDs, Dolphin is now able to change CBR bitrate for the MP3 encoder and fixes timestamps for FLAC
- Dolphin’s 'Control' menu now shows the 'Create New…' items and has a new 'Show Hidden Places' menu item
- Dolphin now quits when there is only one tab open and the standard 'close tab' keyboard shortcut (Ctrl+w) is pressed
- After unmounting a volume from the Places panel, it is now possible to mount it again
- The Recent Documents view (available by browsing to recentdocuments:/ in Dolphin) now only shows actual documents, and automatically filters out web URLs
- Dolphin now shows a warning before allowing you to rename file or directory in such a manner that would cause it to immediately become hidden
- It is no longer possible to try to unmount the disks for your active operating system or home directory from the Places Panel

<a href='https://www.kde.org/applications/utilities/kfind'>KFind</a>, KDE's traditional file search, now has a metadata search method based on KFileMetaData.

### Office

<a href='https://www.kde.org/applications/internet/kmail/'>KMail</a>, KDE's powerful email client:

- KMail can now display a unified inbox
- New plugin: Generate HTML mail from Markdown Language
- Using Purpose for Sharing Text (as email)
- HTML emails are now readable no matter what color scheme is in use

{{<figure src="/announcements/applications/18.12.0/app1812_okular01.png" width="600px" >}}

<a href='https://www.kde.org/applications/graphics/okular/'>Okular</a>, KDE's versatile document viewer:

- New 'Typewriter' annotation tool that can be used to write text anywhere
- The hierarchical table of contents view now has the ability to expand and collapse everything, or just a specific section
- Improved word-wrap behavior in inline annotations
- When hovering the mouse over a link, the URL is now shown anytime it could be clicked on, instead of only when in Browse mode
- ePub files containing resources with spaces in their URLs now display correctly

### Development

{{<figure src="/announcements/applications/18.12.0/app1812_kate01.png" width="600px" >}}

<a href='https://www.kde.org/applications/utilities/kate/'>Kate</a>, KDE's advanced text editor:

- When using the embedded terminal, it now automatically synchronizes the current directory with the active document's on-disk location
- Kate's embedded terminal can now be focused and de-focused using the F4 keyboard shortcut
- Kate’s built-in tab switcher now shows full paths for similarly-named files
- Line numbers are now on by default
- The incredibly useful and powerful Text Filter plugin is now enabled by default and is more discoverable
- Opening an already-open document using the Quick Open feature now switches back to that document
- The Quick Open feature no longer shows duplicate entries
- When using multiple Activities, files are now opened in the correct Activity
- Kate now displays all the correct icons when run on GNOME using the GNOME icon theme

{{<figure src="/announcements/applications/18.12.0/app1812_konsole.png" width="600px" >}}

<a href='https://www.kde.org/applications/system/konsole/'>Konsole</a>, KDE's terminal emulator:

- Konsole now fully supports emoji characters
- Inactive tabs' icons are now highlighted when they receive a bell signal
- Trailing colons are no longer considered parts of a word for the purposes of double-click selection, making it easier to select paths and 'grep' output
- When a mouse with back and forward buttons is plugged in, Konsole can now use those buttons for switching between tabs
- Konsole now has a menu item to reset the font size to the profile default, if it has been enlarged or reduced in size
- Tabs are now harder to accidentally detach, and faster to accurately re-order
- Improved shift-click selection behavior
- Fixed double-clicking on a text line that exceeds the window width
- The search bar once again closes when you hit the Escape key

<a href='https://www.kde.org/applications/development/lokalize/'>Lokalize</a>, KDE's translation tool:

- Hide translated files on the Project tab
- Added basic support of pology, the syntax and glossary checking system
- Simplified navigation with tab ordering and multi-tab opening
- Fixed segfaults due to concurrent access to database objects
- Fixed inconsistent drag and drop
- Fixed a bug on shortcuts which were different between editors
- Improved search behavior (search will find and display plural forms)
- Restore Windows compatibility thanks to the craft building system

### Utilities

<a href='https://www.kde.org/applications/graphics/gwenview/'>Gwenview</a>, KDE's image viewer:

- The 'Reduce Red Eye' tool received a variety of nice usability improvements
- Gwenview now displays a warning dialog when you hide the menubar that tells you how to get it back

{{<figure src="/announcements/applications/18.12.0/app1812_spectacle01.png" width="600px" >}}

<a href='https://www.kde.org/applications/graphics/spectacle/'>Spectacle</a>, KDE's screenshot utility:

- Spectacle now has the ability to sequentially number screenshot files, and defaults to this naming scheme if you clear the filename text field
- Fixed saving images in a format other than .png, when using Save As…
- When using Spectacle to open a screenshot in an external app, it is now possible to modify and save the image after you are done with it
- Spectacle now opens the correct folder when you click Tools > Open Screenshots Folder
- Screenshot timestamps now reflect when the image was created, not when it was saved
- All of the save options are now located on the “Save” page

<a href='https://www.kde.org/applications/utilities/ark/'>Ark</a>, KDE's archive manager:

- Added support for the Zstandard format (tar.zst archives)
- Fixed Ark previewing certain files (e.g. Open Document) as archives instead of opening them in the appropriate application

### Mathematics

<a href='https://www.kde.org/applications/utilities/kcalc/'>KCalc</a>, KDE's simple calculator, now has a setting to repeat the last calculation multiple times.

<a href='https://www.kde.org/applications/education/cantor/'>Cantor</a>, KDE's mathematical frontend:

- Add Markdown entry type
- Animated highlighting of the currently calculated command entry
- Visualization of pending command entries (queued, but not being calculated yet)
- Allow to format command entries (background color, foreground color, font properties)
- Allow to insert new command entries at arbitrary places in the worksheet by placing the cursor at the desired position and by start typing
- For expressions having multiple commands, show the results as independent result objects in the worksheet
- Add support for opening worksheets by relative paths from console
- Add support for opening multiple files in one Cantor shell
- Change the color and the font for when asking for additional information in order to better discriminate from the usual input in the command entry
- Added shortcuts for the navigation across the worksheets (Ctrl+PageUp, Ctrl+PageDown)
- Add action in 'View' submenu for zoom reset
- Enable downloading of Cantor projects from store.kde.org (at the moment upload works only from the website)
- Open the worksheet in read-only mode if the backend is not available on the system

<a href='https://www.kde.org/applications/education/kmplot/'>KmPlot</a>, KDE's function plotter, fixed many issues:

- Fixed wrong names of the plots for derivatives and integral in the conventional notation
- SVG export in Kmplot now works properly
- First derivative feature has no prime-notation
- Unticking unfocused function now hides its graph:
- Solved Kmplot crash when opening 'Edit Constants' from function editor recursively
- Solved KmPlot crash after deleting a function the mouse pointer follows in the plot
- You can now export drawn data as any picture format
