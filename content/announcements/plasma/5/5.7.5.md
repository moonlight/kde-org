---
aliases:
- ../../plasma-5.7.5
changelog: 5.7.4-5.7.5
date: 2016-09-13
layout: plasma
youtube: A9MtFqkRFwQ
figure:
  src: /announcements/plasma/5/5.7.0/plasma-5.7.png
  class: text-center mt-4
asBugfix: true
---

- Plasma Workspace: Fix some status notifier items not appearing. <a href="http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=df4387a21f6eb5ede255ea148143122ae4d5ae9c">Commit.</a> Fixes bug <a href="https://bugs.kde.org/366283">#366283</a>. Fixes bug <a href="https://bugs.kde.org/367756">#367756</a>
- SDDM Config - Fix themes list with SDDM 0.14. <a href="http://quickgit.kde.org/?p=sddm-kcm.git&amp;a=commit&amp;h=d4ca70001222c5b0a9f699c4639c891a6a5c0c05">Commit.</a> Code review <a href="https://git.reviewboard.kde.org/r/128815">#128815</a>
- Make sure people are not trying to sneak invisible characters on the kdesu label. <a href="http://quickgit.kde.org/?p=kde-cli-tools.git&amp;a=commit&amp;h=5eda179a099ba68a20dc21dc0da63e85a565a171">Commit.</a>
