---
date: 2023-05-09
changelog: 5.27.4-5.27.5
layout: plasma
video: false
asBugfix: true
draft: false
---

+ Bluedevil Applet: introduce a brief animation for section height estimates. [Commit.](http://commits.kde.org/bluedevil/1956411588dbed54acb672d89ca003d8806684c4) Fixes bug [#438610](https://bugs.kde.org/438610)
+ Discover Flatpak: Do not crash if for any reason we lack a ref's source. [Commit.](http://commits.kde.org/discover/2bed299745d7c56c53191e46a8affe89f19c0491) Fixes bug [#467827](https://bugs.kde.org/467827)
+ Info Centre About-distro: add a dump mode to print to cli. [Commit.](http://commits.kde.org/kinfocenter/34838dc61b4448791745a53df677f75710e3ca03) Fixes bug [#403077](https://bugs.kde.org/403077)
