---
#scssFiles:
#  - /scss/plasma-5-25.scss
authors:
  - SPDX-FileCopyrightText: 2022 Jonathan Esk-Riddell <jr@jriddell.org>
SPDX-License-Identifier: CC-BY-4.0
date: 2023-01-19
changelog: 5.26.5-5.26.90
layout: plasma
title: Plasma 5.27 Beta
draft: false
---

{{< figure src="/announcements/plasma/5/5.27.0/plasma-5.27-beta.png" alt="Plasma 5.27 Beta"  >}}

Today we are bringing you the preview version of KDE's Plasma 5.27 release. Plasma 5.27 Beta is aimed at testers, developers, and bug-hunters.  As well as our lightweight and feature rich Linux Desktop this release adds a Bigscreen version of Plasma for use on televisions.

To help KDE developers iron out bugs and solve issues, install Plasma 5.27 Beta and test run the features listed below. Please report bugs to our [bug tracker](https://bugs.kde.org).

The final version of Plasma 5.27 will become available for the general public on the 14th of February.

**DISCLAIMER:** This release contains untested and unstable software. It is highly recommended you **do not use this version in a production environment** and do not use it as your daily work environment. You risk crashes and loss of data.

See below the most noteworthy changes that need testing:

## Plasma Welcome

Plasma Welcome introduces new users to the beauty and power of our desktop. It allows to perform inital setup tasks like connecting all your online accounts and suggests how to connect with the KDE Community.

{{< figure src="/announcements/plasma/5/5.27.0/plasma-welcome.png" alt="Plasma Welcome" >}}

## Flatpak Permissions Settings

An increasing number of our users are installing software through containerised packages such as Flatpaks.  These packages offer no access to the rest of the system by default and will request permissions as needed, meaning malicious code is less likely to be able to mess up your computer.  A new System Settings module has been added to easily control these settings for Flatpaks.

{{< figure src="/announcements/plasma/5/5.27.0/flatpak-kcm.png" alt="Flatpak Permissions Settings" >}}

## The Big Multi-monitor Refactor

The Display Configuration widget now appears in the System Tray by default–inactive when you only have one screen or you have a multi-monitor desktop setup, and active when you have a laptop with one or more external screens connected. This makes it easier to quickly change those screens’ settings if needed.

Behind the scenes <a href="https://notmart.org/blog/2022/12/multi-screen/">there is a big multi-monitor revamp</a> to make working with screens much more reliable.  There is also a new fine grained control tools when the user has 3 or more screens connected.

{{< figure src="/announcements/plasma/5/5.27.0/display-configuration.png" alt="Display Configuration" >}}

## Hebrew Calendar

You can now show the Hebrew Calendar in your Digital Clock popup’s calendar.

{{< figure src="/announcements/plasma/5/5.27.0/hebrew-calendar.jpg" alt="Hebrew Calendar" width="600px" >}}

## KWin Tiling System

For those with a large monitor KWin has long been able to place one window on the left and one on the right.  Now with <tt>Meta-T</tt> the quick tiling is launched allowing complete control of where your windows are placed.  Drag windows with <tt>Shift</tt> pressed and it will stick to the tiled layout.

{{< figure src="/announcements/plasma/5/5.27.0/tiling-1.jpg" alt="Window Tiling" width="600px" >}}

{{< figure src="/announcements/plasma/5/5.27.0/tiling-2.jpg" alt="Window Tiling" width="600px" >}}

{{< figure src="/announcements/plasma/5/5.27.0/tiling-3.jpg" alt="Window Tiling" width="600px" >}}
