---
aliases:
- ../../plasma-5.4.95
changelog: 5.4.3-5.4.95
date: 2015-11-19
layout: plasma
figure:
  src: /announcements/plasma/5/5.5.0/plasma-5.5.png
---

{{% i18n_date %}}

Today KDE releases a beta of next month's new feature update, Plasma 5.5.

We have been working hard over the last four months to smooth off the rough edges, add useful new workflows, make Plasma even more beautiful and build the foundations for the future.

{{<figure src="/announcements/plasma/5/5.5.0/breeze.png" alt="Breeze Icons" class="text-center" width="600px" caption="Breeze Icons">}}

### Updated Breeze Plasma Theme

The Breeze Plasma widget theme has been updated to make it more consistent.

While the Breeze icons theme adds new icons and updates the existing icon set to improve the visual design.

### Plasma Widget Explorer

The Plasma Widget explorer now supports a two column view with new widget icons for Breeze, Breeze Dark and Oxygen

### Expanded Feature Set in Application Launcher

Context menus in Application Launcher ('Kickoff') can now list documents recently opened in an application, allow editing the application's menu entry and adding the application to the panel, Task Manager or desktop. Favorites now supports documents, directories and system actions or they can be created from search results. These features (and some others) were previously available only in the alternative Application Menu ('Kicker') and have now become available in the default Application Launcher by sharing the backend between both launchers.

{{<figure src="/announcements/plasma/5/5.5.0/plasma-5.5-colorpicker.png" alt="Color Picker Plasma Applet" class="text-center" width="600px" caption="Color Picker Plasma Applet">}}

### New Applets in Plasma Addons

#### Color Picker

Not only have we restored support for the Color Picker applet, we've given it an entire new UI refresh to fit in with Plasma 5.

The color picker applet lets you pick a color from anywhere on the screen and automatically copies its color code to the clipboard in a variety of formats (RGB, Hex, Qt QML rgba, LaTeX).

{{<figure src="/announcements/plasma/5/5.5.0/plasma-5.5-user-switcher.png" alt="User Switcher Plasma Applet" class="text-center" width="600px" caption="User Switcher Plasma Applet">}}

#### User Switcher

User switching has been updated and improved and is now accessible from the Application Launcher, the new User Switcher applet and in the lock screen. It shows the user's full name and user set avatar. This is very useful for offices with shared desks. More info in <a href='http://blog.broulik.de/2015/10/polish-polish-polish-5-5-edition/'>the developer blog</a>.

#### Disk Quota

Plasma 5.5 sees a new applet designed for business environments or universities. This applet will show you usage assessed not around the real disk usage, but your allowed quota by your system administrator.

#### Activity Pager

Done for users whose use case of activities partly overlaps with virtual desktops: it looks like a pager, it behaves like a pager but uses activities instead of virtual desktops. This gives a quick glimpse of what activities are running and how many windows are associated to each activity.

{{<figure src="/announcements/plasma/5/5.5.0/systray.png" alt="Legacy System Tray Icons" class="text-center" width="600px" caption="Legacy System Tray Icons">}}

### Restored Legacy Icons in System Tray Support

In response to feedback, we've rewritten support for legacy applications not using the <a href='http://www.freedesktop.org/wiki/Specifications/StatusNotifierItem/'>StatusNotifier</a> standard for system tray icons.

### Bug Stats

In the run up to the Plasma 5.5 beta an incredible <a href='https://goo.gl/mckdTF'>over 1,000 bugs were fixed</a>.

### OpenGL ES Support in KWin

Support for switching to OpenGL ES in KWin returns. So far only switching through an environment variable and restarting KWin is supported. Set environment variable KWIN_COMPOSE to 'O2ES' to force the OpenGL ES backend. Please note that OpenGL ES is not supported by all drivers. Because of that it's not exposed through a configuration mechanism. Please consider it as an expert mode.

{{<figure src="/announcements/plasma/5/5.5.0/kscreenlocker.png" alt="Screen Locker" class="text-center" width="600px" caption="Screen Locker">}}

### Wayland Progress:

With Plasma 5.5 a basic Wayland session is provided. Wayland is the successor of the dated X11 windowing system providing a modern approach. The system is more secure (e.g. key loggers are no longer trivial to implement) and follows the paradigm of 'every frame perfect' which makes screen tearing very difficult. With Plasma 5.4 the KDE community already provided a technology preview based on the feature set of the Phone project. With Plasma 5.5 this is now extended with more 'desktop style' usages. Important features like move/resize of windows is now supported as well as many integration features for the desktop shell. This allows for usage by early adopters, though we need to point out that it is not yet up to the task of fully replacing an X session. We encourage our more technical users to give it a try and report as many bugs as you can find.

A new <a href='https://vizzzion.org/blog/2015/11/screen-management-in-wayland/'>screen management protocol</a> has been created for configuring the connected screens of a Wayland session.

Also added are some protocols for controlling KWin effects in Wayland such as window background blur and windows minimize animation

Plasma on Wayland session now features secure screen locking, something never fully achievable with X. Read more about fixing this 11 year old bug on the <a href='https://bhush9.github.io/2015/11/17/screenlocker-in-wayland/'>screenlocker integration developer blog</a>.

Please also see the list of <a href='https://community.kde.org/Plasma/5.5_Errata#KWin_Wayland'>known issues with Wayland on the Errata page</a>.

{{<figure src="/announcements/plasma/5/5.5.0/discover.png" alt="Discover" class="text-center" width="600px" caption="Discover">}}

### New Discover design

With the help of the KDE Visual Design Group we came up with a new design that will improve the usability of our software installer.

{{<figure src="/announcements/plasma/5/5.5.0/baloo-kinfocenter.png" alt="File Indexer Status" class="text-center" width="600px" caption="File Indexer Status">}}

### Info Center

A status module for the file indexer was added.

### Plasma Networkmanager

There have <a href='https://grulja.wordpress.com/2015/11/08/upcoming-news-in-plasma-5-5/'>been several improvements</a> to our network manager applet. WPA/WPA2 Enterprise validation was added, it uses a new password field widget and OpenVPN has more options.

### Known Issues

Please see the <a href='https://community.kde.org/Plasma/5.5_Errata'>Plasma 5.5 Errata page</a> for some of the highest profile issues including some significant problems caused by Intel drivers.</a>
