---
aliases:
- /announcements/plasma-5.13.0-5.13.1-changelog
hidden: true
plasma: true
title: Plasma 5.13.1 Complete Changelog
type: fulllog
version: 5.13.1
---

### <a name='discover' href='https://commits.kde.org/discover'>Discover</a>

- ASAN: Specify buffer size. <a href='https://commits.kde.org/discover/17c0b578920f01a84d1fd0bc12c47ffff87e850a'>Commit.</a> </li>
- ASAN: Fix possible division by 0. <a href='https://commits.kde.org/discover/24d0bdc6e3271e0a6f68481626fd14fc057d97a4'>Commit.</a> </li>
- Fix warning, add missing interface. <a href='https://commits.kde.org/discover/7bebf11ad9bdc1a8bccf1e800c28b1cacb333e43'>Commit.</a> </li>
- Fix build with newer flatpak. <a href='https://commits.kde.org/discover/e95083f4a3a3cc64be9c98913a42759bce2716ee'>Commit.</a> </li>

### <a name='kdeplasma-addons' href='https://commits.kde.org/kdeplasma-addons'>Plasma Addons</a>

- Do not restore windows on minimise change. <a href='https://commits.kde.org/kdeplasma-addons/70ecec7298e4391251e435d2471447fa9d0f0c13'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/395519'>#395519</a>. Phabricator Code review <a href='https://phabricator.kde.org/D13598'>D13598</a></li>
- [weather applet] Fix tooltip to add only existing wind information. <a href='https://commits.kde.org/kdeplasma-addons/68dfbd15783daf79ee638945ac0d9a71886adbec'>Commit.</a> </li>
- [weather applet] Fix new wind direction arrows to match direction of old. <a href='https://commits.kde.org/kdeplasma-addons/10e7ede98324168c7ba79b3ec6b26892337c1850'>Commit.</a> </li>

### <a name='khotkeys' href='https://commits.kde.org/khotkeys'>KDE Hotkeys</a>

- Remove verbose debugging statement. <a href='https://commits.kde.org/khotkeys/c5743089667d29e32191409b600e5640fbad173f'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D13435'>D13435</a></li>

### <a name='kwin' href='https://commits.kde.org/kwin'>KWin</a>

- [libkwineffects/kwinglutils] Calculate correct srcY0 and srcY1 in GLRenderTarget::blitFromFramebuffer. <a href='https://commits.kde.org/kwin/982316072d5f83783042b7c83ebf52e7a56a650c'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D12452'>D12452</a></li>
- Fix multimonitor blur. <a href='https://commits.kde.org/kwin/2419c949bfc4673157555f191f417cafb1bb04a0'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/393723'>#393723</a>. Phabricator Code review <a href='https://phabricator.kde.org/D12678'>D12678</a></li>
- [effects/blur] Check for blitting support. <a href='https://commits.kde.org/kwin/be3168b832fb050a9ee2c735dff7cceed077369e'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D13246'>D13246</a></li>
- Fix blur on Wayland when scaling is used. <a href='https://commits.kde.org/kwin/37f4c54d17f9da3e1c2ddee7592a2dfcffd3e9e3'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/391387'>#391387</a>. Phabricator Code review <a href='https://phabricator.kde.org/D12700'>D12700</a></li>
- Hide decoration tooltip when the decoration gets destroyed. <a href='https://commits.kde.org/kwin/c44adda40f47418ce6eea974b2a3c3b1e77fa205'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/394977'>#394977</a>. Phabricator Code review <a href='https://phabricator.kde.org/D13299'>D13299</a></li>

### <a name='plasma-desktop' href='https://commits.kde.org/plasma-desktop'>Plasma Desktop</a>

- Fix tooltip woes. <a href='https://commits.kde.org/plasma-desktop/24803dc9dc4000e06edb60a3a8afe5925f0d72d7'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/382571'>#382571</a>. Fixes bug <a href='https://bugs.kde.org/385947'>#385947</a>. Fixes bug <a href='https://bugs.kde.org/389469'>#389469</a>. Fixes bug <a href='https://bugs.kde.org/388749'>#388749</a>. Phabricator Code review <a href='https://phabricator.kde.org/D13602'>D13602</a></li>
- Ref KConfig whilst we're using it. <a href='https://commits.kde.org/plasma-desktop/ead2c3e859dcb8531df99e87c0fe6eff64650952'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/394534'>#394534</a>. Phabricator Code review <a href='https://phabricator.kde.org/D13599'>D13599</a></li>
- Fix accident. <a href='https://commits.kde.org/plasma-desktop/adbc35b0d82d332ac4826ddfe34d73bee04fd8a0'>Commit.</a> </li>
- Touchpad KDED module: Convert to JSON metadata. <a href='https://commits.kde.org/plasma-desktop/de475576e65fd8aff4d86379e322c41f4429c745'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D13360'>D13360</a></li>
- Swap Trash for Delete action when only one is enabled and the Shift modifier is pressed. <a href='https://commits.kde.org/plasma-desktop/6f3a486330d02cd1feb10969ab56113a09f3e0af'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/395155'>#395155</a>. Phabricator Code review <a href='https://phabricator.kde.org/D13508'>D13508</a></li>
- Fonts KCM: Fix text readability regression. <a href='https://commits.kde.org/plasma-desktop/540edfdd2a88190a1f3665a952a1d2d57c72ab3f'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D13390'>D13390</a></li>

### <a name='plasma-nm' href='https://commits.kde.org/plasma-nm'>Plasma Networkmanager (plasma-nm)</a>

- Properly fix icon size for connection delegate. <a href='https://commits.kde.org/plasma-nm/c43ae59a06b7f2b6324a5bfaba1dec357c17499d'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/394580'>#394580</a></li>
- Attempt to fix icon size for connection delegate. <a href='https://commits.kde.org/plasma-nm/6dc2fc113a89fd14d4b5144485c639ac1e500e40'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/394580'>#394580</a></li>