------------------------------------------------------------------------
r1107789 | mueller | 2010-03-27 10:07:15 +1300 (Sat, 27 Mar 2010) | 2 lines

it seems this is wrong now when using final build

------------------------------------------------------------------------
r1107860 | scripty | 2010-03-27 15:32:15 +1300 (Sat, 27 Mar 2010) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1108081 | mmrozowski | 2010-03-28 08:26:46 +1300 (Sun, 28 Mar 2010) | 3 lines

Fix indentation

SVN_SILENT
------------------------------------------------------------------------
r1108352 | adawit | 2010-03-29 06:18:39 +1300 (Mon, 29 Mar 2010) | 1 line

Backported the retain SSL meta-data on job redirection fix
------------------------------------------------------------------------
r1108353 | adawit | 2010-03-29 06:21:30 +1300 (Mon, 29 Mar 2010) | 1 line

Use SlaveBase::connectTimeout() so that config changes take immediate effect when a user modifies those values
------------------------------------------------------------------------
r1108644 | jriddell | 2010-03-30 02:58:00 +1300 (Tue, 30 Mar 2010) | 8 lines

backport 1108640
Check for actual metadata file, else it will try to load the plasmoid
from ~/.kde if only the directory exists even though the plasmoid
might be in /usr
BUG:231761
"Python plasmoids can no longer run from system directory"


------------------------------------------------------------------------
r1109205 | dfaure | 2010-03-31 08:13:00 +1300 (Wed, 31 Mar 2010) | 4 lines

Backport both fixes for renaming-on-VFAT
Fixed for: 4.4.2
BUG: 162358

------------------------------------------------------------------------
r1109728 | mwolff | 2010-04-01 10:50:31 +1300 (Thu, 01 Apr 2010) | 3 lines

Backport recent fixes and changes to Javascript language file

CCBUG: 229605
------------------------------------------------------------------------
r1109765 | scripty | 2010-04-01 15:08:53 +1300 (Thu, 01 Apr 2010) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1110075 | rdieter | 2010-04-02 09:03:14 +1300 (Fri, 02 Apr 2010) | 2 lines

backport r1095576 , fix for build against Qt 4.7

------------------------------------------------------------------------
r1110122 | adawit | 2010-04-02 12:31:29 +1300 (Fri, 02 Apr 2010) | 1 line

A one liner addition to support kdewebkit
------------------------------------------------------------------------
r1110211 | darioandres | 2010-04-03 01:25:40 +1300 (Sat, 03 Apr 2010) | 11 lines

Backport to 4.4 of:
SVN commit 1110210 by darioandres:

- Update KFileItem and KFileMetaPropsPlugin to only request limited metaData
  (avoid reading all the file and blocking the UI)
  This implements http://reviewboard.kde.org/r/3325/ as a temporary fix until
KFileMetaInfo+Strigi limits get improved
  This is a workaround for bug 216932

BUG: 216932

------------------------------------------------------------------------
r1110286 | adawit | 2010-04-03 06:17:07 +1300 (Sat, 03 Apr 2010) | 5 lines

- Fixed a bug with filling forms on pages that contain unnamed (no 'name' attribute)
  forms of which some of them use 'get' to submit the form.
- Combined the CSS selection query to obtain form elements.
- Corrected typo in the documentation of formsWithCachedData.

------------------------------------------------------------------------
r1110402 | dfaure | 2010-04-03 12:32:03 +1300 (Sat, 03 Apr 2010) | 2 lines

Backport fix for bug 159295, moving tabs in konq would focus the location bar (and prevent moving further using the shortcuts).

------------------------------------------------------------------------
r1110421 | scripty | 2010-04-03 14:50:30 +1300 (Sat, 03 Apr 2010) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1111264 | ppenz | 2010-04-05 22:42:58 +1200 (Mon, 05 Apr 2010) | 5 lines

Backport of SVN commit 1111263: KStringHandler::naturalCompare() should assure that "1" < "a" < "v01 1" < "v01 a"

CCBUG: 231445


------------------------------------------------------------------------
r1111659 | gokcen | 2010-04-06 23:28:01 +1200 (Tue, 06 Apr 2010) | 1 line

set POLKITQT_POLICY_FILES_INSTALL_DIR variable before message using this variable.
------------------------------------------------------------------------
r1112337 | aseigo | 2010-04-08 09:43:51 +1200 (Thu, 08 Apr 2010) | 2 lines

pass on the resize event

------------------------------------------------------------------------
r1112340 | aseigo | 2010-04-08 09:46:30 +1200 (Thu, 08 Apr 2010) | 2 lines

do the proxy widget resize first, so that the edit rect is correct

------------------------------------------------------------------------
r1112748 | scripty | 2010-04-09 13:49:38 +1200 (Fri, 09 Apr 2010) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1113361 | adawit | 2010-04-11 03:41:04 +1200 (Sun, 11 Apr 2010) | 1 line

Backport the fix for a very sublte bug that incorrectly triggers the emission of the saveFormDataRequested signal.
------------------------------------------------------------------------
r1113806 | pletourn | 2010-04-12 08:40:54 +1200 (Mon, 12 Apr 2010) | 4 lines

Don't draw marks if not asked to

CCBUG:168493

------------------------------------------------------------------------
r1114118 | dfaure | 2010-04-13 07:36:40 +1200 (Tue, 13 Apr 2010) | 2 lines

backport 1114117: repair test

------------------------------------------------------------------------
r1114124 | pletourn | 2010-04-13 07:53:18 +1200 (Tue, 13 Apr 2010) | 2 lines

Init var

------------------------------------------------------------------------
r1114514 | pletourn | 2010-04-14 07:48:59 +1200 (Wed, 14 Apr 2010) | 4 lines

Fix deleting block selection containing tabs

CCBUG:232320

------------------------------------------------------------------------
r1114745 | dafre | 2010-04-14 23:45:42 +1200 (Wed, 14 Apr 2010) | 5 lines

CCBUG: 227117

Backporting r1114744


------------------------------------------------------------------------
r1114848 | pletourn | 2010-04-15 06:37:31 +1200 (Thu, 15 Apr 2010) | 4 lines

Unbreaking compilation
Sorry


------------------------------------------------------------------------
r1114958 | scripty | 2010-04-15 13:41:34 +1200 (Thu, 15 Apr 2010) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1115598 | scripty | 2010-04-17 13:42:07 +1200 (Sat, 17 Apr 2010) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1116664 | scripty | 2010-04-20 14:07:29 +1200 (Tue, 20 Apr 2010) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1116669 | cfeck | 2010-04-20 14:21:45 +1200 (Tue, 20 Apr 2010) | 4 lines

Always use maxWidth for the text width (backport r1116668)

CCBUG: 234838

------------------------------------------------------------------------
r1116991 | aseigo | 2010-04-21 07:53:16 +1200 (Wed, 21 Apr 2010) | 2 lines

if the source is new, but comes with data, we need to do an update immediately

------------------------------------------------------------------------
r1117153 | lunakl | 2010-04-22 00:01:09 +1200 (Thu, 22 Apr 2010) | 4 lines

Detect desktop changes when mapping from viewports.
Patch by Danny Baumann.


------------------------------------------------------------------------
r1117269 | dhaumann | 2010-04-22 05:58:29 +1200 (Thu, 22 Apr 2010) | 5 lines

Attempt to fix input method event handling to allow japanese text.
If possible, please try the 4.4 branch.

CCBUG: 206455

------------------------------------------------------------------------
r1117452 | annma | 2010-04-22 21:00:55 +1200 (Thu, 22 Apr 2010) | 5 lines

try to unbreak build but not sure what was intented!
Please Dominik look at it and see if this is correct
CCMAIL=dhdev@gmx.de
CCMAIL=kwrite-devel@kde.org

------------------------------------------------------------------------
r1117461 | trueg | 2010-04-22 21:22:40 +1200 (Thu, 22 Apr 2010) | 1 line

Backport: Make sure a thread never deletes the model used by another thread (at least on the most upper level)
------------------------------------------------------------------------
r1117590 | dhaumann | 2010-04-23 02:53:31 +1200 (Fri, 23 Apr 2010) | 1 line

sorry for the previous compilation error...
------------------------------------------------------------------------
r1117595 | dhaumann | 2010-04-23 03:05:24 +1200 (Fri, 23 Apr 2010) | 1 line

CCBUG: 206455
------------------------------------------------------------------------
r1117788 | scripty | 2010-04-23 13:57:08 +1200 (Fri, 23 Apr 2010) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1117915 | segato | 2010-04-24 00:43:33 +1200 (Sat, 24 Apr 2010) | 4 lines

backport r1117767
emit finished/accepted/rejected signals when using the native filedialog too
CCBUG:231918

------------------------------------------------------------------------
r1118218 | scripty | 2010-04-24 14:02:53 +1200 (Sat, 24 Apr 2010) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1118297 | dafre | 2010-04-24 22:47:03 +1200 (Sat, 24 Apr 2010) | 6 lines

CCBUG: 227279
CCBUG: 218468

Backporting previous fix


------------------------------------------------------------------------
r1118305 | dafre | 2010-04-24 23:30:53 +1200 (Sat, 24 Apr 2010) | 6 lines

Backporting r1118303:

 * Fixes and optimizations to KIdleTime controller
 * Remove useless #ifdef's from xsync


------------------------------------------------------------------------
r1118319 | dafre | 2010-04-25 00:06:32 +1200 (Sun, 25 Apr 2010) | 6 lines

Backport r1118318:

 * Fix and optimize some more things in XSync, like more #ifdefs removed, a memleak and some slow iterations
 * KUtils no longer links to XTest \o/


------------------------------------------------------------------------
r1118330 | dafre | 2010-04-25 00:29:39 +1200 (Sun, 25 Apr 2010) | 7 lines

CCBUG: 231628

Backporting r1118329:

Be sure to stop catching resume events when requested.


------------------------------------------------------------------------
r1118443 | orlovich | 2010-04-25 07:21:13 +1200 (Sun, 25 Apr 2010) | 10 lines

Merged revision:r1116645 | orlovich | 2010-04-19 19:55:23 -0400 (Mon, 19 Apr 2010) | 10 lines

Some fixes for <object> handling:
- Properly attempt part creation when no src set if we have datatype; 
  needed for a part of new youtube skin's probe sequence; for now 
  only kmplayer's nsp support is capable enough for running it though.
  (khtmlpart's portion also needs some tweaks)
  
- Properly destroy plugin when object removed from document, should 
  fix the 'ghost sound' problem on youtube.
------------------------------------------------------------------------
r1118444 | orlovich | 2010-04-25 07:22:46 +1200 (Sun, 25 Apr 2010) | 4 lines

Merged revision:r1117251 | orlovich | 2010-04-21 12:55:51 -0400 (Wed, 21 Apr 2010) | 4 lines

Provide nspluginviewer with the location to help it out a bit, so I can fix 
the youtube thing in a 4.4-sane way (w/o tons of new infrastructure)
------------------------------------------------------------------------
r1118521 | cfeck | 2010-04-25 12:14:14 +1200 (Sun, 25 Apr 2010) | 7 lines

Never show horizontal scroll bar (backport r1118520)

Revert commit r1104322, and try a different workaround.

CCBUG: 233163
CCBUG: 213068

------------------------------------------------------------------------
r1118536 | scripty | 2010-04-25 13:57:37 +1200 (Sun, 25 Apr 2010) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1119949 | scripty | 2010-04-28 13:54:39 +1200 (Wed, 28 Apr 2010) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1120394 | scripty | 2010-04-29 13:58:59 +1200 (Thu, 29 Apr 2010) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1120717 | mueller | 2010-04-30 07:50:58 +1200 (Fri, 30 Apr 2010) | 2 lines

bump version

------------------------------------------------------------------------
