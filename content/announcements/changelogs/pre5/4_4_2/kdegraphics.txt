------------------------------------------------------------------------
r1096488 | cfeck | 2010-02-27 07:40:42 +1300 (Sat, 27 Feb 2010) | 5 lines

Use alpha transparency instead of mask (backport r1096486)

5 commits too late for 4.4.1, so fixed for 4.4.2
CCBUG: 228650

------------------------------------------------------------------------
r1098396 | lueck | 2010-03-04 05:53:48 +1300 (Thu, 04 Mar 2010) | 2 lines

doc backport from trunk
BUG:228532
------------------------------------------------------------------------
r1098667 | scripty | 2010-03-04 15:17:03 +1300 (Thu, 04 Mar 2010) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1098743 | gateau | 2010-03-04 22:00:44 +1300 (Thu, 04 Mar 2010) | 1 line

Use separate copies of libjpeg internal files depending on libjpeg version BUG:227313
------------------------------------------------------------------------
r1098783 | gateau | 2010-03-04 23:36:25 +1300 (Thu, 04 Mar 2010) | 1 line

Store thumbnails coming from EXIF to disk: it's faster to access them this way
------------------------------------------------------------------------
r1098784 | gateau | 2010-03-04 23:36:32 +1300 (Thu, 04 Mar 2010) | 1 line

Bumped version numbers
------------------------------------------------------------------------
r1098785 | gateau | 2010-03-04 23:36:39 +1300 (Thu, 04 Mar 2010) | 1 line

Long overdue update :/
------------------------------------------------------------------------
r1099539 | reed | 2010-03-06 07:36:34 +1300 (Sat, 06 Mar 2010) | 1 line

more missing include directories
------------------------------------------------------------------------
r1099973 | ilic | 2010-03-07 01:58:36 +1300 (Sun, 07 Mar 2010) | 1 line

KColorChooser has its own catalog, setting it as kdelibs4 is probably a leftover from some earlier state.
------------------------------------------------------------------------
r1104495 | aacid | 2010-03-18 08:40:50 +1300 (Thu, 18 Mar 2010) | 6 lines

Backport r1104494 | aacid | 2010-03-17 19:37:52 +0000 (Wed, 17 Mar 2010) | 4 lines

make the iconloader in GuiUtils a stack
Fixes problems when the same program uses more than one okular part at once
BUGS: 231123

------------------------------------------------------------------------
r1105307 | pino | 2010-03-20 08:55:14 +1300 (Sat, 20 Mar 2010) | 2 lines

use abs() before comparing to the current time, otherwise the comparison will always fail when the midnight passes

------------------------------------------------------------------------
r1105826 | scripty | 2010-03-22 02:22:27 +1300 (Mon, 22 Mar 2010) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1106764 | aacid | 2010-03-24 10:51:53 +1300 (Wed, 24 Mar 2010) | 5 lines

backport r1106763 | aacid | 2010-03-23 21:50:13 +0000 (Tue, 23 Mar 2010) | 3 lines

do not trust kdelibs to give a pixmap even if we ask for it since sometimes it fails
BUGS: 230282

------------------------------------------------------------------------
r1107588 | harshj | 2010-03-26 17:03:23 +1300 (Fri, 26 Mar 2010) | 8 lines

For Okular's Comic Book backend (.cbr files):
 * Add support for the 'rar' executable, since some distros seem to use it.

(Backport of 1107586)

CCBUG:232061


------------------------------------------------------------------------
