------------------------------------------------------------------------
r1181772 | scripty | 2010-10-02 15:43:49 +1300 (Sat, 02 Oct 2010) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1182277 | scripty | 2010-10-04 15:47:05 +1300 (Mon, 04 Oct 2010) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1182590 | scripty | 2010-10-05 16:23:16 +1300 (Tue, 05 Oct 2010) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1182817 | lueck | 2010-10-06 07:22:08 +1300 (Wed, 06 Oct 2010) | 1 line

documentation backport from trunk for 4.5.3
------------------------------------------------------------------------
r1183201 | lueck | 2010-10-07 03:07:01 +1300 (Thu, 07 Oct 2010) | 1 line

this is not in 4.5
------------------------------------------------------------------------
r1183274 | lueck | 2010-10-07 08:22:21 +1300 (Thu, 07 Oct 2010) | 1 line

backport for 4.5
------------------------------------------------------------------------
r1183342 | scripty | 2010-10-07 15:50:12 +1300 (Thu, 07 Oct 2010) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1183603 | scripty | 2010-10-08 15:57:46 +1300 (Fri, 08 Oct 2010) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1186403 | pino | 2010-10-16 21:41:03 +1300 (Sat, 16 Oct 2010) | 5 lines

fix default value for a QString parameter

BUG: 254321
FIXED-IN: 4.5.3

------------------------------------------------------------------------
r1187573 | weidendo | 2010-10-20 09:22:04 +1300 (Wed, 20 Oct 2010) | 1 line

Backport r1187154: Fix crash in source annotation
------------------------------------------------------------------------
r1187574 | weidendo | 2010-10-20 09:22:05 +1300 (Wed, 20 Oct 2010) | 1 line

Backport r1187542: Increase maximal number of event counters per cost item.
------------------------------------------------------------------------
r1188290 | kkofler | 2010-10-22 11:26:28 +1300 (Fri, 22 Oct 2010) | 1 line

Mark kompare.desktop executable. (Backport of the Kompare portions of revisions 1182578 (dfaure) and 1187509 (mlaurent) from trunk.)
------------------------------------------------------------------------
r1188292 | kkofler | 2010-10-22 11:32:56 +1300 (Fri, 22 Oct 2010) | 8 lines

remove whitespace after -I, this will cause diff to parse the whitespace as the parameter instead of the actual regexp. spotted by thesteo82
Merge revision 1102932 by je4d from 3_way_kompare.

(This has been sitting in the branch for 7 months! Next time, if you fix something in a work branch, please also commit the fix to the trunk and the current release branch. Thanks in advance.)

Backport revision 1188291 from trunk.

CCMAIL: jeff@caffeinated.me.uk
------------------------------------------------------------------------
r1189416 | scripty | 2010-10-25 15:43:35 +1300 (Mon, 25 Oct 2010) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
