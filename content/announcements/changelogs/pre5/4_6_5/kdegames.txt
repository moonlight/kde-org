------------------------------------------------------------------------
r1237709 | scripty | 2011-06-21 03:56:09 +1200 (Tue, 21 Jun 2011) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1238772 | scripty | 2011-06-29 15:29:16 +1200 (Wed, 29 Jun 2011) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1238793 | schwarzer | 2011-06-30 03:00:24 +1200 (Thu, 30 Jun 2011) | 12 lines

Fix initial label bug.

(port to 4.6 branch)

The initial text was not removed when pausing the game and
then remained in place when unpausing and starting the game.

This should be done cleaner at some point, e.g. by creating
a distinctive state for INITIAL that does not allow pausing.

CCBUG: 267865

------------------------------------------------------------------------
r1238796 | schwarzer | 2011-06-30 03:04:07 +1200 (Thu, 30 Jun 2011) | 2 lines

Bump Kapman version.

------------------------------------------------------------------------
