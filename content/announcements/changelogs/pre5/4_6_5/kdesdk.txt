------------------------------------------------------------------------
r1238190 | kkofler | 2011-06-24 15:27:12 +1200 (Fri, 24 Jun 2011) | 6 lines

Set sort order

Backport revision 1238160 by cfeck from trunk.

BUG: 269299
FIXED-IN: 4.6.5
------------------------------------------------------------------------
r1238191 | scripty | 2011-06-24 15:41:09 +1200 (Fri, 24 Jun 2011) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1238249 | kossebau | 2011-06-25 08:17:55 +1200 (Sat, 25 Jun 2011) | 1 line

bump patchlevel version to 0.6.5
------------------------------------------------------------------------
