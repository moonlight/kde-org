------------------------------------------------------------------------
r1055340 | dfaure | 2009-11-27 21:02:45 +0000 (Fri, 27 Nov 2009) | 2 lines

Qt-4.6 fix: ensure url is really invalid

------------------------------------------------------------------------
r1055351 | dfaure | 2009-11-27 21:18:36 +0000 (Fri, 27 Nov 2009) | 2 lines

backport the Qt-4.6 fixes

------------------------------------------------------------------------
r1055386 | dfaure | 2009-11-27 21:58:14 +0000 (Fri, 27 Nov 2009) | 4 lines

Backport: fix crash when killing a job while a rename or skip dialog for it is shown.
Fixed for: 4.3.4
CCBUG: 192976

------------------------------------------------------------------------
r1055400 | dfaure | 2009-11-27 22:30:59 +0000 (Fri, 27 Nov 2009) | 4 lines

kservicetest often failed, showing that kbuildsycoca4 didn't manage to really send the dbus signal
notifyDatabaseChanged before quitting (seems that libdbus sometimes queues up stuff to be sent).
With this line added, it passes 100% of the time, so this seems good now.

------------------------------------------------------------------------
r1055404 | dfaure | 2009-11-27 22:44:30 +0000 (Fri, 27 Nov 2009) | 3 lines

Backport r1040221:
Repair unittest after KGlobalSettings API change (sigh)

------------------------------------------------------------------------
r1055431 | dfaure | 2009-11-28 01:29:43 +0000 (Sat, 28 Nov 2009) | 3 lines

Initialize more variables to fix a valgrind warning about writing out uninitialized data to ksycoca
(we were indeed, but then in a later phase we are overwriting it with the proper offset anyway)

------------------------------------------------------------------------
r1055788 | jacopods | 2009-11-28 18:25:53 +0000 (Sat, 28 Nov 2009) | 5 lines

Save the frame with the proper key (i.e. the same we are going to check afterwards); 
without this patch we *always* get a cache miss.
This should improve performance quite a lot


------------------------------------------------------------------------
r1055856 | dfaure | 2009-11-28 21:39:00 +0000 (Sat, 28 Nov 2009) | 7 lines

Fix wrong initial focus in CategorySelectDialog -- made me lose a few seconds for each event I created :-)

Apparently the re-creation of the layout in KDialog loses the focuswidget; so fixing the bug there,
and removing the enterEvent workaround in CategorySelectDialog itself (IMHO, better have correct focus
upfront, without moving the mouse into the widget).
CCMAIL: winter@kde.org

------------------------------------------------------------------------
r1056317 | osterfeld | 2009-11-29 20:49:29 +0000 (Sun, 29 Nov 2009) | 6 lines

don't kill and emit slaveDied() again if already marked as dead. If clients show a dialog e.g. after error/slaveDied is emitted,
the timeout might occur while the dialog is shown, causing slaveDied() to be emitted twice. That results in an extra deref(), which then 
causes the slave to be deleted while the dialog is shown. Return from dialog, local deref() (e.g. in gotInput() ) => crash
Might fix 191589
CCBUG:191589

------------------------------------------------------------------------
r1057219 | winterz | 2009-12-01 18:41:17 +0000 (Tue, 01 Dec 2009) | 8 lines

merge r1057081 | tmcguire | 2009-12-01 07:53:13 -0500 (Tue, 01 Dec 2009)

Make empty directories work in KZip and add a unit test for this.

This is essentially a forward port of 1057062 from the 3.5 branch.



------------------------------------------------------------------------
r1057220 | winterz | 2009-12-01 18:42:36 +0000 (Tue, 01 Dec 2009) | 4 lines

merge SVN commit 1057093 by tmcguire:

Make sure dirs are always written with S_IFDIR permissions, even if the caller forgets that.

------------------------------------------------------------------------
r1057321 | dfaure | 2009-12-02 00:07:59 +0000 (Wed, 02 Dec 2009) | 2 lines

Backport crash fix for 202871.

------------------------------------------------------------------------
r1060847 | scripty | 2009-12-10 04:15:35 +0000 (Thu, 10 Dec 2009) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1061442 | adawit | 2009-12-11 18:10:33 +0000 (Fri, 11 Dec 2009) | 1 line

Backport fixes for BR# 61235 and 215736
------------------------------------------------------------------------
r1061510 | adawit | 2009-12-11 22:27:22 +0000 (Fri, 11 Dec 2009) | 1 line

Backport the fix for BR# 205088
------------------------------------------------------------------------
r1062108 | adawit | 2009-12-13 20:18:42 +0000 (Sun, 13 Dec 2009) | 2 lines

Backport fix for BR# 209573...

------------------------------------------------------------------------
r1062412 | adawit | 2009-12-14 17:30:19 +0000 (Mon, 14 Dec 2009) | 1 line

Backport the fix for the Y2K38 problem with the cookiejar's expiration date variable
------------------------------------------------------------------------
r1062542 | scripty | 2009-12-15 04:02:03 +0000 (Tue, 15 Dec 2009) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1062732 | habacker | 2009-12-15 20:18:57 +0000 (Tue, 15 Dec 2009) | 1 line

backported 1050665 win32 fix: made path() implementation similar to unix implementation in case of local files
------------------------------------------------------------------------
r1063377 | adawit | 2009-12-17 23:18:47 +0000 (Thu, 17 Dec 2009) | 1 line

Backport the #define cleanup from the trunk
------------------------------------------------------------------------
r1063613 | adawit | 2009-12-18 22:15:26 +0000 (Fri, 18 Dec 2009) | 1 line

Backported the cookie expiration date parsing fix
------------------------------------------------------------------------
r1064068 | scripty | 2009-12-20 04:10:33 +0000 (Sun, 20 Dec 2009) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1064709 | kossebau | 2009-12-21 14:20:56 +0000 (Mon, 21 Dec 2009) | 1 line

backport of 1064708: added: missing CamelCase include for KBookmarkDialog
------------------------------------------------------------------------
r1065965 | scripty | 2009-12-25 04:05:15 +0000 (Fri, 25 Dec 2009) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1066854 | scripty | 2009-12-28 04:20:28 +0000 (Mon, 28 Dec 2009) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1067168 | scripty | 2009-12-29 04:15:36 +0000 (Tue, 29 Dec 2009) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1067209 | zwabel | 2009-12-29 08:46:23 +0000 (Tue, 29 Dec 2009) | 3 lines

Backport r1067208:
Fix broken condition that could lead to an assertion

------------------------------------------------------------------------
r1068586 | lunakl | 2010-01-01 16:41:01 +0000 (Fri, 01 Jan 2010) | 4 lines

backport r1068585
(no hiding of the popup when the toplevel resizes harmlessly)


------------------------------------------------------------------------
r1069227 | dhaumann | 2010-01-03 00:32:30 +0000 (Sun, 03 Jan 2010) | 6 lines

backport SVN commit 1067616 by pletourn:

Fix printing when a line is split between 2 pages

CCBUG: 219598

------------------------------------------------------------------------
r1069524 | lunakl | 2010-01-03 16:33:55 +0000 (Sun, 03 Jan 2010) | 5 lines

backport r1069523:
Movement of a toplevel window of course matters only
if it is not some other random window.


------------------------------------------------------------------------
r1071954 | scripty | 2010-01-09 04:16:35 +0000 (Sat, 09 Jan 2010) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1072373 | scripty | 2010-01-10 04:07:20 +0000 (Sun, 10 Jan 2010) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1073716 | dfaure | 2010-01-12 18:33:21 +0000 (Tue, 12 Jan 2010) | 3 lines

Backport 1073715: Add the possibility to specify a working directory in KRun::runCommand, and make it
default to documentPath for consistency with apps started via kdeinit. To help with bug 183534.

------------------------------------------------------------------------
r1073752 | dfaure | 2010-01-12 20:11:12 +0000 (Tue, 12 Jan 2010) | 2 lines

revert 1073716, wrong branch. Thanks to Andre Woebbeking for noticing.

------------------------------------------------------------------------
r1074978 | kossebau | 2010-01-15 03:20:41 +0000 (Fri, 15 Jan 2010) | 2 lines

backport of 1074976: fixed: KIconDialog could call hasContext() before themes have been init'ed, so ensure they are also in hasContext()

------------------------------------------------------------------------
r1075174 | freininghaus | 2010-01-15 16:19:28 +0000 (Fri, 15 Jan 2010) | 9 lines

Backport commit 1062832 to the 4.3 branch:

Do not check the storage service via DBus anymore since the latter is not
thread-safe.

Fix will be in KDE 4.3.5.

CCBUG: 208921

------------------------------------------------------------------------
r1075213 | kossebau | 2010-01-15 17:22:11 +0000 (Fri, 15 Jan 2010) | 2 lines

backport of 1075209: fixed: user bookmarks are browser bookmarks, so setup the editor accordingly

------------------------------------------------------------------------
r1075313 | freininghaus | 2010-01-15 19:59:46 +0000 (Fri, 15 Jan 2010) | 12 lines

Backport of commit 1055748 to the 4.3 branch:

If items are removed from the dir model, remove them also from
KFilePreviewGenerator's list of recently changed items. This should
fix crashes that occur when a file item becomes invalid just after it
is changed, e.g., if a large file is moved to a volume which is then
unmounted immediately.

Fix will be in KDE 4.3.5.

CCBUG: 211525

------------------------------------------------------------------------
r1075749 | adawit | 2010-01-16 17:08:22 +0000 (Sat, 16 Jan 2010) | 1 line

Backported the send correct redirection code up the chain fix
------------------------------------------------------------------------
r1076301 | sebsauer | 2010-01-17 23:32:44 +0000 (Sun, 17 Jan 2010) | 5 lines

backport r1048522 by kuemmel from trunk to 4.3 branch;
don't crash
BUG:211401


------------------------------------------------------------------------
r1076365 | scripty | 2010-01-18 04:07:40 +0000 (Mon, 18 Jan 2010) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1076755 | mueller | 2010-01-18 20:39:31 +0000 (Mon, 18 Jan 2010) | 2 lines

bump version

------------------------------------------------------------------------
r1076881 | sengels | 2010-01-19 01:03:27 +0000 (Tue, 19 Jan 2010) | 3 lines

fix bug on behalf of SadEagle
BUG:223146

------------------------------------------------------------------------
r1077629 | mwolff | 2010-01-20 14:46:16 +0000 (Wed, 20 Jan 2010) | 2 lines

backport: only expand forwards, fixes crash when the group size was bigger than the default group size and there was no following group available.

------------------------------------------------------------------------
r1077737 | mwolff | 2010-01-20 19:41:35 +0000 (Wed, 20 Jan 2010) | 6 lines

backport r1064050 by pletourn:

Prevent crash when folding

CCBUG: 200858

------------------------------------------------------------------------
r1078132 | dfaure | 2010-01-21 16:15:25 +0000 (Thu, 21 Jan 2010) | 4 lines

Backport fixes for 213799 and 219547: Fix lack of auto-update when viewing a directory via a symlink.
Fixed for: 4.3.5
CCBUG: 213799 219547

------------------------------------------------------------------------
r1078133 | dfaure | 2010-01-21 16:16:59 +0000 (Thu, 21 Jan 2010) | 4 lines

Backport r1065898: Fix inotify regression with linux kernel 2.6.31/32.
Fixed for: 4.3.5
CCBUG: 207361

------------------------------------------------------------------------
r1078134 | dfaure | 2010-01-21 16:18:10 +0000 (Thu, 21 Jan 2010) | 4 lines

Backport 1063293: Ignore proxy urls that don't have a protocol, fixes FTP not working on OpenSuse due to an old kioslaverc file.
Fixed for: 4.3.5
CCBUG: 214896

------------------------------------------------------------------------
r1078135 | dfaure | 2010-01-21 16:18:57 +0000 (Thu, 21 Jan 2010) | 4 lines

Backport 1062999: Fix crash after an error happens in a TransferJob.
Fixed for: 4.3.5
CCBUG: 214100 202091 197289 180791

------------------------------------------------------------------------
r1078174 | dfaure | 2010-01-21 17:39:06 +0000 (Thu, 21 Jan 2010) | 8 lines

Backporting all remaining fixes from 4.4 to 4.3 branch; (in fact ended up copying the files
after backporting all and checking up the remaining diff).

This backports the fixes for the bugs below to the upcoming 4.3.5 release.
Svn revisions backported: 1061529, 1062515, 1065297, 1065291, 1071530, 1061513
Fixed for: 4.3.5
CCBUG: 190535 219547

------------------------------------------------------------------------
r1078187 | dfaure | 2010-01-21 18:20:58 +0000 (Thu, 21 Jan 2010) | 4 lines

Backport 1048570: Don't let the tabbar "buttons" (in konq's sidebar) take focus, it's confusing (hidden) and annoying (many <tab> keypresses to get out).
Fixed for: 4.3.5
CCBUG: 45557

------------------------------------------------------------------------
