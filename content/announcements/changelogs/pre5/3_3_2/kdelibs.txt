Dir: kdelibs
----------------------------
new version numbers
----------------------------
new version number
----------------------------
Declare UTF-8 as encoding
CCBUG: 90023



Dir: kdelibs/dcop/client
----------------------------
Fix problem with the DCOP client. See
http://lists.kde.org/?l=kde-devel&m=109964773413225&w=2 for more information.



Dir: kdelibs/kabc
----------------------------
Backport real fix for 87827. (Don't mess with user supplied QByteArray in endLDIF())
CCMAIL: 87827-done at bugs.kde.org



Dir: kdelibs/kabc/plugins/ldapkio
----------------------------
Backport 2 fixes: Reset the URL on init() and clear the addressees on reload.



Dir: kdelibs/kate/data
----------------------------
* Much better string support:
  - quoted constructs now allows space/newline between q[qxrw]? and the delimiter, including comments on the first line.
  - support for single quoted patterns (m'', s'', q[rx]'')
  - improved HERE document support, also respects single quote delimiters.
* String delimiters are now allways rendered as Operator, so that the string data is distinct. this includes all quoted constructs, '', "", q[qrxw]//, m//, s/// in all variants. (You may wish to change the style for Operator, please let me know if you have a good idea for this).
* REPLACEMENT is now rendered as a [interpolated] string.
* HERE documents is rendered either as a interpolated string or bare text.
* About all foldable strings are folded: PATTERNs, REPLACEMENTs, strings, word lists etc, HERE documents in addition to code blocks. Missing is still parethesed "blocks".
* Much better variable support in strings and PATTERNs, special variables are now supported except a few ($[}\]>'] + in addition to those not supported by perl).
* The REPLACEMENT in s{} {} is now recursively parsed, so that "s{PATTERN} { { } { { } } }cigmosx" will not break.
* Hash keys are now code. That means that unquoted hash keys ("$hash{key}") appears as barewords, but otoh complex keys are allways correct, and code folding is not broken.
* Last but not least, FASTER! The number of checks in the base context is now less than half of before, and in general regex usage is minimized.

The new versions should allready be available in the highlight download dialog in the editor configuration.
Do not hesitate to get and test this, and mail me/contact me in #kate/file more bug reports if you experience problems.
BUG: 83715
BUG: 79257
CCBUG: 92613
CCBUG 91603
----------------------------
remove attention list and use Alerts list instead.
----------------------------
* missing keywords from Gavrila, entered by Cullmann
* use insensitive flag in regexps instead of [Xx] characteer classes (67 instances!)
----------------------------
update ruby highlighting, thanks to Thibauld Favre.
new features are code folding and some minor changes.
----------------------------
backport



Dir: kdelibs/kate/part
----------------------------
Gotoline should clear selection, unless it is persistent
CCBUG: 92894
----------------------------
revert, forgot about message freeze :(
----------------------------
Backport for cullmann: drag'n'drop text in one step.
CCBUG: 64459
----------------------------
Backport fix for 90681
BUG: 90681
----------------------------
Dont use textmode. It's not suitable for many common text encodings, so we won't use it again.
If it has to be enabled, it must be settable on a pr transaction bases, in kio?
BUG: 86851
----------------------------
Backport memory leak fix for cullmann
CCBUG: 91860
----------------------------
fix bug #93484.
CCBUG: 93484
----------------------------
This commit fixes the problem of setting wrong microFocusHint
for ASIAN input method. Current code sets it at the cursor pos,
but it should be at the start pos of selected preedit.
Please have a look at the comment below.

  // Cursor placement code is changed for Asian input method that
  // shows candidate window. This behavior is same as Qt/E 2.3.7
  // which supports Asian input methods. Asian input methods need
  // start point of IM selection text to place candidate window as
  // adjacent to the selection text.
----------------------------
Drawing the underline below the "preedit".
"preedit" is the string which is composed by InputMethod, but not
committed yet.
----------------------------
add the "%s" (sed replace in full document) command



Dir: kdelibs/kate/plugins/wordcompletion
----------------------------
Backport, make complete() work with TABs in the line



Dir: kdelibs/kdecore
----------------------------
Add docu on control module restrictions
----------------------------
Backport names for MacRoman charsets used on the Web.
(Codepage 10000 is probably not used as such on the Web, so I have not
backported it.)
----------------------------
just because an OS has getservbyname_r doesn't imply it has getservbyport_r. (backport revision 1.44)
----------------------------
Backport fix of KConfigGroup::deleteGroup



Dir: kdelibs/kdecore/Attic
----------------------------
backport.. in case somebody tries to compile HEAD code against BRANCH
kdelibs



Dir: kdelibs/kdecore
----------------------------
Backport r1.137 (#90944).
----------------------------
KIOSK: Provide some information when rejecting execution of a .desktop file
----------------------------
backport: look harder for posix shell.
----------------------------
Revert
----------------------------
Backport r1.60.
----------------------------
Backport part of #42609.
----------------------------
new version number



Dir: kdelibs/kdecore/malloc
----------------------------
Backport.



Dir: kdelibs/kdecore/network
----------------------------
(Backport 1.37:1.38) Cache the end iterator.
----------------------------
(Backport 1.8:1.9) NoError is not a fatal error.
----------------------------
(Merge 1.7:1.8) Backporting forgotten bugfix. Sorry about forgetting
this for so long.
----------------------------
Backport of the fix that went into r1.31. See Bug #86271 for
information.

BUG: 86271
----------------------------
(Merge 1.4:1.5) Backporting the forgotten change.
----------------------------
Checking in the differences between 1.26 and 1.28: kiobuffer.h was forgotten



Dir: kdelibs/kdefx
----------------------------
fix undefined behaviour that could cause weird artefacts with optimizing compilers (backport revision 1.55)



Dir: kdelibs/kdeprint
----------------------------
better layout



Dir: kdelibs/kdeui
----------------------------
Backport r1.144.
----------------------------
Added missing "still hovered?" check (#90376)
----------------------------
What's right for KTextEdit must not be right for KLineEdit which keeps all
context menu entries when it's disabled.
----------------------------
Don't use an uninitialized var (mCurrent) if there are no tips.
----------------------------
uptodate package list
----------------------------
#ifdef __GNUC__
----------------------------
added moc and lo-files
----------------------------
Changes for aspell-0.6: process 3-letter ISO-codes and unclutter the language list. This solves the most annoying problems from bug 89830.
----------------------------
Make as-you-type spell checking work with aspell.

Also replace some QTimer::singleShot calls with a standard QTimer, this
probably won't change the semantics of the code but I'm paranoid about
a race condition causing the slot associated with the timer to be called
multiple times (erroneously).

I checked to make sure that I didn't break normal spell checking.

Basically the problem is that aspell has a habit of emitting spurious
white space to stdout. This breaks the inter process
communication (IPC) between kspell.cpp and the spell process.

The IPC between kspell.cpp and the spell process is fragile. kspell.cpp
uses a pair of macros OUTPUT and NOOUTPUT to hook and unhook the
readReady signal from the spell process to a slot in kspell.cpp. This is
fragile because if the spell process emits data to stdout when the
readReady signal is not hooked up to any slot (which happens due to
the spurious emissions by aspell) then the KProcIO instance
managing the spell process will emit a readyRead signal and then not
emit a further readyRead signal until the data that is ready for reading
is read by kspell.cpp. Unfortunately due to the fragile logic of kspell.cpp
this will never happen (because the emission occurred outside of a
OUTPUT/NOOUTPUT connect/disconnect pair).

Thus before kspell.cpp pipes a word into the stdin of the spell process for
checking it must flush any data that has been written to stdout by the
spell process and buffered by KProcIO.

BUG: 92016
----------------------------
Don't stop the loop when finding a comment. Reviewed by David Faure
----------------------------
Fixed keyboard navigation in the inputdialogs which show a listbox: give
the listbox focus, and make Return accept the dialog.



Dir: kdelibs/kdoctools/customization/fr
----------------------------
added entity



Dir: kdelibs/kdoctools/customization/fr/entities
----------------------------
translated in french



Dir: kdelibs/kdoctools/customization/nl
----------------------------
Backport from head



Dir: kdelibs/khtml
----------------------------
more backported $Id: kdelibs.txt 369537 2004-12-09 23:30:54Z waba $ removals
----------------------------
backport crash fix (#93059)
----------------------------
backporting spelling fix so scripty can catch it
----------------------------
something went wrong in the patch logic. But the check was supposed to
end up in openWallet not in saveWallet
----------------------------
1:1 copy of HEAD - only change is to revert keyboardMouseState -> keyboardModifiers
(not in branch). Specific reverts of HEAD features I will do after this to allow
peer review



Dir: kdelibs/khtml/css
----------------------------
backport floats improvements (#92979, #93511, #89527)
even the ChangeLog says so :)
----------------------------
more backported $Id: kdelibs.txt 369537 2004-12-09 23:30:54Z waba $ removals
----------------------------
Color 'http://foo' links as visited if 'http://foo/' is in history (#75771)
----------------------------
removing $Id: kdelibs.txt 369537 2004-12-09 23:30:54Z waba $ tags in the branch too
----------------------------
1:1 copy of HEAD - only change is to revert keyboardMouseState -> keyboardModifiers
(not in branch). Specific reverts of HEAD features I will do after this to allow
peer review



Dir: kdelibs/khtml/dom
----------------------------
more backported $Id: kdelibs.txt 369537 2004-12-09 23:30:54Z waba $ removals
----------------------------
removing $Id: kdelibs.txt 369537 2004-12-09 23:30:54Z waba $ tags in the branch too
----------------------------
1:1 copy of HEAD - only change is to revert keyboardMouseState -> keyboardModifiers
(not in branch). Specific reverts of HEAD features I will do after this to allow
peer review



Dir: kdelibs/khtml/ecma
----------------------------
backported cvs diff -r last_3_3_merge xmlhttprequest.cpp. Mostly David's:
>Fix the crash at the right place - job must be set to 0 before
>calling changeState, and "job" must be the member variable, not the argument
>of the method!
----------------------------
backported George's fix:
>the comment was right, this code was wrong.  Tested in IE and Mozilla, we now
>behave as they do.  The result: JS popups are now generally without scrollbars.
----------------------------
No empty hashtable, it breaks some compilers.
BUG: 93842
----------------------------
1:1 copy of HEAD - only change is to revert keyboardMouseState -> keyboardModifiers
(not in branch). Specific reverts of HEAD features I will do after this to allow
peer review
----------------------------
Disable document.layer
----------------------------
just minimizing the diff: "Smallish fixes."



Dir: kdelibs/khtml/html
----------------------------
more backported $Id: kdelibs.txt 369537 2004-12-09 23:30:54Z waba $ removals
----------------------------
Backport my fix for the layer-induced regression.
BUG: 93786
----------------------------
removing $Id: kdelibs.txt 369537 2004-12-09 23:30:54Z waba $ tags in the branch too
----------------------------
1:1 copy of HEAD - only change is to revert keyboardMouseState -> keyboardModifiers
(not in branch). Specific reverts of HEAD features I will do after this to allow
peer review
----------------------------
backport crash fix



Dir: kdelibs/khtml/java
----------------------------
removing $Id: kdelibs.txt 369537 2004-12-09 23:30:54Z waba $ tags in the branch too
----------------------------
1:1 copy of HEAD - only change is to revert keyboardMouseState -> keyboardModifiers
(not in branch). Specific reverts of HEAD features I will do after this to allow
peer review
----------------------------
Backport Check if connection is closed  in flushBuffers
----------------------------
Backport internal classes access restrictions



Dir: kdelibs/khtml/java/org/kde/javascript
----------------------------
Backport internal classes access restrictions



Dir: kdelibs/khtml/java/org/kde/kjas/server
----------------------------
Backport internal classes access restrictions



Dir: kdelibs/khtml/misc
----------------------------
more backported $Id: kdelibs.txt 369537 2004-12-09 23:30:54Z waba $ removals
----------------------------
removing $Id: kdelibs.txt 369537 2004-12-09 23:30:54Z waba $ tags in the branch too
----------------------------
1:1 copy of HEAD - only change is to revert keyboardMouseState -> keyboardModifiers
(not in branch). Specific reverts of HEAD features I will do after this to allow
peer review



Dir: kdelibs/khtml/rendering
----------------------------
backport floats improvements (#92979, #93511, #89527)
even the ChangeLog says so :)
----------------------------
more backported $Id: kdelibs.txt 369537 2004-12-09 23:30:54Z waba $ removals
----------------------------
backport latest changes and merges.
Mutually reviewed and tested by Allan and me.
----------------------------
Belongs with the table-fix
----------------------------
removing $Id: kdelibs.txt 369537 2004-12-09 23:30:54Z waba $ tags in the branch too
----------------------------
1:1 copy of HEAD - only change is to revert keyboardMouseState -> keyboardModifiers
(not in branch). Specific reverts of HEAD features I will do after this to allow
peer review
----------------------------
backport containingBlock crash fix
----------------------------
backport "css overflow:scroll/auto: scrollbars cover contents of element" fix (#89136)
----------------------------
backporting fix for 93193
----------------------------
And now it even compiles..
oops



Dir: kdelibs/khtml/test
----------------------------
removing $Id: kdelibs.txt 369537 2004-12-09 23:30:54Z waba $ tags in the branch too



Dir: kdelibs/khtml/xml
----------------------------
more backported $Id: kdelibs.txt 369537 2004-12-09 23:30:54Z waba $ removals
----------------------------
removing $Id: kdelibs.txt 369537 2004-12-09 23:30:54Z waba $ tags in the branch too
----------------------------
1:1 copy of HEAD - only change is to revert keyboardMouseState -> keyboardModifiers
(not in branch). Specific reverts of HEAD features I will do after this to allow
peer review



Dir: kdelibs/kinit
----------------------------
backport HEAD
----------------------------
implement suggestion by Waldo: add [kdeinit] behind the process name (backport revision 1.12)



Dir: kdelibs/kio/bookmarks
----------------------------
Don't crash when dragging around a separator
Not perfect but at least it no longer crashes

BUG: 83436



Dir: kdelibs/kio/kfile
----------------------------
Backport avoid crashing: unset the m_lastFoundURL if an item is deleted with the same URL.
----------------------------
KIOSK: Make it possible to disable the file sharing tab
----------------------------
layout fixes



Dir: kdelibs/kio/kio
----------------------------
The condition isn't complex after all, we have a bool for this already :)
Patch tested by JRT on kde-devel
----------------------------
fixed crash in data slave by not trying to delete it early

BUG: 92170
----------------------------
Prevent recursion somewhat/guard against unexpected recursion side-effects
(Backport of r1.78)
----------------------------
Update documentation.
----------------------------
Fixed EmptyIcon support for the case where "." ".." and ".directory" are not
returned in that order by readdir() (e.g. with ext3's hashed b-tree indexes enabled).
Thanks to Walt H for confirming my suspicions and testing the patch.
BUG: 79826
----------------------------
Show "skip/auto skip/overwrite all" when moving a bunch of files into the same
partition, and also for the last file when copying files.
Factorized some code.

No BR number, but see thread "Intended behavior of File \"Already Exists\" dialog box"
on kde-devel.



Dir: kdelibs/kio/kpasswdserver
----------------------------
Don't store password in kpasswdserver "for ever" (i.e. even after closing the window)
when kwallet is being used (which is a safer place). This allows to close all
sensitive windows and close the wallet, before leaving your computer. Previously
it would still be possible to reopen konq on password-protected sites, since kpasswdserver
would supply the password. Many thanks to Ferdinand Gassauer for his patience in
explaining the matter to me :)
BUG: 92928



Dir: kdelibs/kio/misc/kwalletd
----------------------------
backport waldo's fix (thanks!)
----------------------------
backport the relevant fixes from HEAD.  Please backup your wallets and test!



Dir: kdelibs/kjs
----------------------------
Assert the validity of the value after taking care of the exception
BUG: 81719
----------------------------
Harri said I should backport all of HEAD for kjs too



Dir: kdelibs/knewstuff
----------------------------
- backport segfault fix: do not delete KGlobal::config() object



Dir: kdelibs/kstyles/keramik
----------------------------
backport: the ProgressBar animation timer should not run when the widget isn't visible
----------------------------
backport: do not stop all animations just because one progressbar gets hidden



Dir: kdelibs/kwallet/backend
----------------------------
backport kwallet changes
----------------------------
backport fix for keyDoesNotExist and folderDoesNotExist



Dir: kdelibs/kwallet/client
----------------------------
backport kwallet changes



Dir: kdelibs/kwallet/tests
----------------------------
backport kwallet changes



Dir: kdelibs/mimetypes/application
----------------------------
preparing khtml backport. These are just aliases - no new strings
