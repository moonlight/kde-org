------------------------------------------------------------------------
r1226871 | arnorehn | 2011-04-03 04:33:49 +1200 (Sun, 03 Apr 2011) | 3 lines

the smokegen doesn't like compiler specific deprecation flags

BUG: 269943
------------------------------------------------------------------------
r1226886 | lbeltrame | 2011-04-03 07:22:22 +1200 (Sun, 03 Apr 2011) | 3 lines

Fix pykdeuic4 to work with the newest PyQt (again...).


------------------------------------------------------------------------
r1226887 | lbeltrame | 2011-04-03 07:24:52 +1200 (Sun, 03 Apr 2011) | 2 lines

Slight adjustments to spacing, makes the code a little more readable

------------------------------------------------------------------------
r1228245 | lbeltrame | 2011-04-16 20:27:12 +1200 (Sat, 16 Apr 2011) | 6 lines

Unbreak Python Phonon bindings. Now it is possible to use a VideoWidget again with a MediaSource object, which would throw a TypeError earlier (this was due to a missing inheritance of a class). My testing confirms that it now works.
Original patch by Carlos Corbacho (with minimal changes on my part). Thanks!

BUG: 188315


------------------------------------------------------------------------
