---
aliases:
- ../changelog3_0_5to3_1
hidden: true
title: KDE 3.0.5 to KDE 3.1.0 Changelog
---

<p>
This page tries to present as much as possible the additions
and corrections that occured in KDE between the 3.0.5 and 3.1.0 releases.
</p>

<h3>General</h3><ul>
  <li>New default widget style: Keramik</li>
  <li>New default icon theme: Crystal</li>

  <li>Created and enhanced framework to effectively disable certain features
  of the GUI, like for example shell access, disallowing to select
  custom wall papers. "Kiosk mode"
  </li>
  <li>Usability improvements:
    <ul>
      <li>Kicker: Context menu, applet menus, wording and panel feature consistency improved</li>
      <li>Kicker: Redesigned control module.</li>
      <li>KCalc: Usability Improvements.</li>
      <li>KSnapshot: Usability Improvements.</li>
      <li>KsCD: User Interface Improvements in configuration and information dialogs</li>
    </ul>
  </li>
</ul>

<!--
<h3>arts</h3><ul>
</ul>
-->

<h3>kdelibs</h3><ul>
<li>Extended the DCOP commandline client to
  <ul>
    <li>enable nonGUI applications (such as shell scripts) to use DCOP</li>
    <li>send DCOP messages to multiple users and/or sessions concurrently</li>
  </ul>
</li>
<li>Added to notification dialog to allow applications to configure their
  notifications with a common GUI.</li>
<li>Added "Open File", "Open Destination" and "Keep window open" to download
progress dialog.</li>
<li>KDatePicker has now week number display and selection.</li>
<li>Added KLocale support for setting/retrieving the first day of a week</li>
<li>Support for "Multimedia keys" on your keyboard.</li>
<li>Added on-demand icon loading support to improve startup performance of applications.</li>
<li>KHTML:
  <ul>
    <li>Improvements to printing support.</li>
    <li>Page Info and Frame info dialog.</li>
    <li>Auto-Scroll support of webpages using Shift combined with arrow keys.</li>
    <li>Many, many Bugfixes and improvements.</li>
    <li>Small codebase merges with Safari. The main merge is planned for a
    future release however, as it requires more testing.</li>
  </ul>
</li>
<li>KIO:
  <ul>
    <li>Modified thumbnail ioslave that can extract a thumbnail from the file
    directly if it contains one, rather than generating one itself.</li>
    <li>Added fish:// protocol slave, which can access remote filesystems
    via SSH and SCP</li>
    <li>Cleanups and Corrections in smtp:// protocol slave.</li>
    <li>Activated rtsp:// protocol slave.</li>
    <li>Added support for Public Key Authentication and interopability with newer
    versions of OpenSSH to sftp:// protocol slave.</li>
  </ul>
</li>
<li>KFile plugins:
  <ul>
     <li><b>jpeg</b>: From the GUI or the command line, able to display &amp; set jpeg comments, and show digital camera exif (extended information) data.  Who knows, one day it may even play audio files embedded inside images.  Frank Pieczynski &lt;pieczy at knuut dot de&gt; and Bryce (bryce2 at obviously.com).</li>
<li><b>diff</b>: Display some basic info about a diff file, such as number of lines changed, added, removed, format of the diff, etc. Otto Bruggeman &lt;bruggie at bruggie dot dnsalias dot org&gt;</li>
<li><b>c++</b>: Get some stats like lines of code and comment and the number of included files, Rolf Magnus &lt;ramagnus at kde dot org&gt;</li>
<li><b>rpm</b>: Information like package name and version, Laurence Anderson &lt;l.d.anderson at warwick dot ac dot uk&gt;</li>
<li><b>deb</b>: Information like package name and version, Laurence Anderson &lt;l.d.anderson at warwick dot ac dot uk&gt;</li>
<li><b>ico</b>: Display # of icons, resolution and # colors of 1st icon, Shane Wright &lt;me at shanewright.co.uk&gt;</li>
<li><b>targa</b>: Display resolution, bit depth, and compression method, Shane Wright &lt;me at shanewright.co.uk&gt;</li>
<li><b>xbm</b>: Display resolution, Shane Wright &lt;me at shanewright.co.uk&gt;</li>
<li><b>bmp</b>: Display type, resolution, bit depth, and compression method, Shane Wright &lt;me at shanewright.co.uk&gt;</li>
<li><b>au</b>: Display length, sample rate, encoding, Shane Wright &lt;me at shanewright.co.uk&gt;</li>
<li><b>html</b>: Read title, doctype and metatags, and whether the file contains javascript, Rolf Magnus &lt;ramagnus at kde dot org&gt;</li>
<li><b>tiff</b>: Read the usual image information like resolution as well as the comments that are available, Nadeem Hasan &lt;nhasan at nadmm dot com&gt;</li>
<li><b>txt</b>: Create output similar to the wc command, showing things like number of words and lines, Nadeem Hasan &lt;nhasan at nadmm dot com&gt;</li>
<li><b>folder</b>: Displays number of files contained and space used (Simon MacMullen)</li>
<li><b>avi</b>: Displays resolution, length, fps and codecs used, Shane Wright &lt;me at shanewright.co.uk&gt;</li>
<li><b>message/rfc822</b>: Displays From, To, Subject and Content-Type lines, Shane Wright &lt;me at shanewright.co.uk&gt;</li>
</ul>
<li>KSSL:
  <ul>
    <li>SSL certificate policies can now be configured based on
        the host name</li>
  </ul>
</li>
</ul>

<h3>kdeaddons</h3><ul>
  <li><b>NEW IN KDE:</b> Noatun plugin to search for lyrics of the current song.</li>
</ul>

<h3>kdeadmin</h3><ul>
<li><b>kcmlinuz</b>: is now a standalone application</li>
<li><b>kcmlinuz</b>: Added capability to search for configuration options</li>
</ul>

<h3>kdeartwork</h3><ul>
</ul>

<h3>kdebase</h3><ul>
<li>KAppfinder: Allowing to configure which entries should be added to
the menu.</li>
<li>Kate:
  <ul>
    <li>The XML completion plugin pops up now automatically.</li>
    <li>Added an XML checker</li>
    <li>Added &quot;Dynamic Word Wrap&quot; (soft wrapping of the textlines at the window border)</li>
    <li>Add &quot;Code Folding&quot; (now you can collapse/expand for example functions on C++)</li>
    <li>Improved printing, which means: print line number, print with syntaxhighlighting, ...</li>
    <li>Fix XIM input, enhance Unicode support (sorry, bidi support still missing)</li>
    <li>Updated Plugin-Interface, to make sure to be BC with 3.2 from 3.1</li>
    <li>Static word wrap fixed (at least all known bugs solved)</li>
    <li>Optional new GUI mode: &quot;Modern Style&quot;</li>
  </ul>
<li>KControl:
  <ul>
    <li>Added control module for changing X11 resolution and configuring
    Gamma correction</li>
    <li>Fonts module: Extra Anti-Alias settings for excluding range, sub-pixel
    type.</li>
    <li>Font installer: Consistency improvements, removed install-from list,
    saves changes upon pressing the apply button. Allows fonts to be disabled,
    Thumbnail generator.</li>
  </ul>
</li>
<li>KDesktop:
  <ul>
    <li>Ability to merge custom menu with the desktop menu.</li>
    <li>Added configuration option GridXSpacing in kdeglobals to control
    the LineUp horizontal spacing.</li>
    <li>Added configuration option to activate Workspace-Switching using
    the Mouse wheel</li>
    <li>Look up desktop files when executing commands from the minicli.</li>
  </ul>
</li>
<li>KHelpCenter:
  <ul>
    <li>Integrated table of contents of application manuals into documentation
     tree view.</li>
     <li>Improved integration for documentation of KControl modules, Kicker
      applets, KInfoCenter modules and Konqueror plugins.</li>�
     <li>Cleaned up interface for plugging in third-party documentation.</li>
   </ul>
<li>Kicker:
  <ul>
    <li>Added possibility to replace the K-Menu with a custom menu</li>
    <li>New hiding mode: into background. Kicker bar unhiding can be triggered
    by moving the mouse into a corner of the desktop.</li>
    <li>Reorganized hiding options in the control module.</li>
    <li>Support for several clock applets running at the same time,
    each configurable to different time zones.</li>
    <li>Added a Menuextension for Konsole with full bookmark support.</li>
    <li>Added support for mouse-over animation, like in Konqueror and KDesktop.</li>
    <li>Display number of containing tasks in a taskbar group</li>
  </ul>
</li>
<li>Klipper: Optionally allow synchronizing of the X11 Clipboard and the
  current mouse selection.</li>
<li>Konqueror:
  <ul>
    <li>Tabbed Browsing support by Doug Hanley</li>
    <li>Folder icons now reflect their contents</li>
    <li>Added possibility to enable / disable all previews at once</li>
    <li>Digital camera previews obey their orientation now.</li>
    <li>Added multiple selection support to the bookmark editor.</li>
    <li>Show 256x256 pixel previews of images in the iconview extended tooltips</li>
    <li>A Konqueror sidebar plugin for KFileMetaInfo</li>
    <li>A detailed list view for KFileMetaInfo</li>
    <li>Integrated searching for metadata and multimedia files in the
      "Find" dialog</li>
    <li>KFileMetaInfo plugins:
      <ul>
        <li><b>GIF</b>: Is able to set GIF comments, and to display GIF metadata</li>
        <li><b>desktop</b>: Shows information about desktop entry files</li>
        <li><b>vCard</b>: Displays name and email address</li>
      </ul>
    </li>
  </ul>
</li>
<li>Konsole:
  <ul>
    <li>Added &quot;cutToBeginningOfLine&quot; option.</li>
    <li>Made timeout for &quot;Monitor for Silence&quot; configurable.</li>
    <li>kfile-bookmarks menu integration (&quot;Add Bookmark&quot; on non-GNU/Linux only with &quot;\[\e]31;\w\a\]&quot; prompt).</li>
    <li>Start new shell at given bookmark (supports ssh://user@host and telnet://host like bookmarks).</li>
    <li>konsolepart uses BrowserExtension::openURLRequest() (only with &quot;\[\e]31;\w\a\]&quot; prompt).</li>
    <li>Session management saves initial or current (non-Linux only if set with &quot;\[\e]31;\w\a\]&quot; prompt) directory.</li>
    <li>Extended drag and drop popupmenu with &quot;cp&quot;, &quot;ln&quot; and &quot;mv&quot; entries.</li>
    <li>Session views are temporarily detachable from main window.</li>
    <li>Improved &quot;Find in History...&quot;: Regular expressions support, &quot;Find Next&quot;, &quot;Find Previous&quot;.</li>
    <li>Shortcuts are now configurable via graphical interface.</li>
    <li>Added shortcuts for session switch menu, switching to first 12 sessions and font size variation.</li>
    <li>Made Ctrl-S/Ctrl-Q flow control (Ctrl-S freezes shell) an option with default off.</li>
    <li>Parameters for keyboard and schema selection.</li>
    <li>&quot;Clear Terminal&quot; and &quot;Reset and Clear Terminal&quot; commands.</li>
    <li>Optionally prevent programs from changing the window size.</li>
    <li>Support for Unix98 tty devices.</li>
    <li>&quot;Copy&quot; menu entries and don't write to clipboard automatically.</li>
    <li>Addition of "random" background colours, and the "BlackOnLightColour" schema, Alexander Kellett &lt;lyp&#x61;n&#111;&#00118;&#064;&#x6b;&#100;&#0101;&#x2e;or&#x67;</li>
    <li>Allow to save current sessions as profile and start them with --profile parameter.</li>
    <li>Added "--noclose" parameter to not close Konsole when command exits.</li>
  </ul>
</li>
<li>KPager: bugfixes.</li>
<li>KWin:
  <ul>
  <li>Added a GUI for the "Desktops navigation wraps around" feature that was added in 3.0.</li>
  <li>Made minor key accelerators fixes.</li>
  <li>Keyboard navigation of the operations menu corrected.</li>
  <li>Fixed order of cascading for windows present on a desktop</li>
  <li>Fixed the problem with kicker systray applets vanishing after a kicker crash</li>
  <li>Desktop name popups on desktop switch, Alexander Kellett &lt;l&#121;pa&#110;&#x6f;&#0118;&#00064;kde&#x2e;&#x6f;&#0114;g&gt;</li>
  <li>Added titlebar buttons layout configurability to the KStep decoration style, Keunwoo Lee &lt;klee@cs.washington.edu&gt;</li>
  </ul>
</ul>

<h3>kdebindings</h3><ul>
<li>bindings updated.</li>
</ul>

<h3>kdeedu</h3><ul>
<li><b>NEW IN KDE:</b> Kiten, a <a href="http://www.katzbrown.com/kiten/">Japanese Reference Tool</a>.</li>
<li><b>NEW IN KDE:</b> FlashKard, a vocabulary tool. Is able to read KVTML files which were written
by KVocTrain.</li>
<li><b>NEW IN KDE:</b> started a libkdeedu, which allows interopability and code sharing between
KDE Educational applications.</li>
</ul>

<h3>kdegames</h3><ul>
  <li>Move Atlantik from kdenonbeta to kdegames, Rob Kaper &lt;k&#x61;&#x70;&#x65;r&#x40;kde.org&gt;</li>
  <li>Move Atlantik Designer from kdenonbeta to kdeaddons, Jason Katz-Brown &lt;jason@katzbrown.com&gt;</li>
  <li>Move Megami from kdenonbeta to kdegames, Neil Stevens &lt;neil@qualityassistant.com&gt;</li>
  <li>Move Kolf (<a href="http://www.katzbrown.com/kolf/">web page</a>) from kdenonbeta to kdegames, Jason Katz-Brown &lt;jason@katzbrown.com&gt;</li>
  <li>Add Klickety to kdegames (which uses heavily KSirtet libraries), Nicolas Hadacek &lt;&#104;ad&#x61;&#99;&#101;&#x6b;&#064;&#00107;&#x64;&#00101;&#46;&#111;&#00114;g&gt;</li>
  <li>Solver/adviser and game logs (replay/save/load) for KMines, Nicolas Hadacek &lt;&#104;&#97;&#x64;ac&#101;&#x6b;&#0064;&#107;&#100;&#0101;.org&gt;</li>
  <li>Export, statistics and histogram for highscores (KMines, KSirtet, KFouleggs and Klickety), Nicolas Hadacek &lt;h&#97;d&#97;c&#101;k&#x40;kde&#046;org&gt;</li>
  <li>Added different difficulty levels to KLines, Waldo Bastian &lt;b&#x61;&#00115;&#0116;i&#x61;&#110;&#064;&#0107;&#0100;&#0101;&#0046;o&#114;g&gt;</li>
</ul>

<h3>kdegraphics</h3><ul>
<li>KView:
  <ul>
    <li>Added KImageViewer interface</li>
    <li>Added possibility for plugins</li>
    <li>Added Slideshow plugin</li>
    <li>Enhanced Mousewheel support</li>
    <li>Automatic resizing to fit images with varying size</li>
  </ul>
<li>kviewshell: added statusbar</li>
<li>kviewshell: usability improvements</li>
<li>KuickShow: Digital camera jpeg images obey orientaion information</li>
<li>KuickShow: General UI improvements, now features a menubar</li>
<li>KuickShow: Ability to browse image collections on remote servers</li>
<li>KuickShow: Digital camera jpeg images obey orientaion information</li>
<li>KPovModeler: Implement support for the missing PovRAY 3.1 primitives, zehender at kde org.</li>
<li>KFax: Rewrite all the UI code to be KDE standards compliant, cleanups. (Nadeem Hasan &lt;n&#00104;as&#0097;&#110;&#64;kd&#101;&#46;&#x6f;&#x72;&#x67;&gt;)</li>
<li>libkscan:  support for halftoning scan mode where available</li>
<li>kooka: thumbnail view of scanned images added</li>
<li>kooka: ported to KDockWidgets to provide a customizable GUI</li>
<li>kooka: inline image renaming and drag and drop</li>
<li><b>NEW IN KDE:</b><a href="http://www.kpovmodeler.org/">KPovModeler</a>, a modeling and composition program for creating POV-Ray (tm) scenes</li>
</ul>

<h3>kdemultimedia</h3><ul>
<li>Started a video framework</li>
<li>More consistent internationalisation in many applications.</li>
<li><b>NEW IN KDE:</b> KAudioCreator, a CD ripper and audio encoder frontend.</li>
<li><b>NEW IN KDE:</b> KRec, an audio recorder using aRts.</li>
<li>KMediaPlayer interface added to kdelibs to reuse Kaboodle.</li>
<li>Noatun: Added option to clear the playlist when running with a file as command line option.</li>
<li>Added video thumbnail creator using xine-arts.</li>
<li>KMix: Added mute control support.</li>
</ul>

<h3>kdenetwork</h3><ul>
<li><a name="kmail">KMail</a>
  <ul>
    <li>The 'Delete' Action now irrevocably deletes messages. Use 'Move to Trash' to put messages into the trashcan. (Martin Koller)</li>
    <li>Optional columns for the number of unread messages and the total number of messages in the folder list (Carsten Burghardt)</li>
    <li>Custom folder icons (Zack Rusin)</li>
    <li>Improved OpenPGP support (Ingo Kl&ouml;cker):
      <ul>
        <li>Allow multiple encryption keys per email address, e.g. for distribution lists</li>
        <li>Allow rereading of keys in the key selection dialog, e.g. after the user has imported a key.</li>
        <li>Allow rechecking of keys in the key selection dialog, e.g. after the user has signed a key.</li>
      </ul>
    </li>
    <li>PGP/MIME (encryption/signing of attachments, RFC 3156) for GnuPG users and S/MIME support via plugins (collective effort of &Auml;gypten &lt;gpa-dev@gnupg.org&gt; and KMail &lt;kmail@kde.org&gt; developers)</li>
    <li>Redesign of the identity configuration dialog (Marc Mutz)</li>
    <li>Default identity can now be renamed (Marc Mutz)</li>
    <li>Identity-dependant default Bcc (Ingo Kl&ouml;cker)</li>
    <li>Optional MIME tree viewer, allowing direct access to all body parts (attachments) (Karl-Heinz Zimmer)</li>
    <li>Custom date format (Zack Rusin)</li>
    <li>Improved IMAP support (Carsten Burghardt)</li>
    <li>Reorganized menu bar looks more like other KDE applications (Martin Koller, Marc Mutz) </li>
    <li>Support for decoding uuencoded attachments (Marc Mutz)</li>
    <li>Custom hostname to send to the SMTP server (Aaron Seigo)</li>
    <li>Improved 'Reply to Mailing-List' (Marc Mutz)</li>
    <li>Separate the 'New Message' action and the 'Post to Mailing-List' action. This means with 'New Message' the To: field of the new message will no longer be filled automatically with the mailing list's posting address if the currently selected folder is associated with a mailing list. (Ingo Kl&ouml;cker)</li>
    <li>Improved support for RTL languages esp. Hebrew (Diego Iastrubni, Meni Livne)</li>
    <li>Allow the user to enable HTML rendering temporarily for HTML only messages (Ingo Kl&ouml;cker)</li>
    <li>Optionally show the encryption/signature status of messages in the message list (Kalle Dalheimer)</li>
    <li>Optional popup after dragging a message onto a folder (Zack Rusin)</li>
    <li>Five fixed layouts for the main window (Karl-Heinz Zimmer)</li>
    <li>Send separately encrypted messages to Bcc recipients because else all recipients could deduce the Bcc recipients from the keys that were used for encryption (Steffen Hansen)</li>
    <li>Configurable default domain for sending (Ingo Kl&ouml;cker)</li>
    <li>Don't check spelling of quoted text, urls or email addresses (Dave Corrie)</li>
    <li>New 'Add to Addressbook' action which doesn't open the addressbook when you add an email address to it. Use 'Open in Addressbook' if you want to add more information to a new address. (Cornelius Schumacher)</li>
    <li>IMAP accounts are now top level elements in the folder list (Carsten Burghardt)</li>
    <li>Configurable timeout for when the selected message will be marked as read (Tobias Koenig)</li>
    <li>Tips of the Day (Marc Mutz)</li>
    <li>Show progress dialog on exit so that the user sees that KMail is still doing something although all windows have already been closed (Waldo Bastian, Carsten Pfeiffer)</li>
    <li>Use KNotify to play an arbitrary sound on new mail arrival (Till Krech)</li>
    <li>Configurable alternative background color for the message list (Waldo Bastian)</li>
    <li>Display folder path in window caption (Carsten Burghardt)</li>
  </ul>
</li>
<li>KNode
  <ul>
    <li>Added &quot;Find in Article...&quot; command.</li>
    <li>Added &quot;Mark Last as Unread...&quot; command.</li>
    <li>Bug- and Stability fixes.</li>
  </ul>
</li>
<li>Korn now supports APOP accounts.</li>
<li><b>NEW IN KDE:</b> Desktop Sharing (KRfb). KRfb is a VNC-compatible server to share
   KDE desktops.</li>
<li><b>NEW IN KDE:</b> Remote Desktop Connection (KRdb). VNC compatible client for sharing
   KDE desktops.</li>
</ul>

<h3>kdepim</h3><ul>
<li>libkabc
  <ul>
    <li>Plugin interface for resources and formats.</li>
    <li>Control module for configuring multiple resources.</li>
    <li>Optional directory resource which stores each entry as own file.</li>
    <li>LDAP resource.</li>
    <li>Optional binary storage format for better performance with large
    addressbooks.</li>
    <li>Completed support for vCard entities: sound, geo, key and some more.</li>
    <li>vCard 2.1 import.</li>
    <li>LDAP aware mail address input field.</li>
  </ul>
</li>
<li>KAddressbook
  <ul>
    <li>New card and icon views.</li>
    <li>Multiple configurable views.</li>
    <li>"kab-style" details view for selected entry.</li>
    <li>Customizable view filters.</li>
    <li>Customizable categories for addressbook entries.</li>
    <li>Support for transparently accessing multiple addressbooks.</li>
    <li>Redesigned entry editor widget.</li>
    <li>Printing option supporting two different styles.</li>
    <li>Optional embedded entry and ditribution list editor.</li>
    <li>"Jump bar" for quick alphabetical access of entries.</li>
    <li>Fetch addresses from LDAP servers, including support for multiple
      servers.</li>
    <li>Importing of vCard 2.1 files.</li>
    <li>Improved CSV import (including template for import from Outlook
     2000).</li>
    <li>vCard export.</li>
  </ul>
</li>
<li>KAlarm
  <ul>
    <li>Added option to execute commands in alarms.</li>
    <li>Implemented daily, weekly, monthly and annual recurrences.</li>
    <li>Optionally play audio notification when an alarm triggers.</li>
   </ul>
</li>
<li>KPilot
  <ul>
    <li>Added time conduit</li>
    <li>VCal conduits (TODO and Calendar) have been improved to function properly.</li>
    <li>AvantGo conduit added.</li>
    <li>Addressbook conduit has been ported to use libkabc.</li>
  </ul>
</li>
<li>KOrganizer
  <ul>
    <li>General
      <ul>
        <li>User definable templates for events and TODOs.</li>
        <li>Alarms for TODOs.</li>
        <li>Added support for automatic HTML export on save of a calendar file.</li>
        <li>Time table print view.</li>
        <li>New "location" attribute for events.</li>
        <li>Experimental "Get Hot New Stuff" button for downloading and uploading
            calendar files of common interest.</li>
      </ul>
    </li>
    <li>Views
      <ul>
        <li>Added new "Next 3 days" view.</li>
        <li>Added selection of time span for a new event in day and week views.</li>
        <li>Direct manipulation of priority, completion status and categories of TODOs
          by context menus added. </li>
        <li>Deletion of individual instances of recurring events.</li>
        <li>Rewritten month view.</li>
        <li>Coloring of events in month view based on categories.</li>
        <li>Coloring of due and overdue todos.</li>
        <li>Improved "What's Next" view.</li>
        <li>Configurable cell height in week and day views</li>
      </ul>
    </li>
    <li>Group scheduling
      <ul>
        <li>iMIP group scheduling functions for TODOs.</li>
        <li>Publishing of Free/Busy information by iMIP conformant email.</li>
        <li>Improved automatisation of group scheduling.</li>
      </ul>
    </li>
    <li>Interopability and integration
      <ul>
        <li>Support for "webcal" URLs in Konqueror (known from Apple iCal).</li>
        <li>Support for iCalendar based drag&amp;drop.</li>
        <li>KOrganizer now shares the dialogs for categories with KAddressBook.</li>
        <li>Improved iCalendar conformance.</li>
        <li>Improved right-to-left languages support.</li>
        <li>Bug fixes for non-latin1 encodings.</li>
      </ul>
    </li>
    <li>Plugins
      <ul>
        <li>New plugin for importing Birthdays of contacts in the KDE address book.</li>
        <li>New plugin for accessing calendar data stored on an Exchange 2000 server.</li>
      </ul>
    </li>
  </ul>
</li>
<li>Fixed printing support in KNotes</li>
</ul>

<h3>kdesdk</h3><ul>
  <li>KBugBuster
    <ul>
      <li>Adapted KBugBuster to the new Bugzilla based KDE bug tracking system.</li>
      <li>Implemented Bookmarking support for Bug reports.</li>
      <li>Changing view settings no longer requires a reload.</li>
      <li>Added toolbar</li>
    </ul>
  </li>
  <li>KBabel
    <ul>
      <li>Automatic update of Project-Id-Version</li>
      <li>Possibility to specify localized translator name</li>
      <li>Validation &amp; highlighting of XML</li>
      <li>Save special (the settings can be changed for the particular save)</li>
      <li>KDE specifics added to rough translation (e.g. only add a new translator in TRANSLATORS)</li>
      <li>Load/Save markings in Catalog Manager</li>
      <li>Navigation bar in Catalog Manager</li>
      <li>Mail PO-file</li>
      <li>Tag structure tool</li>
      <li>Rough translation in catalog manager</li>
      <li>Automatic update of PO header comment</li>
      <li>Show source code</li>
      <li>Spellchecking in multiple files</li>
    </ul>
  </li>
  <li>Cervisia: made hardcoded colors configurable.</li>
</ul>

<h3>kdetoys</h3><ul>
<li>Minor improvements and bugfixes.</li>
</ul>

<h3>kdeutils</h3><ul>
<li>Partially ACPI support in KLaptop.</li>
</ul>

<h3>Quanta Plus</h3><ul>
  <li><strong>NEW IN KDE:</strong> Quanta Plus - A web development environment (<a href="http://quanta.sourceforge.net">homepage</a>)</li>
  <li>extended network transparency
    <ul>
      <li>remote projects</li>
      <li>remote files in projects</li>
      <li>remote toolbars</li>
    </ul>
  </li>
  <li>automatic update of Files and Templates tree views</li>
  <li>more DTD's installed by default:
    <ul>
      <li>HTML 4.01 Frameset</li>
      <li>HTML 4.01 Strict</li>
      <li>WML-1-2</li>
    </ul>
  </li>
  <li>possibility to send DTD definition files in e-mail added</li>
  <li>possibility to convert the document to a new DTD added</li>
  <li>"project views" support added</li>
  <li>more project default settings added</li>
  <li>various speedups (smoother editing or large files even with autocompletion turned on), some redesigned dialogs and lots of bugfixes</li>
  <li><b>NEW IN KDE:</b> Kommander: dialog builder/executor tool</li>
</ul>