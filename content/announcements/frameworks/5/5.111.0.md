---
qtversion: 5.15.2
date: 2023-10-18
layout: framework
libCount: 83
---


### Baloo

* Use the FSID as the device identifier where possible (bug 471289)

### Extra CMake Modules

* ecm_add_tests/ecm_add_test: add argument WORKING_DIRECTORY

### KCoreAddons

* Unset bug address for implicitly created KAboutData (bug 473517)

### KDE GUI Addons

* mark geo-scheme-handler as non-gui executable on macOS/Windows

### KImageFormats

* avif: support repetition count
* raw: fix multi image load
* hdr: fix oss-fuzz issue 62197
* hdr: fix crash (oss-fuzz)
* xcf: fix crash (oss-fuzz issue 62075)
* xcf: fix oss-fuzz issue

### KIO

* dbusactivationrunner: only activate well-formed services (bug 475266)
* DBusActivationRunner: fix object path according to spec
* Undeprecate KCoreDirLister::setMimeExcludeFilter
* Fix incomplete error strings in MimeTypeFinderJob
* Don't crash if KMountPoint gives nothing back while checking for CIFS (bug 474451)
* trashimpl: optimize TrashSizeCache::calculateSize (bug 461847)
* trash: Optimize trashimpl::adaptTrashSize
* [kfilewidget] Fixing saving to files with quotes (bug 426728)
* KPropertiesDialog: Force PlainText on size label

### Kirigami

* AboutPage: Allow overriding donateUrl by aliasing it from AboutItem (bug 474864)

### KRunner

* runnerpython: Fix install script paths to desktop files (bug 474866)

### KService

* KService: allow .desktop files without an Exec line (bug 430157)

### KWidgetsAddons

* Remove "No date" option from date picker popup used by date combo box
* Set the modes on the date picker
* Fix file rating being unreadable in certain themes (bug 339863)

### KWindowSystem

* Explain in deprecation message that logical coordinates must be used in KF6

### Syntax Highlighting

* Set mode for more config file endings (bug 475078)
* Highlight MapCSS numeric and string condition values
* textproto syntax: Support multipart strings
* textproto syntax: Add test case for octal escape sequences
* textproto syntax: Support some types of comments
* textproto syntax: Allow empty strings
* textproto syntax: Allow commas between fields
* Add TextProto syntax (protocol buffer Text Format Language)

### Security information

The released code has been GPG-signed using the following key:
pub rsa2048/58D0EE648A48B3BB 2016-09-05 David Faure <faure@kde.org>
Primary key fingerprint: 53E6 B47B 45CE A3E0 D5B7  4577 58D0 EE64 8A48 B3BB
