---
description: KDE's 6th Megarelease - Beta 2
authors:
  - SPDX-FileCopyrightText: 2023 Jonathan Riddell <jr@jriddell.org>
  - SPDX-FileCopyrightText: 2023 Paul Brown <paul.brown@kde.org>
SPDX-License-Identifier: CC-BY-4.0
date: 2023-12-20
images:
  - /announcements/megarelease/6/beta1/cube.png
  - /announcements/megarelease/6/beta1/kirigami.png
scssFiles:
- /scss/gear-23-08.scss
title: "KDE's 6th Megarelease - Beta 2"
aliases:
- ../../plasma/5/5.91.0
draft: false
---

## Plasma 6, Frameworks and Gear draw closer

Every few years we port the key components of our software to a new version of [Qt](https://www.qt.io/product/qt6), taking the opportunity to remove cruft and leverage the updated features the most recent version of Qt has to offer us.

We are now just over two months away from KDE's megarelease. At the end of **February 2024** we will publish **Plasma 6**, **Frameworks 6** and a whole new set of applications in a special edition of **KDE Gear** all in one go.

If  you have been following the updates [here](https://kde.org/announcements/megarelease/6/alpha/) and [here](https://kde.org/announcements/megarelease/6/beta1/), you will know we are deep in the testing phase; and KDE is making available today the second [Beta](#pre-release) version of all the software we will include in the megarelease.

As with versions [Alpha](https://kde.org/announcements/megarelease/6/alpha/) and the [first Beta](https://kde.org/announcements/megarelease/6/beta1/), this is a preview intended for developers and testers. The software in this second beta release is reaching stability fast, but it is still not 100% safe to use in a production environment. We still recommend you continue using stable versions of Plasma, Frameworks and apps for your everyday work. But if you do use this, watch out for bugs and [report them](https://bugs.kde.org) promptly, so we can solve them.

Read on to find out more about KDE's 6th Megarelease, what it covers, and how [you can help make the new versions of Plasma, KDE's apps and Frameworks a success](#contributing) now.

## [Plasma](/plasma-desktop/)

Plasma is KDE's flagship desktop environment. Plasma is like Windows or macOS, but is renowned for being flexible, powerful, lightweight and configurable. It can be used at home, at work, for schools and research.

![](cube.png)

Plasma 6 is the upcoming version of Plasma that integrates the latest version of Qt, Qt 6, the framework upon which Plasma is built.

Plasma 6 incorporates new technologies from Qt and other constantly evolving tools, providing new features, better support for the latest hardware, and support for the hardware and software technologies to come.

You can be part of the new Plasma. Download and install a Plasma 6-powered distribution (like [Neon Unstable](https://neon.kde.org/download)) to a test machine and start trying all its features. Check the [Contributing](#contributing) section below to find out how you can deliver reports of what you find to the developers.

* [Plasma 6 Beta Source Code Info Page](/info/plasma-5.91.0/)
* [Plasma 6 Beta Full Changelog](/announcements/changelogs/plasma/5.90.0-5.91.0/)
* [Plasma 6 packagers' release notes](https://community.kde.org/Plasma/Plasma_6.0_Release_notes)

## KDE Gear

KDE Gear is a collection of [applications](https://apps.kde.org) produced by the KDE community. Gear includes file explorers, music and video players, text and video-editors, apps to manage social media and chats, email and calendaring applications, travel assistants, and much more.

Developers of these apps also rely on the Qt toolbox, so most of the software will also be adapted to use the new Qt6 toolset and we need you to help us test them too.

* [KDE Gear 24.02 Beta 2 Source Code Info Page](/info/releases-24.01.85)
* [KDE Gear 24.02 Beta 2 Full Changelog](/announcements/changelogs/gear/24.01.85/)
* [KDE Gear 24.02 packagers' release notes](https://community.kde.org/KDE_Gear/24.02_Release_notes)

## [Frameworks](https://develop.kde.org/products/frameworks/)

KDE's Frameworks add tools created by the KDE community on top of those provided by the Qt toolbox. These tools give developers more and easier ways of developing interfaces and functionality that work on more platforms.

Among many other things, KDE Frameworks provide

* widgets (buttons, text boxes, etc.) that make building your apps easier and their looks more consistent across platforms, including Windows, Linux, Android and macOS
* libraries that facilitate storing and retrieving configuration settings
* icon sets, or technologies that make the integration of the translation workflow of applications easier

KDE's Frameworks also rely heavily on Qt and will also be upgraded to adapt them to the new version 6. This change will add more features and tools, enable your applications to work on more devices, and give them a longer shelf life.

* [KDE Frameworks 6 Beta Source Code Info Page](/info/kde-frameworks-5.247.0/)
* [KDE Frameworks 6 Beta Full Changelog](/announcements/changelogs/frameworks/5.246.0-5.247.0/)
* [KDE Frameworks 6 packagers release notes](https://community.kde.org/Frameworks/6.0_Release_notes)

![](kirigami.png)

## Contributing{#contributing}

KDE relies on volunteers to create, test and maintain its software. You can help too by...

* **Reporting bugs --** When you come across a bug when testing the software included in this Alpha Megarelease, you can report it so developers can work on it and remove it. When reporting a bug
	* make sure you understand when the bug is triggered so you can give developers a guide on how to check it for themselves
	* check you are using the latest version of the software you are testing, just in case the bug has been solved in the meantime
	* go to [KDE's bug-tracker](https://bugs.kde.org) and search for your bug to  make sure it does not get reported twice
	* if no-one has reported the bug yet, fill in the bug report, giving all the details you think are significant.
	* keep tabs on the report, just in case developers need more details.
* **Solving bugs --** Many bugs are easy to solve. Some just require changing a version number or tweaking the name of a library to its new name. If you have some basic programming knowledge of C++ and Qt, you too can help carry the weight of debugging KDE's software for the grand release in February. 
* **Joining the development effort --** You may have a deeper knowledge development, and would like to contribute to KDE with your own solutions. This is the perfect moment to [get involved](https://community.kde.org/Get_Involved) in KDE and contribute with your own code. 
* **Donating to KDE --** Creating, debugging and maintaining the large catalog of software KDE distributes to users requires a lot of resources, many of which cost money. Donating to KDE helps keep the day-to-day operation of KDE running smoothly and allows developers to concentrate on creating great software. KDE is currently running a drive to encourage more people to become [contributing supporters](/fundraisers/plasma6member/), but you can also give [one-time donations](/fundraisers/yearend2022/) if you want.

## A note on pre-release software{#pre-release}

Pre-release software is only suited for developers and testers. Alpha/Beta/RC software is unfinished, will be unstable and will contain bugs. It is published so volunteers can trial-run it, identify its problems, and report them so they can be solved before the publication of the final product.

The risks of running pre-release software are many. Apart from the hit to productivity produced by instability and the lack of features, using pre-release software can lead to **data loss**, and, in extreme cases, **damage to hardware**. That said, the latter is highly unlikely in the case of KDE software.

The version of the software included in KDE's 6th Megarelease is beta software. We strongly recommend you do not use it as your daily driver.

If, despite the above, you want to try the software distributed in KDE's 6th Megarelease, you do so under your sole responsibility, and in the understanding that the main aim, as a tester, you help us by providing feedback and your know-how regarding the software. Please see the [Contributing](#contributing) section above.
