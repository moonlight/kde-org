---
aliases:
- ../announce-4.8-beta2
date: '2011-12-07'
description: KDE Ships 4.8 Beta2 Workspaces, Applications and Platform.
title: KDE Makes 4.8 Beta2 Available for Testing
---

<p align="justify">
Today KDE released the second beta for its renewed Workspaces, Applications, and Development
Platform. With API, dependency and feature freezes in place, the KDE team's focus is now on fixing
bugs and further polishing new and old functionality. Compared to Beta1, Beta2 contains hundreds of fixes. Please give this release another good round of testing to help us release a rock-solid 4.8 in January.<br />
Highlights of 4.8 include, but are not limited to:

<ul>
    <li>
    Qt Quick in Plasma Workspaces -- Qt Quick is making its way into the Plasma Workspaces, the new Plasma Components provide a standardized API implementation of widgets with native Plasma Look and Feel. The device notifier widget has been ported to using these components and is now written in pure QML. KWin's window switcher is now also QML-based, paving the way for newly designed window switchers.
    </li>
    <li>
    Dolphin's file view has been rewritten for performance, scalability and more attractive visual appearance.
    </li>
    <li>
    Many performance improvements and bugfixes improve the overall user experience, making the KDE Applications and Workspaces more productive and fun to use than ever before.
    </li>

</ul>

<p align="justify">
The KDE Software Compilation, including all its libraries and its applications, is available for free
under Open Source licenses. KDE's software can be obtained in source and various binary
formats from <a
href="http://download.kde.org/stable/4.7.90/">download.kde.org</a>
or with any of the <a href="/distributions">major
GNU/Linux and UNIX systems</a> shipping today.
</p>

<h4>
  Installing 4.7.90 Binary Packages
</h4>
<p align="justify">
  <em>Packages</em>.
  Some Linux/UNIX OS vendors have kindly provided binary packages of 4.7.90
for some versions of their distribution, and in other cases community volunteers
have done so.
  Additional binary packages, as well as updates to the packages now available,
may become available over the coming weeks.
</p>

<p align="justify">
  <a id="package_locations"></a><em>Package Locations</em>.
  For a current list of available binary packages of which the KDE Project has
been informed, please visit the <a href="/info/4/4.7.90#binary">4.7.90 Info
Page</a>.
</p>

<h4>
  Compiling 4.7.90
</h4>
<p align="justify">
  <a id="source_code"></a>
  The complete source code for 4.7.90 may be <a
href="http://download.kde.org/stable/4.7.90/src/">freely downloaded</a>.
Instructions on compiling and installing 4.7.90
  are available from the <a href="/info/4/4.7.90">4.7.90 Info
Page</a>.
</p>

<h4>
  Supporting KDE
</h4>

<p align="justify">
 KDE is a <a href="http://www.gnu.org/philosophy/free-sw.html">Free Software</a>
community that exists and grows only because of the help of many volunteers that
donate their time and effort. KDE is always looking for new volunteers and
contributions, whether it is help with coding, bug fixing or reporting, writing
documentation, translations, promotion, money, etc. All contributions are
gratefully appreciated and eagerly accepted. Please read through the <a
href="/community/donations/">Supporting KDE page</a> for further information or 
become a KDE e.V. supporting member through our new 
<a href="http://jointhegame.kde.org/">Join the Game</a> initiative. </p>


