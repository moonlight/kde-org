---
aliases:
- ../announce-4.6.5
date: '2011-07-07'
description: KDE Updates Development Frameworks, Applications and Plasma Workspaces
  to version 4.6.5
title: KDE Ships July Updates
---

<p align="justify">
Today KDE released updates for its Workspaces, Applications, and Development Platform.
These updates are the fourth in a series of monthly stabilization updates to the 4.6 series. 4.6.5 updates bring many bugfixes and translation updates on top of the latest edition in the 4.6 series and are recommended updates for everyone running 4.6.4 or earlier versions. As the release only contains bugfixes and translation updates, it will be a safe and pleasant update for everyone. KDE’s software is already translated into more than 55 languages, with more to come.
To  download source code or packages to install go to the <a href="/info/4/4.6.5">4.6.5 Info Page</a>. The <a href="/announcements/changelogs/changelog4_6_4to4_6_5">changelog</a> lists more, but not all improvements since 4.6.4.
Note that the changelog is incomplete. For a complete list of changes that went into 4.6.5, you can browse the Subversion and Git logs. 4.6.5 also ships a more complete set of translations for many of the 55+ supported languages.
To find out more about the KDE Workspace and Applications 4.6, please refer to the 4.6.0 release notes and its earlier versions.
</p>

<div class="text-center">
	<a href="/announcements/4/4.6.0/46netbook2.png">
	<img src="/announcements/4/4.6.0/thumbs/46netbook2.png" class="img-fluid" alt="The KDE Plasma Netbook Workspace">
	</a> <br/>
	<em>The KDE Plasma Netbook Workspace</em>
</div>
<br/>

<p align="justify">
The KDE Software Compilation, including all its libraries and its applications, is available for free
under Open Source licenses. KDE's software can be obtained in source and various binary
formats from <a
href="http://download.kde.org/stable/4.6.5/">download.kde.org</a>
or with any of the <a href="/distributions">major
GNU/Linux and UNIX systems</a> shipping today.
</p>

<h4>
  Installing 4.6.5 Binary Packages
</h4>
<p align="justify">
  <em>Packages</em>.
  Some Linux/UNIX OS vendors have kindly provided binary packages of 4.6.5
for some versions of their distribution, and in other cases community volunteers
have done so.
  Additional binary packages, as well as updates to the packages now available,
may become available over the coming weeks.
</p>

<p align="justify">
  <a id="package_locations"></a><em>Package Locations</em>.
  For a current list of available binary packages of which the KDE Project has
been informed, please visit the <a href="/info/4/4.6.5#binary">4.6.5 Info
Page</a>.
</p>

<h4>
  Compiling 4.6.5
</h4>
<p align="justify">
  <a id="source_code"></a>
  The complete source code for 4.6.5 may be <a
href="http://download.kde.org/stable/4.6.5/src/">freely downloaded</a>.
Instructions on compiling and installing 4.6.5
  are available from the <a href="/info/4/4.6.5">4.6.5 Info
Page</a>.
</p>

<h4>
  Supporting KDE
</h4>

<p align="justify">
 KDE is a <a href="http://www.gnu.org/philosophy/free-sw.html">Free Software</a>
community that exists and grows only because of the help of many volunteers that
donate their time and effort. KDE is always looking for new volunteers and
contributions, whether it is help with coding, bug fixing or reporting, writing
documentation, translations, promotion, money, etc. All contributions are
gratefully appreciated and eagerly accepted. Please read through the <a
href="/community/donations/">Supporting KDE page</a> for further information or 
become a KDE e.V. supporting member through our new 
<a href="http://jointhegame.kde.org/">Join the Game</a> initiative. </p>


