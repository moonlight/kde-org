---
aliases:
- ../4.1
- ../4.1.pl
date: '2008-07-29'
title: KDE 4.1 Release Announcement
---

<h3 align="center">
Społeczność KDE ogłasza wydanie KDE 4.1.0
</h3>

<p align="justify">
  <strong>
    KDE wydaje kolejną wersję środowiska graficznego dedykowaną pamięci Uwe Thiem
  </strong>
</p>

<p align="justify">
<a href="/">Społeczność KDE</a> wydała dzisiaj KDE 4.1.0. Wydanie to jest drugim głównym wydaniem KDE z serii 4, prezentującym nowe aplikacje i funkcje korzystające z filarów KDE 4. KDE 4.1 jest też pierwszym wydaniem zawierającym Pakiet Zarządzania Informacjami Osobistymi KDE-PIM składający się z klienta email KMail, organizera KOrganizer, czytnika RSSów Akregator, czytnika grup dyskusyjnych KNode i innych komponentów zgrupowanych w aplikacji Kontact. Nowy interfejs pulpitu - Plasma, wprowadzony w KDE 4.0 dojrzał już do tego stopnia, że może zastąpić interfejs pulpitu znany z KDE 3. Tak jak w przypadku poprzednich wydań wiele czasu poświęcono na poprawki w bibliotekach na których opiera się KDE.
<br />
Dirk M&uuml;ller, jeden z menadżerów wydań KDE mówi:<em>"Pomiędzy KDE 4.0 a 4.1 dokonano 20803 commitów oraz 15432 poprawek i zmian w tłumaczeniach. Prawie 35000 commitów dokonano także w osobnych gałęziach, a potem część z nich została przeniesiona do KDE 4.1."</em>
M&uuml;ller dodaje też, że administratorzy serwerów utworzyli na serwerach KDE SVN 166 nowych kont dla programistów.a

<div class="text-center">
	<a href="/announcements/4/4.1.0/desktop.png">
	<img src="/announcements/4/4.1.0/desktop_thumb.png" class="img-fluid">
	</a> <br/>
	<em>Pulpit KDE 4.1</em>
</div>
<br/>

</p>
<p>
<strong>Kluczowe zmiany w KDE 4.1 to:</strong>
<ul>
    <li>przywrócenie pakietu KDE-PIM,</li>
    <li>dojrzałość Plasmy,</li>
    <li>wiele nowych aplikacji.</li>
</ul>

</p>

<h3>
  <a id="changes">Pamięci Uwe Thiem</a>
</h3>
<p align="justify">
Społeczność KDE dedykuje to wydanie Uwe Thiem, długotrwałemu programiście KDE, który niedawno zmarł z powodu wady nerki. Śmierć Uwe przyszła zupełnie niespodziewanie i była szokiem dla jego przyjaciół. Uwe pracował nad projektem KDE do końca, nie tylko jako programista. Odgrywał również ważną rolę w edukacji użytkowników Afryki w temacie Wolnego Oprogramowania. Razem z nagłą śmiercią Uwe, projekt KDE stracił nieocenionego przyjaciela. Łączymy się w bólu z jego rodziną i tymi, których opuścił.
</p>

<h3>
  <a id="changes">Przeszłość, teraźniejszość, przyszłość</a>
</h3>
<p align="justify">
Mimo, że KDE 4.1 jest pierwszym wydaniem przeznaczonym dla szerszego grona użytkowników, nadal brakuje mu niektórych funkcji znanych z serii 3.5. Ekipa KDE ciągle jednak nad tym pracuje i stara się, aby były one dostępne w kolejnych wydaniach. Nie ma gwarancji, że pojawi się każda pojedyncza funkcja z KDE 3.5, ale KDE 4.1 już teraz jest potężnym środowiskiem.<br />
Część opcji w interfejsie została przeniesiona w inne miejsca, tak aby pasowała do danych na jakich operuje, wiec przed zgłoszeniem brakującej funkcji należy się upewnić, czy szukamy jej w dobrym miejscu.<p />
KDE 4.1 jest dużym krokiem w rozwoju KDE 4 i tworzy solidne fundamenty pod kolejne wydania. KDE 4.2 można oczekiwać w styczniu 2009.
</p>

<div class="text-center">
	<a href="/announcements/4/4.1.0/kdepim-screenie.png">
	<img src="/announcements/4/4.1.0/kdepim-screenie_thumb.png" class="img-fluid">
	</a> <br/>
	<em>Pakiet KDE PIM powrócił</em>
</div>
<br/>

<h3>
  <a id="changes">Zmiany</a>
</h3>
<p align="justify">
Podczas stabilizacji nowych frameworków w KDE 4.1, dużo nacisku położono na elementy widoczne dla użytkowników. Poniżej znajduje się lista zmian w dokonanych w KDE 4.1. Więcej informacji można znaleźć na stronach <a href="http://techbase.kde.org/Schedules/KDE4/4.1_Release_Goals">KDE 4.1 Release Goals</a> i <a href="http://techbase.kde.org/Schedules/KDE4/4.1_Feature_Plan">KDE 4.1 Feature Plan</a>.
</p>

<h4>
  Dla użytkowników
</h4>
<p align="justify">

<ul>
    <li>
        W wydaniu 4.1 powrócił pakiet <strong>KDE-PIM</strong> zawierający programy do zarządzania informacjami osobistymi i komunikacji. KMail - klient email, organizer KOrganizer i Akregator - czytnik kanałów RSS powróciły z nowym wyglądem i funkcjonalnością,
    </li>
    <li>
        Na scenę wkroczył także <strong>Dragon Player</strong> - odtwarzacz wideo,
    </li>
    <li>
        <strong>Okteta</strong> to nowy zaawansowany hexedytor,
    </li>
    <li>
        <strong>Step</strong> symulator procesów fizycznych czyni naukę łatwą i przyjemną,
    </li>
    <li>
        <strong>KSystemLog</strong> pomaga w śledzeniu tego, co dzieje się w systemie,
    </li>
    <li>
        <strong>Nowe gry</strong> takie jak KDiamond, Kollision, KBreakOut i Kubrick sprawią, że nie oprzesz się pokusie zrobienia sobie przerwy w pracy,
    </li>
    <li>
        <strong>Lokalize</strong> sprawi, że tłumacze będą w stanie przetłumczyć KDE 4 na twój język (jeśli jeszcze nie ma go na liście ponad 50 dostępnych języków),
    </li>
    <li>
        <strong>KSCD</strong> - odtwarzacz CD znowu jest dostępny w KDE.
    </li>
</ul>

Odpowiedzi na najczęściej zadawane pytania znalazły się w <a href="http://www.kde.org/users/faq">FAQ dla użytkowników</a>, które warto przeczytać, jeśli chcesz się dowiedzieć się więcej na temat KDE 4.

</p>

<p align="justify">

<div class="text-center">
	<a href="/announcements/4/4.1.0/dolphin-screenie.png">
	<img src="/announcements/4/4.1.0/dolphin-screenie_thumb.png" class="img-fluid">
	</a> <br/>
	<em>Nowy mechanizm zaznaczania w Dolphinie</em>
</div>
<br/>

<ul>
    <li>
        <strong>Dolphin</strong> - menadżer plików KDE otrzymał widok drzewa oraz obsługę kart. Nowy i innowacyjny system zaznaczania elementów za pomocą jednego kliknięcia zwiększa wygodę pracy, a akcje menu kontekstowego "Kopiuj do" oraz "Przenieś do" ułatwiają dostęp do tych funkcji. Konqueror oczywiście nadal jest dostępny jako alternatywa dla Dolphina i ma zaimplementowanych większość z tych funkcji. [<a href="#screenshots-dolphin">Zrzuty ekranu.</a>]
    </li>
    <li>
        <strong>Konqueror</strong> - przeglądarka internetowa dla KDE uzyskała obsługę przywracania zamkniętych okien i kart. Poprawie uległo także przewijanie stron, które jest teraz bardziej "gładkie".
    </li>
    <li>
        <strong>Gwenview</strong> Gwenview - przeglądarka zdjęć otrzymała nowy widok pełnoekranowy, pasek miniatur, służący do łatwego poruszania się pomiędzy zdjęciami, inteligentny system cofania oraz możliwość oceniania zdjęć. [<a href="#screenshots-gwenview">Zrzuty ekranu.</a>]
    </li>
    <li>
        <strong>KRDC</strong>  - klient zdalnego pulpitu od teraz może automatycznie znajdować zdalne pulpity w sieci lokalnej dzięki wykorzystaniu protokołu ZeroConf.
    </li>
    <li>
        <strong>Marble</strong> - globus dla KDE integruje się teraz z serwisem <a
        href="http://www.openstreetmap.org/">OpenStreetMap</a>, dzięki czemu zawsze odnajdziesz drogę wykorzystując Wolne Mapy. [<a href="#screenshots-marble">Zrzuty ekranu.</a>]
    </li>
    <li>
        <strong>KSysGuard</strong> umożliwia monitorowanie wyjścia procesów lub działających aplikacji, dzięki czemu nie trzeba ponownie uruchamiać aplikacji z poziomu konsoli, aby zobaczyć, co aplikacja robi.
    </li>
    <li>
        Efekty graficzne w <strong>KWin</strong> zostały ustabilizowane i rozszerzone. Dodano także efekty takie jak Coverswitch window switcher oraz słynne "wobbly windows". [<a href="#screenshots-kwin">Zrzuty ekranu.</a>]
    </li>
    <li>
        Rozszerzono możliwości konfiguracyjne panelu <strong>Plasmy</strong>. Nowy kontroler panelu pozwala na łatwe dostosowanie jego wyglądu do swoich potrzeb. Można także dodawać panele na wszystkich krawędziach ekranu. Nowy aplet Folderview pozwala na przechowywanie plików na pulpicie (tak w zasadzie to umożliwia wyświetlenie na pulpicie zawartości dowolnego katalogu). Na pulpicie można umieścić kilka apletów folderview, uzyskując łatwy i szybki dostęp do potrzebnych plików. [<a href="#screenshots-plasma">Zrzuty ekranu.</a>]
    </li>
</ul>
</p>

<h4>
  Dla programistów
</h4>
<p align="justify">

<ul>
    <li>
        Biblioteka <strong>Akonadi</strong> PIM umożliwia łatwe przechowywanie i wymianę emaili i informacji na temat kontaktów pomiędzy aplikacjami. <a href="http://en.wikipedia.org/wiki/Akonadi">Akonadi</a> obsługuje wyszukiwanie danych oraz powiadamianie aplikacji, jeśli dane ulegną zmianie.
    </li>
    <li>
        Aplikacje KDE mogą być pisane w Pythonie i Rubym. <strong>Interfejsy tych języków</strong> zostały <a href="http://techbase.kde.org/Development/Languages">określone</a> jako stabilne,dojrzale i dogodne dla programistów aplikacji.
    </li>
    <li>
        <strong>Libksane</strong> umożliwia łatwą obsługę skanera w aplikacjach takich jak Skanlite.
    </li>
    <li>
        Powstała ujednolicona obsługa <strong>emotikon</strong>, wykorzystywana jak na razie przez KMail i Kopete.
    </li>
    <li>
        Silniki dla <strong>Phonona</strong> do obsługi multimediów poprzez GStreamer, QuickTime i DirectShow9 poprawiają działanie KDE na systemach Windows i Mac OS.
    </li>

</ul>
</p>

<h3>
  Nowe platformy
</h3>
<p align="justify">
<ul>
    <li>
        Działanie KDE na <a href="http://techbase.kde.org/Projects/KDE_on_Solaris"><strong>OpenSolarisie</strong></a> poprawiło się. Pozostało jednak jeszcze kilka błędów.
    </li>
    <li>
        Dostępne są już pierwsze wersje <a href="http://windows.kde.org">aplikacji KDE dla platformy <strong>Windows</strong></a>. Biblioteki są już dość stabilne, jednak pełna funkcjonalność kdelibs nie jest jeszcze dostępna. Niektóre aplikacje działają na Windowsie całkiem ładnie, inne mogą jeszcze nie działać wcale.
    </li>
    <li>
        <strong>Mac OS X</strong> jest kolejną platformą na którą wkracza KDE. <a href="http://mac.kde.org">KDE na Macach</a> nie nadaje się jeszcze do normalnej pracy. Obsługa multimediów dzięki Phononowi jest już dostępna, ale pełna obsługa sprzętu i wyszukiwania nie jest jeszcze ukończona.
    </li>
</ul>
</p>

<a id="screenshots-dolphin"></a>

<h3>
  Zrzuty ekranu
</h3>
<p align="justify">

<a id="screenshots-dolphin"></a>

<h4>Dolphin</h4>

<div class="text-center">
	<a href="/announcements/4/4.1.0/dolphin-treeview.png">
	<img src="/announcements/4/4.1.0/dolphin-treeview_thumb.png" class="img-fluid">
	</a> <br/>
	<em>Nowy widok drzewa w Dolphinie daje Ci szybszy dostęp do katalogów. Opcja ta jest wyłączona w ustawieniach domyślnych.</em>
</div>
<br/>

<div class="text-center">
	<a href="/announcements/4/4.1.0/dolphin-tagging.png">
	<img src="/announcements/4/4.1.0/dolphin-tagging_thumb.png" class="img-fluid">
	</a> <br/>
	<em>Nepomuk wprowadza tagowanie i ocenianie do KDE - a więc też i do Dolphina.</em>
</div>
<br/>

<div class="text-center">
	<a href="/announcements/4/4.1.0/dolphin-icons.png">
	<img src="/announcements/4/4.1.0/dolphin-icons_thumb.png" class="img-fluid">
	</a> <br/>
	<em>Podgląd i panel informacyjny dostarczają inforamcji o pliku.</em>
</div>
<br/>

<div class="text-center">
	<a href="/announcements/4/4.1.0/dolphin-filterbar.png">
	<img src="/announcements/4/4.1.0/dolphin-filterbar_thumb.png" class="img-fluid">
	</a> <br/>
	<em>Łatwiej odnajduj swoje pliki dzięki filtrowaniu.</em>
</div>
<br/>

<a id="screenshots-gwenview"></a>

<h4>Gwenview</h4>

<div class="text-center">
	<a href="/announcements/4/4.1.0/gwenview-browse.png">
	<img src="/announcements/4/4.1.0/gwenview-browse_thumb.png" class="img-fluid">
	</a> <br/>
	<em>Gwenview umożliwia przeglądanie katalogów zawierających obrazy.</em>
</div>
<br/>

<div class="text-center">
	<a href="/announcements/4/4.1.0/gwenview-open.png">
	<img src="/announcements/4/4.1.0/gwenview-open_thumb.png" class="img-fluid">
	</a> <br/>
	<em>Otwieranie plików z dysku twardego czy sieci jest tak samo łatwe dzięki infrastrukturze KDE.</em>
</div>
<br/>

<div class="text-center">
	<a href="/announcements/4/4.1.0/gwenview-thumbnailbar.png">
	<img src="/announcements/4/4.1.0/gwenview-thumbnailbar_thumb.png" class="img-fluid">
	</a> <br/>
	<em>Nowy pasek z miniaturkami pozwala na łatwe przełączanie się między obrazami. Jest on również dostępny w trybie pełnoekranowym.</em>
</div>
<br/>

<div class="text-center">
	<a href="/announcements/4/4.1.0/gwenview-sidebar.png">
	<img src="/announcements/4/4.1.0/gwenview-sidebar_thumb.png" class="img-fluid">
	</a> <br/>
	<em>Boczny panel Gwenview pokazuje dodatkowe informacje o obrazie oraz daje dostęp do opcji manipulacyjnych.</em>
</div>
<br/>

<a id="screenshots-marble"></a>

<h4>Marble</h4>

<div class="text-center">
	<a href="/announcements/4/4.1.0/marble-globe.png">
	<img src="/announcements/4/4.1.0/marble-globe_thumb.png" class="img-fluid">
	</a> <br/>
	<em>Globus w aplikacji Marble.</em>
</div>
<br/>

<div class="text-center">
	<a href="/announcements/4/4.1.0/marble-osm.png">
	<img src="/announcements/4/4.1.0/marble-osm_thumb.png" class="img-fluid">
	</a> <br/>
	<em>Integracja Marble z OpenStreetMap dostarcza również informacji o transporcie publicznym.</em>
</div>
<br/>

<a id="screenshots-kwin"></a>

<h4>KWin</h4>

<div class="text-center">
	<a href="/announcements/4/4.1.0/kwin-desktopgrid.png">
	<img src="/announcements/4/4.1.0/kwin-desktopgrid_thumb.png" class="img-fluid">
	</a> <br/>
	<em>Siatka pulpitów KWin ukazuje wirtualne pulpity i ułatwia zapamiętanie gdzie mamy otwarte interesujące nas okno.</em>
</div>
<br/>

<div class="text-center">
	<a href="/announcements/4/4.1.0/kwin-coverswitch.png">
	<img src="/announcements/4/4.1.0/kwin-coverswitch_thumb.png" class="img-fluid">
	</a> <br/>
	<em>Coverswitcher sprawia, że przełączanie się między oknami za pomocą Alt+Tab jest miłe dla oka. Można je wybrać w ustawieniach efektów pulpitu.</em>
</div>
<br/>

<div class="text-center">
	<a href="/announcements/4/4.1.0/kwin-wobbly.png">
	<img src="/announcements/4/4.1.0/kwin-wobbly_thumb.png" class="img-fluid">
	</a> <br/>
	<em>KWin obsługuje również efekt gumowych okien (wobbly windows, domyślnie wyłączone).</em>
</div>
<br/>

<a id="screenshots-plasma"></a>

<h4>Plasma</h4>

<div class="text-center">
	<a href="/announcements/4/4.1.0/plasma-folderview.png">
	<img src="/announcements/4/4.1.0/plasma-folderview_thumb.png" class="img-fluid">
	</a> <br/> 
	<em>Nowy aplet Widoku Folderu wyświetla zawartość dowolnego katalogu na Twoim pulpicie. Upuść katalog na odblokowanym pulpicie, aby stworzyć nowy aplet. Możesz nie tylko wyświetlać lokalne foldery, ale również zasoby sieciowe.</em>
</div>
<br/>

<div class="text-center">
	<a href="/announcements/4/4.1.0/panel-controller.png">
	<img src="/announcements/4/4.1.0/panel-controller_thumb.png" class="img-fluid">
	</a> <br/>
	<em>Nowe funkcje ustawień paneli pozwalają na łatwe zmienianie ich rozmiaru i położenia. Możesz również zmienić pozycje apletów na panelu poprzez przeciąganie ich na nowe położenie.</em>
</div>
<br/>

<div class="text-center">
	<a href="/announcements/4/4.1.0/krunner-screenie.png">
	<img src="/announcements/4/4.1.0/krunner-screenie_thumb.png" class="img-fluid">
	</a> <br/>
	<em>Dzięki KRunner możesz uruchamiać programy, pisać bezpośrednio maile do przyjaciół oraz wykonywać wiele innych drobnych zadań.</em>
</div>
<br/>

<div class="text-center">
	<a href="/announcements/4/4.1.0/plasma-kickoff.png">
	<img src="/announcements/4/4.1.0/plasma-kickoff_thumb.png" class="img-fluid">
	</a> <br/>
	<em>Menu Kickoff zostało odświeżone.</em>
</div>
<br/>

<div class="text-center">
	<a href="/announcements/4/4.1.0/switch-menu.png">
	<img src="/announcements/4/4.1.0/switch-menu_thumb.png" class="img-fluid">
	</a> <br/>
	<em>Możesz wybrać pomiędzy menu Kickoff, a klasycznym menu.</em>
</div>
<br/>
</p>

<h4>
  Znane problemy
</h4>
<p align="justify">
<ul>
    <li>Użytkownicy kart <strong>NVidia</strong> i sterowników binarnych, przygotowanych przez NVidia mogą mieć problemy przy przełączaniu między oknami i przy zmianie ich rozmiarów. Powiadomiliśmy o tym fakcie inżynierów NVidia, jednak do tej pory nie pojawiły się jeszcze sterowniki naprawiające tą usterkę. Na <a href="http://techbase.kde.org/User:Lemma/GPU-Performance">Techbase</a> możecie znaleźć informacje jak poprawić wydajność w takim wypadku, mimo to ostatecznie musimy poczekać na poprawione sterowniki do kart.</li>
</ul>

</p>

<h4>
  Pobierz, uruchom, przetestuj
</h4>
<p align="justify">
  Społeczność użytkowników oraz dostawcy systemów Linux/UNIX przygotowali już binarne pakiety instalacyjne dla części dystrybucji, systemu Mac OS X i Windows. Sprawdź, w swoim zarządcy pakietów, czy są one już dostępne.
</p>
<h4>
  Kompilacja KDE 4.1.0
</h4>
<p align="justify">
  <a id="source_code"></a><em>Kod źródłowy</em>.
  Kod źródłowy KDE 4.1.0 jest w <a
  href="/info/4/4.1.0">całości dostępny</a>. Instrukcje dotyczące kompilacji KDE można znaleźć w serwisie <a href="http://techbase.kde.org/Getting_Started/Build/KDE4">TechBase</a>.
</p>



