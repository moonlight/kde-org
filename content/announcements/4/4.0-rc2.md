---
aliases:
- ../announce-4.0-rc2
date: '2007-12-11'
description: KDE Project Ships Second Release Candidate for Leading Free Software
  Desktop.
title: KDE 4.0 Release Candidate 2
---

<p>FOR IMMEDIATE RELEASE</p>

<h3 align="center">
   KDE Project Ships Second Release Candidate for Leading Free Software Desktop,
Codename "Coenig"
</h3>
<p align="justify">
With the second release candidate, the KDE project would like to collect feedback to
ensure the quality of KDE 4.0.
</p>

<p>
The KDE Community is happy to announce the immediate availability of <a
href="/announcements/4/4.0-rc2">the
second release candidate for KDE 4.0</a>. This release candidate marks the last
mile on the road to KDE 4.0.
</p>

<div class="text-center">
<a href="/announcements/4/4.0-rc2/konqueror.jpg">
<img src="/announcements/4/4.0-rc2/konqueror_thumb.jpg" class="img-fluid">
</a> <br/>
<em>KDE's webbrowser Konqueror</em>
</div>
<br/>

<p>
While progress on the quality and completeness of what is to become the KDE 4.0 desktop
has been great, the KDE Community decided to have another release candidate before releasing
KDE 4.0 on January, 11th. The codebase is now feature-complete. Some work is still being done
to put the icing on the KDE 4.0 cake. This includes fixing some major and minor bugs,
finishing off artwork and smoothening out the user experience.
</p>

<div class="text-center">
<a href="/announcements/4/4.0-rc2/dolphin.jpg">
<img src="/announcements/4/4.0-rc2/dolphin_thumb.jpg" class="img-fluid">
</a> <br/>
<em>Dolphin, the KDE 4.0 filemanager</em>
</div>
<br/>

<p>
The second release candidate incorporates many improvements from the
previous Release Candidate, Beta and alpha releases:
<a href="/announcements/4/4.0-rc1">RC1</a>,
<a href="/announcements/4/4.0-beta4">Beta
4</a>, <a href="/announcements/4/4.0-beta3">Beta
3</a>, <a href="/announcements/4/4.0-beta2">Beta
2</a>, <a href="/announcements/4/4.0-beta1">Beta
1</a>, <a href="/announcements/4/4.0-alpha2">Alpha
2</a> and <a
href="/announcements/visualguide-4.0-alpha1">Alpha 1</a>.
</p>

<div class="text-center">
<a href="/announcements/4/4.0-rc2/systemsettings.jpg">
<img src="/announcements/4/4.0-rc2/systemsettings_thumb.jpg" class="img-fluid">
</a> <br/>
<em>Configure your system with Systemsettings</em>
</div>
<br/>

<h2>RC 2</h2>
<p>
With this second release candidate, the KDE developers hope to collect comments
and bug reports from the wider KDE community. With their help, we hope to solve the most
pressing problems with the current KDE 4 codebase to ensure the final 4.0
release is stable, usable and fun to work with.
We would like to encourage anyone who is willing and able to
spend some time on testing to find and <a href="http://bugs.kde.org">report</a>
problems to the KDE developers. It is recommended to have a current snapshot of the
codebase handy. That makes trying things easier, it also helps the process by
not having  to hunt down bugs that have already been fixed and makes it easier to
test patches proposed by developers.
</p>

<div class="text-center">
<a href="/announcements/4/4.0-rc2/krunner.jpg">
<img src="/announcements/4/4.0-rc2/krunner_thumb.jpg" class="img-fluid">
</a> <br/>
<em>KRunner, part of the Plasma workspace</em>
</div>
<br/>

<p>
You can find <a
href="http://techbase.kde.org/Contribute/Bugsquad/How_to_create_useful_crash_reports">some documentation</a> on the pages of our
<a href="http://techbase.kde.org/Contribute/Bugsquad">Bugsquad</a> and <a
href="http://www.chiark.greenend.org.uk/~sgtatham/bugs.html">in other
places</a>. There will be weekly <a
href="http://aseigo.blogspot.com/2007/10/kde4-krush-days-saturday.html">KDE 4
Krush Days on Saturdays</a> and everyone is invited to help out. Having a recent
SVN checkout (follow instructions on <a
href="http://techbase.kde.org">TechBase</a>) is great, but using the regularly
updated <a href="http://home.kde.org/~binner/kde-four-live/">openSUSE KDE 4 Live CD</a> 
while very easy, is also a big help to us.
</p>

<h4>Get it, run it, test it...</h4>
<p>
For those interested in getting packages to test and contribute, several
distributions notified us that they will have KDE 4.0-rc2 packages available
at or soon after the release. The complete and current list can be found on the
<a href="/info/1-2-3/3.97">KDE 4.0-rc2 Info Page</a>, where you
can also find links to the source code, information about compiling, security
and other issues.
</p>

<br>
<i>Compiled by Sebastian K&uuml;gler with extensive help from the KDE community</i>

<h2>About KDE 4</h2>
<p>
KDE 4 is the next generation of the popular KDE Desktop Environment which seeks
to fulfill the need for a powerful yet easy to use desktop for UNIX and LINUX
workstations. The aim of the KDE project for the 4.0 release is to put the
foundations in place for future innovations on the Free Desktop. The many newly
introduced technologies incorporated in the KDE libraries will make it easier
for developers to add rich functionality to their applications, combining and
connecting different components in any way they want.
</p>


