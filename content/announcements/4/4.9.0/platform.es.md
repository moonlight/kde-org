---
title: La Plataforma de KDE 4.9 – Mejor interoperabilidad, desarrollo más fácil
date: "2012-12-05"
hidden: true
---

<p>
KDE se enorgullece de anunciar el lanzamiento de la Plataforma de KDE 4.9. La Plataforma de KDE es la base para los Espacios de trabajo Plasma y las Aplicaciones de KDE. El equipo de KDE está trabajando en la actualidad en la siguiente generación de la Plataforma de Desarrollo de KDE, conocida como «Frameworks 5». Aunque Frameworks 5 será bastante compatible a nivel de software, estará basada en Qt5. Proporcionará más granularidad, haciendo que sea más fácil usar partes de la plataforma de forma selectiva y reduciendo considerablemente el número de dependencias. Como resultado, la Plataforma de KDE permanece sin cambios en su mayor parte. La mayor parte del trabajo realizado en la Plataforma ha consistido en corregir los errores y mejorar el rendimiento.
</p>
<p>
Ha progresado la separación de QGraphicsView de Plasma para facilitar la transición a un Plasma basado en QML puro. También hay mejoras en la implementación de Qt Quick en las bibliotecas de KDE y en los espacios de trabajo Plasma.
</p>
<p>
Se ha realizado algo de trabajo a bajo nivel en las bibliotecas de KDE dentro del área de las redes. El acceso a los recursos compartidos de una red se ha hecho más rápido para todas las aplicaciones de KDE. Las conexiones NFS, Samba y SSHFS, además de KIO, ya no cuentan los elementos en las carpetas, lo que hace que sean más rápidas. El protocolo HTTP también se ha acelerado al evitar interacciones innecesarias con el servidor. Esto se nota especialmente en aplicaciones en red, como Korganizer, que es alrededor de un 20% más rápido debido a estas correcciones. También se ha mejorado el almacenamiento de las contraseñas para los recursos compartidos en red.
</p>
<p>
Se han corregido muchos errores críticos en Nepomuk y en Soprano, haciendo que la búsqueda del escritorio sea más rápida y más fiable para las aplicaciones y los espacios de trabajo construidos sobre la Plataforma de KDE 4.9. La mejora del rendimiento y la estabilidad han sido los objetivos principales de estos proyectos para este lanzamiento.
</p>

<h4>Instalación de la Plataforma de Desarrollo de KDE</h4>

<p align="justify">
El software de KDE, incluidas todas sus bibliotecas y aplicaciones, está libremente disponible bajo licencias de Código Abierto. El software de KDE funciona sobre diversas configuraciones de hardware y arquitecturas de CPU, como ARM y x86, sistemas operativos y funciona con cualquier tipo de gestor de ventanas o entorno de escritorio. Además de Linux y otros sistemas operativos basados en UNIX, puede encontrar versiones para Microsoft Windows de la mayoría de las aplicaciones de KDE en el sitio web <a href="http://windows.kde.org">KDE software on Windows</a> y versiones para Apple Mac OS X en el sitio web <a href="http://mac.kde.org/">KDE software on Mac</a>. En la web puede encontrar compilaciones experimentales de aplicaciones de KDE para diversas plataformas móviles como MeeGo, MS Windows Mobile y Symbian, aunque en la actualidad no tienen soporte. <a href="http://plasma-active.org">Plasma Active</a> es una experiencia de usuario para un amplio abanico de dispositivos, como tabletas y otro tipo de hardware 
móvil.
<br />
Puede obtener el software de KDE en forma de código fuente y distintos formatos binarios en <a
href="http://download.kde.org/stable/4.9.0/">download.kde.org</a>, 
y también en <a href="/download">CD-ROM</a>
o con cualquiera de los <a href="/distributions">principales
sistemas GNU/Linux y UNIX</a> de la actualidad.
</p>
<p align="justify">
  <a id="packages"><em>Paquetes</em></a>.
  Algunos distribuidores del SO Linux/UNIX tienen la gentileza de proporcionar paquetes binarios de 4.9.0 
para algunas versiones de sus distribuciones, y en otros casos han sido voluntarios de la comunidad
los que lo han hecho. <br />
  Algunos de estos paquetes binarios están disponibles para su libre descarga en el sitio web de KDE <a
href="http://download.kde.org/binarydownload.html?url=/stable/4.9.0/">download.kde.org</a>.
  Paquetes binarios adicionales, así como actualizaciones de los paquetes ya disponibles,
estarán disponibles durante las próximas semanas.
<a id="package_locations"><em>Ubicación de los paquetes</em></a>.
Para una lista actual de los paquetes binarios disponibles de los que el Equipo de Lanzamiento de KDE ha
sido notificado, visite la <a href="/info/4/4.9.0">Página de información sobre 4.9</a>.
</p>
<p align="justify">
  <a id="source_code"></a>
  La totalidad del código fuente de 4.9.0 se puede <a href="/info/4/4.9.0">descargar libremente</a>.
Dispone de instrucciones sobre cómo compilar e instalar el software de KDE 4.9.0
  en la <a href="/info/4/4.9.0#binary">Página de información sobre 4.9.0</a>.
</p>

<h4>
Requisitos del sistema
</h4>
<p align="justify">
Para obtener lo máximo de estos lanzamientos, le recomendamos que use una versión reciente de Qt, ya sea la 4.7.4 o la 4.8.0. Esto es necesario para asegurar una experiencia estable y con rendimiento, ya que algunas de las mejoras realizadas en el software de KDE están hechas realmente en la infraestructura subyacente Qt.<br />
Para hacer un uso completo de las funcionalidades del software de KDE, también le recomendamos que use en su sistema los últimos controladores gráficos, ya que pueden mejorar sustancialmente la experiencia del usuario, tanto en las funcionalidades opcionales como en el rendimiento y la estabilidad general.
</p>




<h2>También se han anunciado hoy:</h2>

<h2><a href="../plasma"><img src="/announcements/4/4.9.0/images/plasma.png" class="app-icon float-left m-3"/>Espacios de trabajo Plasma 4.9 – Mejoras globales</a></h2>
<p>
Lo más destacado de los espacios de trabajo Plasma incluye mejoras sustanciales en el gestor de archivos Dolphin, el emulador de terminal de X Konsole, las Actividades y el gestor de ventanas KWin. Lea el <a href="../plasma">«Anuncio de los espacios de trabajo Plasma»</a>.
</p>
<h2><a href="../applications"><img src="/announcements/4/4.9.0/images/applications.png" class="app-icon float-left m-3"/>Aplicaciones de KDE 4.9 nuevas y mejoradas</a></h2>
<p>
Hoy se han liberado aplicaciones de KDE nuevas y mejoradas que incluyen Okular, Kopete, KDE PIM y aplicaciones y juegos educativos. Lea el <a href="../applications">«Anuncio de aplicaciones de KDE»</a>.
</p>
