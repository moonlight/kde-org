---
aliases:
- ../4.5
date: '2010-08-10'
description: KDE Releases Development Platform, Applications and Plasma Workspaces
  4.5.0.
title: KDE Releases Development Platform, Applications and Plasma Workspaces 4.5.0
custom_about: true
custom_contact: true
---

<div class="float-right m-3"> 
<p><a href="/announcements/4.5/"><img src="/announcements/4/4.5.0/images/kde45-small.png" class="img-fluid"/></a></p> <br/> <br/>
</div> 
<p align="justify">
New Versions of the  KDE Development Platform, the Plasma Desktop and Netbook workspaces, and many applications are released today with the version number 4.5.0. The KDE team focused on the usability, performance and stability of many previously introduced new features and technologies. Below, find the 3 separate announcements for each of KDE's products: The Development Platform, the Applications Compilation and the Plasma Workspaces.

<h2>KDE Development Platform 4.5.0 gains performance, stability, new high-speed cache and support for WebKit</h2>

<a href="./platform">
<img src="/announcements/4/4.5.0/images/platform.png" class="app-icon float-left m-3" alt="The KDE Development Platform 4.5.0"/>
</a>

<p align="justify">
 KDE today releases the KDE Development Platform 4.5.0. This release brings many performance  and stability improvements. The new <b>KSharedDataCache</b> is optimized for fast access to resources stored on disk, such as icons. The new <b>KDE WebKit</b> library provides integration with network settings, password-storage and many other features found in Konqueror. <a href="./platform"><b>Read The Full Announcement</b></a>
</p>

<h2>Plasma Desktop and Netbook 4.5.0: improved user experience</h2>
<p align="justify">

<a href="./plasma">
<img src="/announcements/4/4.5.0/images/plasma.png" class="app-icon float-left m-3" alt="The KDE Plasma Workspaces 4.5.0" />
</a>

 KDE today releases the Plasma Desktop and Plasma Netbook Workspaces 4.5.0. Plasma Desktop received many usability refinements. The Notification and Job handling workflows have been streamlined. The notification area has been cleaned up visually, and its input handling across applications is now made more consistent by the extended use of the Freedesktop.org notification protocol first introduced in Plasma's previous version. <a href="./plasma"><b>Read The Full Announcement</b></a>

</p>

<h2>KDE Applications 4.5.0: enhanced usability and map routing</h2>
<p align="justify">

<a href="./applications">
<img src="/announcements/4/4.5.0/images/applications.png" class="app-icon float-left m-3" alt="The KDE Applications 4.5.0"/>
</a>

 The KDE team today releases a new version of the KDE Applications. Many educational titles, tools, games and graphical utilities have seen further enhancement and usability improvements. Routing backed by OpenRouteService has made its entry in Marble, the virtual globe. Konqueror, KDE's webbrowser can now be configured to use WebKit with the KWebKit component available in the Extragear repository. <a href="./applications"><b>Read The Full Announcement</b></a>

</p>

<div class="text-center">
	<a href="/announcements/4/4.5.0/kde-general45.png">
	<img src="/announcements/4/4.5.0/thumbs/kde-general45.png" class="img-fluid" alt="The KDE Plasma Netbook in 4.5 RC1">
	</a> <br/>
	<em>KDE Development Platform, Applications and Plasma Desktop and Netbook Released in Version 4.5</em>
</div>
<br/>

<h4>
    Spread the Word and See What Happens: Tag as "KDE"
</h4>
<p align="justify">
KDE encourages everybody to <strong>spread the word</strong> on the Social Web.
Submit stories to news sites, use channels like delicious, digg, reddit, twitter,
identi.ca. Upload screenshots to services like Facebook, Flickr,
ipernity and Picasa and post them to appropriate groups. Create screencasts and
upload them to YouTube, Blip.tv, Vimeo and others. Do not forget to tag uploaded
material with the <em>tag <strong>kde</strong></em> so it is easier for everybody to find the
material, and for the KDE team to compile reports of coverage for the KDE SC 4.5
 announcement. <strong>Help us spreading the word, be part of it!</strong></p>

<div align="center">
<table border="0" cellspacing="2" cellpadding="2">
<tr>
    <td>
        <a href="http://digg.com/search?s=kde45"><img src="/announcements/buttons/digg.gif" alt="Digg" title="Digg" /></a>
    </td>
    <td>
        <a href="http://www.reddit.com/search?q=kde45"><img src="/announcements/buttons/reddit.gif" alt="Reddit" title="Reddit" /></a>
    </td>
    <td>
        <a href="http://twitter.com/#search?q=kde45"><img src="/announcements/buttons/twitter.gif" alt="Twitter" title="Twitter" /></a>
    </td>
    <td>
        <a href="http://identi.ca/search/notice?q=kde45"><img src="/announcements/buttons/identica.gif" alt="Identi.ca" title="Identi.ca" /></a>
    </td>
</tr>
<tr>
    <td>
        <a href="http://www.flickr.com/photos/tags/kde45"><img src="/announcements/buttons/flickr.gif" alt="Flickr" title="Flickr" /></a>
    </td>
    <td>
        <a href="http://www.youtube.com/results?search_query=kde45"><img src="/announcements/buttons/youtube.gif" alt="Youtube" title="Youtube" /></a>
    </td>
    <td>
        <a href="http://www.facebook.com/#!/pages/K-Desktop-Environment/6344818917?ref=ts"><img src="/announcements/buttons/facebook.gif" alt="Facebook" title="Facebook" /></a>
    </td>
    <td>
        <a href="http://delicious.com/tag/kde45"><img src="/announcements/buttons/delicious.gif" alt="del.icio.us" title="del.icio.us" /></a>
    </td>
</tr>
</table>
<span style="font-size: 6pt"><a href="http://microbuttons.wordpress.com">microbuttons</a></span>
</div>

<h4>Support KDE</h4>

<a href="http://jointhegame.kde.org/"><img src="/announcements/4/4.5.0/images/join-the-game.png" class="img-fluid float-left mr-3"
alt="Join the Game"/> </a>

<p align="justify"> KDE e.V.'s new <a
href="http://jointhegame.kde.org/">Supporting Member programme</a> is
now open.  For &euro;25 a quarter you can ensure the international
community of KDE continues to grow making world class Free
Software.</p>
<br />
<p>&nbsp;</p>
