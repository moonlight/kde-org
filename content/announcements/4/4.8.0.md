---
aliases:
- ../4.8
custom_about: true
custom_contact: true
date: '2012-01-25'
description: KDE Ships 4.8.0 Workspaces, Applications and Platform.
title: KDE Plasma Workspaces, Applications and Platform 4.8 Improve User Experience
---

<p>
KDE is delighted to announce its latest set of releases, providing major updates to KDE Plasma Workspaces, KDE Applications, and the KDE Platform. Version 4.8 provides many new features, and improved stability and performance.
</p>

<div class="text-center">
	<a href="/announcements/4/4.8.0/plasma-desktop-4.8.png">
	<img src="/announcements/4/4.8.0/thumbs/plasma-desktop-4.8.png" class="img-fluid" alt="Plasma and Applications 4.8">
	</a> <br/>
	<em>Plasma and Applications 4.8</em>
</div>
<br/>

<h3>
<a href="./plasma">
KDE Plasma Workspaces 4.8 Gain Adaptive Power Management
</a>
</h3>

<p>
<a href="./plasma">
<img src="/announcements/4/4.8.0/images/plasma.png" class="app-icon float-left m-3" alt="The KDE Plasma Workspaces 4.8" />
</a>

Highlights for Plasma Workspaces include <a href="http://philipp.knechtges.com/?p=10">Kwin optimizations</a>, the redesign of power management, and integration with Activities. The first QtQuick-based Plasma widgets have entered the default installation of Plasma Desktop, with more to follow in future releases. Read the complete <a href="./plasma">Plasma Workspaces Announcement</a>.
<br />
</p>


<h3>
<a href="./applications">
KDE Applications 4.8 Offer Faster, More Scalable File Management
</a>
</h3>

<p>

<a href="./applications">
<img src="/announcements/4/4.8.0/images/applications.png" class="app-icon float-left m-3" alt="The KDE Applications 4.8"/>
</a>
KDE applications released today include <a href="http://userbase.kde.org/Dolphin">Dolphin</a> with its new display engine, new <a href="http://userbase.kde.org/Kate">Kate</a> features and improvements, <a href="http://userbase.kde.org/Gwenview">Gwenview</a> with functional and visual improvements. KDE Telepathy reaches first beta milestone. <a href="http://userbase.kde.org/Marble/en">Marble's</a> new features keep arriving, among which are: Elevation Profile, satellite tracking and <a href="http://userbase.kde.org/Plasma/Krunner">Krunner</a> integration. Read the complete <a href="./applications">'KDE Applications Announcement'</a>.
<br />
</p>


<h3>
<a href="./platform">
KDE Platform 4.8 Enhances Interoperability, Introduces Touch-Friendly Components
</a>
</h3>

<p>
<a href="./platform">
<img src="/announcements/4/4.8.0/images/platform.png" class="app-icon float-left m-3" alt="The KDE Development Platform 4.8"/>
</a>

KDE Platform provides the foundation for KDE software. KDE software is more stable than ever before. In addition to stability improvements and bugfixes, Platform 4.8 provides better tools for building fluid and touch-friendly user interfaces, integrates with other systems' password saving mechanisms and lays the base for more powerful interaction with other people using the new KDE Telepathy framework. For full details, read the <a href="./platform">KDE Platform 4.8 release announcement</a>.
<br />
</p>


To celebrate the 4.8 releases, community members have organized <a href="http://community.kde.org/Promo/Events/Release_Parties/4.8">release parties all over the world</a>. Come to a party nearby and meet your fellow KDE contributors and users.

<h3>
Spread the Word and See What Happens: Tag as "KDE"
</h3>
<p>
KDE encourages everybody to spread the word on the Social Web. Submit stories to news sites, use channels like delicious, digg, reddit, twitter, identi.ca. Upload screenshots to services like Facebook, Flickr, ipernity and Picasa, and post them to appropriate groups. Create screencasts and upload them to YouTube, Blip.tv, Vimeo and others. Please tag uploaded materials with "KDE" to make them easy to find, and so that the KDE promo team can analyze coverage for the 4.8 releases of KDE software.

<div align="center">
<table border="0" cellspacing="2" cellpadding="2" class="social">
<tr>
	<td>
		<a class="DiggThisButton DiggCompact" href="http://digg.com/submit?url=http%3A//kde.org/announcements/4.8/&amp;title=KDE%20releases%20version%204.8%20of%20Plasma%20Workspaces,%20Applications%20and%20Platform" rev="news, tech_news"></a>
	</td>
	<td>
		<a href="http://twitter.com/share" class="twitter-share-button" data-url="/announcements/4.8/" data-text="#KDE releases version 4.8 of Plasma Workspaces, Applications and Platform → https://www.kde.org/announcements/4.8/ #kde48" data-count="horizontal" data-via="kdecommunity">Tweet</a><script type="text/javascript" src="http://platform.twitter.com/widgets.js"></script>
	</td>
	<td>
		<script type="text/javascript" src="http://www.reddit.com/static/button/button1.js?url=http%3A//kde.org/announcements/4.8/"></script>
	</td>
	<td>
		<iframe src="http://www.facebook.com/plugins/like.php?app_id=225109044193701&amp;href=http%3A%2F%2Fkde.org%2Fannouncements%2F4.8%2F&amp;send=false&amp;layout=button_count&amp;width=80&amp;show_faces=false&amp;action=like&amp;colorscheme=light&amp;font&amp;height=21" scrolling="no" frameborder="0" style="border:none; overflow:hidden; width:80px; height:21px;" allowTransparency="true"></iframe>
	</td>
	<td>
		<g:plusone size="medium" href="/announcements/4.8/"></g:plusone>
	</td>
	<td>
	</td>
</tr>
</table>
<table border="0" cellspacing="2" cellpadding="2" class="social">
<tr>
	<td>
		<a href="http://identi.ca/search/notice?q=kde48"><img src="/announcements/buttons/identica.gif" alt="Identi.ca" title="Identi.ca" /></a>
	</td>
	<td>
		<a href="http://www.flickr.com/photos/tags/kde48"><img src="/announcements/buttons/flickr.gif" alt="Flickr" title="Flickr" /></a>
	</td>
	<td>
		<a href="http://www.youtube.com/results?search_query=kde48"><img src="/announcements/buttons/youtube.gif" alt="Youtube" title="Youtube" /></a>
	</td>
	<td>
		<a href="http://delicious.com/tag/kde48"><img src="/announcements/buttons/delicious.gif" alt="del.icio.us" title="del.icio.us" /></a>
	</td>
</tr>
</table>
<span style="font-size: 6pt"><a href="http://microbuttons.wordpress.com">microbuttons</a></span>
</div>
</p>

<h3>
About these release announcements
</h3><p>
These release announcements were prepared by Stu Jarvis, Nikos Pantazis, Dennis Nienhüser, Sebastian Kügler, Carl Symons, Vivek Prakash, Eshat Cakar, other members of the KDE Promotion Team and the wider KDE community. They cover highlights of the many changes made to KDE software over the past six months.
</p>

<h4>Support KDE</h4>

<a href="http://jointhegame.kde.org/"><img src="/announcements/4/4.8.0/images/join-the-game.png" class="img-fluid float-left mr-3"
alt="Join the Game"/> </a>

<p align="justify"> KDE e.V.'s new <a
href="http://jointhegame.kde.org/">Supporting Member programme</a> is
now open.  For &euro;25 a quarter you can ensure the international
community of KDE continues to grow making world class Free
Software.</p>
<br />
<p>&nbsp;</p>
