---
aliases:
- ../announce-4.1-beta2
date: '2008-06-24'
title: KDE 4.1 Beta 2 Release Announcement
---

<h3 align="center">
  Andra betaversionen av KDE 4.1 lanserad
</h3>

<p align="justify">
  <strong>
KDE-projektet lanserar andra betaversionen av KDE 4.1.</strong>
</p>

<p align="justify">
<a href="/">KDE Projektet</a> tillkännager stolt den andra
betaversionen av KDE 4.1. Beta 2 är till för testare, projektmedlemmar och andra
entusiaster för att identifiera fel och regressioner, så att KDE 4.1 äntligen
kan ersätta KDE 3 för slutanvändare. KDE 4.1 beta 2 finns tillgänglig som binära
paket för ett stort antal plattformar och även som källkodspaket. Den slutliga versionen
av KDE 4.1 är planerad att släppas under juli 2008.
</p>

<h4>
  <a id="changes">KDE 4.1 Beta 2 höjdpunkter</a>
</h4>

<p align="justify">
En månad har nu passerat sedan KDE 4.1 grenen frös för nya funktioner och utvecklarna
har jobbat på att polera upp alla de nya funktionerna, integrerar dom in i skrivbordsmiljön
och på dokumentation och översättning. Flera felrättningssessioner har hållits för att
få bort så många fel som möjligt i betakoden. Det återstår dock flera att fixa. All testning
och återkoppling på den här betaversionen uppskattas för att säkerställa att KDE 4.1 blir
så bra som möjligt.

<div class="text-center">
	<a href="/announcements/4/4.1-beta2/desktop-folderview.png">
	<img src="/announcements/4/4.1-beta2/desktop-folderview_thumb.png" class="img-fluid">
	</a> <br/>
	<em>KDE 4.1 Beta 2 med Plasma</em>
</div>
<br/>

<ul>
    <li>Stöd för andra programmeringsspråk i KDE 4.1 bland annat Python och Ruby.
    </li>
    <li>Förbättrat användargränssnitt i Dolphin
    </li>
    <li>Ett stort antal förbättringar i Gwenview
    </li>
</ul>

En komplett lista över funktionerna i KDE 4.1 finns på
<a href="http://techbase.kde.org/Schedules/KDE4/4.1_Release_Goals">Techbase</a>.

</p>

<h4>
  Programmatiskt språkstöd
</h4>
<p align="justify">
De flesta KDE 4.1 applikationer är skrivna i C++, men om man föredrar
ett annat programmeringssprål så tillhandahåller språkbindningarna den här 
möjligheten. KDE 4.1 innehåller stöd för bland annat Python och Ruby. Som exempel
kan nämnas att den nya utskriftsapplikationen är skriven i Python utan att en 
slutanvändare kan se någon skillnad på den och en C++-applikation.
</p>

<h4>
  Dolphin mognar
</h4>
<p align="justify">
KDE4's filhanterare Dolphin har fått en en hel del förbättringar.

<div class="text-center">
	<a href="/announcements/4/4.1-beta2/dolphin-tagging.png">
	<img src="/announcements/4/4.1-beta2/dolphin-tagging_thumb.png" class="img-fluid">
	</a> <br/>
	<em>Den semantiska miljön med dess taggar</em>
</div>
<br/>

De första resultaten av <a href="http://nepomuk.kde.org/">NEPOMUK</a>, den sociala semantiska
skrivbordsmiljön som utvecklas av ett Europeiskt forskningsprogram börjar synas mer tydligt.
Stöd för taggning finns nu i Dolphin.

<div class="text-center">
	<a href="/announcements/4/4.1-beta2/dolphin-selection.png">
	<img src="/announcements/4/4.1-beta2/dolphin-selection_thumb.png" class="img-fluid">
	</a> <br/>
	<em>Välja filer i en-klicksläget</em>
</div>
<br/>

Att välja filer har blivit enklare tack vare ett litet plustecken i övre vänsta hörnet,
klickar man på den väljer man filen istället för att öppna den. Det här gör
det mycket enklare att välja filer i en-klicksläge och hjälper till att förhindra
att filer öppnas av misstag.<br />

<div class="text-center">
	<a href="/announcements/4/4.1-beta2/dolphin-treeview.png">
	<img src="/announcements/4/4.1-beta2/dolphin-treeview_thumb.png" class="img-fluid">
	</a> <br/>
	<em>Ny trädvy Dolphin</em>
</div>
<br/>

Många användra har frågat efter en trädvy i den detaljerade listvyn. Tillsammans
med en-klickslägesförbättringarna har det nu blivit enklare att flytta och kopiera
filer.

</p>

<h4>
  Polering av Gwenview
</h4>
<p align="justify">
Gwenview är KDE 4:as standardbildvisare och har blivit rejält polerad. En del
av funktionerna som till exempel rotera och fullskärm har flyttats in i bildens
kontext vilket gör användargränssnittet enklare och minskar behovet att röra musen
så långt.
</p>

<div class="text-center">
	<a href="/announcements/4/4.1-beta2/gwenview-browse.png">
	<img src="/announcements/4/4.1-beta2/gwenview-browse_thumb.png" class="img-fluid">
	</a> <br/>
	<em>Gwenview visar översikten av en katalog.</em>
</div>
<br/>

<div class="text-center">
	<a href="/announcements/4/4.1-beta2/gwenview.png">
	<img src="/announcements/4/4.1-beta2/gwenview_thumb.png" class="img-fluid">
	</a> <br/>
	<em>Gwenviews ikonvy</em>
</div>
<br/>

<div class="text-center">
	<a href="/announcements/4/4.1-beta2/systemsettings-emoticons.png">
	<img src="/announcements/4/4.1-beta2/systemsettings-emoticons_thumb.png" class="img-fluid">
	</a> <br/>
	<em>En ny modul för att välja chattikoner.</em>
</div>
<br/>

<h4>
KDE 4 applikationerna växer
</h4>
<p align="justify">
Över hela KDE gemenskapen har flera applikationer nu portats över till KDE 4
eller har förbättrats sedan KDE 4 lanserades. Dragon Player den snabba videospelaren
debuterar. KDE:s CD-spelare återvänder. En ny utskriftsapplet ger överlägset stöd för
att styra sin utskrifter. Konqueror får stöd för sessioner, ett Undoläge och 
förbättrad scrollning. Ett nytt läge för bildvisning inklusive ett fullskärmsläge
för Gwenview. Dolphin får stöd för flikar och ett antal andra funktioner från KDE 3.
Många applikationer har fått stöd för att hämta nya teman, ikoner m.m via Get New Stuff
som har fått ett nytt förbättrat gränssnitt. Stöd för Zeroconf nätverkshantering har
lagts till till flera spel och andra applikationer för att förenkla hanteringen för 
att sätta upp nätverksspel eller fjärråtkomst.
</p>

<h4>
Förbättringar av ramverken
</h4>
<p align="justify">
Utvecklarna har lagt ner mycket tid på att utöka och förbättra 
KDE:s kärnbibliotek. KHTML får en prestandraförbättring genom 
att inkludera

Developers have been busy enriching the core KDE libraries and
infrastructure too. KHTML gets a speed boost from anticiperande resursladdning,
och WebKit, dess avkomma, läggs till Plasma för att ge stöd för OSX
Dashboard widgets i KDE. Widgets on Canvas stödet i Qt 4.4 gör Plasma
mer stabilt och lättviktigt. KDE:s karakteristiska en-klicks baserade gränssnitt
får en ny markeringsmekanism för ökad hastighet och tillgänglighet. Phonon det plattformsoberoende
mediaramverket får stöd för undertexter och GStreamer, DirectShow 9 and QuickTime
implementationer. Nätverkshantering får stöd för flera versioner av NetworkManager.  
Stöd för flera freedesktop.org standarder såsom popupnotifiering och bokmärken har påbörjats
så att andra applikationer passar in i KDE 4.1 skrivbordsmiljö.

</p>

<h4>
  KDE 4.1 slutlig lansering
</h4>
<p align="justify">
KDE 4.1 är planerad att släppas den 29 juli 2008, sex månader efter KDE 4.0.
</p>

<h4>Installera KDE 4.1 beta 2</h4>
<p align="justify">
  <em>Paket</em>.
  Några Linux/UNIX distributörer tillhandahåller binära paket av KDE 4.1 beta 2 och i 
vissa fall har andra personer i omgivningaen skapat paket. Kolla din distributions pakethanterare
och hemsida för information om hur du får tag i paketen.
</p>

<h4>
  Kompilera KDE 4.1 beta 1 (4.0.83) själv
</h4>
<p align="justify">
  <a id="source_code"></a><em>Källkod</em>.
  All källkod till KDE 4.1 beta 2 kan laddas ner  <a
href="/info/4/4.0.83">härifrån</a>.
Instruktioner hur man kompilerar och installerar KDE 4.0.83 finns på
<a href="/info/4/4.0.83">KDE 4.0.83 Info Page</a>, eller på 
<a href="http://techbase.kde.org/Getting_Started/Build/KDE4">TechBase</a>.
</p>

<h4>
  Stöd KDE
</h4>
<p align="justify">
 KDE är ett <a href="http://www.gnu.org/philosophy/free-sw.html">Fri Mjukvaru</a>
projekt som existerar och växer endast tack vare hjälp från volontärer som donerar
sin tid och sitt arbete. KDE behöver alltid mer hjälp från frivilliga vare sig det
gäller kodning, buggrättning eller rapportering, hjälp med dokumentation, översättning,
marknadsföring, ekonomisk hjälp etc. All hjälp uppskattas och accepteras gladeligen. 
Se <ahref="/community/donations/">Stöd KDE</a> för mer information. </p>

<p align="justify">
Vi ser fram emot att få höra av dig snart!
</p>

<h2>Om KDE 4</h2>
<p>
KDE 4.0 är den innovativa fria skrivbordsmiljön med massor av appplikationer för alla uppgifter
i dagligt användande och för specifika uppgifter. Plasma är det nya skrivbordsskalet för KDE 4 
som tillhandahåller ett intiutivt gränssnitt för att interagera med skrivbordet och applikationerna.
Webbläsaren Konqueror integrerar Internet med skrivbordet. Filhanteraren Dolphin, dokumentvisaren 
Okular och kontrollpanelen System Settings kompletterar den grundläggande skrivbordsmiljön.
<br />
KDE är byggt på KDEs biblioteken som ger enkel åtkomst till nätverket genom KIO och avancerade
grafiska funktioner genom Qt4. Phonon och Solid, som också ingår i KDEs bibliotek tillhandahåller
stöd för multimedia och bättre hårdvaruintegration för alla KDEs applikationer.
</p>



